#include "ObjectManager.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <fstream>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include "glm/gtc/matrix_transform.hpp"

#include "objects/Ship.h"
#include "objects/Skybox.h"
#include "objects/Light.h"
#include "objects/SpaceDust.h"
#include "objects/Camera.h"
#include "objects/Sztuczka.h"
#include "Application.h"

#include "auxiliary/HelperOperations.h"


ObjectManager::ObjectManager()
{
//    m_ship1 = new Ship("Wedrowiec");
//    m_ship1->setModelMatrix(glm::mat4(1));
//    m_ship1->setPosition(2,1,-7);
//    m_ship1->rotateX(90.f);
//    m_objects.insert(m_ship1);
//    m_geometryObjects.push_back(m_ship1);

	PhysicalObject* physicalTestO;
	PhysicalObject* physicalTest1;

    physicalTestO = new Sztuczka(91854, glm::vec3( -3, 1,  5), "MatSztuczka01"); m_objects.insert(physicalTestO); m_geometryObjects.push_back(physicalTestO);
    physicalTest1 = new Sztuczka(14968, glm::vec3( -3, 1, -5), "MatSztuczka02"); m_objects.insert(physicalTest1); m_geometryObjects.push_back(physicalTest1);
    physicalTest1 = new Sztuczka(87621, glm::vec3(  0, 1,  5), "MatSztuczka03"); m_objects.insert(physicalTest1); m_geometryObjects.push_back(physicalTest1);
    physicalTest1 = new Sztuczka(31685, glm::vec3(  0, 1, -5), "MatSztuczka04"); m_objects.insert(physicalTest1); m_geometryObjects.push_back(physicalTest1);
    physicalTest1 = new Sztuczka(26987, glm::vec3(  3, 1,  5), "MatSztuczka05"); m_objects.insert(physicalTest1); m_geometryObjects.push_back(physicalTest1);
    physicalTest1 = new Sztuczka(16876, glm::vec3(  3, 1, -5), "MatSztuczka06"); m_objects.insert(physicalTest1); m_geometryObjects.push_back(physicalTest1);
    physicalTest1 = new Sztuczka(64869, glm::vec3(  7, 1,  0), "MatSztuczka07"); m_objects.insert(physicalTest1); m_geometryObjects.push_back(physicalTest1);

    physicalTest1 = new Sztuczka(-1, glm::vec3( 5.5f, 0.6,  -9.2f), "MatSztuczka08"); m_objects.insert(physicalTest1); m_geometryObjects.push_back(physicalTest1);
    physicalTest1 = new Sztuczka(-1, glm::vec3( 5.5f,  5,  -9.2f), "MatSztuczka05"); m_objects.insert(physicalTest1); m_geometryObjects.push_back(physicalTest1);
    physicalTest1 = new Sztuczka(-1, glm::vec3( -3.f , 5,  -9.2f), "MatSztuczka08"); m_objects.insert(physicalTest1); m_geometryObjects.push_back(physicalTest1);
    physicalTest1 = new Sztuczka(-1, glm::vec3( -3.f , 6,  -9.2f), "MatSztuczka04"); m_objects.insert(physicalTest1); m_geometryObjects.push_back(physicalTest1);
    physicalTest1 = new Sztuczka(-1, glm::vec3( -3.f , 7,  -9.2f), "MatSztuczka01"); m_objects.insert(physicalTest1); m_geometryObjects.push_back(physicalTest1);

    m_testMesh = new PhysicalObject("kolumny/limes.obj");
    m_objects.insert(m_testMesh);

    m_testMesh = new PhysicalObject("kolumny/kolumny.obj");
//    m_testMesh = new Object("sponza/sponza.obj");
//    m_testMesh = new Object("cornell/cornell.obj");
//    m_testMesh->btBody()->set
    m_testMesh->setStatic();
    m_objects.insert(m_testMesh);
    m_geometryObjects.push_back(m_testMesh);


    m_skybox = new Skybox();
    m_skybox->rotateX(180.f);
    m_objects.insert(m_skybox);

    m_spaceDust = new SpaceDust();
    m_objects.insert(m_spaceDust);
    m_particleSystems.insert(m_spaceDust);

    m_sun = new Object(LowPolySphereMesh);
    m_objects.insert(m_sun);
    m_sun->setModelMatrix(glm::scale(glm::mat4(),glm::vec3(40)));


    Light* sunLight;
    sunLight = new Light(nullptr);
    if ( fileExists("light0PoseFile.tmp") ) {
        std::ifstream infile("light0PoseFile.tmp", std::ios::binary);
        glm::mat4x4 modelM;
        read(infile, modelM);
        sunLight->setModelMatrix(modelM);
    }
    m_lights.push_back(sunLight);

    Light* testLight;
    testLight = new Light();
    testLight->setColor(1.f, 0.f, 0.f);
    testLight->readAnimFromFile("light1Anim.dae");
    m_objects.insert(testLight);
    m_lights.push_back(testLight);

//    testLight = new Light();
//    testLight->setColor(1.f, 0.f, 0.f);
//    testLight->readAnimFromFile("light2Anim.dae");
//    m_objects.insert(testLight);
//    m_lights.push_back(testLight);


//    m_sunlight = new Light();
    //violendays sunpos (150,-118,225)
//    m_sunlight->setPosition(337.436f, 189.451f, -319.029f);
    m_sun->setPosition(337.436f, 189.451f, -319.029f);

//    m_sunlight->setColor(1.0f, 0.98f, 1.0f);

//    m_objects.insert(m_sunlight);
//    m_lights.insert(m_sunlight);

}

ObjectManager::~ObjectManager()
{
    for(auto iter = m_objects.begin(); iter != m_objects.end(); iter++)
    {
        Object* currentObject = *iter;
        delete currentObject;
    }

    for(auto iter = m_lights.begin(); iter != m_lights.end(); iter++)
    {
        Light* currentLight = *iter;
        delete currentLight;
    }
}

void ObjectManager::update(float time)
{
    for(auto iter = m_objects.begin(); iter != m_objects.end(); iter++)
    {
        (*iter)->update(time);
    }
}

void ObjectManager::drawGeometryObjects(Camera *camera)
{
    camera->activate();
    for(auto obj : m_geometryObjects) {
        internalDrawObject(obj, camera);
    }
}

void ObjectManager::drawParticleSystems(Camera *camera)
{
    camera->activate();
    for(auto obj : m_particleSystems) {
        internalDrawObject(obj, camera);
    }
}

void ObjectManager::drawSkyBox(Camera *camera)
{
    camera->activate();
    m_skybox->setCamera(camera);
    m_skybox->updatePosition();
    glDisable(GL_CULL_FACE);
    internalDrawObject(m_skybox, camera);
    glEnable(GL_CULL_FACE);
}

void ObjectManager::drawObject(Object* object, Camera* camera)
{
    camera->activate();
    internalDrawObject(object, camera);
}

void ObjectManager::drawScreenQuad()
{
    Application::instance()->camera()->activate();
    glm::mat4 modelMatrix;
    glm::mat4 viewProjectionMatrix = glm::inverse(glm::ortho(-1.f, 1.f, -1.f, 1.f));
    Mesh* mesh = MeshManager::instance()->loadMesh(ScreenQuadMesh);

    mesh->activate();
    //disable writing to depthbuffer
    glDepthMask(GL_FALSE);
    glDisable(GL_DEPTH_TEST);
    mesh->draw(modelMatrix, viewProjectionMatrix);
    glEnable(GL_DEPTH_TEST);
    //enable writing to depthbuffer
    glDepthMask(GL_TRUE);
}


Object* ObjectManager::sun() const
{
    return m_sun;
}

ObjectManager *ObjectManager::instance()
{
    return Application::instance()->objectManager();
}

void ObjectManager::internalDrawObject(Object *object, Camera *camera)
{
    glm::mat4 viewProjectionMatrix = camera->viewProjectionMatrix();
    if(viewFrustrumTest(object, camera)){
        object->activateMeshForDrawing();
        object->draw(viewProjectionMatrix);
    }
}

bool ObjectManager::viewFrustrumTest(Object *o, Camera *camera) const
{
    if(! o->mesh()->hasBoundingSphere())
        return true;
    if(camera->fovY() < 1.f) return true;

    glm::vec4 center4 = o->modelMatrix()*glm::vec4(o->mesh()->boundingSphereCenter(),1.f);
    glm::vec3 center = glm::vec3(center4.x, center4.y, center4.z);
    float radius = o->mesh()->boundingSphereRadius();

    float distance;
    bool result = true;

    for(int i=0; i < 6; i++) {
        distance = camera->viewPlanes()[i].distance(center);
        if (distance < -radius){
            return false;
        }
    }
    return result;
}
