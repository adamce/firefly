/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GLRENDERER_H
#define GLRENDERER_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>

#include <unordered_set>
#include <vector>

class Shader;
class Object;
class Camera;
class LensFlare;
class ShaderManager;
class Cuda;
class RenderPass;
class MainRenderPass;
class PostProcessPass;
class ParticleSystemsPass;
class ShadowMapPass;
class DebugDrawPass;
class Framebuffer;

class Renderer
{
public:
    Renderer(void);
    virtual ~Renderer(void);
    void init(); 	// Creates OpenGL Rendering Context
    void initSceneOld();		// Scene preparation stuff
    void applicationInitFinished();
    void resize(int w, int h);
    int width() const { return m_width; }
    int height() const { return m_height; }
    void renderScene(Camera* camera);
    void destroyScene();
    void switchUsingVSync();

    void writeFrontBufferToFile(std::string filename);

    void addTrianglesToDrawnCounter(unsigned int num);
    unsigned int trianglesDrawn() const;

    Cuda* cudaManager() const;
    GLFWwindow* glfwWindowHandle() const { return m_windowHandle; }
    PostProcessPass* postProcessPass() const { return m_postProcessPass; }
    DebugDrawPass* debugDrawPass() const { return m_debugDrawPass; }
    ParticleSystemsPass* particleSystemsPass() const { return m_particleSystemsPass; }
    const std::vector<ShadowMapPass*>* shadowMapPasses() const { return &m_shadowMapPasses; }



private:
    GLFWwindow* m_windowHandle;

    Framebuffer* m_resultFramebuffer;

    MainRenderPass* m_mainRenderPass;
    ParticleSystemsPass* m_particleSystemsPass;
    PostProcessPass* m_postProcessPass;
    DebugDrawPass* m_debugDrawPass;
    std::vector<ShadowMapPass*> m_shadowMapPasses;
    std::unordered_set<RenderPass*> m_renderPasses;


    int m_width;
    int m_height;

    bool m_useVSync;
    unsigned int m_trianglesDrawn = 0;
};

#endif // GLRENDERER_H
