#include "GlmHelper.h"





std::ostream &operator<<(std::ostream &stream, const glm::vec3 &v)
{
    return stream << "glm::vec3(" << v.x << "f, " << v.y << "f, " << v.z << ")" << std::endl;
}


std::ostream &operator<<(std::ostream &stream, const glm::vec4 &v)
{
    return stream << "glm::vec4(" << v.x << "f, " << v.y << "f, " << v.z << "f, " << v.w << ")" << std::endl;
}


std::ostream &operator<<(std::ostream &stream, const glm::mat4 &m)
{
    return stream << std::fixed << std::showpos << std::setw(5) << std::setprecision(8) <<
                     "glm::mat4(" << m[0].x << "f, " << m[1].x << "f, " << m[2].x << "f, " << m[3].x << "," << std::endl <<
                     "          " << m[0].y << "f, " << m[1].y << "f, " << m[2].y << "f, " << m[3].y << "," << std::endl <<
                     "          " << m[0].z << "f, " << m[1].z << "f, " << m[2].z << "f, " << m[3].z << "," << std::endl <<
                     "          " << m[0].w << "f, " << m[1].w << "f, " << m[2].w << "f, " << m[3].w << ")" << std::endl;
}
