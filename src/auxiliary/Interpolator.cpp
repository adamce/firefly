#include "Interpolator.h"

#include <gsl/gsl_spline.h>
#include <iostream>
#include <algorithm>

Interpolator::Interpolator(InterpolationMethod)
{
}

Interpolator::~Interpolator()
{
    if(m_builded) {
        gsl_spline_free(static_cast<gsl_spline*>(m_spline));
        gsl_interp_accel_free(static_cast<gsl_interp_accel*>(m_accelerator));
    }
}

void Interpolator::add(double t, double value)
{
    if(m_builded) return;
    m_xValues.push_back(t);
    m_yValues.push_back(value);

    m_minT = std::min(t, m_minT);
    m_maxT = std::max(t, m_maxT);
}

void Interpolator::build()
{
    if(m_builded) {
        gsl_spline_free(static_cast<gsl_spline*>(m_spline));
        gsl_interp_accel_free(static_cast<gsl_interp_accel*>(m_accelerator));

    }
    m_accelerator = gsl_interp_accel_alloc();

    size_t size = m_xValues.size();
    switch (m_interpolationMethod) {
    case Polynomial:
        m_spline = gsl_spline_alloc(gsl_interp_polynomial, size);
        break;
    case CSpline:
        m_spline = gsl_spline_alloc(gsl_interp_cspline, size);
        break;
    case Akima:
        m_spline = gsl_spline_alloc(gsl_interp_akima, size);
        break;
    case InterpolationMethod::Linear:
    default:
        m_spline = gsl_spline_alloc(gsl_interp_linear, size);
        break;
    }
    gsl_spline_init(static_cast<gsl_spline*>(m_spline), m_xValues.data(), m_yValues.data(), size);

    m_builded = true;
}

float Interpolator::interpolate(float t) const {
    if(!m_builded) {
        std::cerr << "Interpolator::interpolate(): not yet builded!" << std::endl;
    }
    if(t < m_minT) return m_yValues.at(0);
    if(t > m_maxT) return m_yValues.at(m_yValues.size() - 1);

    return gsl_spline_eval(static_cast<gsl_spline*>(m_spline), t, static_cast<gsl_interp_accel*>(m_accelerator));
}
