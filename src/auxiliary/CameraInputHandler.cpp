#include "CameraInputHandler.h"
#include "Application.h"
#include "Renderer.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

CameraInputHandler::CameraInputHandler(Camera *camera) : m_camera(camera), m_firstFrameWidthoutRotationMode(false)
{
}

void CameraInputHandler::handleInput(float time)
{
    float moveSpeed = 2.f;
    float zoomSpeed = 50.f;
    float rotateKeySpeed = 30.f;
    float rotateMouseFactor = 0.1f;

    float moveDist = moveSpeed * time;
    float rotateKeyDist = rotateKeySpeed * time;

    GLFWwindow* windowHandle = Application::instance()->renderer()->glfwWindowHandle();
    // keyboard
    if(glfwGetKey(windowHandle, 'A'))
        m_camera->stopAnimation(), m_camera->translate(glm::vec3(-moveDist, 0, 0));
    if(glfwGetKey(windowHandle, 'W'))
        m_camera->stopAnimation(), m_camera->translate(glm::vec3(0, 0, -moveDist));
    if(glfwGetKey(windowHandle, 'D'))
        m_camera->stopAnimation(), m_camera->translate(glm::vec3(moveDist, 0, 0));
    if(glfwGetKey(windowHandle, 'S'))
        m_camera->stopAnimation(), m_camera->translate(glm::vec3(0, 0, moveDist));
    if(glfwGetKey(windowHandle, 'Q'))
        m_camera->stopAnimation(), m_camera->rotateZ(rotateKeyDist);
    if(glfwGetKey(windowHandle, 'E'))
        m_camera->stopAnimation(), m_camera->rotateZ(-rotateKeyDist);
    if(glfwGetKey(windowHandle, GLFW_KEY_LEFT_SHIFT))
        m_camera->stopAnimation(), m_camera->translate(glm::vec3(0, -moveDist, 0));
    if(glfwGetKey(windowHandle, GLFW_KEY_SPACE))
        m_camera->stopAnimation(), m_camera->translate(glm::vec3(0, moveDist, 0));

    if(glfwGetKey(windowHandle, '1'))
        m_camera->stopAnimation(), m_camera->increaseFovy(-zoomSpeed*time);
    if(glfwGetKey(windowHandle, '2'))
        m_camera->stopAnimation(), m_camera->increaseFovy(zoomSpeed*time);

    if(glfwGetMouseButton(windowHandle, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
        m_camera->stopAnimation();
        double newX, newY;
        glfwGetCursorPos(windowHandle, &newX, &newY);

        double diffX, diffY;
        diffX = m_lastMousePosX - newX;
        diffY = m_lastMousePosY - newY;

        m_camera->rotateY(rotateMouseFactor * (float) diffX);
        m_camera->rotateX(rotateMouseFactor * (float) diffY);

        glfwSetCursorPos(windowHandle, m_lastMousePosX, m_lastMousePosY);
        glfwSetInputMode(windowHandle, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
        m_firstFrameWidthoutRotationMode = true;
    }
    else {
        if(m_firstFrameWidthoutRotationMode) {
            m_firstFrameWidthoutRotationMode = false;
            glfwSetInputMode(windowHandle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        }
        glfwGetCursorPos(windowHandle, &m_lastMousePosX, &m_lastMousePosY);
    }
}
