#ifndef APPLICATIONCONSTANTS_H
#define APPLICATIONCONSTANTS_H

#define VIENNA_MAX_LIGHT_COUNT 10
#define VIENNA_SHADOW_MAP_SIZE 1024
#define VIENNA_SHADOW_MAP_SIZE_ONE_OVER 1.f/1024.f
#define VIENNA_MAX_TEXTURE_SIZE 2048
#define VIENNA_MAX_MATERIAL_COUNT 100   // must be the same as in all shaders reading from the material buffer uniform. currently DeferredGeometryShader.frag

#define VIENNA_DEFAULT_WINDOW_WIDTH 1280
#define VIENNA_DEFAULT_WINDOW_HEIGHT 768

#define VIENNA_TIMING_SAMPLES 10
#define VIENNA_TIMING_PRECISION std::chrono::microseconds

#ifdef _LINUX
#define __stdcall
#endif//#define VIENNA_DEBUG_CUDA_PIXEL 1



#define VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR 256


#define VIENNA_MODEL_BASE_PATH "../data/models/"

#endif // APPLICATIONCONSTANTS_H
