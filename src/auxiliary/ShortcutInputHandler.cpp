/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <fstream>
#include <cstdio>

#include "ShortcutInputHandler.h"

#include "Application.h"
#include "IngameGui.h"
#include "Renderer.h"
#include "objects/Camera.h"
#include "renderPasses/PostProcessPass.h"
#include "renderPasses/DebugDrawPass.h"
#include "renderPasses/ParticleSystemsPass.h"
#include "auxiliary/ApplicationConstants.h"
#include "auxiliary/GlmHelper.h"
#include "ObjectManager.h"
#include "objects/Light.h"
#include "auxiliary/HelperOperations.h"
#include "bvh/BvhManager.h"

void VIENNA_STDCALL chawahShortcutHandlerFunction(GLFWwindow*, int key, int scancode, int action, int mods)
{
    if(action != GLFW_PRESS)
        return;

    Application* application = Application::instance();
    if(key==GLFW_KEY_F1)
        application->switchEnvironmentMapping();

    if(key==GLFW_KEY_F2)
        application->ingameGui()->switchPerformanceStatsVisibility();

//    if(key==GLFW_KEY_F3)
//        application->bvhManager()->debugTraceImage();

	if(key==GLFW_KEY_F4)
        application->switchTextureFiltering();

	if(key==GLFW_KEY_F5)
        application->switchMipMapping();

//    if(key==GLFW_KEY_F6)
//        application->switchBufferMode();

//	if(key==GLFW_KEY_F7)
//        application->switchSkybox();


    if(key==GLFW_KEY_F9) {
        application->renderer()->particleSystemsPass()->setEnabled(!application->renderer()->particleSystemsPass()->enabled());
        std::cout << "Particle drawing is now " << application->renderer()->particleSystemsPass()->enabled() << std::endl;
    }

    if(key==GLFW_KEY_F10) {
        application->renderer()->debugDrawPass()->setEnabled(!application->renderer()->debugDrawPass()->enabled());
        std::cout << "Debug drawing is now " << application->renderer()->debugDrawPass()->enabled() << std::endl;
    }

    if(key==GLFW_KEY_F11) {
        application->renderer()->postProcessPass()->setEnabled(!application->renderer()->postProcessPass()->enabled());
        std::cout << "Post processing is now " << application->renderer()->postProcessPass()->enabled() << std::endl;
    }

    if(key==GLFW_KEY_PAUSE)
        application->switchPaused();

    if(key=='V')
        application->renderer()->switchUsingVSync();

    if(key == GLFW_KEY_G) {
        Camera* camera = application->camera();
        if((mods & GLFW_MOD_CONTROL) == GLFW_MOD_CONTROL) { // ctrl down
            std::ofstream ofile("cameraPoseFile.tmp", std::ios::binary);
            glm::mat4x4 camModelM = camera->modelMatrix();
            float fov = camera->fovY();
            write(ofile, camModelM);
            write(ofile, fov);
        }
        else {
            camera->stopAnimation();
            if ( fileExists("cameraPoseFile.tmp") ) {
                std::ifstream infile("cameraPoseFile.tmp", std::ios::binary);
                glm::mat4x4 camModelM;
                float fov;
                read(infile, camModelM);
                read(infile, fov);
                camera->setModelMatrix(camModelM);
                camera->setFovY(fov);
            }
        }
    }
    if(key == GLFW_KEY_P) {
        Camera* camera = application->camera();
        std::cout << "Camera: pos " << camera->position() << " model matrix:" << std::endl << camera->modelMatrix();
    }
    if(key == GLFW_KEY_L) {
        Camera* camera = application->camera();
        Light* light = *ObjectManager::instance()->lights().begin();
        light->setModelMatrix(camera->modelMatrix());
        std::ofstream ofile("light0PoseFile.tmp", std::ios::binary);
        glm::mat4x4 camModelM = camera->modelMatrix();
        write(ofile, camModelM);
        std::cout << "Light model matrix set to camera matrix: pos " << light->position() << " model matrix:" << std::endl << light->modelMatrix();
    }
    if(key == GLFW_KEY_K) {
        Camera* camera = application->camera();
        Light* light = *(++ObjectManager::instance()->lights().begin());
        light->setModelMatrix(camera->modelMatrix());
        std::ofstream ofile("light1PoseFile.tmp", std::ios::binary);
        glm::mat4x4 camModelM = camera->modelMatrix();
        write(ofile, camModelM);
        std::cout << "Light model matrix set to camera matrix: pos " << light->position() << " model matrix:" << std::endl << light->modelMatrix();
    }
    if(key == GLFW_KEY_T) {
        std::cout << "============= TIMINGS =============" << std::endl;
        printf("====== resolution: %4dx%-4d ======\n", application->renderer()->width(), application->renderer()->height());

        const std::map<std::string, double> timings = application->timings();
        for(std::pair<std::string, double> pair : timings) {
            printf("%-27s: %9.2f\n", pair.first.c_str(), pair.second);
//            std::cout << pair.first << ": " << pair.second << std::endl;
        }
        std::cout << std::flush;
    }
}

void ShortcutInputHandler::enableShortcuts()
{
    GLFWwindow* windowHandle = Application::instance()->renderer()->glfwWindowHandle();
    glfwSetKeyCallback(windowHandle, &chawahShortcutHandlerFunction);
}

void ShortcutInputHandler::disableShortcuts()
{
    GLFWwindow* windowHandle = Application::instance()->renderer()->glfwWindowHandle();
    glfwSetKeyCallback(windowHandle, 0);
}

