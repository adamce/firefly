#ifndef APPLICATIONCONSTANTSCUDA_H
#define APPLICATIONCONSTANTSCUDA_H

#define VIENNA_CUDA_BLOCK_WIDTH_RT 16
#define VIENNA_CUDA_BLOCK_HEIGHT_RT 8

// watch out because of shared memory !! file IndirectLightFilterKernel
// width must be 16|32 = width + 2 * filtersize
#define VIENNA_CUDA_BLOCK_WIDTH_INDIR_FILTER 8
#define VIENNA_BLOCK_HEIGHT_INDIR_FILTER 32     // note that the cuda thread block height will be half the size.

// 1 would mean filter size 3, 2 -> 5, 10 -> 21:
#define VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE 4

#define VIENNA_CUDA_BLOCK_WIDTH_DEFERRED 16
#define VIENNA_CUDA_BLOCK_HEIGHT_DEFERRED 8

#define VIENNA_SECONDARY_RAYS 3

#define VIENNA_NVIDIA_BVH
//#define VIENNA_BVH_MIDPOINT_DIVISION
//#define VIENNA_BVH_MAX_SPACE_BETWEEN_MIDPOINTS
//#define VIENNA_BVH_MacDonaldBooth_EVAL_ON_64_INDICES


#define VIENNA_HDR_SCALING_FACTOR 2.0f
#define VIENNA_BG_COLOUR make_float3(0.831f, 0.341f, 0.f)

//#define VIENNA_DEBUG_TRACE_FIRST_RAY

#endif // APPLICATIONCONSTANTSCUDA_H
