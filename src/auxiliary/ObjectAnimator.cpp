#include "ObjectAnimator.h"

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <cmath>

glm::mat4 quatToMat4(glm::quat q) {
        float sqw = q.w*q.w;
        float sqx = q.x*q.x;
        float sqy = q.y*q.y;
        float sqz = q.z*q.z;

        glm::mat4 m;

        // invs (inverse square length) is only required if quaternion is not already normalised
        float invs = 1 / (sqx + sqy + sqz + sqw);
        m[0][0] = ( sqx - sqy - sqz + sqw)*invs ; // since sqw + sqx + sqy + sqz =1/invs*invs
        m[1][1] = (-sqx + sqy - sqz + sqw)*invs ;
        m[2][2] = (-sqx - sqy + sqz + sqw)*invs ;

        double tmp1 = q.x*q.y;
        double tmp2 = q.z*q.w;
        m[1][0] = 2.0 * (tmp1 + tmp2)*invs ;
        m[0][1] = 2.0 * (tmp1 - tmp2)*invs ;

        tmp1 = q.x*q.z;
        tmp2 = q.y*q.w;
        m[2][0] = 2.0 * (tmp1 - tmp2)*invs ;
        m[0][2] = 2.0 * (tmp1 + tmp2)*invs ;
        tmp1 = q.y*q.z;
        tmp2 = q.x*q.w;
        m[2][1] = 2.0 * (tmp1 + tmp2)*invs ;
        m[1][2] = 2.0 * (tmp1 - tmp2)*invs ;
        return glm::transpose(m);
}

ObjectAnimator::ObjectAnimator()
{
}

void ObjectAnimator::addKeyFrame(float t, const glm::vec3 &position)
{
    m_posXInterpolator.add(t, position.x);
    m_posYInterpolator.add(t, position.y);
    m_posZInterpolator.add(t, position.z);
}

void ObjectAnimator::addKeyFrame(float t, const glm::quat &rotation)
{
    m_rotKeyFrames.push_back(std::pair<float, glm::quat>(t, rotation));
    m_quatXInterpolator.add(t, rotation.x);
    m_quatYInterpolator.add(t, rotation.y);
    m_quatZInterpolator.add(t, rotation.z);
    m_quatWInterpolator.add(t, rotation.w);

}

void ObjectAnimator::build()
{
    m_posXInterpolator.build();
    m_posYInterpolator.build();
    m_posZInterpolator.build();

    m_quatXInterpolator.build();
    m_quatYInterpolator.build();
    m_quatZInterpolator.build();
    m_quatWInterpolator.build();

}

glm::mat4 ObjectAnimator::transformationForTime(float t)
{
    float posX = m_posXInterpolator.interpolate(t);
    float posY = m_posYInterpolator.interpolate(t);
    float posZ = m_posZInterpolator.interpolate(t);

//    float quatX = m_quatXInterpolator.interpolate(t);
//    float quatY = m_quatYInterpolator.interpolate(t);
//    float quatZ = m_quatZInterpolator.interpolate(t);
//    float quatW = m_quatWInterpolator.interpolate(t);

    int smallerKey = m_lastRotKey;
    if(m_rotKeyFrames.at(smallerKey).first > t) {
        smallerKey = 0;
    }
    while (smallerKey + 1 < m_rotKeyFrames.size() && m_rotKeyFrames.at(smallerKey + 1).first < t) {
        smallerKey++;
    }
    m_lastRotKey = smallerKey;
    int biggerKey = smallerKey + 1;
    if(biggerKey >= m_rotKeyFrames.size())
        biggerKey--;

    float smallerTime = m_rotKeyFrames.at(smallerKey).first;
    glm::quat smallerQuat = m_rotKeyFrames.at(smallerKey).second;
    float biggerTime = m_rotKeyFrames.at(biggerKey).first;
    glm::quat biggerQuat = m_rotKeyFrames.at(biggerKey).second;

    glm::quat quat;
    if(biggerKey != smallerKey)
        quat = glm::slerp(smallerQuat, biggerQuat, (t - smallerTime)/(biggerTime - smallerTime));
    else
        quat = biggerQuat;


//    float normalisationFactor = 1.f / sqrt(quatX*quatX + quatY*quatY + quatZ*quatZ + quatW*quatW);
//    quatX *= normalisationFactor;
//    quatY *= normalisationFactor;
//    quatZ *= normalisationFactor;
//    quatW *= normalisationFactor;

    float normalisationFactor = 1.f / sqrt(quat.x*quat.x + quat.y*quat.y + quat.z*quat.z + quat.w*quat.w);
    quat.x *= normalisationFactor;
    quat.y *= normalisationFactor;
    quat.z *= normalisationFactor;
    quat.w *= normalisationFactor;

//    glm::quat quat = glm::quat(quatW, quatX, quatY, quatZ);
    glm::mat4 transformation = quatToMat4(quat);
    glm::mat4 translation;
    translation[3].x = posX;
    translation[3].y = posY;
    translation[3].z = posZ;
    transformation = translation * transformation;
    return transformation;
}
