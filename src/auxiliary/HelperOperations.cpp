#include "HelperOperations.h"

bool fileExists( const std::string& fn )
{
    FILE* pF = fopen( fn.c_str(), "rb" );
    if( pF != 0 )
    {
        fclose( pF );
        return true;
    }
    else
    {
        return false;
    }
}
