#ifndef OBJECTANIMATOR_H
#define OBJECTANIMATOR_H

#include "Interpolator.h"
#define GLM_FORCE_RADIANS
#include "glm/glm.hpp"

#include <vector>


class ObjectAnimator {
public:
    ObjectAnimator();
    void addKeyFrame(float t, const glm::vec3& position);
    void addKeyFrame(float t, const glm::quat& rotation);
    void build();
    glm::mat4 transformationForTime(float t);

private:
    Interpolator m_posXInterpolator;
    Interpolator m_posYInterpolator;
    Interpolator m_posZInterpolator;


    std::vector<std::pair<float, glm::quat>> m_rotKeyFrames;
    int m_lastRotKey = 0;

    Interpolator m_quatXInterpolator;
    Interpolator m_quatYInterpolator;
    Interpolator m_quatZInterpolator;
    Interpolator m_quatWInterpolator;

};

#endif // OBJECTANIMATOR_H
