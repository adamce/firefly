#include "FmodSoundManager.h"

#include "fmod_errors.h"

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

FmodSoundManager::FmodSoundManager()
{
	initSoundEngine();
	initChannels();
	initSounds();

}

FmodSoundManager::~FmodSoundManager()
{
	for(std::map<SoundType::value, FMOD::Sound*>::iterator iter = m_sounds.begin(); iter != m_sounds.end(); iter++)
	{
		FMOD::Sound* currentObject = (*iter).second;
		delete currentObject;
	}
	m_fmodSystem->release();
}

void FmodSoundManager::initSoundEngine()
{

	FMOD_RESULT      result;
	unsigned int     version;
	int              numdrivers;
	FMOD_SPEAKERMODE speakermode;
	FMOD_CAPS        caps;
	char             name[256];
	/*
    Create a System object and initialize.
	*/
	result = FMOD::System_Create(&m_fmodSystem);
	fmodERRCHECK(result);

	result = m_fmodSystem->getVersion(&version);
	fmodERRCHECK(result);

	if (version < FMOD_VERSION)
	{
		printf("Error!  You are using an old version of FMOD %08x.  This program requires %08x\n",
	version, FMOD_VERSION);
		exit(-5);
	}

	result = m_fmodSystem->getNumDrivers(&numdrivers);
	fmodERRCHECK(result);

	if (numdrivers == 0)
	{
		result = m_fmodSystem->setOutput(FMOD_OUTPUTTYPE_NOSOUND);
		fmodERRCHECK(result);
	}
	else
	{
        result = m_fmodSystem->getDriverCaps(0, &caps, 0, &speakermode);
		fmodERRCHECK(result);

		/*
			Set the user selected speaker mode.
		*/
		result = m_fmodSystem->setSpeakerMode(speakermode);
		fmodERRCHECK(result);

		if (caps & FMOD_CAPS_HARDWARE_EMULATED)
		{
			/*
				The user has the 'Acceleration' slider set to off!  This is really bad
				for latency! You might want to warn the user about this.
			*/
			result = m_fmodSystem->setDSPBufferSize(1024, 10);
			fmodERRCHECK(result);
		}

		result = m_fmodSystem->getDriverInfo(0, name, 256, 0);
		fmodERRCHECK(result);

		if (strstr(name, "SigmaTel"))
		{
			/*
				Sigmatel sound devices crackle for some reason if the format is PCM 16bit.
				PCM floating point output seems to solve it.
			*/
			result = m_fmodSystem->setSoftwareFormat(48000, FMOD_SOUND_FORMAT_PCMFLOAT, 0,0,
	FMOD_DSP_RESAMPLER_LINEAR);
			fmodERRCHECK(result);
		}
	}

	result = m_fmodSystem->init(100, FMOD_INIT_NORMAL, 0);
	if (result == FMOD_ERR_OUTPUT_CREATEBUFFER)
	{
		/*
			Ok, the speaker mode selected isn't supported by this soundcard.  Switch it
			back to stereo...
		*/
		result = m_fmodSystem->setSpeakerMode(FMOD_SPEAKERMODE_STEREO);
		fmodERRCHECK(result);

		/*
			... and re-init.
		*/
		result = m_fmodSystem->init(100, FMOD_INIT_NORMAL, 0);
		fmodERRCHECK(result);
	}
}

void FmodSoundManager::initSounds()
{
	FMOD_RESULT result;
	FMOD::Sound* sound = NULL;

	result = m_fmodSystem->createSound("../data/sounds/Lohstana_David_-_La_rupture.mp3", FMOD_LOOP_NORMAL, NULL, &sound);
	fmodERRCHECK(result);
	m_sounds.insert(std::pair<SoundType::value, FMOD::Sound*>(SoundType::BackgroundMusic, sound));
}

void FmodSoundManager::initChannels()
{
	for(int i = 0; i < SoundType::COUNT ; i++){
		m_channelArray[i] = 0;
		m_volumes[i] = 1.0f;
	}
}

void FmodSoundManager::fmodERRCHECK(FMOD_RESULT result)
{
    if (result != FMOD_OK){
        //printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		std::cerr << "FMOD error! ( " << result << " " << FMOD_ErrorString(result) << std::endl;
		//exit(-5);
    }
}

void FmodSoundManager::playSound(SoundType::value type)
{
	FMOD_RESULT result;
	FMOD::Sound* sound;
	//FMOD::Channel* channel = 0;
	if(m_sounds.count(type)){
		sound= (*(m_sounds.find(type))).second;
	}
	else{
		std::cerr << "Sound type " << type << ": not found" << std::endl;
		return;
	}

	result = m_fmodSystem->playSound(FMOD_CHANNEL_REUSE, sound, false, &m_channelArray[type]);

	m_channelArray[type]->setPaused(true);
	m_channelArray[type]->setVolume(m_volumes[type]);
	m_channelArray[type]->setPaused(false);

	fmodERRCHECK(result);
	m_fmodSystem->update();
}

void FmodSoundManager::adjustVolume(SoundType::value type, float volume)
{
	m_volumes[type] = volume;
}
