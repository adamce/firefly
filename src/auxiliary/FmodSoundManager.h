#ifndef FMODSOUNDMANAGER_H
#define FMODSOUNDMANAGER_H

#include <map>
#include <set>
#include "fmod.hpp"

namespace SoundType{
	enum value {
		RocketFireSound,
		/*RocketHitSound,*/
		ProjectileFireSound,
		/*ProjectileHitSound,*/
		PowerupBoostSound,
		PowerupArmorPlusSound,
		PowerUpRocketPlusSound,
		BackgroundMusic,
		COUNT
	};
};

class FmodSoundManager
{
public:
	FmodSoundManager();
	~FmodSoundManager();
	void playSound(SoundType::value type);
	void adjustVolume(SoundType::value type, float volume);

protected:
	void fmodERRCHECK(FMOD_RESULT result);
	FMOD::System* fmodSoundSystem() const;
	void initSoundEngine();
	void initSounds();
	void initChannels();
	
private:

	FMOD::System* m_fmodSystem;
	std::map<SoundType::value, FMOD::Sound*> m_sounds;
	FMOD::Channel* m_channelArray[SoundType::COUNT];
	float m_volumes[SoundType::COUNT];
};

#endif // FMODSOUNDMANAGER_H
