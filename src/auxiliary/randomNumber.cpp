#include "randomNumber.h"
#include <stdlib.h>

float randomNumber(float lowerBound, float upperBound)
{
    float randomNum = (float) rand() ;
    randomNum /= RAND_MAX; // scale to 0.0 -> 1.0
    float diff = upperBound - lowerBound;
    randomNum*=diff;
    randomNum+=lowerBound;
    return randomNum;
}
