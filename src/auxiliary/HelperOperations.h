#ifndef HELPEROPERATIONS_H
#define HELPEROPERATIONS_H

#include <fstream>

// Write a simple data type to a stream.
template<class T>
std::ostream& write(std::ostream& stream, const T& x) {
    return stream.write(reinterpret_cast<const char*>(&x), sizeof(x));
    //return stream << x << ' ';
}
// Read a simple data type from a stream.
template<class T>
std::istream& read(std::istream& os, T& x) {
    return os.read(reinterpret_cast<char*>(&x), sizeof(x));
}

bool fileExists( const std::string& fn );

#ifndef VIENNA_WINDOWS
#define VIENNA_STDCALL
#else
#define VIENNA_STDCALL __stdcall
#endif


#endif // HELPEROPERATIONS_H
