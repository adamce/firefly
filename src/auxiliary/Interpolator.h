#ifndef INTERPOLATOR_H
#define INTERPOLATOR_H

#include <vector>
#include <limits>

class Interpolator
{
public:
    enum InterpolationMethod {
        Linear,
        Polynomial,
        CSpline,
        Akima
    };
public:
    Interpolator(InterpolationMethod = CSpline);
    ~Interpolator();
    void add(double t, double value);
    void build();
    float interpolate(float t) const;

private:
    InterpolationMethod m_interpolationMethod;
    std::vector<double> m_xValues;
    std::vector<double> m_yValues;

    double m_minT = std::numeric_limits<double>::max();
    double m_maxT = std::numeric_limits<double>::min();
    bool m_builded = false;
    void* m_accelerator = nullptr;
    void* m_spline = nullptr;
};

#endif // INTERPOLATOR_H
