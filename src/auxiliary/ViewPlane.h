#ifndef VIEWPLANE_H
#define VIEWPLANE_H

#include <iostream>
#include "glm/glm.hpp"

struct ViewPlane
{
    ViewPlane() {}

	void setCoefficients(glm::vec4 v)
	{
		setCoefficients(v.x, v.y, v.z, v.w);
	}

	void setCoefficients(float a_ = 0.f, float b_ = 0.f, float c_ = 0.f, float d_ = 0.f)
	{
		float length = glm::sqrt(a_*a_+b_*b_+c_*c_);
		normal.x = a_/length;
		normal.y = b_/length;
		normal.z = c_/length;
		d = d_/length;
	}

	float distance(glm::vec3& point) const
	{
		return glm::dot(normal, point)+d;
	}

	glm::vec3 getNormal() const
	{
		return normal;
	}


	void output(glm::vec3 vec) {
        std::cout << "x/y/z: " << vec.x << "/" << vec.y << "/" << vec.z << std::endl;
    }

    void set3Points( glm::vec3 &v1,  glm::vec3 &v2,  glm::vec3 &v3) {


        glm::vec3 aux1, aux2;

        aux1 = glm::normalize(v1 - v2);
        aux2 = glm::normalize(v3 - v2);

        normal = glm::cross(aux2, aux1);


        normal = glm::normalize(normal);
        //point.copy(v2);
        d = -glm::dot(normal,v2);

//         std::cout << "========" << std::endl;
//         output(aux1);
//         output(aux2);
//         output(normal);
//         std::cout << ".";
    }

    void setNormalAndPoint(glm::vec3 &normal_, glm::vec3 &point_) {

        normal = normal_;
        glm::normalize(normal);
        d = -glm::dot(normal,point_);
    }


    float d;
    glm::vec3 normal;

};

#endif // VIEWPLANE_H
