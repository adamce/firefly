#ifndef GLMHELPER_H
#define GLMHELPER_H

#include <glm/glm.hpp>
#include <iostream>
#include <iomanip>

std::ostream& operator<<(std::ostream &stream, const glm::vec3& v);
std::ostream& operator<<(std::ostream &stream, const glm::vec4& v);
std::ostream& operator<<(std::ostream &stream, const glm::mat4& m);

#endif // GLMHELPER_H
