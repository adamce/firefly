#include "IngameGui.h"

#include <sstream>
#include <iostream>


#include "Application.h"
#include "Renderer.h"

IngameGui::IngameGui() :  m_performanceStatsVisible(true), m_messageTimer(0), m_frameRate(0.f)
{
}


void IngameGui::setFramerate(float fps)
{
    m_frameRate = fps;
}

void IngameGui::update(float time)
{
    m_messageTimer+=time;
    if(m_performanceStatsVisible) {
        if(m_messageTimer > 3.0) {
            std::stringstream statsStringStream;
            statsStringStream << "triangles: ";
            statsStringStream << Application::instance()->renderer()->trianglesDrawn();
            statsStringStream << "  fps: ";
            statsStringStream << m_frameRate;

            postMessage(statsStringStream.str());
        }
    }
}

void IngameGui::switchPerformanceStatsVisibility()
{
    m_performanceStatsVisible = !m_performanceStatsVisible;
    std::string message = "Switched performance stats display ";
    message+= m_performanceStatsVisible?"on.":"off.";
	postMessage(message.c_str());
}

void IngameGui::postMessage(const std::string& message)
{
    std::cout << message << std::endl;
    m_messageTimer = 0;
}
