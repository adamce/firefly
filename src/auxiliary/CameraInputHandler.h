#ifndef CAMERAINPUTHANDLER_H
#define CAMERAINPUTHANDLER_H

#include "objects/Camera.h"

class CameraInputHandler
{
public:
    CameraInputHandler(Camera* camera);
    void handleInput(float time);
private:
    Camera* m_camera;
    double m_lastMousePosX, m_lastMousePosY;
    bool m_firstFrameWidthoutRotationMode;
};

#endif // CAMERAINPUTHANDLER_H
