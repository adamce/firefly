#ifndef INGAMEGUI_H
#define INGAMEGUI_H
#include <string>

class IngameGui
{
public:
    IngameGui();
    void setFramerate(float fps);
    void update(float time);
    void switchPerformanceStatsVisibility();
    void postMessage(const std::string& message);

private:
    bool m_performanceStatsVisible;
    float m_messageTimer;
    float m_frameRate;
};

#endif // INGAMEGUI_H
