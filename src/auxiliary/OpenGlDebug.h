#ifndef OPENGLDEBUG_H
#define OPENGLDEBUG_H

#include <iostream>
#include <sstream>
#include <GL/glew.h>
#include <GL/gl.h>

static std::string FormatDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity, const char* msg);

//static void APIENTRY DebugCallbackAMD(GLuint id, GLenum category, GLenum severity, GLsizei length, const GLchar* message, GLvoid* userParam) {
//    std::string error = FormatDebugOutput(category, category, id, severity, message);
//    std::cout << error << std::endl;
//}

// version found on the cg2ue page from tuwien

static void APIENTRY openGlDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const GLvoid* userParam) {
    if(severity == GL_DEBUG_SEVERITY_NOTIFICATION)
        return;

    std::string error = FormatDebugOutput(source, type, id, severity, message);
    std::cout << error << std::endl;
}

static std::string FormatDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity, const char* msg) {
    std::stringstream stringStream;
    std::string sourceString;
    std::string typeString;
    std::string severityString;

    // The AMD variant of this extension provides a less detailed classification of the error,
    // which is why some arguments might be "Unknown".
    switch (source) {
        case GL_DEBUG_CATEGORY_API_ERROR_AMD:
        case GL_DEBUG_SOURCE_API: {
            sourceString = "API";
            break;
        }
        case GL_DEBUG_CATEGORY_APPLICATION_AMD:
        case GL_DEBUG_SOURCE_APPLICATION: {
            sourceString = "Application";
            break;
        }
        case GL_DEBUG_CATEGORY_WINDOW_SYSTEM_AMD:
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM: {
            sourceString = "Window System";
            break;
        }
        case GL_DEBUG_CATEGORY_SHADER_COMPILER_AMD:
        case GL_DEBUG_SOURCE_SHADER_COMPILER: {
            sourceString = "Shader Compiler";
            break;
        }
        case GL_DEBUG_SOURCE_THIRD_PARTY: {
            sourceString = "Third Party";
            break;
        }
        case GL_DEBUG_CATEGORY_OTHER_AMD:
        case GL_DEBUG_SOURCE_OTHER: {
            sourceString = "Other";
            break;
        }
        default: {
            sourceString = "Unknown";
            break;
        }
    }

    switch (type) {
        case GL_DEBUG_TYPE_ERROR: {
            typeString = "Error";
            break;
        }
        case GL_DEBUG_CATEGORY_DEPRECATION_AMD:
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: {
            typeString = "Deprecated Behavior";
            break;
        }
        case GL_DEBUG_CATEGORY_UNDEFINED_BEHAVIOR_AMD:
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: {
            typeString = "Undefined Behavior";
            break;
        }
        case GL_DEBUG_TYPE_PORTABILITY_ARB: {
            typeString = "Portability";
            break;
        }
        case GL_DEBUG_CATEGORY_PERFORMANCE_AMD:
        case GL_DEBUG_TYPE_PERFORMANCE: {
            typeString = "Performance";
            break;
        }
        case GL_DEBUG_CATEGORY_OTHER_AMD:
        case GL_DEBUG_TYPE_OTHER: {
            typeString = "Other";
            break;
        }
        default: {
            typeString = "Unknown";
            break;
        }
    }

    switch (severity) {
        case GL_DEBUG_SEVERITY_HIGH: {
            severityString = "High";
            break;
        }
        case GL_DEBUG_SEVERITY_MEDIUM: {
            severityString = "Medium";
            break;
        }
        case GL_DEBUG_SEVERITY_LOW: {
            severityString = "Low";
            break;
        }
        default: {
            severityString = "Unknown";
            break;
        }
    }

    stringStream << "OpenGL Error: " << msg;
    stringStream << " [Source = " << sourceString;
    stringStream << ", Type = " << typeString;
    stringStream << ", Severity = " << severityString;
    stringStream << ", ID = " << id << "]";

    return stringStream.str();
}

// version found on the net
//void APIENTRY openglCallbackFunction(GLenum source,
//                                           GLenum type,
//                                           GLuint id,
//                                           GLenum severity,
//                                           GLsizei length,
//                                           const GLchar* message,
//                                           void* userParam){

//    if(severity == GL_DEBUG_SEVERITY_NOTIFICATION)
//        return;
//    cout << "---------------------opengl-callback-start------------" << endl;
//    cout << "message: "<< message << endl;
//    cout << "type: ";
//    switch (type) {
//    case GL_DEBUG_TYPE_ERROR:
//        cout << "ERROR";
//        break;
//    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
//        cout << "DEPRECATED_BEHAVIOR";
//        break;
//    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
//        cout << "UNDEFINED_BEHAVIOR";
//        break;
//    case GL_DEBUG_TYPE_PORTABILITY:
//        cout << "PORTABILITY";
//        break;
//    case GL_DEBUG_TYPE_PERFORMANCE:
//        cout << "PERFORMANCE";
//        break;
//    case GL_DEBUG_TYPE_OTHER:
//        cout << "OTHER";
//        break;
//    }
//    cout << endl;

//    cout << "id: " << id << endl;
//    cout << "severity: ";
//    switch (severity){
//    case GL_DEBUG_SEVERITY_LOW:
//        cout << "LOW";
//        break;
//    case GL_DEBUG_SEVERITY_MEDIUM:
//        cout << "MEDIUM";
//        break;
//    case GL_DEBUG_SEVERITY_HIGH:
//        cout << "HIGH";
//        break;
//    case  GL_DEBUG_SEVERITY_NOTIFICATION:
//        cout << "NOTIFICATION";
//    }
//    cout << endl;
//    cout << "---------------------opengl-callback-end--------------" << endl;
//}



static void addOpenGlDebugCallback() {
    //    PFNGLDEBUGMESSAGECALLBACKPROC _glDebugMessageCallback = (PFNGLDEBUGMESSAGECALLBACKPROC) wglGetProcAddress("glDebugMessageCallback");
    //    PFNGLDEBUGMESSAGECALLBACKARBPROC _glDebugMessageCallbackARB = (PFNGLDEBUGMESSAGECALLBACKARBPROC) wglGetProcAddress("glDebugMessageCallbackARB");
    //    PFNGLDEBUGMESSAGECALLBACKAMDPROC _glDebugMessageCallbackAMD = (PFNGLDEBUGMESSAGECALLBACKAMDPROC) wglGetProcAddress("glDebugMessageCallbackAMD");

    //    // Register your callback function.
    //    if (_glDebugMessageCallback != NULL) {
    //        _glDebugMessageCallback(DebugCallback, NULL);
    //    }
    //    else if (_glDebugMessageCallbackARB != NULL) {
    //        _glDebugMessageCallbackARB(DebugCallback, NULL);
    //    }
    //    else if (_glDebugMessageCallbackAMD != NULL) {
    //        _glDebugMessageCallbackAMD(DebugCallbackAMD, NULL);
    //    }

    //    // Enable synchronous callback. This ensures that your callback function is called
    //    // right after an error has occurred. This capability is not defined in the AMD
    //    // version.
    //    if ((_glDebugMessageCallback != NULL) || (_glDebugMessageCallbackARB != NULL)) {
    //        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    //    }
        if(glDebugMessageCallback) {
            std::cout << "Register OpenGL debug callback " << std::endl;
            glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
            glDebugMessageCallback(openGlDebugCallback, 0);
            GLuint unusedIds = 0;
            glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, &unusedIds, true);
        }
        else
            std::cout << "glDebugMessageCallback not available" << std::endl;
}

#endif // OPENGLDEBUG_H
