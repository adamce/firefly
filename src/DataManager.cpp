#include "DataManager.h"

#include <cmath>

DataManager* DataManager::singletonPointer = 0;

DataManager::DataManager()
{
}

DataManager* DataManager::instance() {
    if(singletonPointer==0)
        singletonPointer = new DataManager();
    return singletonPointer;
}

int DataManager::addMaterialDescription(const MaterialDescription &d)
{
    for(int i=0; i<m_materialDescriptions.size(); i++) {
        MaterialDescription c = m_materialDescriptions[i];
        if(c.type == d.type &&
                std::fabs(c.factor - d.factor) < 0.001f &&
                c.diffuseTexture.textureArrayNo == d.diffuseTexture.textureArrayNo &&
                c.diffuseTexture.textureIndex == d.diffuseTexture.textureIndex &&
                c.normalMapTexture.textureArrayNo == d.normalMapTexture.textureArrayNo &&
                c.normalMapTexture.textureIndex == d.normalMapTexture.textureIndex &&
                c.diffuseRGBA == d.diffuseRGBA) {
            return i;
        }
    }

    int position = (int) m_materialDescriptions.size();
	m_materialDescriptions.push_back(d);
    return position;
}
