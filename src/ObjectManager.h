#ifndef OBJECTMANAGER_H
#define OBJECTMANAGER_H

#include <set>
#include <vector>
#include <unordered_set>

class SpaceDust;
class Object;
class Ship;
class Camera;
class Light;
class Skybox;
class LensFlare;
class Application;
class PhysicalObject;

class ObjectManager
{
    friend class Application;
public:
    ObjectManager();
    ~ObjectManager();

    void update(float time);
    void drawObject(Object* object, Camera* camera);
    void drawGeometryObjects(Camera* camera);
    void drawParticleSystems(Camera* camera);
    void drawSkyBox(Camera* camera);
    void drawScreenQuad();
    Object* sun() const;
    const std::unordered_set<Object*>& objects() const { return m_objects; }
    const std::vector<Object*>& geometryObjects() const { return m_geometryObjects; }
    const std::vector<Light*>& lights() const { return m_lights; }

    static ObjectManager* instance();

protected:
    void internalDrawObject(Object* object, Camera* camera);
    bool viewFrustrumTest(Object* o, Camera* camera) const;

private:
    // game objects
    std::unordered_set<Object*> m_objects;
    std::vector<Object*> m_geometryObjects;
    std::unordered_set<Object*> m_particleSystems;
    std::vector<Light*> m_lights;

    Ship* m_ship1 = nullptr;
    PhysicalObject* m_testMesh = nullptr;
    Object* m_sun = nullptr;

    //objects used for debugging
    Light* m_sunlight = nullptr;

    Skybox* m_skybox = nullptr;
    SpaceDust* m_spaceDust = nullptr;
};

#endif // OBJECTMANAGER_H
