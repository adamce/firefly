#include "ShadowMapPass.h"

#include "Application.h"
#include "Framebuffer.h"
#include "ObjectManager.h"
#include "shaders/ShaderManager.h"
#include "shaders/Shader.h"
#include "objects/Light.h"
#include "auxiliary/ApplicationConstants.h"
#include "objects/Camera.h"

ShadowMapPass::ShadowMapPass(Light* light) : m_light(light)
{
}

ShadowMapPass::~ShadowMapPass()
{
    delete m_shadowMap;
}

void ShadowMapPass::init()
{
    FramebufferTexture* colour = new FramebufferTexture(GL_TEXTURE_2D, GL_COLOR_ATTACHMENT0, FramebufferTexture::RG32);
    FramebufferTexture* depth = new FramebufferTexture(GL_TEXTURE_2D, GL_DEPTH_ATTACHMENT, FramebufferTexture::DepthBuffer);
    m_shadowMap = new Framebuffer(colour, depth);
    m_shadowMap->resize(VIENNA_SHADOW_MAP_SIZE, VIENNA_SHADOW_MAP_SIZE);
}

FramebufferTexture *ShadowMapPass::depthBuffer() const
{
    return m_shadowMap->getTex0();
}

void ShadowMapPass::internalRender()
{
    m_shadowMap->clear(Framebuffer::Attach0 | Framebuffer::DepthAttachment);
    m_shadowMap->bindForDrawing();

    Shader* shader = ShaderManager::instance()->activateShader(ShaderManager::ShadowMap);
    shader->setCamera(m_light->lightCamera());
    ObjectManager::instance()->drawGeometryObjects(m_light->lightCamera());
}
