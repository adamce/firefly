#ifndef BULLETDEBUGDRAWPASS_H
#define BULLETDEBUGDRAWPASS_H

#include "RenderPass.h"

class DebugDrawPass : public RenderPass
{
public:
    DebugDrawPass(Framebuffer* resultFbo);

    virtual void init();
protected:
    virtual void internalRender();

    Framebuffer* m_resultFramebuffer;
};

#endif // BULLETDEBUGDRAWPASS_H
