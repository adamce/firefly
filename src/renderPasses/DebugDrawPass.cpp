#include "DebugDrawPass.h"

#include <BulletDynamics/btBulletDynamicsCommon.h>

#include "Application.h"
#include "ObjectManager.h"
#include "Framebuffer.h"
#include "shaders/Shader.h"
#include "objects/Camera.h"
#include "shaders/ShaderManager.h"

DebugDrawPass::DebugDrawPass(Framebuffer *resultFbo) : m_resultFramebuffer(resultFbo)
{
}

void DebugDrawPass::init()
{
    setEnabled(false);
}

void DebugDrawPass::internalRender()
{
    m_resultFramebuffer->bindForDrawing();
    Shader* shader = ShaderManager::instance()->activateShader(ShaderManager::Default);
    //draw boundingSpheres for frustum culling debug

//    for(Object* object : ObjectManager::instance()->objects())
//     {
//         if(object->mesh() != 0){
//             if(object->mesh()->hasBoundingSphere())
//             {
//                 Object* bSphere = new Object(LowPolySphereMesh);
//                 float radius = object->mesh()->boundingSphereRadius();
//                 bSphere->translate(glm::vec3(object->modelMatrix()*glm::vec4(object->mesh()->boundingSphereCenter(),1.f)));
//                 bSphere->scale(radius, radius, radius);
//                 ObjectManager::instance()->drawObject(bSphere, Application::instance()->camera());
//                 delete bSphere;
//             }
//         }
//     }

    //draw sun for lense flare debug
    glDepthMask(GL_FALSE);
    ObjectManager::instance()->drawObject(ObjectManager::instance()->sun(), Application::instance()->camera());
    glDepthMask(GL_TRUE);

    shader->setCamera(Application::instance()->camera());
    shader->setModelViewProjectionMatrix(glm::mat4(1), Application::instance()->camera()->viewProjectionMatrix());
    Application::instance()->dynamicsWorld()->debugDrawWorld();
}
