#include "ParticleSystemsPass.h"

#include <GL/glew.h>
#include <GL/gl.h>

#include "Application.h"
#include "ObjectManager.h"
#include "Framebuffer.h"
#include "shaders/ShaderManager.h"
#include "shaders/Shader.h"

ParticleSystemsPass::ParticleSystemsPass(Framebuffer* resultFBO) : m_resultFramebuffer(resultFBO)
{
}

void ParticleSystemsPass::init()
{

}

void ParticleSystemsPass::screenResize(int width, int height)
{
}

void ParticleSystemsPass::internalRender()
{
    Shader* shader = ShaderManager::instance()->activateShader(ShaderManager::SpaceDust);
    m_resultFramebuffer->bindForDrawing(nullptr, shader);

    //TODO: dust should be drawn and saved only near the camera. it should be "moved" in the graphics card. not realy move, but instead remove old far away points and add new ones.
    glEnable(GL_BLEND);
    //disable depth write, so that the space dust has correct depth testing, but invisible parts of it doesn't write into the depth buffer and therefore cover other dust particles.
    glDepthMask(false);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    ObjectManager::instance()->drawParticleSystems(Application::instance()->camera());

    glDepthMask(true);
    glDisable(GL_BLEND);
}
