#ifndef POSTPROCESSPASS_H
#define POSTPROCESSPASS_H
#include "RenderPass.h"

#define VIENNA_BLUR_SIZE_FACTOR  0.5f

class LensFlare;
class FramebufferTexture;

class PostProcessPass : public RenderPass
{
public:
    PostProcessPass(Framebuffer *result);
    virtual ~PostProcessPass();
    virtual void init();
    virtual void screenResize(int width, int height);
    virtual void applicationInitFinished();

protected:
    virtual void internalRender();
    bool doSunVisibilityTestAndCalculations();
    void drawLensflare();
    void drawBloom();
    void drawGlare();

private:
    float m_sunGlareFactor = 0.f;
    float m_sunVisibility = 0.f;
    LensFlare* m_lensFlare;
    unsigned int m_lensFlareOcclusionQueryName[2];

    Framebuffer* m_resultFramebuffer;

    Framebuffer* m_postProcessFbo;
    Framebuffer* m_postProcessBlur1Fbo;
    Framebuffer* m_postProcessBlur2Fbo;

    FramebufferTexture* m_postProcessBlur1Texture;
    FramebufferTexture* m_postProcessBlur2Texture;
    FramebufferTexture* m_postProcessTexture;
};

#endif // POSTPROCESSPASS_H
