#ifndef RENDERPASS_H
#define RENDERPASS_H

#include <set>
#include <memory>

class Framebuffer;
class Application;

class RenderPass
{
public:
    RenderPass();
    virtual ~RenderPass();
    virtual void init() = 0;
    void render();

    void setEnabled(bool value) { m_enabled = value; }
    bool enabled() const { return m_enabled; }

    virtual void screenResize(int width, int height) {}
    virtual void applicationInitFinished() {}

protected:
    virtual void internalRender() = 0;
    Application* application() const;

private:
    bool m_enabled = true;

};

#endif // RENDERPASS_H
