#include "PostProcessPass.h"

#include <GL/glew.h>
#include <GL/gl.h>

#define GLM_FORCE_RADIANS
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/matrix_access.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "Application.h"
#include "ObjectManager.h"
#include "Renderer.h"
#include "shaders/Shader.h"
#include "Framebuffer.h"
#include "objects/LensFlare.h"
#include "shaders/ShaderManager.h"

PostProcessPass::PostProcessPass(Framebuffer* resultFBO) : m_resultFramebuffer(resultFBO)
{
}

PostProcessPass::~PostProcessPass()
{
    delete m_lensFlare;
    delete m_postProcessFbo;
    delete m_postProcessBlur1Fbo;
    delete m_postProcessBlur2Fbo;
}

void PostProcessPass::init()
{
    m_lensFlare = new LensFlare(glm::vec4(1, 1, 1, 0.5));


    // ## setting up frame buffers etc for post processing
    m_postProcessTexture = new FramebufferTexture(GL_TEXTURE_RECTANGLE);
    m_postProcessBlur1Texture = new FramebufferTexture(GL_TEXTURE_RECTANGLE);
    m_postProcessBlur2Texture = new FramebufferTexture(GL_TEXTURE_2D);

    m_postProcessFbo = new Framebuffer(m_postProcessTexture);
    m_postProcessBlur1Fbo = new Framebuffer(m_postProcessBlur1Texture);    // use a separate depthbuffer, so that it can be copied. this is needed for drawing the limes in the smaller buffer
    m_postProcessBlur2Fbo = new Framebuffer(m_postProcessBlur2Texture);
}

void PostProcessPass::screenResize(int w, int h)
{
    m_postProcessFbo->resize(w, h);
    m_postProcessBlur1Fbo->resize((int) (w*VIENNA_BLUR_SIZE_FACTOR), (int) (h*VIENNA_BLUR_SIZE_FACTOR));
    m_postProcessBlur2Fbo->resize((int) (w*VIENNA_BLUR_SIZE_FACTOR), (int) (h*VIENNA_BLUR_SIZE_FACTOR));
}

void PostProcessPass::applicationInitFinished()
{
    m_lensFlare->setSun(ObjectManager::instance()->sun());
}

bool PostProcessPass::doSunVisibilityTestAndCalculations()
{
    glm::mat4 viewProjectionMatrix = application()->camera()->viewProjectionMatrix();

    GLuint queryId;
    glGenQueries(1,&queryId);

    //disable writing to depthbuffer
    glDepthMask(GL_FALSE);

    //disable writing to renderbuffer
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

    //start query
    glBeginQuery(GL_SAMPLES_PASSED, queryId);

    //draw sun
    m_lensFlare->getSun()->activateMeshForDrawing();
    m_lensFlare->getSun()->draw(viewProjectionMatrix);

    //end query
    glEndQuery(GL_SAMPLES_PASSED);

    GLint result;
    glGetQueryObjectiv(queryId, GL_QUERY_RESULT, &result);

    //enable writing to renderbuffer
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

    //enable writing to depthbuffer
    glDepthMask(GL_TRUE);
    glDeleteQueries(1,&queryId);

    if(result==0) {
        m_sunGlareFactor=0;
        m_sunVisibility=0;
        return false;
    }

    m_sunVisibility = result/6200.0f;
    if(m_sunVisibility>1)
        m_sunVisibility=1;

    m_lensFlare->setCamera(application()->camera());
    m_lensFlare->calculateLensflare();

    m_sunGlareFactor = m_lensFlare->glareFactor();
    m_sunGlareFactor *= m_sunVisibility;
    return true;
}


void PostProcessPass::internalRender()
{
    bool isSunVisible = doSunVisibilityTestAndCalculations();
    if(isSunVisible) {
        drawLensflare();
    }
    drawBloom();
    if(isSunVisible) {
        drawGlare();
    }
}

void PostProcessPass::drawBloom()
{
    // copy big rendered image
    m_resultFramebuffer->copyTo(m_postProcessFbo, Framebuffer::Attach0);

    // render small image for blur
    m_postProcessFbo->copyTo(m_postProcessBlur1Fbo, Framebuffer::Attach0);

    // initialise shader and mesh for the next two steps
    Shader* shader = Application::instance()->shaderManager()->activateShader(ShaderManager::PostProcessBloom);

    // blur the small image
    shader->setColorMapRectSampler(m_postProcessBlur1Texture->textureId());
    shader->setMode(3);
    m_postProcessBlur2Fbo->bindForDrawing(application()->camera(), shader);

    //first pass
    ObjectManager::instance()->drawScreenQuad();

    //copy back data
    //unnecessary, but quicker to implement
    m_postProcessBlur2Fbo->copyTo(m_postProcessBlur1Fbo, Framebuffer::Attach0);
    m_postProcessBlur2Fbo->bindForDrawing();
    Framebuffer::unbindFromReading();

    //second pass
    shader->setMode(4);
    ObjectManager::instance()->drawScreenQuad();

    // blend the small blured image and the raw image together
    m_resultFramebuffer->bindForDrawing(application()->camera(), shader);

    shader->setMode(2);
    shader->setColorMapSampler(m_postProcessBlur2Texture->textureId());
    glGenerateMipmap(GL_TEXTURE_2D);
    shader->setColorMapRectSampler(m_postProcessTexture->textureId());

    ObjectManager::instance()->drawScreenQuad();
}

void PostProcessPass::drawGlare()
{
    // initialise shader and mesh for the next two steps
    Shader* shader = Application::instance()->shaderManager()->activateShader(ShaderManager::PostProcessBloom);

    // blend the glare and the raw image together
    m_resultFramebuffer->copyTo(m_postProcessFbo, Framebuffer::Attach0);
    m_resultFramebuffer->bindForDrawing(application()->camera(), shader);


    shader->setMode(1);
    shader->setTime(m_sunGlareFactor*0.9f);
    shader->setColorMapRectSampler(m_postProcessTexture->textureId());

    ObjectManager::instance()->drawScreenQuad();
}

void PostProcessPass::drawLensflare()
{
    ShaderManager* shaderManager = Application::instance()->shaderManager();

    Shader* shader = shaderManager->activateShader(ShaderManager::LensFlareShader);
    glm::vec4 color = m_lensFlare->getColor();

    shader->setFlareColor(glm::vec4(color.r, color.g, color.b, color.a * m_sunVisibility));
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    glDisable(GL_DEPTH_TEST);

    application()->drawWithDefaultCamera(m_lensFlare);

    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
}

