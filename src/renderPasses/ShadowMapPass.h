#ifndef LIGHTMAPPASS_H
#define LIGHTMAPPASS_H

#include "RenderPass.h"


class FramebufferTexture;
class Light;

class ShadowMapPass : public RenderPass
{
public:
    ShadowMapPass(Light *light);
    ~ShadowMapPass();
    virtual void init();
    FramebufferTexture* depthBuffer() const;
    const Light& light() const { return *m_light; }

protected:
    virtual void internalRender();
    Light* m_light = nullptr;
    Framebuffer* m_shadowMap = nullptr;
};

#endif // LIGHTMAPPASS_H
