#ifndef MAINPASS_H
#define MAINPASS_H

#include "RenderPass.h"
#include <vector>

class Framebuffer;
class Cuda;
class ShadowMapPass;

class MainRenderPass : public RenderPass
{
public:
    MainRenderPass(Framebuffer* resultFbo, const std::vector<ShadowMapPass*>* shadowMapPasses);
    ~MainRenderPass();
    virtual void init();
    virtual void screenResize(int width, int height);
    virtual void applicationInitFinished();
    Cuda*  cudaManager() const { return m_cuda; }
protected:
    void internalRender();

private:
    Framebuffer* m_geometryFramebuffer = nullptr;
    Framebuffer* m_resultFramebuffer = nullptr;
    const std::vector<ShadowMapPass*>* m_shadowMapPasses;
    int m_lastShadowMapCount = 0;
    Cuda* m_cuda = nullptr;
};

#endif // MAINPASS_H
