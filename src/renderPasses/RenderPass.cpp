#include "RenderPass.h"

#include "Framebuffer.h"
#include "Application.h"

RenderPass::RenderPass()
{
}

RenderPass::~RenderPass()
{}

void RenderPass::render()
{
    if(m_enabled) {
        internalRender();
    }
}

Application* RenderPass::application() const
{
    return Application::instance();
}
