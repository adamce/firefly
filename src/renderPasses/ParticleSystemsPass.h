#ifndef PARTICLESYSTEMPASS_H
#define PARTICLESYSTEMPASS_H

#include "RenderPass.h"

class ParticleSystemsPass : public RenderPass
{
public:
    ParticleSystemsPass(Framebuffer *resultFBO);

    virtual void init();
    virtual void screenResize(int width, int height);
protected:
    virtual void internalRender();

private:
    Framebuffer* m_resultFramebuffer;

};

#endif // PARTICLESYSTEMPASS_H
