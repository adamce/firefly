#include "MainRenderPass.h"

#include "Application.h"
#include "ObjectManager.h"
#include "Framebuffer.h"
#include "objects/Camera.h"
#include "cuda/Cuda.h"
#include "shaders/ShaderManager.h"
#include "shaders/Shader.h"
#include "shaders/DeferredGeometryShader.h"
#include "shaders/DeferredSkyBoxShader.h"

MainRenderPass::MainRenderPass(Framebuffer *resultFbo, const std::vector<ShadowMapPass *> *shadowMapPasses) : m_resultFramebuffer(resultFbo), m_shadowMapPasses(shadowMapPasses)
{
}

MainRenderPass::~MainRenderPass()
{
    delete m_cuda;
}

void MainRenderPass::init()
{
    m_cuda = new Cuda();

    FramebufferTexture* texture0 = new FramebufferTexture(GL_TEXTURE_2D, GL_COLOR_ATTACHMENT0, FramebufferTexture::RGBA32UI);
    FramebufferTexture* texture1 = 0;
//    FramebufferTexture* texture1 = new FramebufferTexture(GL_TEXTURE_2D, GL_COLOR_ATTACHMENT1, FramebufferTexture::RGBA32UI);
    m_geometryFramebuffer = new Framebuffer(texture0, texture1, m_resultFramebuffer->getDepthTex());

    m_cuda->initFramebuffers(m_geometryFramebuffer, m_resultFramebuffer);
}

void MainRenderPass::screenResize(int w, int h)
{
    m_cuda->deInitFramebuffers();
    m_resultFramebuffer->resize(w, h);
    m_geometryFramebuffer->resize(w, h);
    m_cuda->initFramebuffers(m_geometryFramebuffer, m_resultFramebuffer);
    m_cuda->resize(w, h);
}

void MainRenderPass::applicationInitFinished()
{
    DeferredGeometryShader* deferredGeomShader = dynamic_cast<DeferredGeometryShader*>(ShaderManager::instance()->activateShader(ShaderManager::DeferredGeometryPass));
    deferredGeomShader->loadMaterials();

    m_cuda->initConstData();
    if(m_lastShadowMapCount != m_shadowMapPasses->size()) {
        m_lastShadowMapCount = (int) m_shadowMapPasses->size();
        m_cuda->updateShadowMaps(m_shadowMapPasses);
    }
}

void MainRenderPass::internalRender()
{
#ifdef VIENNA_DEBUG
    auto begin = std::chrono::high_resolution_clock::now();
#endif

    m_geometryFramebuffer->clear(Framebuffer::Attach0 | Framebuffer::Attach1 | Framebuffer::DepthAttachment);
    m_geometryFramebuffer->bindForDrawing();

    Shader* shader;
    //draw skybox
    shader = ShaderManager::instance()->activateShader(ShaderManager::DeferredSkybox);
    shader->setCamera(Application::instance()->camera());
    ObjectManager::instance()->drawSkyBox(Application::instance()->camera());

#ifdef VIENNA_DEBUG
    glFinish();
    auto interim = std::chrono::high_resolution_clock::now();
    Application::instance()->addTiming("2.1 init + skybox", std::chrono::duration_cast<VIENNA_TIMING_PRECISION>(interim - begin));
    begin = interim;
#endif

    shader = ShaderManager::instance()->activateShader(ShaderManager::DeferredGeometryPass);
    shader->setCamera(Application::instance()->camera());
    ObjectManager::instance()->drawGeometryObjects(Application::instance()->camera());

#ifdef VIENNA_DEBUG
    glFinish();
    interim = std::chrono::high_resolution_clock::now();
    Application::instance()->addTiming("2.2 geometry pass", std::chrono::duration_cast<VIENNA_TIMING_PRECISION>(interim - begin));
    begin = interim;
#endif

    m_cuda->deferredRender();

#ifdef VIENNA_DEBUG
    interim = std::chrono::high_resolution_clock::now();
    Application::instance()->addTiming("2.3 cuda shading pass", std::chrono::duration_cast<VIENNA_TIMING_PRECISION>(interim - begin));
    begin = interim;
#endif
}
