#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H

#include <GL/glew.h>
#include <string>
#include <set>
#include <map>

#define GLM_FORCE_RADIANS
#include "glm/glm.hpp"

#define VIENNA_RENDERER_GL_SHADER_SOURCE(CODE) #CODE

class Camera;
struct MeshVertex;

namespace VertexAttribute {
    enum Value {
        TextureCoordinate = 1,
        Normal = 4,
        Position = 8,
        MaterialIndex = 16,
        All = TextureCoordinate | Normal | Position | MaterialIndex
    };
};
namespace Uniform {
    enum Value {
        ModelMatrix = 1,
        NormalMatrix = 2,
        ModelViewProjectionMatrix = 4,
        ColorMap = 16,
        ColorMapS = 32,
        ColorMapRect = 64,
        CubeMap = 128,
        FramebufferWidth = 256,
        FramebufferHeight = 512,
        FramebufferSize = FramebufferWidth|FramebufferHeight,
        Time = 1024,
        RandomNumber = 2048,
        ModeSwitch = 4096,
        AllMatrix = ModelMatrix | NormalMatrix | ModelViewProjectionMatrix,
        All = AllMatrix | ColorMap | ColorMapS | ColorMapRect | CubeMap | FramebufferSize | Time | RandomNumber | ModeSwitch
    };
};

namespace Capability {
    enum Value {
        TransformFeedback = 1,
        GeometryShader = 2,
        IsFlare = 4
    };
};

class Shader
{
    static const unsigned int VERTEX_POSITION_ID = 0;
    static const unsigned int VERTEX_NORMAL_ID = 1;
    static const unsigned int VERTEX_TEXCOORD_ID = 2;
    static const unsigned int VERTEX_MATERIALID_ID = 3;
	static const unsigned int FLARE_POSITION_ID = 0;
	static const unsigned int FLARE_SIZE_ID = 1;
    static const unsigned int FLARE_TYPE_ID = 2;
    static const unsigned int FLARE_COLOR_ID = 3;

    // the size of the light array in the glsl shader, must be kept in sync with shader.frag etc.
    static const unsigned int LIGHTS_ARRAY_SIZE = 4;

public:
    static const unsigned int NUM_GENERATOR_VERTICES_IN_PARTICLE_SYSTEM = 10;
    static const unsigned int NUM_VERTICES_IN_PARTICLE_SYSTEM = 1000;
    const float TIME_BETWEEN_GEOMETRY_SHADER_VERTEX_GENERATE;

public:
    Shader(const std::string& name, bool readShaderSourceFiles = true);
    virtual ~Shader();

    void assemble();
    void use();

    GLuint id() const;

    void setModelViewProjectionMatrix(const glm::mat4& modelMatrix, const glm::mat4& viewProjectionMatrix);
    void setCamera(const Camera* camera);
	void setCubeMapSampler(const GLint &textureHandle);
	void setColorMapSampler(const GLint &textureHandle);
    void setColorMapRectSampler(const GLint &textureHandle);
    void setFramebufferSize(int width, int height);
    void setTime(float time);
    void setMode(int modeSwitch);
	void setFlareColor(const glm::vec4 &color);
    void setColorMapSamplers(const std::set<GLuint> handles);

    void setUniforms(unsigned int uniformBitMask);
    void setVertexAttributes(unsigned int vertexAttributeBitMask);
    void setCapabilities(unsigned int capabilitiyBitMask);

	static void setupFlareAttribPointers();
    static void setupVertexAttribPointers();
    static void setupVertexAttribPointers(MeshVertex* location);
	void enableVertexAttribArrays();
	void disableVertexAttribArrays();

protected:
    void writeConstantIntUniform(const std::string& uniformName, int value);

    bool isUsed(VertexAttribute::Value vertexAttribute) const;
    bool isUsed(Uniform::Value uniform) const;
    bool isUsed(Capability::Value capability) const;

    void setNormalMatrix(const glm::mat3& normalMatrix);
	void setModelMatrixUniform(const glm::mat4& modelMatrix);
	void setMVPUniform(const glm::mat4& mvpMatrix);
    void setParticelGeneratorValues();
    void updateRandomNumber();

    std::string readFile(const std::string& shaderFileName) const;
    int createPipelineShader(GLenum type, const std::string& sourceCode, const std::string name) const;

    void retrieveUniformLocations();
    void bindAttribLocation();
    void bindFeedbackVaryings();
    void link();

    std::string basePath() const { return "../data/shader/"; }

    std::string m_vertexShaderSource;
    std::string m_fragmentShaderSource;
    std::string m_geometryShaderSource;
    void initPackingOps();
    std::string m_packingOps;

private:
    GLuint m_programId;
    std::string m_programName;

    unsigned int m_vertexAttributes;
    unsigned int m_uniforms;
    unsigned int m_capabilities;

    // uniform ids won't change after linking, so we can retrieve them once and save some time
    // note that glget calls are stalling the pipeline.
    int m_modelMatrixUniformId;
    int m_modelViewProjectionMatrixUniformId;
    int m_normalMatrixUniformId;
    int m_cameraPositionUniformId;

    int m_shipPositionUniformId;
    int m_shipFlightVectorUniformId;

    int m_lightCountUniformId;
    int m_lightPositionsUniformId;
    int m_diffuseLightColorsUniformId;
    int m_lightAttenuationConstansUniformId;
    int m_ambientLightColorsUniformId;
    int m_specularLightColorsUniformId;

    int m_widthUniformId;
    int m_heightUniformId;

    int m_timeUniformId;
    int m_timeBetweenGeneratingVerticesUniformId;
    int m_particleOldAgeUniformId;
    int m_randomNumberUniformId;
    int m_modeSwitchUniformId;

    int m_cubeMapUniformId;
    int m_colorMapUniformId;
    int m_colorMapRectUniformId;
    int m_flareColorUniformId;

    std::map<std::string, GLuint> m_colorMapUniformIdMap;

    bool m_firstStart;
};

#endif // SHADERPROGRAM_H
