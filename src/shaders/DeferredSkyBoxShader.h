#ifndef DEFERREDSKYBOXSHADER_H
#define DEFERREDSKYBOXSHADER_H

#include "Shader.h"

class DeferredSkyBoxShader : public Shader
{
public:
    DeferredSkyBoxShader();
private:
    std::string vertexShaderSource() const;
    std::string fragmentShaderSource() const;
};

#endif // DEFERREDSKYBOXSHADER_H
