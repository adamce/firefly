#include "DeferredSkyBoxShader.h"

DeferredSkyBoxShader::DeferredSkyBoxShader() : Shader("DeferredSkyBoxShader", false)
{
    m_vertexShaderSource = vertexShaderSource();
    m_fragmentShaderSource = fragmentShaderSource();
}


std::string DeferredSkyBoxShader::vertexShaderSource() const
{
    std::string source = "#version 150\n";
    source += VIENNA_RENDERER_GL_SHADER_SOURCE(
        in vec4 vertexPosition;
        uniform mat4 modelViewProjectionMatrix;

        //cube map texture coordinates for the fragment shader
        out vec3 vVaryingTexCoords;

        void main(void)
        {
            vVaryingTexCoords = normalize(vertexPosition.xyz);
            gl_Position = modelViewProjectionMatrix * vertexPosition;
        }
    );
    return source;
}

std::string DeferredSkyBoxShader::fragmentShaderSource() const
{

    std::string source = "#version 440 core\n";
    source += m_packingOps;
    source += readFile("DeferredSkyBoxShader.frag");
    return source;
}

