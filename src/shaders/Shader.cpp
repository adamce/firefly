#include "Shader.h"
#include "meshes/TextureManager.h"

#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <string>
#include <sstream>
#include <fstream>

#define GLM_FORCE_RADIANS
#include "glm/gtc/type_ptr.hpp"

#include "objects/Light.h"
#include "objects/Camera.h"
#include "meshes/MeshVertex.h"
#include "meshes/FlareVertex.h"
#include "auxiliary/randomNumber.h"



#define BUFFER_OFFSET(i) ((char *)NULL + (i))

void printShaderInfoLog(GLint shader);

Shader::Shader(const std::string& name, bool readShaderSourceFiles) : TIME_BETWEEN_GEOMETRY_SHADER_VERTEX_GENERATE(0.05f), m_programId(0)
{
    initPackingOps();

    m_programName = name;
    if(readShaderSourceFiles) {
        m_vertexShaderSource = readFile(m_programName+".vert");
        m_fragmentShaderSource = readFile(m_programName+".frag");
        m_geometryShaderSource = readFile(m_programName+".geom");
    }

    m_vertexAttributes=0;
    m_uniforms=0;
    m_capabilities=0;

    m_firstStart=true;
}

Shader::~Shader()
{
	//TODO: clean up
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    glUseProgram(0);
    glDeleteProgram(id());
    m_programId=0;
}

void Shader::assemble()
{
    m_programId = glCreateProgram();
    if(m_programId ==0) {
        std::cerr << "Failed to create ShaderProgram";
    }

    int vertexShaderId = createPipelineShader(GL_VERTEX_SHADER, m_vertexShaderSource, m_programName + ".vertex");
    int fragmentShaderId= createPipelineShader(GL_FRAGMENT_SHADER, m_fragmentShaderSource, m_programName + ".fragment");
    glAttachShader(m_programId, vertexShaderId);
    glAttachShader(m_programId, fragmentShaderId);

    int geometryShaderId;
    if(isUsed(Capability::GeometryShader)) {
        geometryShaderId = createPipelineShader(GL_GEOMETRY_SHADER, m_geometryShaderSource, m_programName + ".geometry");
        glAttachShader(m_programId, geometryShaderId);
    }

    bindAttribLocation();
    bindFeedbackVaryings();

    link();

    glDeleteShader(vertexShaderId);
    glDeleteShader(fragmentShaderId);
    if(isUsed(Capability::GeometryShader))
        glDeleteShader(geometryShaderId);

    retrieveUniformLocations();
}

GLuint Shader::id() const
{
    if(m_programId==0) {
        std::cerr << "Shader::id: Trying to retrieve id before the program was assembled. This can mean no good, so I'll better die.." << std::endl;
        exit(1);
    }
    return m_programId;
}

void Shader::setModelViewProjectionMatrix(const glm::mat4& modelMatrix, const glm::mat4& viewProjectionMatrix)
{
	glm::mat4 mvpMatrix = viewProjectionMatrix*modelMatrix;
	if(isUsed(Uniform::ModelMatrix))
		setModelMatrixUniform(modelMatrix);
    if(isUsed(Uniform::ModelViewProjectionMatrix))
		setMVPUniform(mvpMatrix);
    if(isUsed(Uniform::NormalMatrix))
        setNormalMatrix(glm::mat3(modelMatrix));
}

void Shader::setModelMatrixUniform(const glm::mat4& modelMatrix)
{
    if(m_modelMatrixUniformId==-1) {
        //std::cerr << "Shader::setModelViewProjectionMatrix->glGetUniformLocation(): wrong uniform name: modelMatrix" << std::endl;
        //exit(1);
    }
	else
	{
		glUniformMatrix4fv(m_modelMatrixUniformId, 1, GL_FALSE, glm::value_ptr(modelMatrix));
	}
}

void Shader::setMVPUniform(const glm::mat4& mvpMatrix)
{
    if(m_modelViewProjectionMatrixUniformId==-1) {
        //std::cerr << "setNormalMatrix->glGetUniformLocation(): wrong uniform name: modelViewProjectionMatrix" << std::endl;
        //exit(1);
    }
	else
	{
		glUniformMatrix4fv(m_modelViewProjectionMatrixUniformId, 1, GL_FALSE, glm::value_ptr(mvpMatrix));
	}
}

void Shader::setNormalMatrix(const glm::mat3& normalMatrix)
{
    if(m_normalMatrixUniformId==-1) {
        //std::cerr << "Shader::setNormalMatrix->glGetUniformLocation(): wrong uniform name: normalMatrix" << std::endl;
        //exit(1);
    }
	else
	{
    glUniformMatrix3fv(m_normalMatrixUniformId, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	}

}

void Shader::setParticelGeneratorValues()
{
    if (m_timeBetweenGeneratingVerticesUniformId != -1) {
       glUniform1f(m_timeBetweenGeneratingVerticesUniformId, TIME_BETWEEN_GEOMETRY_SHADER_VERTEX_GENERATE);
    }
    if (m_particleOldAgeUniformId != -1) {
        float maxCount = NUM_VERTICES_IN_PARTICLE_SYSTEM;
        float outPerIteration = NUM_GENERATOR_VERTICES_IN_PARTICLE_SYSTEM;
        float timeBetweenIteration = TIME_BETWEEN_GEOMETRY_SHADER_VERTEX_GENERATE;
        float oldAge = maxCount/(outPerIteration/timeBetweenIteration) - 2*timeBetweenIteration;
        glUniform1f(m_particleOldAgeUniformId, oldAge);
    }
}

void Shader::updateRandomNumber()
{
    if(isUsed(Uniform::RandomNumber) && m_randomNumberUniformId!=-1) {
        glUniform4f(m_randomNumberUniformId, randomNumber(0.0, 1.0), randomNumber(0.0, 1.0), randomNumber(0.0, 1.0), randomNumber(0.0, 1.0));
    }
}

std::string Shader::readFile(const std::string &shaderFileName) const
{
    std::ifstream shaderFile(basePath() + shaderFileName);
    if(shaderFile.is_open()) {
        std::stringstream stringStream;
        std::string line;
        while(getline(shaderFile, line)) {
          stringStream << line << '\n';
        }
        shaderFile.close();
        return stringStream.str();
    }
    else {
        std::cerr << "Shader failed to read file " << shaderFileName << std::endl;
        return "";
    }
}

int Shader::createPipelineShader(GLenum type, const std::string &sourceCode, const std::string name) const
{
    if(sourceCode.length() < 5) // 5 would cover all boms, newlines and weiss der herrgott was..
        return -1;

    int shaderId = glCreateShader(type);

    const char* cstringCode = sourceCode.c_str();
    glShaderSource(shaderId, 1, (const GLchar**) &cstringCode, 0);

    GLint compiled;
    glCompileShader(shaderId);
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &compiled);
    if (!compiled)
    {
        std::cerr << "Shader failed to compile (" << name << ")" << std::endl;
        printShaderInfoLog(shaderId);
    }

    return shaderId;
}

void Shader::setCamera(const Camera* camera)
{
    if(m_cameraPositionUniformId==-1) {
        //std::cerr << "Shader::setCamera->glGetUniformLocation(): wrong uniform name: cameraPosition" << std::endl;
        //exit(1);
    }
	else
	{
		glUniform3fv(m_cameraPositionUniformId, 1, glm::value_ptr(camera->position()));
	}
}

void Shader::setFramebufferSize (int width, int height)
{
    if(m_widthUniformId!=-1) {
        glUniform1i(m_widthUniformId, width);
    }

    if(m_heightUniformId!=-1) {
        glUniform1i(m_heightUniformId, height);
    }
}

void Shader::setMode(int modeSwitch)
{
    if(m_modeSwitchUniformId!=-1)
    {
        glUniform1i(m_modeSwitchUniformId, modeSwitch);
    }
}

void Shader::link()
{
    glLinkProgram(id());

	GLint testVal;
	glGetProgramiv(id(), GL_LINK_STATUS, &testVal);
    if(testVal == GL_FALSE)
	{
        std::cerr << "Shader::link: Linking Shader failed for shader '" << m_programName << "'" << std::endl;
		char* infoLog = new char[1000];
		GLsizei length = 0;
		glGetProgramInfoLog(id(), 1000, &length, infoLog);
        std::cerr << infoLog << std::endl;

		delete[] infoLog;
		glDeleteProgram(id());
        exit(1);
    }
}

void Shader::initPackingOps()
{
    m_packingOps = readFile("viennaPack.glsl");
}

void Shader::use()
{
	//clear errors
	if(m_firstStart)
        glGetError();

    glUseProgram(id());
	glBindFragDataLocation(id(), 0, "vFragColor");		//required since GLSL 150

    if(m_firstStart) {
    	switch(glGetError())
    	{
    	case GL_INVALID_VALUE:
    		std::cerr << "glUseProgram(): wrong shader program" << std::endl;
    		break;
    	case GL_INVALID_OPERATION:
    		std::cerr << "glUseProgram(): invalid operation" << std::endl;
    		break;
        }
    }
    m_firstStart=false;
    updateRandomNumber();
    setParticelGeneratorValues
();
}

void Shader::retrieveUniformLocations()
{
    m_modelMatrixUniformId = glGetUniformLocation(id(), "modelMatrix");
    m_modelViewProjectionMatrixUniformId = glGetUniformLocation(id(), "modelViewProjectionMatrix");
    m_normalMatrixUniformId = glGetUniformLocation(id(), "normalMatrix");
    m_cameraPositionUniformId = glGetUniformLocation(id(), "cameraPosition");

    m_shipPositionUniformId = glGetUniformLocation(id(), "shipPosition");
    m_shipFlightVectorUniformId = glGetUniformLocation(id(), "shipFlightVector");

    m_lightCountUniformId = glGetUniformLocation(id(), "lightCount");
    m_lightPositionsUniformId = glGetUniformLocation(id(), "lightPositions");
    m_diffuseLightColorsUniformId = glGetUniformLocation(id(), "diffuseLightColors");
    m_lightAttenuationConstansUniformId = glGetUniformLocation(id(), "lightAttenuationConstants");
    m_ambientLightColorsUniformId = glGetUniformLocation(id(), "ambientLightColors");
    m_specularLightColorsUniformId = glGetUniformLocation(id(), "specularLightColors");

    m_widthUniformId = glGetUniformLocation(id(), "framebufferWidth");
    m_heightUniformId = glGetUniformLocation(id(), "framebufferHeight");

    m_timeUniformId = glGetUniformLocation(id(), "time");
    m_timeBetweenGeneratingVerticesUniformId = glGetUniformLocation(id(), "timeBetweenGeneratingVertices");
    m_particleOldAgeUniformId = glGetUniformLocation(id(), "particleOldAge");
    m_randomNumberUniformId = glGetUniformLocation(id(), "randomNumber");
    m_modeSwitchUniformId = glGetUniformLocation(id(), "shaderModeSwitch");

    m_cubeMapUniformId = glGetUniformLocation(id(), "cubeMap");
    m_colorMapUniformId = glGetUniformLocation(id(), "colorMap");
    m_colorMapRectUniformId = glGetUniformLocation(id(), "colorMapRect");
    m_flareColorUniformId = glGetUniformLocation(id(), "flareColor");
}

void Shader::bindAttribLocation()
{
    if(isUsed(Capability::IsFlare)){
		glBindAttribLocation(id(), FLARE_POSITION_ID, "vertexPosition");
		glBindAttribLocation(id(), FLARE_SIZE_ID, "flareSize");
        glBindAttribLocation(id(), FLARE_TYPE_ID, "flareType");
        glBindAttribLocation(id(), FLARE_COLOR_ID, "flareColor");
		return;
	}
	if(isUsed(VertexAttribute::Position))
        glBindAttribLocation(id(), VERTEX_POSITION_ID, "vertexPosition");
    if(isUsed(VertexAttribute::Normal))
		glBindAttribLocation(id(), VERTEX_NORMAL_ID,   "vertexNormal");
    if(isUsed(VertexAttribute::TextureCoordinate))
		glBindAttribLocation(id(), VERTEX_TEXCOORD_ID,   "vertexTexCoords");
    if(isUsed(VertexAttribute::MaterialIndex))
        glBindAttribLocation(id(), VERTEX_MATERIALID_ID,   "vertexMaterialId");
}

void Shader::bindFeedbackVaryings()
{
    if(isUsed(Capability::TransformFeedback)) {
        static const char* varaying_names[] =
        {
            "transformFeedbackPosition",
            "transformFeedbackNormal",
            "transformFeedbackTextcoord",
            "transformFeedbackMaterialIndex",
            "padding"
        };
        glTransformFeedbackVaryings(id(), 5, varaying_names, GL_INTERLEAVED_ATTRIBS);
    }
}

void Shader::setupFlareAttribPointers()
{
	glVertexAttribPointer(FLARE_POSITION_ID,	3, GL_FLOAT, GL_FALSE, sizeof(FlareVertex), BUFFER_OFFSET(0));
	glVertexAttribPointer(FLARE_SIZE_ID,		1, GL_FLOAT, GL_FALSE, sizeof(FlareVertex), BUFFER_OFFSET(sizeof(GLfloat)*3));
    glVertexAttribIPointer(FLARE_TYPE_ID,		1, GL_INT,             sizeof(FlareVertex), BUFFER_OFFSET(sizeof(GLfloat)*4));
    glVertexAttribPointer(FLARE_COLOR_ID,       4, GL_FLOAT, GL_FALSE, sizeof(FlareVertex), BUFFER_OFFSET(sizeof(GLfloat)*4+sizeof(GL_INT)));
}

void Shader::setupVertexAttribPointers()
{
    glVertexAttribPointer(VERTEX_POSITION_ID,   4, GL_FLOAT,        GL_FALSE, sizeof(MeshVertex), BUFFER_OFFSET(sizeof(GLfloat)*0));
    glVertexAttribPointer(VERTEX_NORMAL_ID,     4, GL_FLOAT,        GL_FALSE, sizeof(MeshVertex), BUFFER_OFFSET(sizeof(GLfloat)*4));
    glVertexAttribPointer(VERTEX_TEXCOORD_ID,   2, GL_FLOAT,        GL_FALSE, sizeof(MeshVertex), BUFFER_OFFSET(sizeof(GLfloat)*8));
    glVertexAttribIPointer(VERTEX_MATERIALID_ID,1, GL_INT,                    sizeof(MeshVertex), BUFFER_OFFSET(sizeof(GLfloat)*10));
}

void Shader::setupVertexAttribPointers(MeshVertex* location)
{
    glVertexAttribPointer(VERTEX_POSITION_ID,   4, GL_FLOAT,        GL_FALSE, sizeof(MeshVertex), &(location->x));
    glVertexAttribPointer(VERTEX_NORMAL_ID,     4, GL_FLOAT,        GL_FALSE, sizeof(MeshVertex), &(location->nx));
    glVertexAttribPointer(VERTEX_TEXCOORD_ID,   2, GL_FLOAT,        GL_FALSE, sizeof(MeshVertex), &(location->s0));
    glVertexAttribPointer(VERTEX_MATERIALID_ID, 1, GL_INT,          GL_FALSE, sizeof(MeshVertex), &(location->materialIndex));
}

void Shader::enableVertexAttribArrays()
{
    if(isUsed(Capability::IsFlare)){
		glEnableVertexAttribArray(Shader::FLARE_POSITION_ID);
		glEnableVertexAttribArray(Shader::FLARE_SIZE_ID);
		glEnableVertexAttribArray(Shader::FLARE_TYPE_ID);
        glEnableVertexAttribArray(Shader::FLARE_COLOR_ID);
		return;
	}
	if(isUsed(VertexAttribute::Position))
        glEnableVertexAttribArray(Shader::VERTEX_POSITION_ID);
    if(isUsed(VertexAttribute::Normal))
		glEnableVertexAttribArray(Shader::VERTEX_NORMAL_ID);
    if(isUsed(VertexAttribute::TextureCoordinate))
        glEnableVertexAttribArray(Shader::VERTEX_TEXCOORD_ID);
    if(isUsed(VertexAttribute::MaterialIndex))
        glEnableVertexAttribArray(Shader::VERTEX_MATERIALID_ID);
}
void Shader::disableVertexAttribArrays()
{
	if(isUsed(Capability::IsFlare)){
		glDisableVertexAttribArray(Shader::FLARE_POSITION_ID);
		glDisableVertexAttribArray(Shader::FLARE_SIZE_ID);
        glDisableVertexAttribArray(Shader::FLARE_TYPE_ID);
        glDisableVertexAttribArray(Shader::FLARE_COLOR_ID);
		return;
	}
	if(isUsed(VertexAttribute::Position))
        glDisableVertexAttribArray(Shader::VERTEX_POSITION_ID);
    if(isUsed(VertexAttribute::Normal))
		glDisableVertexAttribArray(Shader::VERTEX_NORMAL_ID);
    if(isUsed(VertexAttribute::TextureCoordinate))
		glDisableVertexAttribArray(Shader::VERTEX_TEXCOORD_ID);
    if(isUsed(VertexAttribute::MaterialIndex))
        glDisableVertexAttribArray(Shader::VERTEX_MATERIALID_ID);
}

void Shader::writeConstantIntUniform(const std::string &uniformName, int value)
{   // untested
    GLint location = glGetProgramResourceLocation(id(), GL_UNIFORM, uniformName.c_str());
    glUniform1i(location, value);
}

// printShaderInfoLog
// From OpenGL Shading Language 3rd Edition, p215-216
// Display (hopefully) useful error messages if shader fails to compile
void printShaderInfoLog(GLint shader)
{
        int infoLogLen = 0;
        int charsWritten = 0;
        GLchar *infoLog;

        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLen);

        if (infoLogLen > 0)
        {
                infoLog = new GLchar[infoLogLen];
                // error check for fail to allocate memory omitted
                glGetShaderInfoLog(shader,infoLogLen, &charsWritten, infoLog);
                std::cerr << "Shader compile:" << std::endl << infoLog << std::endl;
                delete [] infoLog;
        }
}

void Shader::setCubeMapSampler(const GLint &textureHandle)
{
	if(isUsed(Uniform::CubeMap)){
		if(m_cubeMapUniformId!=-1) {
			//WARNING texture gets unbound in destructor (not any sooner)
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_CUBE_MAP, textureHandle);
			glUniform1i(m_cubeMapUniformId, 0);
		}
	}
}

void Shader::setColorMapSampler(const GLint &textureHandle)
{
	if(isUsed(Uniform::ColorMap)){
		if(m_colorMapUniformId!=-1) {
			//WARNING texture gets unbound in destructor (not any sooner)
// 			glGetError();

			if(isUsed(Uniform::CubeMap)){//check if this shader uses multiple texture samplers
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, textureHandle);
				glUniform1i(m_colorMapUniformId, 1);
			}
			else
			{
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, textureHandle);
				glUniform1i(m_colorMapUniformId, 0);
			}
// 			switch(glGetError()){
// 			case (GL_INVALID_ENUM):
// 				std::cout << "Shader::setCubeMapSampler: invalid Texture Unit" << std::endl;
// 			}
		}
	}
}

void Shader::setColorMapRectSampler(const GLint& textureHandle)
{
    if(isUsed(Uniform::ColorMapRect)){
        if(m_colorMapRectUniformId!=-1) {
            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_RECTANGLE, textureHandle);
            glUniform1i(m_colorMapRectUniformId, 1);
        }
    }
}

void Shader::setTime(float time)
{
    if(isUsed(Uniform::Time) && m_timeUniformId!=-1) {
        glUniform1f(m_timeUniformId, time);
    }
}

void Shader::setUniforms(unsigned int uniformBitMask)
{
    m_uniforms = uniformBitMask;
}

void Shader::setVertexAttributes(unsigned int vertexAttributeBitMask)
{
    m_vertexAttributes = vertexAttributeBitMask;
}

void Shader::setCapabilities(unsigned int capabilitiyBitMask)
{
    m_capabilities = capabilitiyBitMask;
}

bool Shader::isUsed(VertexAttribute::Value vertexAttribute) const
{
    unsigned int attr = vertexAttribute;
    return (m_vertexAttributes&attr)==attr;
}

bool Shader::isUsed(Uniform::Value uniform) const
{
    unsigned int attr = uniform;
    return (m_uniforms&attr)==attr;
}

bool Shader::isUsed(Capability::Value capability) const
{
    unsigned int attr = capability;
    return (m_capabilities&attr)==attr;
}

void Shader::setFlareColor(const glm::vec4 &color)
{
	if(isUsed(Capability::IsFlare)){
		if(m_flareColorUniformId==-1){
			//std::cerr << "wrong uniform name in Shader::setFlareColor -> flareColorId" << std::endl;
		}
		else
			glUniform4f(m_flareColorUniformId, (GLfloat)color.r, (GLfloat)color.g, (GLfloat)color.b, (GLfloat)color.a);
	}

}

void Shader::setColorMapSamplers(const std::set<GLuint> handles)
{
	if(isUsed(Uniform::ColorMapS)){
		GLint uniformId;
		int offset = 0;
		if(isUsed(Uniform::CubeMap))
			offset = 1;
		int counter = 0;
		std::string baseName = "colorMap";

		for(std::set<GLuint>::iterator iter = handles.begin() ; iter != handles.end() ; iter++){
			std::stringstream colorMapName;
			colorMapName << baseName << counter;
            if(m_colorMapUniformIdMap.count(colorMapName.str())) {
                uniformId = m_colorMapUniformIdMap.at(colorMapName.str());
            } else {
                uniformId = glGetUniformLocation(id(), colorMapName.str().c_str());
                m_colorMapUniformIdMap.insert(std::pair<std::string, GLuint>(colorMapName.str(), uniformId));
            }
			if(uniformId==-1) {
// 				std::cerr << "wrong uniform name: colorMap" << counter << std::endl;
			}
			else {

				glActiveTexture(GL_TEXTURE0 + counter + offset);
				glBindTexture(GL_TEXTURE_2D, (*iter));
				glUniform1i(uniformId, counter + offset);
			}
			counter++;
		}
	}
}
