#include "ShaderManager.h"
#include "Shader.h"
#include "Application.h"
#include "auxiliary/IngameGui.h"

#include "DeferredGeometryShader.h"
#include "DeferredSkyBoxShader.h"

ShaderManager::ShaderManager() : m_activeShader(NULL)
{
}


ShaderManager::~ShaderManager()
{
    std::map<ShaderName, const Shader*>::iterator iterator = m_shaders.begin();

    while(iterator != m_shaders.end()) {
        delete iterator->second;
        iterator++;
    }

}

Shader* ShaderManager::activateShader(ShaderName name)
{
    if(m_shaders.count(name)) {
        Shader* shaderProg = const_cast<Shader*>((*(m_shaders.find(name))).second);
        shaderProg->use();
        m_activeShader=shaderProg;
        return shaderProg;
    }

    Shader* shaderProg;
    switch (name) {
    case Default:
        shaderProg = new Shader("shader");
        shaderProg->setVertexAttributes(VertexAttribute::Position | VertexAttribute::Normal);
        shaderProg->setUniforms(Uniform::AllMatrix);
        break;
    case DeferredGeometryPass:
        shaderProg = new DeferredGeometryShader();
        shaderProg->setVertexAttributes(VertexAttribute::All);
        shaderProg->setUniforms(Uniform::AllMatrix | Uniform::ColorMapS | Uniform::CubeMap);
        break;
    case DeferredSkybox:
        shaderProg = new DeferredSkyBoxShader();
        shaderProg->setVertexAttributes(VertexAttribute::Position);
        shaderProg->setUniforms(Uniform::ModelViewProjectionMatrix | Uniform::CubeMap);
        break;
    case SpaceDust:
        shaderProg = new Shader("SpaceDustShader");
        shaderProg->setVertexAttributes(VertexAttribute::All);
        shaderProg->setUniforms(Uniform::AllMatrix | Uniform::ColorMapS | Uniform::FramebufferSize | Uniform::ModeSwitch | Uniform::RandomNumber | Uniform::Time);
        shaderProg->setCapabilities(Capability::TransformFeedback);
		break;
    case PostProcessBloom:
        shaderProg = new Shader("PostProcessBloom");
        shaderProg->setVertexAttributes(VertexAttribute::All);
        shaderProg->setUniforms(Uniform::ModelViewProjectionMatrix | Uniform::ColorMap | Uniform::ColorMapRect | Uniform::FramebufferSize | Uniform::Time | Uniform::ModeSwitch);
        break;
	case LensFlareShader:
        shaderProg = new Shader("LensFlareShader");
//         shaderProg->setVertexAttributes(VertexAttribute::All);
        shaderProg->setUniforms(Uniform::ModelViewProjectionMatrix | Uniform::ColorMapS);
        shaderProg->setCapabilities(Capability::IsFlare);
        break;
    case ShadowMap:
        shaderProg = new Shader("ShadowMap");
        shaderProg->setVertexAttributes(VertexAttribute::Position);
        shaderProg->setUniforms(Uniform::AllMatrix);
        break;
    }
    shaderProg->assemble();
    shaderProg->use();

    m_shaders.insert(std::pair<ShaderName, Shader*>(name, shaderProg));
	m_activeShader=shaderProg;
    return shaderProg;
}

void ShaderManager::reloadAllShaders()
{
    std::map<ShaderName, const Shader*>::iterator iterator = m_shaders.begin();

    while(iterator != m_shaders.end()) {
        delete iterator->second;
        iterator++;
    }

    m_shaders.clear();

    std::string message = "Reloaded all Shaders.";
    Application::instance()->ingameGui()->postMessage(message);
}

Shader* ShaderManager::activeShader() const
{
    return m_activeShader;
}

ShaderManager *ShaderManager::instance()
{
    return Application::instance()->shaderManager();
}
