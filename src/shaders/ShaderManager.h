#ifndef SHADERMANAGER_H
#define SHADERMANAGER_H

#include <map>
//#include "Shader.h"

class Shader;

class ShaderManager
{
public:
    enum ShaderName {
        Default,
        DeferredGeometryPass,
        DeferredSkybox,
        SpaceDust,
        LensFlareShader,
        PostProcessBloom,
        ShadowMap
    };

public:
    ShaderManager();
    ~ShaderManager();

    Shader* activateShader(ShaderName name);
	Shader* activeShader() const;
    static ShaderManager* instance();

    void reloadAllShaders();
    std::map<ShaderName, const Shader*> m_shaders;

private:
	Shader* m_activeShader;
};

#endif // SHADERMANAGER_H
