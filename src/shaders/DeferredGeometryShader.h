#ifndef DEFERREDGEOMETRYSHADER_H
#define DEFERREDGEOMETRYSHADER_H

#include "Shader.h"

class DeferredGeometryShader : public Shader
{
public:
    DeferredGeometryShader();
    void loadMaterials();
private:
    std::string vertexShaderSource() const;
    std::string fragmentShaderSource() const;
    GLuint m_materialUniformBufferId;
};

#endif // DEFERREDGEOMETRYSHADER_H
