#include "DeferredGeometryShader.h"

#include <GL/glew.h>
#include <GL/gl.h>

#include <vector>
#include <iostream>
#include <string>

#include "DataManager.h"

DeferredGeometryShader::DeferredGeometryShader() : Shader("DeferredGeometryShader", false)
{
    m_vertexShaderSource = vertexShaderSource();
    m_fragmentShaderSource = fragmentShaderSource();
}

struct UniformInfo {
    GLint type;
    GLint size;
    GLint blockIndex;
    GLint offset;
    GLint arrayStride;
};

void DeferredGeometryShader::loadMaterials()
{
    // uniformNames and materialAddressOffsets must be in the same order
    std::vector<std::string> uniformNames;
    uniformNames.push_back("Materials.diffuseTextures.textureIndex");
    uniformNames.push_back("Materials.diffuseTextures.width");
    uniformNames.push_back("Materials.diffuseTextures.height");
    uniformNames.push_back("Materials.diffuseTextures.textureArrayNo");

    uniformNames.push_back("Materials.normalMapTextures.textureIndex");
    uniformNames.push_back("Materials.normalMapTextures.width");
    uniformNames.push_back("Materials.normalMapTextures.height");
    uniformNames.push_back("Materials.normalMapTextures.textureArrayNo");

    uniformNames.push_back("Materials.diffuseRGBA");
    uniformNames.push_back("Materials.phongExponents");
    uniformNames.push_back("Materials.types");

    std::vector<long long> materialAddressOffsets;
    {
        MaterialDescription model;
        unsigned int* base = reinterpret_cast<unsigned int*>(&model);
        materialAddressOffsets.push_back(&model.diffuseTexture.textureIndex - base);    // must be in same order as uniform names
        materialAddressOffsets.push_back(&model.diffuseTexture.width - base);
        materialAddressOffsets.push_back(&model.diffuseTexture.height - base);
        materialAddressOffsets.push_back(&model.diffuseTexture.textureArrayNo - base);

        materialAddressOffsets.push_back(&model.normalMapTexture.textureIndex - base);
        materialAddressOffsets.push_back(&model.normalMapTexture.width - base);
        materialAddressOffsets.push_back(&model.normalMapTexture.height - base);
        materialAddressOffsets.push_back(&model.normalMapTexture.textureArrayNo - base);

        materialAddressOffsets.push_back(&model.diffuseRGBA - base);
        materialAddressOffsets.push_back(((unsigned int*)&model.factor) - base);
        materialAddressOffsets.push_back(&model.type - base);
    }

    //std::cout << "sizeof unsigned int: " << sizeof(unsigned int) << std::endl;

    // uniform info
    // not the most performant solution, but it's called only in the init phase, so it doesn't matter.
    std::vector<UniformInfo> uniformInfos;
    for (int i=0; i<uniformNames.size(); i++) {
        std::string uniformName = uniformNames.at(i);
        GLuint index = glGetProgramResourceIndex(id(), GL_UNIFORM, uniformName.c_str());
        UniformInfo info;
        glGetActiveUniformsiv(id(), 1, &index, GL_UNIFORM_TYPE, &info.type);
        glGetActiveUniformsiv(id(), 1, &index, GL_UNIFORM_SIZE, &info.size);
        glGetActiveUniformsiv(id(), 1, &index, GL_UNIFORM_BLOCK_INDEX, &info.blockIndex);
        glGetActiveUniformsiv(id(), 1, &index, GL_UNIFORM_OFFSET, &info.offset);
        glGetActiveUniformsiv(id(), 1, &index, GL_UNIFORM_ARRAY_STRIDE, &info.arrayStride);
        uniformInfos.push_back(info);
//        std::cout << "uniform " << i << " info: type=" << info.type << " size=" << info.size << " blockIndex=" << info.blockIndex << " offset=" << info.offset << " arrayStride=" << info.arrayStride << std::endl;
    }

    // block info
    GLint blockDataSize, blockBinding;
    glGetActiveUniformBlockiv(id(), uniformInfos.at(0).blockIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &blockDataSize);
    glGetActiveUniformBlockiv(id(), uniformInfos.at(0).blockIndex, GL_UNIFORM_BLOCK_BINDING, &blockBinding);
    std::cout << "uniform block data size=" << blockDataSize << " binding=" << blockBinding << std::endl;

    // setup data
    // copy the data from our materials to the data array using the special layout used by the shader
    std::vector<MaterialDescription> materials = DataManager::instance()->getMaterialDescriptions();
    unsigned int* data = new unsigned int[blockDataSize/sizeof(unsigned int)]; // blockDataSize in char
    for (int i=0; i<materials.size(); i++) {
        if(i > uniformInfos.at(0).size) {
            std::cerr << "DeferredGeometryShader::loadMaterials: not enough space in shader uniforms. increase array size in DeferredGeometryShader.frag" << std::endl;
            break;
        }
        MaterialDescription& mat = materials.at(i);
        unsigned int* offsetAddress = reinterpret_cast<unsigned int*>(&mat);

        for(int j=0; j<uniformInfos.size(); j++) {
            UniformInfo info = uniformInfos.at(j);
            long long currentUniformOffset = (info.offset + i * info.arrayStride) / 4;
            long long currentMatOffset = materialAddressOffsets.at(j);
            data[currentUniformOffset] = offsetAddress[currentMatOffset];
        }
    }


    // setup buffer
    glGenBuffers(1, &m_materialUniformBufferId);
    glBindBuffer(GL_UNIFORM_BUFFER, m_materialUniformBufferId);
    glBufferStorage(GL_UNIFORM_BUFFER, blockDataSize, data, GL_MAP_WRITE_BIT);
    glBindBufferBase(GL_UNIFORM_BUFFER, blockBinding, m_materialUniformBufferId);
    delete[] data;
}

std::string DeferredGeometryShader::vertexShaderSource() const
{
    std::string source = "#version 440 core\n";
    source += readFile("DeferredGeometryShader.vert");
    return source;
}

std::string DeferredGeometryShader::fragmentShaderSource() const
{
    std::string source = "#version 440 core\n";
    source += m_packingOps;
    source += readFile("DeferredGeometryShader.frag");
    return source;
}
