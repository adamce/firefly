/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Adam Celarek

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H
#include <GL/glew.h>
#include <vector>

class Shader;
class Camera;
class FramebufferTexture;

class Framebuffer
{
    friend class FramebufferTexture;
public:
    enum Channel {
        DepthAttachment = 1, Attach0 = 2, Attach1 = 4
    };

public:
    Framebuffer(FramebufferTexture* colorBuffer = 0, FramebufferTexture* depthBuffer = 0);
    Framebuffer(FramebufferTexture* colorBuffer0, FramebufferTexture* colorBuffer1, FramebufferTexture* depthBuffer);
    ~Framebuffer();
    void resize(int width, int height);
    int width() const { return m_width; }
    int height() const { return m_height; }
    void bindForDrawing(Camera* camera = 0, Shader* shader = 0);
    void bindForReading();
    void copyTo(Framebuffer* other, int channel);
    void clear(int channel);
    static void unbindFromDrawing();
    static void unbindFromReading();
    static Framebuffer* createGeometryBuffer();

    FramebufferTexture* getDepthTex() const { return m_texture[0]; }
    FramebufferTexture* getTex0() const { return m_texture[1]; }
    FramebufferTexture* getTex1() const { return m_texture[2]; }


private:
    void useTexture(FramebufferTexture* tex);

    int m_width;
    int m_height;
    GLuint m_framebufferId;
    FramebufferTexture* m_texture[3];  // for now up to two colour attachments + depthbuffer. [0] = depth, [1] colour0, [2] colour1
};

class FramebufferTexture {
    virtual ~FramebufferTexture();
public:
    enum Format {
        R32 = GL_R32F,
        RG32 = GL_RG32F,
        RGBA8 = GL_RGBA8,
        RGBA32UI = GL_RGBA32UI,
        RGBA32I = GL_RGBA32I,
        DepthBuffer = GL_DEPTH_COMPONENT32F
    };

    FramebufferTexture(GLenum type = GL_TEXTURE_2D, GLenum attachmentPoint = GL_COLOR_ATTACHMENT0, Format format = RGBA8, bool useMultisample = false);

    // resize is designed to only increase the size of the buffer. this is not always ideal, but framebuffer sizes will decrease only, if the window size decreases
    // and with small windows we shouldn't have performance problems. additionally this probably won't be the performance bottleneck. keeping track of other pixel buffers' sizes is much work.
    void resize(int width, int height);
    int width() const { return m_width; }
    int height() const { return m_height; }
    GLuint textureId() const { return m_texturebufferId; }
    GLuint target() const { return m_target; }

private:
    // these three are used for reference counting. addParent is called in a framebuffer's constructor and remove in its destructor. if parentCount reaches zero, the object gets deleted.
    void addParent(Framebuffer* parent) { m_parents.push_back(parent); }
    void removeParent(Framebuffer* parent);
    int parentCount() const { return (int) m_parents.size(); }
    bool isFirstParent(Framebuffer* parent) const { return parentCount() > 0 && m_parents.at(0) == parent; }

    int m_width;
    int m_height;
    std::vector<Framebuffer*> m_parents;
    friend class Framebuffer;


    void internalResize(int width, int height);
    void useInFramebuffer();


private:
    bool m_useMultisample;
    Format m_format;
    GLenum m_target;            // == type GL_TEXTURE_2D, GL_TEXTURE_RECTANGLE, GL_TEXTURE_CUBE_MAP_POSITIVE_X, ..
    GLenum m_internalFormat;    // GL_RGBA8, GL_RGBA16, GL_RGBA32I, GL_DEPTH_COMPONENT, ..
    GLenum m_attachmentPoint;   // GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_DEPTH_ATTACHMENT, ...
    GLuint m_texturebufferId;
};



#endif // FRAMEBUFFER_H
