/*
 *  Copyright (c) 2009-2011, NVIDIA Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of NVIDIA Corporation nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "Util.hpp"

using namespace FW;


//------------------------------------------------------------------------


glm::vec2 Intersect::RayBox(const AABB& box, const Ray& ray)
{
    const glm::vec3& orig = ray.origin;
    const glm::vec3& dir  = ray.direction;

    glm::vec3 t0 = (box.min() - orig) / dir;
    glm::vec3 t1 = (box.max() - orig) / dir;

    float tmin = max(glm::min(t0,t1));
    float tmax = min(glm::max(t0,t1));

    return glm::vec2(tmin,tmax);
}

//------------------------------------------------------------------------

glm::vec3 Intersect::RayTriangle(const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2, const Ray& ray)
{
//  const float EPSILON = 0.000001f; // breaks FairyForest
    const float EPSILON = 0.f; // works better
    const glm::vec3 miss(FW_F32_MAX,FW_F32_MAX,FW_F32_MAX);

    glm::vec3 edge1 = v1-v0;
    glm::vec3 edge2 = v2-v0;
    glm::vec3 pvec  = glm::cross(ray.direction, edge2);
    float det   = glm::dot(edge1, pvec);

    glm::vec3 tvec = ray.origin - v0;
    float u = glm::dot(tvec,pvec);

    glm::vec3 qvec = glm::cross(tvec, edge1);
    float v = glm::dot(ray.direction, qvec);

    // TODO: clear this
    if (det > EPSILON)
    {
        if (u < 0.0 || u > det)
            return miss;
        if (v < 0.0 || u + v > det)
            return miss;
    }
    else if(det < -EPSILON)
    {
        if (u > 0.0 || u < det)
            return miss;
        if (v > 0.0 || u + v < det)
            return miss;
    }
    else
        return miss;

    float inv_det = 1.f / det;
    float t = glm::dot(edge2, qvec) * inv_det;
    u *= inv_det;
    v *= inv_det;

    if(t>ray.tmin && t<ray.tmax)
        return glm::vec3(u,v,t);

    return miss;
}


//------------------------------------------------------------------------

glm::vec3 Intersect::RayTriangleWoop(const  glm::vec4& zpleq, const  glm::vec4& upleq, const  glm::vec4& vpleq, const Ray& ray)
{
    const glm::vec3 miss(FW_F32_MAX,FW_F32_MAX,FW_F32_MAX);

     glm::vec4 orig(ray.origin,1.f);
     glm::vec4 dir (ray.direction,0.f);

    float Oz   = glm::dot(zpleq,orig);           // NOTE: differs from HPG kernels!
    float ooDz = 1.f / glm::dot(dir,zpleq);
    float t = -Oz * ooDz;
    if (t>ray.tmin && t<ray.tmax)
    {
        float Ou = glm::dot(upleq,orig);
        float Du = glm::dot(upleq,dir);
        float u = Ou + t*Du;
        if (u >= 0)
        {
            float Ov = glm::dot(vpleq,orig);
            float Dv = glm::dot(vpleq,dir);
            float v = Ov + t*Dv;
            if (v >= 0 && (u+v) <= 1.f)
            {
                return glm::vec3(u,v,t);
            }
        }
    }

    return miss;
}

//------------------------------------------------------------------------
