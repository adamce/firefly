/**
 * Vienna Renderer - Firefly demo part for UT-Vienna
 * Entwurf und Programmierung einer Rendering-Engine cource
 * (http://cg.tuwien.ac.at/courses/RendEng/)
 * Copyright (C) 2014-2015 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HELSINKIHELPERCLASSES_H
#define HELSINKIHELPERCLASSES_H

#include <vector>
#include <memory>
#include <glm/glm.hpp>
#include "TriangleSoup.h"

#define MYINLINE inline

namespace Firefly {

class RTTriangle {
    int m_triangleIndex;
    const TriangleSoup* m_triangleSoup;
public:
    RTTriangle(int triangleIndex, const TriangleSoup* triangleSoup) : m_triangleIndex(triangleIndex), m_triangleSoup(triangleSoup) {}
    int triangleIndex() const { return m_triangleIndex; }
    glm::vec3 getVertex(int no) const {
        const glm::ivec3 vertexIndices = m_triangleSoup->triangles().at(m_triangleIndex);
        return m_triangleSoup->positions().at(vertexIndices[no]);
    }
};

struct RaycastResult {
    const RTTriangle* tri;
    float t;
    float u, v;
    glm::vec3 point;
    glm::vec3 orig, dir; // convenience for tracing and visualization

    RaycastResult(const RTTriangle* tri, float t, float u, float v,
        glm::vec3 point, const glm::vec3& orig, const glm::vec3& dir)
        : tri(tri), t(t), u(u), v(v), point(point), orig(orig), dir(dir) {}
    RaycastResult() : tri(nullptr), t(std::numeric_limits<float>::max()),
        u(), v(), point(), orig(), dir() {}

    inline operator bool() { return tri != nullptr; }
};

const int NUMBER_TRIANGLES_IN_BVH_LEAF = 8;

struct AABBTestResult {
    AABBTestResult() : tStart(-1), tEnd(-2) {}
    AABBTestResult(float tStart, float tEnd) : tStart(tStart), tEnd(tEnd) {}
    MYINLINE bool isHit() { return tStart <= tEnd && tEnd > 0; }
    float tStart;
    float tEnd;
};

struct AABB {
    AABB(const RTTriangle& triangle) {
        lower = min(triangle);
        higher = max(triangle);
    }
    AABB(const std::vector<RTTriangle>&, std::vector<RTTriangle>::iterator startPrim, std::vector<RTTriangle>::iterator endPrim) {
        lower = min(*startPrim);
        higher = max(*startPrim);
        startPrim++;

        while (startPrim < endPrim) {
            min(&lower, *startPrim);
            max(&higher, *startPrim);
            ++startPrim;
        };
    }
    glm::vec3 lower;
    glm::vec3 higher;

    int biggestDimension() {
        glm::vec3 diff = glm::abs(higher - lower);
        int dimension = -1;
        if (diff[0] > diff[1])
            dimension = 0;
        else dimension = 1;

        if (diff[dimension] > diff[2])
            return dimension;
        return 2;
    }

    float surfaceSize() {
        glm::vec3 diff = glm::abs(higher - lower);
        float x = diff.x;
        float y = diff.y;
        float z = diff.z;
        float surfaceSize = (x*y + x*z + y*z)*2.0f;
        return surfaceSize;
    }

    MYINLINE void testRayAgainstBox(AABBTestResult* result, const glm::vec3& orig, const glm::vec3& dir, const glm::vec3& oneOverDir, float minDist, float maxDist) const;

private:
    void min(glm::vec3* result, const glm::vec3& b) {
        if (b.x < result->x) result->x = b.x;
        if (b.y < result->y) result->y = b.y;
        if (b.z < result->z) result->z = b.z;
    }
    glm::vec3 min(const glm::vec3& a, const glm::vec3& b) {
        glm::vec3 newVector = a;
        if (b.x < a.x) newVector.x = b.x;
        if (b.y < a.y) newVector.y = b.y;
        if (b.z < a.z) newVector.z = b.z;
        return newVector;
    }
    glm::vec3 min(const RTTriangle& a) {
        return min(min(a.getVertex(0), a.getVertex(1)), a.getVertex(2));
    }
    void min(glm::vec3* result, const RTTriangle& a) {
        min(result, a.getVertex(0));
        min(result, a.getVertex(1));
        min(result, a.getVertex(2));
    }

    void max(glm::vec3* result, const glm::vec3& b) {
        if (b.x > result->x) result->x = b.x;
        if (b.y > result->y) result->y = b.y;
        if (b.z > result->z) result->z = b.z;
    }
    glm::vec3 max(const glm::vec3& a, const glm::vec3& b) {
        glm::vec3 newVector = a;
        if (b.x > a.x) newVector.x = b.x;
        if (b.y > a.y) newVector.y = b.y;
        if (b.z > a.z) newVector.z = b.z;
        return newVector;
    }
    glm::vec3 max(const RTTriangle& a) {
        return max(max(a.getVertex(0), a.getVertex(1)), a.getVertex(2));
    }
    void max(glm::vec3* result, const RTTriangle& a) {
        max(result, a.getVertex(0));
        max(result, a.getVertex(1));
        max(result, a.getVertex(2));
    }
};


struct Node
{
    AABB box; // Axis-aligned bounding box
    const std::vector<RTTriangle>::iterator startPrim, endPrim; // Indices in the global list
    int startPrimInt, endPrimInt;
    std::shared_ptr<Node> leftChild;
    std::shared_ptr<Node> rightChild;

    std::vector<std::shared_ptr<Node> > leaves;

    Node(std::vector<RTTriangle>& triangleList, const std::vector<RTTriangle>::iterator startPrim, const std::vector<RTTriangle>::iterator endPrim, int dimension = 0);
    Node(std::vector<RTTriangle>& triangleList, const std::vector<RTTriangle>::iterator triangleIt);
    Node(std::vector<RTTriangle>& triangleList, int start, int end);
//    RaycastResult getHitPoint(const glm::vec3& orig, const glm::vec3& dir, const glm::vec3& oneOverDir, float minDist, float maxDist) const;
private:
//    MYINLINE RaycastResult nodeIntersect(const glm::vec3& orig, const glm::vec3& dir, const glm::vec3& oneOverDir, float minDist, float maxDist) const;
};

namespace Firefly {
    inline std::shared_ptr<Node> make_unique(Node* newNode) {
        std::shared_ptr<Node> newUP(newNode);
        return newUP;
    }
}

}

#endif // HELSINKIHELPERCLASSES_H
