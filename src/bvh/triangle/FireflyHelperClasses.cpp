/**
 * Vienna Renderer - Firefly demo part for UT-Vienna
 * Entwurf und Programmierung einer Rendering-Engine cource
 * (http://cg.tuwien.ac.at/courses/RendEng/)
 * Copyright (C) 2014-2015 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "FireflyHelperClasses.h"
#include "auxiliary/ApplicationConstantsCuda.h"

using namespace Firefly;

inline float triangleMidpointX3(const RTTriangle& triangle, int dimension) {
    float sum = 0;
    sum += (triangle.getVertex(0))[dimension];
    sum += (triangle.getVertex(1))[dimension];
    sum += (triangle.getVertex(2))[dimension];
    return sum;
}

inline float triangleMidpoint(const RTTriangle& triangle, int dimension) {
    return triangleMidpointX3(triangle, dimension) / 3.0f;
}

Node::Node(std::vector<RTTriangle>& triangleList, const std::vector<RTTriangle>::iterator startPrim, const std::vector<RTTriangle>::iterator endPrim, int dimension)
    : box(triangleList, startPrim, endPrim), startPrim(startPrim), endPrim(endPrim) {
    int elementCount = (int) (endPrim - startPrim);
    startPrimInt = startPrim - triangleList.begin();
    endPrimInt = endPrim - triangleList.begin();

    if (elementCount <= NUMBER_TRIANGLES_IN_BVH_LEAF) {
        std::vector<RTTriangle>::iterator iter = startPrim;
        while (iter < endPrim) {
            leaves.push_back(Firefly::make_unique(new Node(triangleList, iter)));
            iter++;
        }
        return;
    }

    float l, r;
    std::vector<RTTriangle>::iterator startRightChild;

#ifdef VIENNA_BVH_MIDPOINT_DIVISION
    { // midpoint division
        dimension = (dimension + 1) % 3;
        float midpointX3 = 0;
        std::for_each(startPrim, endPrim, [&midpointX3, dimension](const RTTriangle& triangle) {midpointX3 += triangleMidpointX3(triangle, dimension); });
        midpointX3 /= (float)elementCount;

        startRightChild = std::partition(startPrim, endPrim, [dimension, midpointX3](const RTTriangle& a) {return triangleMidpointX3(a, dimension) < midpointX3; });
    }
#endif
#ifdef VIENNA_BVH_MAX_SPACE_BETWEEN_MIDPOINTS
    {	 //maximise space between midpoints, the fastests for now.
        int bestDim = (dimension + 1) % 3;
        float biggestSpace = 0;
        startRightChild = startPrim + (endPrim - startPrim) / 2;	// if there is no space, for instance because all triangles lie on one plane.

        for (int dimensionIter = 0; dimensionIter < 3; dimensionIter++) {
            if (dimension == dimensionIter)
                continue;
            std::sort(startPrim, endPrim, [dimensionIter](const RTTriangle& a, const RTTriangle& b) {return triangleMidpointX3(a, dimensionIter) < triangleMidpointX3(b, dimensionIter); });

            std::vector<RTTriangle>::iterator iter = startPrim+1;

            while (iter < endPrim - 1) {
//				int iterInt = iter - triangleList.begin();
                float currSpace = fabs(triangleMidpointX3(iter[0], dimensionIter) - triangleMidpointX3(iter[1], dimensionIter));
                if (currSpace > biggestSpace) {
                    biggestSpace = currSpace;
                    startRightChild = iter + 1;
                    bestDim = dimensionIter;
                }
                iter++;
            }
        }
        std::sort(startPrim, endPrim, [bestDim](const RTTriangle& a, const RTTriangle& b) {return triangleMidpointX3(a, bestDim) < triangleMidpointX3(b, bestDim); });
        dimension = bestDim;
    }
#endif
#ifdef VIENNA_BVH_MacDonaldBooth_EVAL_ON_64_INDICES
    {
        int bestDim = (dimension + 1) % 3;
        startRightChild = startPrim + (endPrim - startPrim) / 2;
        float smallestMacDonaldBoothValue = std::numeric_limits<float>::max();
        int stepsize = std::max((int) (endPrim - startPrim) / 64, 1);

        for (int dimensionIter = 0; dimensionIter < 3; dimensionIter++) {
            std::sort(startPrim, endPrim, [dimensionIter](const RTTriangle& a, const RTTriangle& b) {return triangleMidpointX3(a, dimensionIter) < triangleMidpointX3(b, dimensionIter); });

            std::vector<RTTriangle>::iterator iter = startPrim + stepsize;
            while (iter < endPrim) {
//				int iterInt = iter - triangleList.begin();

                AABB leftBox = AABB(triangleList, startPrim, iter);
                AABB rightBox = AABB(triangleList, iter, endPrim);
                float leftMacDonaldBoothValue = ((float)(iter - startPrim)) * leftBox.surfaceSize();
                float rightMacDonaldBoothValue = ((float)(endPrim - iter)) * rightBox.surfaceSize();

                float macDonaldBoothValue = leftMacDonaldBoothValue + rightMacDonaldBoothValue;

                if (macDonaldBoothValue < smallestMacDonaldBoothValue) {
                    smallestMacDonaldBoothValue = macDonaldBoothValue;
                    bestDim = dimensionIter;
                    startRightChild = iter;
                }
                iter += stepsize;
            }
        }

        std::sort(startPrim, endPrim, [bestDim](const RTTriangle& a, const RTTriangle& b) {return triangleMidpointX3(a, bestDim) < triangleMidpointX3(b, bestDim); });
        dimension = bestDim;
    }
#endif

    if (startRightChild - startPrim >= 1)
        leftChild = Firefly::make_unique(new Node(triangleList, startPrim, startRightChild, dimension));

    if (endPrim - startRightChild >= 1)
        rightChild = Firefly::make_unique(new Node(triangleList, startRightChild, endPrim, dimension));

}

Node::Node(std::vector<RTTriangle>& triangleList, const std::vector<RTTriangle>::iterator triangleIt) : box(*triangleIt), startPrim(triangleIt), endPrim(triangleIt + 1) {
    startPrimInt = startPrim - triangleList.begin();
    endPrimInt = endPrim - triangleList.begin();
}

Node::Node(std::vector<RTTriangle>& triangleList, int start, int end)
    : box(triangleList, triangleList.begin()+start, triangleList.begin()+end),
      startPrim(triangleList.begin()+start),
      endPrim(triangleList.begin()+end) {
    startPrimInt = startPrim - triangleList.begin();
    endPrimInt = endPrim - triangleList.begin();
}

//RaycastResult Node::getHitPoint(const glm::vec3& orig, const glm::vec3& dir, const glm::vec3& oneOverDir, float minDist, float maxDist) const {
//    //AABBTestResult thisAABBResult = testRayAgainstBox(orig, dir, minDist, maxDist);
//    //if (!thisAABBResult.isHit())
//    //	return RaycastResult();

//    if (endPrim - startPrim <= NUMBER_TRIANGLES_IN_BVH_LEAF)
//        return nodeIntersect(orig, dir, oneOverDir, minDist, maxDist);

//    AABBTestResult leftResult;
//    if(leftChild)
//        leftChild->box.testRayAgainstBox(&leftResult, orig, dir, oneOverDir, minDist, maxDist);
//    AABBTestResult rightResult;
//    if(rightChild)
//        rightChild->box.testRayAgainstBox(&rightResult, orig, dir, oneOverDir, minDist, maxDist);

//    if (leftResult.isHit() && rightResult.isHit()) {
//        AABBTestResult* closerResult = &rightResult;
//        AABBTestResult* furtherResult = &leftResult;
//        Node* closerChild = rightChild.get();
//        Node* furtherChild = leftChild.get();

//        if (leftResult.tStart < rightResult.tStart) {
//            closerResult = &leftResult;
//            furtherResult = &rightResult;
//            closerChild = leftChild.get();
//            furtherChild = rightChild.get();
//        }

//        RaycastResult closerChildResult = closerChild->getHitPoint(orig, dir, oneOverDir, minDist, maxDist);
//        if (closerChildResult.t > furtherResult->tStart) {
//            RaycastResult furtherChildResult = furtherChild->getHitPoint(orig, dir, oneOverDir, minDist, maxDist);
//            if (furtherChildResult.t < closerChildResult.t)
//                return furtherChildResult;
//        }
//        return closerChildResult;
//    }


//    // only one hit
//    if (leftResult.isHit()) {
//        return leftChild->getHitPoint(orig, dir, oneOverDir, minDist, maxDist);
//    }
//    if (rightResult.isHit()) {
//        return rightChild->getHitPoint(orig, dir, oneOverDir, minDist, maxDist);
//    }

//    // no hit
//    return RaycastResult();
//}

MYINLINE void AABB::testRayAgainstBox(AABBTestResult* result, const glm::vec3& orig, const glm::vec3& dir, const glm::vec3& oneOverDir, float minDist, float maxDist) const { // inline
    /*if (!(
        !(!(orig.x > lower.x && orig.x < higher.x) && dir.x < FW::App::epsilon && dir.x > -FW::App::epsilon) &&
        !(!(orig.y > lower.y && orig.y < higher.y) && dir.y < FW::App::epsilon && dir.y > -FW::App::epsilon) &&
        !(!(orig.z > lower.z && orig.z < higher.z) && dir.z < FW::App::epsilon && dir.z > -FW::App::epsilon)))
        return AABBTestResult();*/

    /* // just slow and doesn't give any benefit
    if ((fabs(dir.x) < FW::App::epsilon && !(orig.x > lower.x && orig.x < higher.x)) ||
        (fabs(dir.y) < FW::App::epsilon && !(orig.y > lower.y && orig.y < higher.y)) ||
        (fabs(dir.z) < FW::App::epsilon && !(orig.z > lower.z && orig.z < higher.z))) {
        return AABBTestResult();
    }*/


    //float tStart = minDist;
    //float tEnd = maxDist;

    glm::vec3 t1 = (lower - orig);
    glm::vec3 t2 = (higher - orig);
    t1 *= oneOverDir;
    t2 *= oneOverDir;

    glm::vec3 tmin, tmax;

    if (t1.x < t2.x) {
        tmin.x = t1.x; tmax.x = t2.x;
    }
    else {
        tmax.x = t1.x; tmin.x = t2.x;
    }
    result->tStart = tmin.x;
    result->tEnd = tmax.x;



    if (t1.y < t2.y) {
        tmin.y = t1.y; tmax.y = t2.y;
    }
    else {
        tmax.y = t1.y; tmin.y = t2.y;
    }
    if (tmin.y > result->tStart)
        result->tStart = tmin.y;
    if (tmax.y < result->tEnd)
        result->tEnd = tmax.y;

    //if (result->tStart > result->tEnd)
    //	return;

    if (t1.z < t2.z) {
        tmin.z = t1.z; tmax.z = t2.z;
    }
    else {
        tmax.z = t1.z; tmin.z = t2.z;
    }
    if (tmin.z > result->tStart)
        result->tStart = tmin.z;
    if (tmax.z < result->tEnd)
        result->tEnd = tmax.z;
}

//MYINLINE RaycastResult triangleIntersect(const RTTriangle& triangle, const glm::vec3& orig, const glm::vec3& dir, float minDist, float maxDist) {
//    float t, u, v;

//    if (intersect_triangle1(&orig.x,
//        &dir.x,
//        &triangle.getVertex(0).x,
//        &triangle.getVertex(0).x,
//        &triangle.getVertex(0).x,
//        t, u, v)) {
//        if (t > minDist && t < maxDist)
//            return RaycastResult(&triangle, t, u, v, orig + dir*t, orig, dir);
//    }

//    return RaycastResult();
//}

//MYINLINE RaycastResult Node::nodeIntersect(const glm::vec3& orig, const glm::vec3& dir, const glm::vec3& oneOverDir, float minDist, float maxDist) const {
//    float t, u, v;

//    /* * /
//    std::vector<std::unique_ptr<Node> >::const_iterator iter = leaves.begin();

//    while (iter < leaves.end()) {
//        if (iter->get()->box.testRayAgainstBox(orig, dir, oneOverDir, minDist, maxDist).isHit()) {
//            RaycastResult newResult = triangleIntersect(*(iter->get()->startPrim), orig, dir, minDist, maxDist);
//            if (newResult.t < smallestResult.t)
//                smallestResult = newResult;
//        }
//        iter++;
//    }
//    /* */
//    /* * /
//    std::vector<RTTriangle>::iterator iterator = startPrim;
//    while (iterator < endPrim) {
//        RaycastResult newResult = triangleIntersect(*iterator, orig, dir, minDist, maxDist);
//        if (newResult.t < smallestResult.t)
//            smallestResult = newResult;
//        iterator++;
//    }
//    /* */
//    /* */

//    float smallestT = std::numeric_limits<float>::max(), closestTriangleU, closestTriangleV;
//    std::vector<RTTriangle>::iterator closestTriangleIter = endPrim;
//    std::vector<RTTriangle>::iterator iterator = startPrim;
//    while (iterator < endPrim) {
//        if (intersect_triangle1(&orig.x,
//            &dir.x,
//            &iterator->m_vertices[0]->x,
//            &iterator->m_vertices[1]->x,
//            &iterator->m_vertices[2]->x,
//            t, u, v) && t < smallestT && t > minDist && t < maxDist) {
//                smallestT = t;
//                closestTriangleU = u;
//                closestTriangleV = v;
//                closestTriangleIter = iterator;
//            }
//        iterator++;
//    }

//    if (closestTriangleIter != endPrim)
//        return RaycastResult(&*closestTriangleIter, smallestT, closestTriangleU, closestTriangleV, orig + dir * smallestT, orig, dir);

//    /* */

//    return RaycastResult();
//}
