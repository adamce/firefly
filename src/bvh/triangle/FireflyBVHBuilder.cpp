/**
 * Vienna Renderer - Firefly demo part for UT-Vienna
 * Entwurf und Programmierung einer Rendering-Engine cource
 * (http://cg.tuwien.ac.at/courses/RendEng/)
 * Copyright (C) 2014-2015 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "FireflyBVHBuilder.h"

#include <memory>

#include "FireflyHelperClasses.h"


FireflyBVHBuilder::FireflyBVHBuilder(FW::BVH &bvh, const FW::BVH::BuildParams &params) : m_bvh(bvh), m_params(params)
{
}

FireflyBVHBuilder::~FireflyBVHBuilder()
{

}

Firefly::Node* findNextJunction(Firefly::Node* node) {
    if(node->leaves.size() > 0) return node;
    if(node->leftChild != nullptr && node->rightChild != nullptr) return node;
    if(node->leftChild == nullptr && node->rightChild == nullptr) {node->leftChild->leaves.size() / 0; return nullptr; }//crash
    if(node->leftChild != nullptr) return findNextJunction(node->leftChild.get());
    else return findNextJunction(node->rightChild.get());
}

FW::BVHNode* createNvidiaNode(Firefly::Node* helsinkiNode, FW::BVH &bvh, const std::vector<Firefly::RTTriangle>& triangleList) {
    helsinkiNode = findNextJunction(helsinkiNode);

    FW::AABB aabb;
    aabb.grow(helsinkiNode->box.lower);
    aabb.grow(helsinkiNode->box.higher);

    if(helsinkiNode->leaves.size() > 0) {
        //leaf node
        unsigned int low = bvh.getTriIndices().size();
        for(std::shared_ptr<Firefly::Node> node : helsinkiNode->leaves) {
            Firefly::RTTriangle tri = triangleList.at(node->startPrimInt);
            bvh.getTriIndices().push_back(tri.triangleIndex());
        }
        return new FW::LeafNode(aabb, low, bvh.getTriIndices().size());
    }
    else {
        //inner node
        FW::BVHNode* leftChild = createNvidiaNode(helsinkiNode->leftChild.get(), bvh, triangleList);
        FW::BVHNode* rightChild = createNvidiaNode(helsinkiNode->rightChild.get(), bvh, triangleList);

        return new FW::InnerNode(aabb, leftChild, rightChild);
    }
//    newNode = new FW::InnerNode()
}

FW::BVHNode *FireflyBVHBuilder::run()
{
    std::vector<Firefly::RTTriangle> triangleList;
    for(int i = 0; i < m_bvh.triangleSoup()->triangles().size(); i++) {
        triangleList.push_back(Firefly::RTTriangle(i, m_bvh.triangleSoup()));
    }
    Firefly::Node helperRootNode(triangleList, triangleList.begin(), triangleList.end());

    return createNvidiaNode(&helperRootNode, m_bvh, triangleList);
}
