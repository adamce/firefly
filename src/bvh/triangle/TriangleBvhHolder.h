#ifndef TRIANGLEBVHHOLDER_H
#define TRIANGLEBVHHOLDER_H

#include <glm/glm.hpp>
#include <bvh/Util.hpp>

class TriangleSoup;
class Object;
class SerialisedBVH;

class TriangleBvhHolder
{
public:
    TriangleBvhHolder(const TriangleSoup* triangleSoup, const Object* objectReference = nullptr);
    ~TriangleBvhHolder();
    void createBvh();
    void updateMatrices();
    FW::RayResult trace(const FW::Ray& ray) const;
    const TriangleSoup* triangleSoup() const { return m_triangleSoup; }
    glm::mat4 worldToStructMatrix() const { return m_cachedRayPosMatrix; }
    SerialisedBVH* serialisedBvh() const { return m_serialisedBvh; }

private:
    const TriangleSoup* m_triangleSoup = nullptr;
    SerialisedBVH* m_serialisedBvh = nullptr;
    const Object* m_objectReference = nullptr;

    glm::mat4 m_cachedModelMatrix;
    glm::mat3 m_cachedNormalMatrix;
    glm::mat4 m_cachedRayPosMatrix;
    glm::mat3 m_cachedRayDirMatrix;

};

#endif // TRIANGLEBVHHOLDER_H
