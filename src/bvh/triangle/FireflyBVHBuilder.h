/**
 * Vienna Renderer - Firefly demo part for UT-Vienna
 * Entwurf und Programmierung einer Rendering-Engine cource
 * (http://cg.tuwien.ac.at/courses/RendEng/)
 * Copyright (C) 2014-2015 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
 * This Builder is called Helsinki because it's based on a builder I made for a course in Aalto University/Helsinki.
 * I expect it to be slower than NVIDIA's SplitBVHBuilder.
 */

#ifndef HELSINKIBVHBUILDER_H
#define HELSINKIBVHBUILDER_H

#include "BVH.hpp"

class FireflyBVHBuilder
{
public:
    FireflyBVHBuilder(FW::BVH& bvh, const FW::BVH::BuildParams& params);
    ~FireflyBVHBuilder(void);

    FW::BVHNode* run(void);

private:
    FW::BVH& m_bvh;
    const FW::BVH::BuildParams &m_params;
};

#endif // HELSINKIBVHBUILDER_H
