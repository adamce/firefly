#include "TriangleBvhHolder.h"

#include <iostream>
#include <string>

#include "TriangleSoup.h"
#include "objects/Object.h"
#include "SerialisedBVH.h"
#include "BVH.hpp"

TriangleBvhHolder::TriangleBvhHolder(const TriangleSoup *triangleSoup, const Object *objectReference) : m_triangleSoup(triangleSoup), m_objectReference(objectReference)
{
}

TriangleBvhHolder::~TriangleBvhHolder()
{
    delete m_serialisedBvh;
    delete m_triangleSoup;
}

void TriangleBvhHolder::createBvh()
{
    m_serialisedBvh = new SerialisedBVH(m_triangleSoup);
}

void TriangleBvhHolder::updateMatrices()
{
    if(m_objectReference == nullptr) {
        m_cachedModelMatrix = glm::mat4();
        m_cachedNormalMatrix = glm::mat3();
        m_cachedRayPosMatrix = glm::mat4();
        m_cachedRayDirMatrix = glm::mat3();
        return;
    }

    m_cachedModelMatrix = m_objectReference->modelMatrix();
    m_cachedNormalMatrix = glm::mat3(m_cachedModelMatrix);
    m_cachedRayPosMatrix = glm::inverse(m_cachedModelMatrix);
    m_cachedRayDirMatrix = glm::mat3(m_cachedRayPosMatrix);
}

FW::RayResult TriangleBvhHolder::trace(const FW::Ray &ray) const
{
    if(m_serialisedBvh->bvh() == nullptr) {
        std::cout << std::string("TriangleBvhHolder::trace(): WARNING: Serialised BVH was read from file, ") +
                     "therefore we have no main memory BVH and we can't debug trace. Delete the cache files and restart!" << std::endl;
        return FW::RayResult();
    }

    FW::Ray transformedRay;
    transformedRay.origin = glm::vec3(m_cachedRayPosMatrix * glm::vec4(ray.origin, 1.f));
    transformedRay.direction = m_cachedRayDirMatrix * ray.direction;
    transformedRay.tmax = ray.tmax;
    transformedRay.tmin = ray.tmin;

    FW_ASSERT(m_serialisedBvh->bvh() != nullptr);

    FW::RayResult result = m_serialisedBvh->bvh()->trace(transformedRay);

    if(result.hit()) {
        result.pos = ray.origin + result.t * ray.direction;

        const glm::ivec3& triangle = triangleSoup()->triangles()[result.id];
        const glm::vec3 n3 = triangleSoup()->normals()[triangle.x];
        const glm::vec3 n1 = triangleSoup()->normals()[triangle.y];
        const glm::vec3 n2 = triangleSoup()->normals()[triangle.z];

        float w = 1.f - result.u - result.v;
        result.normal = n1 * result.u + n2 * result.v + n3 * w;
        result.normal = m_cachedNormalMatrix * result.normal;
    }

    return result;
}
