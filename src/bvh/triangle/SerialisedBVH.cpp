/*
 * Copyright (c) 2014 Adam Celarek
 * Copyright (c) 2009-2011, NVIDIA Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * * Neither the name of NVIDIA Corporation nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "SerialisedBVH.h"

#include <iostream>
#include <fstream>
#include <string>

#include "auxiliary/HelperOperations.h"
#include "BVH.hpp"
#include "TriangleSoup.h"
#include "bvh/nv/Defs.hpp"
#include "cuda/Cuda.h"

SerialisedBVH::SerialisedBVH(const TriangleSoup *soup)
{
    m_bvh = new FW::BVH(soup);
    m_layout = Cuda::bvhLayout();
    init();
}

void SerialisedBVH::rebuildWithOffsets(unsigned int triIndexOffset, unsigned int vertexIndexOffset)
{
    if(m_bvh->root() == nullptr) return;
    m_triIndexOffset = triIndexOffset;
    m_vertexIndexOffset = vertexIndexOffset;

    init(false);

//    std::string filename(std::to_string((long long) hash()) + ".dat");
//    std::ofstream fileOut(filename, std::ios::binary);
//    if(fileOut.is_open()) {
//        serialise(fileOut);
//        fileOut.close();
//    }
}

void SerialisedBVH::init(bool rebuildBVH)
{
    FW_ASSERT(m_layout >= 0 && m_layout < BVHLayout_Max);

    std::string filename(std::to_string((long long) hash()) + ".dat");
    std::ifstream fileIn(filename, std::ios::binary);
    if(fileIn.is_open()) {
        deSerialise(fileIn);
        return;
    }

    if(rebuildBVH)
        m_bvh->build();

// those two layouts are unsupported currently
//    if (m_layout == BVHLayout_Compact)
//    {
//        createCompact(*m_bvh, 1);
//        return;
//    }

//    if (m_layout == BVHLayout_Compact2)
//    {
//        createCompact(*m_bvh, 16);
//        return;
//    }

    createNodeBasic(*m_bvh);
    createTriWoopBasic(*m_bvh);
    createTriIndexBasic(*m_bvh);
}

//------------------------------------------------------------------------
void readStreamToArray(std::ifstream& in, std::vector<FW::U8>& array) {
    unsigned int size;
    read(in, size);
    array.resize(size);
    array.shrink_to_fit();
	for (unsigned int i = 0; i < size; i++) {
		array[i] = in.get();
	}
	//in.read((char*) array.data(), size);
}

void SerialisedBVH::deSerialise(std::ifstream& in)
{
    read(in, (FW::S32&)m_layout);
    readStreamToArray(in, m_nodes);
    readStreamToArray(in, m_triWoop);
    readStreamToArray(in, m_triIndex);
}

//------------------------------------------------------------------------

void writeArrayToStream(std::ofstream& out, const std::vector<FW::U8>& array) {
    write(out, (unsigned int) array.size());
    out.write((char*) array.data(), array.size());
}

void SerialisedBVH::serialise(std::ofstream& out)
{
    write(out, (FW::S32)m_layout);
    writeArrayToStream(out, m_nodes);
    writeArrayToStream(out, m_triWoop);
    writeArrayToStream(out, m_triIndex);
}

FW::U32 SerialisedBVH::hash() const
{
    return FW::hashBits(
            m_bvh->triangleSoup()->hash(),
            m_bvh->platform().computeHash(),
            m_bvh->buildParams().computeHash(),
            m_layout);
}

//------------------------------------------------------------------------

glm::ivec2 SerialisedBVH::getNodeSubArray(int idx) const
{
    FW_ASSERT(idx >= 0 && idx < 4);
    FW::S32 size = (FW::S32)m_nodes.size();

    return glm::ivec2(0, size);
}

//------------------------------------------------------------------------

glm::ivec2 SerialisedBVH::getTriWoopSubArray(int idx) const
{
    FW_ASSERT(idx >= 0 && idx < 4);
    FW::S32 size = (FW::S32)m_triWoop.size();

    return glm::ivec2(0, size);
}

//------------------------------------------------------------------------

void SerialisedBVH::createNodeBasic(const FW::BVH& bvh)
{
    struct StackEntry
    {
        const FW::BVHNode* node;
        FW::S32 idx;

        StackEntry(const FW::BVHNode* n = nullptr, int i = 0) : node(n), idx(i) {}
        int encodeIdx(void) const { return (node->isLeaf()) ? ~idx : idx; }
    };

    const FW::BVHNode* root = bvh.root();
    int subtreeSize = root->getSubtreeSize(FW::BVH_STAT_NODE_COUNT);
    m_nodes.resize((subtreeSize * 64 + Align - 1) & -Align);
    m_nodes.shrink_to_fit();

    int nextNodeIdx = 0;
    std::vector<StackEntry> stack;
    stack.push_back(StackEntry(root, nextNodeIdx++));
    while (stack.size())
    {
        StackEntry e = stack.back();
        stack.pop_back();
        const FW::AABB* b0;
        const FW::AABB* b1;
        int c0;
        int c1;

        // Leaf?

        if (e.node->isLeaf())
        {
            const FW::LeafNode* leaf = reinterpret_cast<const FW::LeafNode*>(e.node);
            b0 = &leaf->m_bounds;
            b1 = &leaf->m_bounds;
            c0 = leaf->m_lo + m_triIndexOffset; // woop and index offsets are the same
            c1 = leaf->m_hi + m_triIndexOffset; // the woop array is 4 times bigger, but cuda ray tracing code is dealing with it
        }

        // Internal node?

        else
        {
            stack.push_back(StackEntry(e.node->getChildNode(0), nextNodeIdx++));
            StackEntry e0 = stack.back();

            stack.push_back(StackEntry(e.node->getChildNode(1), nextNodeIdx++));
            StackEntry e1 = stack.back();

            b0 = &e0.node->m_bounds;
            b1 = &e1.node->m_bounds;
            c0 = e0.encodeIdx();
            c1 = e1.encodeIdx();
        }

        // Write entry.

        glm::ivec4 data[] =
        {
            glm::ivec4(FW::floatToBits(b0->min().x), FW::floatToBits(b0->max().x), FW::floatToBits(b0->min().y), FW::floatToBits(b0->max().y)),
            glm::ivec4(FW::floatToBits(b1->min().x), FW::floatToBits(b1->max().x), FW::floatToBits(b1->min().y), FW::floatToBits(b1->max().y)),
            glm::ivec4(FW::floatToBits(b0->min().z), FW::floatToBits(b0->max().z), FW::floatToBits(b1->min().z), FW::floatToBits(b1->max().z)),
            glm::ivec4(c0, c1, 0, 0)
        };

        switch (m_layout)
        {
        case BVHLayout_AOS_AOS:
            memcpy(&m_nodes[e.idx * 64], data, 64);
            break;

        default:
            FW_ASSERT(false);
            break;
        }
    }
}

//------------------------------------------------------------------------

void SerialisedBVH::createTriWoopBasic(const FW::BVH& bvh)
{
    const std::vector<FW::S32>& tidx = bvh.getTriIndices();
    //m_triWoop.resize((tidx.size() * 64 + Align - 1) & -Align);
	m_triWoop.resize(tidx.size() * 64);
    m_triWoop.shrink_to_fit();

    for (int i = 0; i < tidx.size(); i++)
    {
        woopifyTri(bvh, i);

        switch (m_layout)
        {
        case BVHLayout_AOS_AOS:
            memcpy(&m_triWoop[i * 64], m_woop, 48);
            break;

        default:
            FW_ASSERT(false);
            break;
        }
    }
}

//------------------------------------------------------------------------

void SerialisedBVH::createTriIndexBasic(const FW::BVH& bvh)
{
    const std::vector<FW::S32>& tidx = bvh.getTriIndices();
    m_triIndex.resize(tidx.size() * 4);
    m_triIndex.shrink_to_fit();

    for (int i = 0; i < tidx.size(); i++)
        *(FW::S32*)(&m_triIndex[i * 4]) = tidx[i] + m_vertexIndexOffset;
}

//------------------------------------------------------------------------

void SerialisedBVH::createCompact(const FW::BVH& bvh, int nodeOffsetSizeDiv)
{
    struct StackEntry
    {
        const FW::BVHNode* node;
        FW::S32 idx;

        StackEntry(const FW::BVHNode* n = NULL, int i = 0) : node(n), idx(i) {}
    };

    // Construct data.

    std::vector<glm::ivec4> nodeData;
    nodeData.resize(4);
    std::vector<glm::ivec4> triWoopData;
    std::vector<FW::S32> triIndexData;
    std::vector<StackEntry> stack;
    stack.push_back(StackEntry(bvh.root(), 0));

    while (stack.size())
    {
        StackEntry e = stack.back();
        stack.pop_back();

        FW_ASSERT(e.node->getNumChildNodes() == 2);
        const FW::AABB* cbox[2];
        int cidx[2];

        // Process children.

        for (int i = 0; i < 2; i++)
        {
            // Inner node => push to stack.

            const FW::BVHNode* child = e.node->getChildNode(i);
            cbox[i] = &child->m_bounds;
            if (!child->isLeaf())
            {
                cidx[i] = (int) (nodeData.size() * sizeof(*(nodeData.data()))) / nodeOffsetSizeDiv;
                stack.push_back(StackEntry(child, (int) nodeData.size()));
                nodeData.resize(nodeData.size() + 4);
                continue;
            }

            // Leaf => append triangles.

            const FW::LeafNode* leaf = reinterpret_cast<const FW::LeafNode*>(child);
            cidx[i] = ~((FW::S32) triWoopData.size());
            for (int j = leaf->m_lo; j < leaf->m_hi; j++)
            {
                woopifyTri(bvh, j);
                if (m_woop[0].x == 0.0f)
                    m_woop[0].x = 0.0f;

                for(int t = 0; t < 3; t++) {
                    glm::ivec4 tmpWoop;
                    tmpWoop.x = FW::floatToBits(m_woop[t].x);
                    tmpWoop.y = FW::floatToBits(m_woop[t].y);
                    tmpWoop.z = FW::floatToBits(m_woop[t].z);
                    tmpWoop.w = FW::floatToBits(m_woop[t].w);
                    triWoopData.push_back(tmpWoop);
                }
//                triWoopData.add((glm::ivec4*)m_woop, 3);

                triIndexData.push_back(bvh.getTriIndices()[j]);
                triIndexData.push_back(0);
                triIndexData.push_back(0);
            }

            // Terminator.

            triWoopData.push_back(glm::ivec4(0x80000000));
            triIndexData.push_back(0);
        }

        // Write entry.

        glm::ivec4* dst = &nodeData[e.idx];
        dst[0] = glm::ivec4(FW::floatToBits(cbox[0]->min().x), FW::floatToBits(cbox[0]->max().x), FW::floatToBits(cbox[0]->min().y), FW::floatToBits(cbox[0]->max().y));
        dst[1] = glm::ivec4(FW::floatToBits(cbox[1]->min().x), FW::floatToBits(cbox[1]->max().x), FW::floatToBits(cbox[1]->min().y), FW::floatToBits(cbox[1]->max().y));
        dst[2] = glm::ivec4(FW::floatToBits(cbox[0]->min().z), FW::floatToBits(cbox[0]->max().z), FW::floatToBits(cbox[1]->min().z), FW::floatToBits(cbox[1]->max().z));
        dst[3] = glm::ivec4(cidx[0], cidx[1], 0, 0);
    }

    // Write to buffers.

    size_t numBytes = sizeof(*(nodeData.data())) * nodeData.size();
    m_nodes.resize(numBytes);
    m_nodes.shrink_to_fit();
    std::memcpy(m_nodes.data(), nodeData.data(), numBytes);
//    m_nodes.set(nodeData.getPtr(), nodeData.getNumBytes());

    numBytes = sizeof(*(triWoopData.data())) * triWoopData.size();
    m_triWoop.resize(numBytes);
    m_triWoop.shrink_to_fit();
    std::memcpy(m_triWoop.data(), triWoopData.data(), numBytes);
//    m_triWoop.set(triWoopData.getPtr(), triWoopData.getNumBytes());

    numBytes = sizeof(*(triIndexData.data())) * triIndexData.size();
    m_triIndex.resize(numBytes);
    m_triIndex.shrink_to_fit();
    std::memcpy(m_triIndex.data(), triIndexData.data(), numBytes);
//    m_triIndex.set(triIndexData.getPtr(), triIndexData.getNumBytes());
}

//------------------------------------------------------------------------

void SerialisedBVH::woopifyTri(const FW::BVH& bvh, int idx)
{
    const glm::ivec3* triVtxIndex = bvh.triangleSoup()->triangles().data();
    const glm::vec3* vtxPos = bvh.triangleSoup()->positions().data();
    const glm::ivec3& inds = triVtxIndex[bvh.getTriIndices()[idx]];
    const glm::vec3& v0 = vtxPos[inds.x];
    const glm::vec3& v1 = vtxPos[inds.y];
    const glm::vec3& v2 = vtxPos[inds.z];

//    glm::mat4 mtx;
//    mtx.setCol(0, glm::vec4(v0 - v2, 0.0f));
//    mtx.setCol(1, glm::vec4(v1 - v2, 0.0f));
//    mtx.setCol(2, glm::vec4(glm::cross(v0 - v2, v1 - v2), 0.0f));
//    mtx.setCol(3, glm::vec4(v2, 1.0f));
//    mtx = invert(mtx);
    glm::mat4 mtx = glm::mat4(glm::vec4(v0 - v2, 0.0f),
                              glm::vec4(v1 - v2, 0.0f),
                              glm::vec4(glm::cross(v0 - v2, v1 - v2), 0.0f),
                              glm::vec4(v2, 1.0f));
    mtx = glm::inverse(mtx);

    // nvidia matrix was row major. glm is column major. so transposing it should yield the same values.
    mtx = glm::transpose(mtx);
    m_woop[0] = glm::vec4(mtx[2][0], mtx[2][1], mtx[2][2], -mtx[2][3]);
    m_woop[1] = mtx[0]; // was getRow(0)
    m_woop[2] = mtx[1]; // was getRow(1)
}

//------------------------------------------------------------------------
