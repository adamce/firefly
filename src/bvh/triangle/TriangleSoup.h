#ifndef TRIANGLESOUP_H
#define TRIANGLESOUP_H

#include <vector>
#include <glm/glm.hpp>
#include "bvh/Util.hpp"
#include "bvh/nv/Defs.hpp"

class Mesh;

class TriangleSoup
{
public:
    TriangleSoup();
    FW::AABB aabb() const { return m_aabb; }
    void addMesh(const Mesh* mesh, const glm::mat4& constantTransformation = glm::mat4());
    FW::U32 hash() const;

    const std::vector<glm::ivec3>& triangles() const { return m_triangleIndices; }
    const std::vector<glm::vec3>& positions() const { return m_positions; }
    const std::vector<glm::vec3>& normals() const { return m_normals; }
    const std::vector<glm::vec2>& texCoords() const { return m_texCoords; }
    const std::vector<int>& triangleMaterials() const { return m_triangleMaterials; }

    int getNumTriangles() const { return (int) m_triangleIndices.size(); }
    int getNumVertices() const { return (int) m_positions.size(); }

private:
    FW::AABB m_aabb;
    std::vector<glm::ivec3> m_triangleIndices;
    std::vector<glm::vec3> m_positions;
    std::vector<glm::vec3> m_normals;
    std::vector<glm::vec2> m_texCoords;
    std::vector<int> m_triangleMaterials;

};

#endif // TRIANGLESOUP_H
