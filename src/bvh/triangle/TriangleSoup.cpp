#include "TriangleSoup.h"

#include <glm/glm.hpp>
#include "meshes/Mesh.h"
#include "meshes/MeshVertex.h"
#include "bvh/nv/Hash.hpp"

TriangleSoup::TriangleSoup()
{
}

void TriangleSoup::addMesh(const Mesh *mesh, const glm::mat4 &constantTransformation)
{
    const GLuint* indexBuffer = mesh->indexData();
    const MeshVertex* vertices = mesh->vertexData();

    m_triangleIndices.reserve(m_triangleIndices.size() + mesh->numberOfTriangles());
    m_triangleMaterials.reserve(m_triangleIndices.size() + mesh->numberOfTriangles());

    glm::ivec3 indexOffset((int) m_positions.size());

    for(int i=0; i<mesh->numberOfTriangles(); i++) {
        glm::ivec3 triangle(indexBuffer[3*i], indexBuffer[3*i+1], indexBuffer[3*i+2]);
        triangle += indexOffset;
        m_triangleIndices.push_back(triangle);
        m_triangleMaterials.push_back(vertices[indexBuffer[3*i]].materialIndex);
    }

    glm::mat3 rotMat(constantTransformation);

    m_positions.reserve(mesh->numberOfVertices());
    m_normals.reserve(mesh->numberOfVertices());
    m_texCoords.reserve(mesh->numberOfVertices());
    for(int i=0; i<mesh->numberOfVertices(); i++) {
        const MeshVertex& curr = vertices[i];
        glm::vec3 pos = glm::vec3(constantTransformation * curr.position4());
        m_aabb.grow(pos);
        m_positions.push_back(pos);
        m_normals.push_back(rotMat * curr.normal());
        m_texCoords.push_back(curr.texCoords());
    }
}

FW::U32 TriangleSoup::hash() const
{
    if(m_triangleIndices.size() == 0) return 0;
    return FW::hashBits(
            FW::hashBuffer(&m_triangleIndices[0].x, (int)m_triangleIndices.size() * m_triangleIndices[0].length()),
            FW::hashBuffer(&m_normals[0].x, (int)m_normals.size() * m_normals[0].length()),
            FW::hashBuffer(&m_triangleMaterials[0], (int) m_triangleMaterials.size()),
            FW::hashBuffer(&m_texCoords[0].x, (int)m_texCoords.size() * m_texCoords[0].length()),
            FW::hashBuffer(&m_positions[0].x, (int)m_positions.size() * m_positions[0].length()));
}
