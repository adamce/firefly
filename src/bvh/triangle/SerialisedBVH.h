/*
 *  Copyright (c) 2014 Adam Celarek
 *  Copyright (c) 2009-2011, NVIDIA Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of NVIDIA Corporation nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SERIALISEDBVH_H
#define SERIALISEDBVH_H

#include <glm/glm.hpp>
#include <vector>

#include "cuda/BvhHelperStructs.h"
#include "bvh/nv/Defs.hpp"

namespace FW {
    class BVH;
}
class TriangleSoup;
namespace std {
    class InputStream;
    class OutputStream;
}

class SerialisedBVH
{
public:
    enum
    {
        Align = 4096
    };

public:
    SerialisedBVH(const TriangleSoup* soup);

    FW::BVH* bvh() const { return m_bvh; }
    BVHLayout getLayout (void) const { return m_layout; }
    const std::vector<FW::U8>& getNodeBuffer() const { return m_nodes; }
    const std::vector<FW::U8>& getTriWoopBuffer() const { return m_triWoop; }
    const std::vector<FW::U8>& getTriIndexBuffer() const { return m_triIndex; }

    void rebuildWithOffsets(unsigned int triIndexOffset, unsigned int vertexIndexOffset);
private:
    void init (bool rebuildBVH = true);

    // AOS: idx ignored, returns entire buffer
    // SOA: 0 <= idx < 4, returns one subarray
    glm::ivec2 getNodeSubArray (int idx) const; // (ofs, size)
    glm::ivec2 getTriWoopSubArray (int idx) const; // (ofs, size)


    void deSerialise (std::ifstream &in);
    void serialise (std::ofstream &out);
    FW::U32 hash() const;
private:
    void createNodeBasic (const FW::BVH& bvh);
    void createTriWoopBasic (const FW::BVH& bvh);
    void createTriIndexBasic (const FW::BVH& bvh);
    void createCompact (const FW::BVH& bvh, int nodeOffsetSizeDiv);

    void woopifyTri (const FW::BVH& bvh, int idx);

private:
    FW::BVH* m_bvh;

    unsigned int m_triIndexOffset = 0;
    unsigned int m_vertexIndexOffset = 0;

    BVHLayout m_layout;
    std::vector<FW::U8> m_nodes;
    std::vector<FW::U8> m_triWoop;
    std::vector<FW::U8> m_triIndex;
    glm::vec4 m_woop[3];
};

#endif // SERIALISEDBVH_H
