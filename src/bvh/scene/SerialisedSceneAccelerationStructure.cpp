#include "SerialisedSceneAccelerationStructure.h"

#include "SceneAccelerationStructure.h"
#include "bvh/triangle/SerialisedBVH.h"
#include "bvh/triangle/TriangleBvhHolder.h"
#include "bvh/triangle/TriangleSoup.h"
#include "cuda/HelperMath.h"

SerialisedSceneAccelerationStructure::SerialisedSceneAccelerationStructure(SceneAccelerationStructure *sas) : m_sceneAccelerationStructure(sas)
{
}

SerialisedSceneAccelerationStructure::~SerialisedSceneAccelerationStructure()
{
    delete m_sceneAccelerationStructure;
}

template <typename T>
void append(T& to, const T& from) {
    to.insert(to.end(), from.begin(), from.end());
}

void SerialisedSceneAccelerationStructure::build()
{
    m_sceneAccelerationStructure->updateStructure();

    for(const TriangleBvhHolder* tbh : m_sceneAccelerationStructure->triangleBvhes()) {
        SceneAccelerationNode san;
        san.bbMax = make_float3(tbh->triangleSoup()->aabb().max());
        san.bbMin = make_float3(tbh->triangleSoup()->aabb().min());
        san.nodesOffset = (unsigned int) m_nodeBuffer.size() / 16;
        san.triIndexOffset = (unsigned int) m_triIndexBuffer.size()/4;
        san.triWoopOffset = (unsigned int) m_triWoopBuffer.size()/16;
        san.vertexIndexOffset = (unsigned int) m_vertexIndices.size();
        san.worldToStructMatrix = make_mat4(tbh->worldToStructMatrix());
        san.layout = tbh->serialisedBvh()->getLayout();

        tbh->serialisedBvh()->rebuildWithOffsets(san.triIndexOffset, san.vertexIndexOffset);
//        tbh->serialisedBvh()->rebuildWithOffsets(0, san.vertexIndexOffset);
        san.triIndexOffset = 0;
        san.triWoopOffset = 0;
        san.vertexIndexOffset = 0;
        m_serialisedSceneAccelerationNodes.push_back(san);

        append(m_nodeBuffer, tbh->serialisedBvh()->getNodeBuffer());
        append(m_triIndexBuffer, tbh->serialisedBvh()->getTriIndexBuffer());
        append(m_triWoopBuffer, tbh->serialisedBvh()->getTriWoopBuffer());

		int indexOffset = (int) m_positions.size();
        for(glm::ivec3 triangle : tbh->triangleSoup()->triangles()) {
            m_vertexIndices.push_back(glm::ivec3(triangle.x + indexOffset, triangle.y + indexOffset, triangle.z + indexOffset));
        }
        append(m_positions, tbh->triangleSoup()->positions());
        append(m_normals, tbh->triangleSoup()->normals());
        append(m_texCoords, tbh->triangleSoup()->texCoords());
        append(m_triangleMaterials, tbh->triangleSoup()->triangleMaterials());
    }
}

void SerialisedSceneAccelerationStructure::updateSceneAccelerationNodes()
{
    m_sceneAccelerationStructure->updateStructure();
    int i = 0;
    for(const TriangleBvhHolder* tbh : m_sceneAccelerationStructure->triangleBvhes()) {
        SceneAccelerationNode& san = m_serialisedSceneAccelerationNodes[i];
        san.worldToStructMatrix = make_mat4(tbh->worldToStructMatrix());
        i++;
    }
}
