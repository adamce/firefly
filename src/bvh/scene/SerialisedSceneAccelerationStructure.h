/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SERIALISEDSCENEACCELERATIONSTRUCTURE_H
#define SERIALISEDSCENEACCELERATIONSTRUCTURE_H

#include <vector>
#include "cuda/CommonStructs.h"

class SceneAccelerationStructure;

class SerialisedSceneAccelerationStructure
{
public:
    SerialisedSceneAccelerationStructure(SceneAccelerationStructure *sas);
    ~SerialisedSceneAccelerationStructure();

    void build();
    void updateSceneAccelerationNodes();

    const SceneAccelerationStructure* sceneAccelerationStructure() const { return m_sceneAccelerationStructure; }
private:
    SceneAccelerationStructure* m_sceneAccelerationStructure;

public:
    std::vector<unsigned char> m_nodeBuffer;
    std::vector<unsigned char> m_triWoopBuffer;
    std::vector<unsigned char> m_triIndexBuffer;
    std::vector<SceneAccelerationNode> m_serialisedSceneAccelerationNodes;

    std::vector<glm::ivec3> m_vertexIndices;
    std::vector<glm::vec3> m_positions;
    std::vector<glm::vec3> m_normals;
    std::vector<glm::vec2> m_texCoords;
    std::vector<int> m_triangleMaterials;
};

#endif // SERIALISEDSCENEACCELERATIONSTRUCTURE_H
