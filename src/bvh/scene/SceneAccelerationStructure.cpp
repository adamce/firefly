#include "SceneAccelerationStructure.h"

#include "bvh/triangle/TriangleBvhHolder.h"
#include "bvh/triangle/TriangleSoup.h"

SceneAccelerationStructure::SceneAccelerationStructure()
{

}

SceneAccelerationStructure::~SceneAccelerationStructure()
{
    for(TriangleBvhHolder* t : m_triangleBvhes) {
        delete t;
    }
}

void SceneAccelerationStructure::addSubBvh(TriangleBvhHolder *bvh)
{
    m_triangleBvhes.push_back(bvh);
}

void SceneAccelerationStructure::updateStructure()
{
    for(TriangleBvhHolder* tbh : m_triangleBvhes) {
        tbh->updateMatrices();
    }

    // for now do nothing, we will iterate over the set.
}

FW::RayResult SceneAccelerationStructure::trace(const FW::Ray &ray) const
{
    FW::RayResult result;
    float t = FW_F32_MAX;
    for(const TriangleBvhHolder* tbh : m_triangleBvhes) {
        if(FW::Intersect::RayBox(tbh->triangleSoup()->aabb(), ray).x < t) {
            FW::RayResult tbhResult = tbh->trace(ray);
			if (tbhResult.t < t && tbhResult.hit()) {
				t = tbhResult.t;
                result = tbhResult;
            }
        }
    }
    return result;
}
