#ifndef SCENEACCELERATIONSTRUCTURE_H
#define SCENEACCELERATIONSTRUCTURE_H

#include <vector>
#include "bvh/Util.hpp"

class TriangleBvhHolder;

class SceneAccelerationStructure
{
public:
    SceneAccelerationStructure();
    ~SceneAccelerationStructure();
    void addSubBvh(TriangleBvhHolder* bvh);
    void updateStructure();
    FW::RayResult trace(const FW::Ray& ray) const;
    const std::vector<TriangleBvhHolder*>& triangleBvhes() const { return m_triangleBvhes; }

private:
    std::vector<TriangleBvhHolder*> m_triangleBvhes;
};

#endif // SCENEACCELERATIONSTRUCTURE_H
