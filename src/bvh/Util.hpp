/*
 *  Copyright (c) 2014 Adam Celarek
 *  Copyright (c) 2009-2011, NVIDIA Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of NVIDIA Corporation nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef BVH_UTIL_H
#define BVH_UTIL_H

#include <glm/glm.hpp>
#include <algorithm>
#include "nv/Defs.hpp"

namespace FW
{

class AABB
{
public:
    AABB        (void) : m_mn(FW_F32_MAX, FW_F32_MAX, FW_F32_MAX), m_mx(-FW_F32_MAX, -FW_F32_MAX, -FW_F32_MAX) {}
    AABB        (const glm::vec3& mn, const glm::vec3& mx) : m_mn(mn), m_mx(mx) {}

    void            grow        (const glm::vec3& pt)   { m_mn = glm::min(m_mn, pt); m_mx = glm::max(m_mx, pt); }
    void            grow        (const AABB& aabb)  { grow(aabb.m_mn); grow(aabb.m_mx); }
    void            intersect   (const AABB& aabb)  { m_mn = glm::max(m_mn, aabb.m_mn); m_mx = glm::min(m_mx, aabb.m_mx); }
    float           volume      (void) const        { if(!valid()) return 0.0f; return (m_mx.x-m_mn.x) * (m_mx.y-m_mn.y) * (m_mx.z-m_mn.z); }
    float           area        (void) const        { if(!valid()) return 0.0f; glm::vec3 d = m_mx - m_mn; return (d.x*d.y + d.y*d.z + d.z*d.x)*2.0f; }
    bool            valid       (void) const        { return m_mn.x<=m_mx.x && m_mn.y<=m_mx.y && m_mn.z<=m_mx.z; }
    glm::vec3           midPoint    (void) const        { return (m_mn+m_mx)*0.5f; }
    const glm::vec3&    min         (void) const        { return m_mn; }
    const glm::vec3&    max         (void) const        { return m_mx; }
    glm::vec3&          min         (void)              { return m_mn; }
    glm::vec3&          max         (void)              { return m_mx; }

    AABB            operator+   (const AABB& aabb) const { AABB u(*this); u.grow(aabb); return u; }

private:
    glm::vec3           m_mn;
    glm::vec3           m_mx;
};

template<typename T>
inline T min(const T& a, const T& b, const T& c) { return std::min(a, std::min(b, c)); }
template<typename T>
inline T max(const T& a, const T& b, const T& c) { return std::max(a, std::max(b, c)); }

inline float min(glm::vec2 v) { return std::min(v.x, v.y); }
inline float min(glm::vec3 v) { return std::min(v.x, std::min(v.y, v.z)); }
inline float min(glm::vec4 v) { return std::min(std::min(v.x, v.y), std::min(v.z, v.w)); }
inline float max(glm::vec2 v) { return std::max(v.x, v.y); }
inline float max(glm::vec3 v) { return std::max(v.x, std::max(v.y, v.z)); }
inline float max(glm::vec4 v) { return std::max(std::max(v.x, v.y), std::max(v.z, v.w)); }

inline float sum(glm::vec2 v) { return v.x + v.y; }
inline float sum(glm::vec3 v) { return v.x + v.y + v.z; }
inline float sum(glm::vec4 v) { return v.x + v.y + v.z + v.w; }


//------------------------------------------------------------------------

struct Ray
{
    Ray (void) : origin(0.0f), tmin(0.0f), direction(0.0f), tmax(0.0f) {}
    void degenerate (void) { tmax = tmin - 1.0f; }

    glm::vec3 origin;
    float tmin;
    glm::vec3 direction;
    float tmax;
};

//------------------------------------------------------------------------

#define RAY_NO_HIT (-1)

struct RayResult
{
    RayResult (S32 ii = RAY_NO_HIT, float ti = 0.f) : id(ii), t(ti) {}
    bool hit (void) const { return (id != RAY_NO_HIT); }
    void clear (void) { id = RAY_NO_HIT; }

    S32 id;
    float t;
    float u;    // barycentric
    float v;

    glm::vec3 pos;
    glm::vec3 normal;
};

//------------------------------------------------------------------------

namespace Intersect
{
    glm::vec2 RayBox(const AABB& box, const Ray& ray);
    glm::vec3 RayTriangle(const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2, const Ray& ray);
    glm::vec3 RayTriangleWoop(const glm::vec4& zpleq, const glm::vec4& upleq, const glm::vec4& vpleq, const Ray& ray);
}

//------------------------------------------------------------------------
}

#endif
