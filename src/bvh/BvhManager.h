#ifndef BVHMANAGER_H
#define BVHMANAGER_H

class SerialisedSceneAccelerationStructure;

class BvhManager
{
public:
    BvhManager();
    ~BvhManager();

    void createBvh();
    void updateSceneAccelerationStructure();
    void debugTraceImage();
    const SerialisedSceneAccelerationStructure* serialisedSceneAccelerationStructure() const { return m_serialisedSceneAccelerationStructure; }

private:
    SerialisedSceneAccelerationStructure* m_serialisedSceneAccelerationStructure = nullptr;
};

#endif // BVHMANAGER_H
