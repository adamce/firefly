/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "BvhManager.h"

#include <iostream>
#include <FreeImage.h>
#include "Application.h"
#include "ObjectManager.h"
#include "objects/Object.h"
#include "bvh/scene/SceneAccelerationStructure.h"
#include "bvh/scene/SerialisedSceneAccelerationStructure.h"
#include "bvh/triangle/TriangleSoup.h"
#include "triangle/TriangleBvhHolder.h"
#include "objects/Camera.h"


BvhManager::BvhManager()
{
}

BvhManager::~BvhManager()
{
    delete m_serialisedSceneAccelerationStructure;
}

void BvhManager::createBvh()
{
    if(m_serialisedSceneAccelerationStructure != nullptr) {
        std::cerr << "BvhManager::createBvh(): attempting to create the Bvh twice. Nahh.." << std::endl;
        return;
    }

    auto start = std::chrono::high_resolution_clock::now();

    SceneAccelerationStructure* sceneAccelerationStructure = nullptr;
    sceneAccelerationStructure = new SceneAccelerationStructure();
    auto objects = ObjectManager::instance()->geometryObjects();
    TriangleSoup* staticTriangles = new TriangleSoup;


    for(Object* object : objects) {
        if(object->mesh() != 0) {
            if(object->isStatic()) {
                staticTriangles->addMesh(object->mesh(), object->modelMatrix());
            }
            else {
                TriangleSoup* objectTriangles = new TriangleSoup;
                objectTriangles->addMesh(object->mesh());
                TriangleBvhHolder* tbh = new TriangleBvhHolder(objectTriangles, object);
                tbh->createBvh();
                sceneAccelerationStructure->addSubBvh(tbh);
            }
        }
    }

    TriangleBvhHolder* tbh = new TriangleBvhHolder(staticTriangles);
    tbh->createBvh();
    sceneAccelerationStructure->addSubBvh(tbh);


    m_serialisedSceneAccelerationStructure = new SerialisedSceneAccelerationStructure(sceneAccelerationStructure);
    m_serialisedSceneAccelerationStructure->build();


    auto end = std::chrono::high_resolution_clock::now();
    double milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    std::cout << "building BVH tree took " << milliseconds << " milliseconds" << std::endl;
}

void BvhManager::updateSceneAccelerationStructure()
{
    m_serialisedSceneAccelerationStructure->updateSceneAccelerationNodes();
}

void BvhManager::debugTraceImage()
{
    std::cout << "BvhManager::debugTraceImage()" << std::endl;
    Camera* camera = Application::instance()->camera();
    int width = camera->viewPort().x;
    int height = camera->viewPort().y;
    glm::mat3 uvw = glm::transpose(glm::mat3(camera->UVW()));

    glm::vec3* result = new glm::vec3[width * height];

    m_serialisedSceneAccelerationStructure->updateSceneAccelerationNodes();
    for(int x = 0; x < width; x++) {
        for(int y = 0; y < height; y++) {

            float xf = (float) x;
            xf /= (float) width;
            xf = xf * 2.f - 1.f;

            float yf = (float) y;
            yf /= (float) height;
            yf = yf * 2.f - 1.f;


            glm::vec3 dir = uvw[2] + xf * uvw[0] + yf * uvw[1];
            dir = glm::normalize(dir);

            FW::Ray ray;
            ray.direction = dir;
            ray.origin = camera->position();
            ray.tmin = 0;
            ray.tmax = FW_F32_MAX;

            FW::RayResult traceResult = m_serialisedSceneAccelerationStructure->sceneAccelerationStructure()->trace(ray);
            glm::vec3 lightDir(+4.03811932f, +5.07565546f, -8.37142754f);
            lightDir = glm::normalize(lightDir);

            glm::vec3 colour(glm::dot(lightDir, traceResult.normal));

            result[y * width + x] = traceResult.normal;
        }
    }

    FIBITMAP* bitmap = FreeImage_Allocate(width, height, 24);
    BYTE* bits(0);
    bits = FreeImage_GetBits(bitmap);
    for(int x = 0; x < width; x++) {
		for (int y = 0; y < height; y++) {
            RGBQUAD colour;
            colour.rgbRed   = (BYTE)(glm::clamp(result[y * width + x].r * 255.f, 0.f, 255.f));
            colour.rgbGreen = (BYTE)(glm::clamp(result[y * width + x].g * 255.f, 0.f, 255.f));
            colour.rgbBlue  = (BYTE)(glm::clamp(result[y * width + x].b * 255.f, 0.f, 255.f));
//            colour.rgbRed = (BYTE) (128);
//            colour.rgbGreen = (BYTE) (255);
//            colour.rgbBlue = (BYTE)(120);
			FreeImage_SetPixelColor(bitmap, x, y, &colour);
        }
    }

    FreeImage_Save(FIF_JPEG, bitmap, "traceResult.jpg");
    FreeImage_Unload(bitmap);
}
