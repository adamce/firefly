/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef APPLICATION_H
#define APPLICATION_H

#include <vector>
#include <set>
#include <map>
#include <string>
#include <chrono>

#include "LinearMath/btAlignedObjectArray.h"

#define GLM_FORCE_RADIANS
#include "glm/glm.hpp"

#include "auxiliary/ApplicationConstants.h"
#include "auxiliary/FmodSoundManager.h"


class btBroadphaseInterface;
class btCollisionShape;
class btOverlappingPairCache;
class btCollisionDispatcher;
class btConstraintSolver;
struct btCollisionAlgorithmCreateFunc;
class btDefaultCollisionConfiguration;
class btDiscreteDynamicsWorld;

class Renderer;
class IngameGui;
class CameraInputHandler;
class ObjectManager;
class Camera;
class Object;
class ShaderManager;
class BvhManager;

//class FmodSoundManager;

class Application
{
    Application();
    ~Application();
    Application(Application const&){}
    Application& operator=(Application const&){return *this;}
    static Application* singletonPointer;
public:
    static Application* instance();


    void init();
    void update(float time);
    void drawWithDefaultCamera(Object* object);
	ShaderManager* shaderManager() const;
    ObjectManager* objectManager() const;
    Renderer* renderer() const;
    IngameGui* ingameGui() const;
    btDiscreteDynamicsWorld* dynamicsWorld() const;
    Camera* camera() const;
    BvhManager* bvhManager() const;

    void switchEnvironmentMapping();
    void switchPaused();
    void switchBulletDebugDrawing();
    void switchBufferMode();
    void switchTransparency();
	void switchMipMapping();
	void switchTextureFiltering();
	void switchSkybox();
	void switchJetStreams();

    void addTiming(const std::string& name, VIENNA_TIMING_PRECISION time);
    const std::map<std::string, double>& timings() const { return m_measuredTime; }

    bool useUnbuffredGeometry();

	FmodSoundManager* soundManager() const;

protected:
    void handleInput(float time);
    void initBulletEngine();
	void updateTextureMode();
	void initSoundEngine();
private:
    // game management
    Camera* m_camera1 = nullptr;
    CameraInputHandler* m_cameraInputHandler = nullptr;
    ShaderManager* m_shaderManager = nullptr;
    Renderer* m_renderer = nullptr;
    IngameGui* m_ingameGui = nullptr;
    ObjectManager* m_objectManager = nullptr;
    BvhManager* m_bvhManager = nullptr;

    // performance measuring
    std::map<std::string, double> m_measuredTime;
    std::map<std::string, int> m_measuredIterations;

	float m_cumulatedTime = 0.f;

	//switches
    bool m_environmentMapping;
    bool m_gamePaused;
    bool m_bulletDebugDrawing;
    bool m_useUnbuffredGeometry;
	bool m_useMipMapping;
	bool m_useLinearFiltering;
    bool m_useJetStreams;

    // bullet
    btAlignedObjectArray<btCollisionShape*> m_collisionShapes;
    btBroadphaseInterface*  m_broadphase = nullptr;
    btCollisionDispatcher*  m_dispatcher = nullptr;
    btConstraintSolver* m_solver = nullptr;
    btDefaultCollisionConfiguration* m_collisionConfiguration = nullptr;
    btDiscreteDynamicsWorld* m_dynamicsWorld = nullptr;

	//Sound
    FmodSoundManager* m_soundManager = nullptr;
};

#endif // APPLICATION_H
