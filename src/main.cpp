/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Application.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <cstdlib>
#include "auxiliary/IngameGui.h"
#include "Renderer.h"


using std::cout;
using std::endl;


int main(int argc, char *argv[]) {

//    Renderer renderer;
    bool running = true;

    // Initialise GLFW

//    renderer.initGL();
//    renderer.initScene();

    Application* obman = Application::instance();
    obman->init();

    // Main loop
    double lastDraw=0.;
    double diff=0.;
    double currentTime=0.;
    int frameCounter = 0;
    int frameCounter2 = 0;
    double timeCounter = 0.;
    while ( running ) {
        currentTime = glfwGetTime();
        diff=currentTime-lastDraw;
//         if(diff<(1./120.))
//             continue;

//        diff = 1.f/30.f;


        lastDraw = currentTime;

        obman->renderer()->renderScene(obman->camera());
        obman->update((float) diff);
//        obman->renderer()->writeFrontBufferToFile(std::string("videoOut/") + std::to_string(frameCounter2));

        timeCounter+=diff;
        frameCounter++;
        frameCounter2++;
        if(timeCounter>1.) {
            float fps = (float) frameCounter / (float) timeCounter;
            timeCounter=0;
            obman->ingameGui()->setFramerate(fps);
            frameCounter=0;
        }

        glfwPollEvents();

        // Check if ESC key was pressed or window was closed
        GLFWwindow* window = obman->renderer()->glfwWindowHandle();
        running = !glfwGetKey (window, GLFW_KEY_ESCAPE ) && !glfwWindowShouldClose(window);
    }

    // Close window and terminate GLFW
    glfwTerminate();

    // Exit program
    return 0;
}
