#include "FlareVertex.h"


std::ostream& operator<<(std::ostream& stream, const FlareVertex& v)
{
    return stream << "Flare - Vertex: xyz(" << v.x << "/" << v.y << "/" << v.z << ") size(" << v.size << ") type(" << v.type << ")" << std::endl;
}