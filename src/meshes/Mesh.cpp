#include "Mesh.h"

#define GLM_FORCE_RADIANS
#include "glm/gtc/type_ptr.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>       // Output data structure
#include <assimp/postprocess.h> // Post processing flags

#include <BulletCollision/CollisionShapes/btTriangleMesh.h>
#include <BulletCollision/Gimpact/btGImpactShape.h>
#include <BulletCollision/CollisionShapes/btConvexHullShape.h>

#include "TextureManager.h"
#include "MeshManager.h"
#include "shaders/ShaderManager.h"
#include "shaders/Shader.h"
#include "MeshVertex.h"
#include "Application.h"
#include "Renderer.h"
#include "MaterialDescription.h"

#include "DataManager.h"


Mesh::Mesh(GLenum drawMode) : m_drawMode(drawMode), m_vertexDataPointer(0), m_indexDataPointer(0), m_meshSupportsUnbufferedGeometry(false), m_hasBoundingSphere(false), m_collisionShape(0)
{}

Mesh::~Mesh()
{
    if(m_collisionShape!=0) {
        delete m_collisionShape->getMeshInterface();
        delete m_collisionShape;
    }
    if(m_vertexDataPointer!=0)
        delete[] m_vertexDataPointer;
    if(m_indexDataPointer!=0)
        delete[] m_indexDataPointer;
}

void Mesh::activate()
{
    if(useBufferedGeometry())
        glBindVertexArray(m_vaoId);
    else
        glBindVertexArray(0);
}

void Mesh::draw(const glm::mat4& modelMatrix, const glm::mat4& viewProjectionMatrix)
{
    //setupUniforms(modelViewProjectionMatrix);
    Shader* shader = Application::instance()->shaderManager()->activeShader();
    shader->setModelViewProjectionMatrix(modelMatrix, viewProjectionMatrix);
    shader->setCubeMapSampler(m_cubeTextureHandle);
    shader->setColorMapSamplers(m_texture2DHandles);

    GLvoid* indices;
    if(useBufferedGeometry()) {
        indices = 0;
    }
    else {
        indices = m_indexDataPointer;
        Shader::setupVertexAttribPointers(m_vertexDataPointer);
    }
    shader->enableVertexAttribArrays();

    if(m_drawMode==GL_TRIANGLES)
        Application::instance()->renderer()->addTrianglesToDrawnCounter(m_numElements);

    if(m_drawMode!=GL_POINTS)
        glDrawElements(m_drawMode, m_numElements*m_numVerticesPerElement, GL_UNSIGNED_INT, indices);
    else
        glDrawArrays(GL_POINTS, 0, m_numElements);

    shader->disableVertexAttribArrays();
}

GLuint* Mesh::cubeTextureHandle()
{
	return &m_cubeTextureHandle;
}

GLuint* Mesh::texture2dHandle()
{
	return &m_texture2dHandle;
}

btCollisionShape* Mesh::collisionShape() const
{
    return m_collisionShape;
}

void Mesh::loadFromFile(std::string path, glm::vec4 specialDiffuseColour)
{
    std::string materialFile = "";
    {
        std::vector<std::string> elems;
        std::stringstream ss(path);
        std::string item;
        while (std::getline(ss, item, '?')) {
            elems.push_back(item);
        }
        path = elems[0];
        if(elems.size() > 1)
            materialFile = elems[1];
    }

    std::cout << "Mesh::loadFromFile: loading " << path << std::endl;
	Assimp::Importer importer;
    importer.SetPropertyInteger(AI_CONFIG_PP_SBP_REMOVE, aiPrimitiveType_POINT|aiPrimitiveType_LINE);

    const aiScene* scene = importer.ReadFile(path,
                                             aiProcess_GenSmoothNormals     |
//                                             aiProcess_CalcTangentSpace     |
//                                             aiProcess_Triangulate          |
                                             aiProcess_SortByPType          |
                                             aiProcess_PreTransformVertices |
                                             aiProcess_FindInvalidData      |
                                             aiProcess_FindDegenerates      |
                                             aiProcess_GenUVCoords);

	if(!(scene && scene->HasMeshes()))
	{
        std::cerr << "Mesh::loadFromFile: failed importing file: " << importer.GetErrorString() << std::endl;
		exit(1);
	}

//	if(scene->mNumMeshes>1)
//        std::cerr << "Mesh::loadFromFile: the file >" << path << "< contains more than one mesh. they will be merged into one chawah mesh." << std::endl;
	if(scene->mNumMeshes==0)
        std::cerr << "Mesh::loadFromFile: the file >" << path << "< doesn't contains a mesh. i can't deal with that so i will crash now.." << std::endl;

    // //////////////////////////////////////////////// Materials ////////////////////////////////////////////////
    std::map<unsigned int, int> assimpToOurIdMap;
    for(unsigned int i = 0; i < scene->mNumMaterials; i++) {
        aiMaterial* mat = scene->mMaterials[i];
        MaterialDescription myMat;
//        unsigned char type;
//        unsigned char phongExponent;
//        unsigned char r, g, b;
//        unsigned char specularTexture, normalMapTexture;
//        unsigned char padding;
        aiColor3D diffuseColour;
        float phongExp = 100;
        mat->Get(AI_MATKEY_COLOR_DIFFUSE, diffuseColour);
        mat->Get(AI_MATKEY_SHININESS, phongExp);

        glm::vec4 glmDiffuseColour(diffuseColour.r, diffuseColour.g, diffuseColour.b, 0.f);
        myMat.diffuseRGBA = glm::packUnorm4x8(glmDiffuseColour);
        myMat.factor = phongExp;

        if(mat->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
            myMat.type = VIENNA_MATERIAL_TYPE_TEXTURED;
            aiString tmpString;
            mat->GetTexture(aiTextureType_DIFFUSE, 0, &tmpString);

            // new vienna code path
            TextureDescription desc = TextureManager::instance()->loadTexture(path.substr(0, path.find_last_of("\\/")) + "/" + tmpString.C_Str());
            myMat.diffuseTexture = desc;

            // old chawah path
//            std::string texturePath = MeshManager::instance()->basePath() + tmpString.C_Str();
//            m_texture2DHandles.insert(TextureManager::instance()->get2DTexture(texturePath));
        }
        else {
            myMat.type = VIENNA_MATERIAL_TYPE_FLAT_COLOUR;
        }

        aiString aiName;
        mat->Get(AI_MATKEY_NAME, aiName);
        std::string stdName(aiName.C_Str());    // for loading my own material file
        loadMyMaterialFileParams(&myMat, stdName);
        loadMyMaterialFileParams(&myMat, materialFile);

        if(specialDiffuseColour.x > 0)
            myMat.diffuseRGBA = glm::packUnorm4x8(specialDiffuseColour);

        int ourId = DataManager::instance()->addMaterialDescription(myMat);
        assimpToOurIdMap.insert(std::pair<unsigned int, int>(i, ourId));
    }

    unsigned int numVertices = 0;
    unsigned int numFaces = 0;
    for(unsigned int i=0; i<scene->mNumMeshes; i++) {
        aiMesh* currentMesh = scene->mMeshes[i];
        numVertices += currentMesh->mNumVertices;
        numFaces += currentMesh->mNumFaces;
    }

    MeshVertex* vertices = new MeshVertex[numVertices];
    //unsigned short* indices = new unsigned short[numFaces*3];
    GLuint* indices = new GLuint[numFaces*3];
    unsigned int currentVertexArrayIndex=0;
    unsigned int currentFaceArrayIndex=0;
    int currentMeshStartTriangleIndex = 0;
	for(unsigned int i=0; i<scene->mNumMeshes; i++) {
		aiMesh* currentMesh = scene->mMeshes[i];
        bool hasTextureCoords = currentMesh->HasTextureCoords(0);

		if (! hasTextureCoords)
            std::cerr << "Mesh::loadFromFile: the file >" << path << "< contains no texture coordinates" << std::endl;

        // //////////////////////////////////////////////// Indices ////////////////////////////////////////////////
		// copy indices
		for(unsigned int j=0; j<currentMesh->mNumFaces; j++) {
			aiFace& currentFace = currentMesh->mFaces[j];
			if(currentFace.mNumIndices!=3) {
                std::cerr << "Mesh::loadFromFile: error, while importing file >" << path << "<, wrong face vertex count."
							<< " the model must consist of triangles only. it will be broken probably.." << std::endl;
				numFaces--;
			}
			else {
				for(int k=0; k<3; k++) {
                    if(currentFace.mIndices[k] < 0 || currentFace.mIndices[k] >= currentMesh->mNumVertices)
                        std::cerr << "Mesh::loadFromFile: invalid index detected" << std::endl;
                    indices[currentFaceArrayIndex*3+k] = currentFace.mIndices[k] + currentVertexArrayIndex;
                }

				currentFaceArrayIndex++;
			}
		}

        // //////////////////////////////////////////////// Vertices ////////////////////////////////////////////////
		// copy vertices
		for(unsigned int j=0; j<currentMesh->mNumVertices; j++) {
			vertices[currentVertexArrayIndex].setValues(
									currentMesh->mVertices[j].x,
									currentMesh->mVertices[j].y,
									currentMesh->mVertices[j].z);
			vertices[currentVertexArrayIndex].setNormal(
									currentMesh->mNormals[j].x,
									currentMesh->mNormals[j].y,
									currentMesh->mNormals[j].z);
			//copy texturecoords
			if(hasTextureCoords){
				vertices[currentVertexArrayIndex].setTextureCoords(
										currentMesh->mTextureCoords[0][j].x,
										currentMesh->mTextureCoords[0][j].y);
            }

            vertices[currentVertexArrayIndex].materialIndex = assimpToOurIdMap.at(currentMesh->mMaterialIndex);

			currentVertexArrayIndex++;
		}
        currentMeshStartTriangleIndex += currentMesh->mNumVertices;


	}

    //copy data to graphics card
    MeshVertex* vertexDataPointer = vertices;
    GLsizeiptr vertexDataSize = numVertices*sizeof(MeshVertex);
    GLuint* indexDataPointer = indices;
    GLsizeiptr indexDataSize = numFaces*3*sizeof(GLuint);
    int numElements = numFaces;
    int numVerticesPerElement = 3;
    setupData(vertexDataPointer, vertexDataSize, numVertices, indexDataPointer, indexDataSize, numElements, numVerticesPerElement);

	//setup the bounding sphere used for view frustum culling
	setupBoundingSphere(vertexDataPointer, numVertices);
	m_hasBoundingSphere = true;

    //setup mesh for bullet engine
    btTriangleMesh* bulletMesh = new btTriangleMesh;
    for(int i=0; i<m_numElements; i++) {
        GLuint i0 = indices[3*i+0];
        GLuint i1 = indices[3*i+1];
        GLuint i2 = indices[3*i+2];

        btVector3 v0(vertices[i0].x, vertices[i0].y, vertices[i0].z);
        btVector3 v1(vertices[i1].x, vertices[i1].y, vertices[i1].z);
        btVector3 v2(vertices[i2].x, vertices[i2].y, vertices[i2].z);

        bulletMesh->addTriangle(v0, v1, v2);
    }

    m_collisionShape = new btGImpactMeshShape(bulletMesh);
    m_collisionShape->updateBound();
    m_collisionShape->setMargin(0.01f);
}

void Mesh::loadFromFunction(void (*geometrySetupFunction)(MeshVertex**, GLsizeiptr*, GLuint**, GLsizeiptr*, int*, int*))
{
    MeshVertex* vertexDataPointer;
    GLsizeiptr vertexDataSize;
    GLuint* indexDataPointer;
    GLsizeiptr indexDataSize;
    int numElements;
    int numVerticesPerElement;

    geometrySetupFunction(&vertexDataPointer, &vertexDataSize, &indexDataPointer, &indexDataSize, &numElements, &numVerticesPerElement);
    setupData(vertexDataPointer, vertexDataSize, vertexDataSize/sizeof(MeshVertex), indexDataPointer, indexDataSize, numElements, numVerticesPerElement);
}

void Mesh::loadFromFunction(void (*geometrySetupFunction)(GLuint* vaoId, int* numElements, int* numVerticesPerElement))
{
    geometrySetupFunction(&m_vaoId, &m_numElements, &m_numVerticesPerElement);
    m_meshSupportsUnbufferedGeometry = false;
}


void Mesh::setupData(MeshVertex* vertexDataPointer, GLsizeiptr vertexDataSize, int numVertices, GLuint* indexDataPointer, GLsizeiptr indexDataSize, int numElements, int numVerticesPerElement)
{
//     std::cout << "Mesh::setupData with " << numElements << " elements and " << numVerticesPerElement << " vertizes per element." << std::endl;
    // setup buffers in case we want to use them
    glGenVertexArrays(1, &m_vaoId);
    glBindVertexArray(m_vaoId);

    glGenBuffers(1, &m_vboId);
    glGenBuffers(1, &m_iboId);

    glBindBuffer(GL_ARRAY_BUFFER, m_vboId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iboId);

    glBufferData(GL_ARRAY_BUFFER, vertexDataSize, vertexDataPointer, GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexDataSize, indexDataPointer, GL_STATIC_DRAW);

    Shader::setupVertexAttribPointers();
    glBindVertexArray(0);

    // also setup data in case we don't want to use the buffer
    m_vertexDataPointer = vertexDataPointer;
    m_vertexDataSize = vertexDataSize;
    m_indexDataPointer = indexDataPointer;
    m_indexDataSize = indexDataSize;
    m_numElements = numElements;
    m_numVertices = numVertices;
    m_numVerticesPerElement = numVerticesPerElement;

    // currently only the lense flares don't support unbuffered display. the reason is the different Vertex format and lack of time
    m_meshSupportsUnbufferedGeometry = true;
}

bool Mesh::useBufferedGeometry()
{
    return !m_meshSupportsUnbufferedGeometry || (m_meshSupportsUnbufferedGeometry && !Application::instance()->useUnbuffredGeometry());
}

void Mesh::setupBoundingSphere(MeshVertex* vertexDataPointer, int numVertices)
{
	float min_x=vertexDataPointer[0].x;
	float min_y=vertexDataPointer[0].y;
	float min_z=vertexDataPointer[0].z;
	float max_x=vertexDataPointer[0].x;
	float max_y=vertexDataPointer[0].y;
	float max_z=vertexDataPointer[0].z;
	glm::vec3 vec_max;
	glm::vec3 vec_min;
	MeshVertex currentVertex;



	for(int i = 1 ; i < numVertices ; i++){
		currentVertex = vertexDataPointer[i];
		if(currentVertex.x < min_x)
			min_x = currentVertex.x;
		if(currentVertex.x > max_x)
			max_x = currentVertex.x;
		if(currentVertex.y < min_y)
			min_y = currentVertex.y;
		if(currentVertex.y > max_y)
			max_y = currentVertex.y;
		if(currentVertex.z < min_z)
			min_z = currentVertex.z;
		if(currentVertex.z > max_z)
			max_z = currentVertex.z;
	}
	vec_min = glm::vec3(min_x, min_y, min_z);
	vec_max = glm::vec3(max_x, max_y, max_z);
	glm::vec3 diff_half = (vec_max - vec_min)/2.f;
	m_boundingSphereCenter = vec_min + diff_half;
    m_boundingSphereRadius = glm::length(diff_half);
}

void Mesh::loadMyMaterialFileParams(MaterialDescription *myMat, const std::string &stdName)
{
    std::string path = MeshManager::instance()->basePath() + stdName;
    std::ifstream materialFile(path);
    if(materialFile.is_open()) {
        std::string line;
        while(std::getline(materialFile, line)) {
            std::string propName;
            std::stringstream stream(line);
            stream >> propName;

            if(propName.compare("===") == 0)
                break;

            if(propName.compare("type") == 0) {
                std::string propValue;
                stream >> propValue;
                if(propValue.compare("flatColour") == 0)
                    myMat->type = VIENNA_MATERIAL_TYPE_FLAT_COLOUR;
                if(propValue.compare("textured") == 0)
                    myMat->type = VIENNA_MATERIAL_TYPE_TEXTURED;
                if(propValue.compare("mirror") == 0) {
                    myMat->type = VIENNA_MATERIAL_TYPE_MIRROR;
                }
                if(propValue.compare("macro") == 0) {
                    myMat->type = VIENNA_MATERIAL_TYPE_MACRO;
                }
                if(propValue.compare("glowing") == 0) {
                    myMat->type = VIENNA_MATERIAL_TYPE_GLOWING;
                }
            }

            if(propName.compare("glowFactor") == 0) {
                float propValue;
                stream >> propValue;
                myMat->factor = propValue;
            }
            if(propName.compare("diffuseColour") == 0) {
                float r, g, b;
                stream >> r >> g >> b;
                myMat->diffuseRGBA = glm::packUnorm4x8(glm::vec4(r, g, b, 0.f));
            }
        }
        materialFile.close();
    }
}

float Mesh::boundingSphereRadius() const
{
	return m_boundingSphereRadius;
}
glm::vec3 Mesh::boundingSphereCenter() const
{
	return m_boundingSphereCenter;
}
bool Mesh::hasBoundingSphere() const
{
	return m_hasBoundingSphere;
}

void Mesh::switchTextureFiltering(int value) const
{
	for(std::set<GLuint>::iterator iter = m_texture2DHandles.begin() ; iter != m_texture2DHandles.end() ; iter++){
		glBindTexture(GL_TEXTURE_2D, (*iter));

			switch(value)
				{
				case 0:
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
					break;

				case 1:
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					break;


				case 2:
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
					break;

				case 3:
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
					break;

				case 4:
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
					break;

				case 5:
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
					break;
				}
	}
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Mesh::setCubeTextureHandle(GLuint id)
{
	m_cubeTextureHandle = id;
}

int Mesh::numElements() const
{
    return m_numElements;
}

void Mesh::setNumElements(int num)
{
    m_numElements = num;
}

