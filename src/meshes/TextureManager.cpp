#include "TextureManager.h"
//#include "TextureLoader.h"

#include <iostream>
#include <algorithm>
#include <FreeImage.h>

#include "auxiliary/ApplicationConstants.h"

TextureManager* TextureManager::singletonPointer = 0;

TextureManager::TextureManager(void)
{
}

TextureManager::~TextureManager(void)
{
    for(auto element : m_texutreArray256) {
        delete[] element.second;
    }
    for(auto element : m_texutreArray512) {
        delete[] element.second;
    }
    for(auto element : m_texutreArray1024) {
        delete[] element.second;
    }
    for(auto element : m_texutreArray2048) {
        delete[] element.second;
    }

    for(auto element : m_texutreArray256small) {
        delete[] element.second;
    }
    for(auto element : m_texutreArray512small) {
        delete[] element.second;
    }
    for(auto element : m_texutreArray1024small) {
        delete[] element.second;
    }
    for(auto element : m_texutreArray2048small) {
        delete[] element.second;
    }
}

TextureManager* TextureManager::instance()
{
	if(singletonPointer==0){
        singletonPointer=new TextureManager();
	}
    return singletonPointer;
}

GLuint TextureManager::get2DTexture(const std::string& path)
{
    if(m_pathToHandleMapper.count(path)) {
        return m_pathToHandleMapper.at(path);
    }
    else {
        GLuint handle = load2dTexture(path);
        m_pathToHandleMapper.insert(std::pair<std::string, GLuint>(path, handle));
        return handle;
    }
}

GLuint TextureManager::getCubeTexture(const std::string& posXpath,
                                      const std::string& negXpath,
                                      const std::string& posYpath,
                                      const std::string& negYpath,
                                      const std::string& posZpath,
                                      const std::string& negZpath)
{
    std::string jointString = posXpath + negXpath + posYpath + negYpath + posZpath + negZpath;

    if(m_pathToHandleMapper.count(jointString)) {
        return m_pathToHandleMapper.at(jointString);
    }
    else {
        GLuint handle = loadCubeTexture(posXpath, negXpath, posYpath, negYpath, posZpath, negZpath);
        m_pathToHandleMapper.insert(std::pair<std::string, GLuint>(jointString, handle));
        return handle;
    }

    return 0;
}

void TextureManager::openGlUpload()
{
    glGenTextures(4, m_textureArrayIds);
    openGlUpload(&m_texutreArray256, m_textureArrayIds[0], 256, 0);
    openGlUpload(&m_texutreArray512, m_textureArrayIds[1], 512, 1);
    openGlUpload(&m_texutreArray1024, m_textureArrayIds[2], 1024, 2);
    openGlUpload(&m_texutreArray2048, m_textureArrayIds[3], 2048, 3);
}

void TextureManager::openGlUpload(std::vector<std::pair<TextureDescription, GLbyte *> > *textureArray, GLuint tex, GLuint size, int texNo)
{
    glActiveTexture(GL_TEXTURE0 + texNo);
    glBindTexture(GL_TEXTURE_2D_ARRAY, tex);


    glTexStorage3D(GL_TEXTURE_2D_ARRAY, 8, GL_RGBA8, size, size, (GLsizei) (textureArray->size()>0?textureArray->size():1));
    for(int i=0; i<textureArray->size(); i++) {
        std::pair<TextureDescription, GLbyte*> entry = textureArray->at(i);
        GLuint offsetX = 0;
        GLbyte* tmpBuf = 0;

        // what we are doing here:
        // since mipmapping expects the textures to be full size (for instance 512x512), but some textures come in other size (for instance 400x85), we are tiling the small texture into the buffer
        // eg, copying it not only into the rect (0,0,400,80), but also into (0,80,400,160), (0,170,400,240) ... (400,240,512,512)
        // not that if loading the textured scaled (see TextureManager::loadTexture and readTextureFromDisk), this code doesn't do anything.
        while(offsetX < size) {
            GLuint width = entry.first.width;
            if(width + offsetX > size) {
                width = size - offsetX;
                // this copy is needed, because naturally opengl expects that one row of image data is entry.first.width long, but with tiling it gets shorter
                // hence we need to prepare it in a way, that a row is width long.
                tmpBuf = new GLbyte[width * entry.first.height * 4];
                for(unsigned int x = 0; x < width; x++) {
                    for(unsigned int y = 0; y < entry.first.height; y++) {
                        for(int c = 0; c < 4; c++) {
                            int locationSource = y * entry.first.width * 4 + x * 4 + c;
                            int locationDest = y * width * 4 + x * 4 + c;
                            tmpBuf[locationDest] = entry.second[locationSource];
                        }
                    }
                }
            }
			GLuint offsetY = 0;
            while(offsetY < size) {
                GLuint height = entry.first.height;
                if(height + offsetY > size)
                    height = size - offsetY;
                if(tmpBuf == 0)
                    glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, offsetX,offsetY,i, width, height, 1, GL_RGBA, GL_UNSIGNED_BYTE, entry.second);
                else
                    glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, offsetX,offsetY,i, width, height, 1, GL_RGBA, GL_UNSIGNED_BYTE, tmpBuf);

                offsetY += height;
            }
            offsetX += width;
        }
        if(tmpBuf!=0) delete[] tmpBuf;
    }
    glGenerateMipmap(GL_TEXTURE_2D_ARRAY);

    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR/*_MIPMAP_LINEAR*/);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

TextureDescription TextureManager::loadTexture(std::string path)
{
    auto findResult = m_pathDescMap.find(path);
    if(findResult != m_pathDescMap.end()) {
        return findResult->second;
    }
    unsigned int width, height;
    GLbyte* smallData = nullptr;
    GLbyte* data = readTextureFromDisk(path, &width, &height, true, &smallData);
	if(data == 0) {
        std::cerr << "TextureManager::loadTexture(): failed to load texture from " << path << std::endl;
		TextureDescription desc;
		desc.height = 0;
		desc.width = 0;
		return desc;
	}

    unsigned int maxSize = std::max(width, height);
    unsigned char arrayNo;
    std::vector<std::pair<TextureDescription, GLbyte*> > * destinationArray;
    std::vector<std::pair<TextureDescription, GLbyte*> > * destinationArraySmall;
    if(maxSize <= 256) {
       arrayNo = 0;
       destinationArray = &m_texutreArray256;
       destinationArraySmall = &m_texutreArray256small;
    }
    else if(maxSize <= 512) {
       arrayNo = 1;
       destinationArray = &m_texutreArray512;
       destinationArraySmall = &m_texutreArray512small;
    }
    else if(maxSize <= 1024) {
       arrayNo = 2;
       destinationArray = &m_texutreArray1024;
       destinationArraySmall = &m_texutreArray1024small;
    }
    else {
       arrayNo = 3;
       destinationArray = &m_texutreArray2048;
       destinationArraySmall = &m_texutreArray2048small;
    }




    TextureDescription desc;
    desc.height = height;
    desc.width = width;
    desc.textureArrayNo = arrayNo;
    desc.textureIndex = (short) destinationArray->size();

    m_pathDescMap.insert(std::pair<std::string, TextureDescription>(path, desc));

    destinationArray->push_back(std::pair<TextureDescription, GLbyte*>(desc, data));
    TextureDescription tmpDesc = desc;
    tmpDesc.height = height / VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR;
    tmpDesc.width = width / VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR;
    destinationArraySmall->push_back(std::pair<TextureDescription, GLbyte*>(tmpDesc, smallData));
    return desc;
}

void TextureManager::bindTextures()
{
    for(int i=0; i<4; i++) {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D_ARRAY, m_textureArrayIds[i]);
    }
}

void TextureManager::switchFiltering(int mode)
{
    for(int i=0; i<4; i++) {
        glBindTexture(GL_TEXTURE_2D_ARRAY, m_textureArrayIds[i]);
        switch(mode)
            {
            case 0:
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                break;

            case 1:
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                break;


            case 2:
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
                break;

            case 3:
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
                break;

            case 4:
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
                break;

            case 5:
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
                break;
            }
    }
}

GLbyte* TextureManager::readTextureFromDisk(std::string path, unsigned int* widthp, unsigned int* heightp, bool scale, GLbyte **smallTexData) const
{
    // FreeImage loader
    // inspired by FreeImage/Examples/OpenGL/TextureManager

    //image format
    FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
    //pointer to the image, once loaded
    FIBITMAP *dib(0);
    //pointer to the image data
    BYTE* bits(0);
    unsigned int width(0), height(0);

    //check the file signature and deduce its format
    fif = FreeImage_GetFileType(path.c_str(), 0);
    //if still unknown, try to guess the file format from the file extension
    if(fif == FIF_UNKNOWN)
        fif = FreeImage_GetFIFFromFilename(path.c_str());
    //if still unkown, return failure
    if(fif == FIF_UNKNOWN)
        return 0;

    //check that the plugin has reading capabilities and load the file
    if(FreeImage_FIFSupportsReading(fif))
        dib = FreeImage_Load(fif, path.c_str());
    //if the image failed to load, return failure
    if(!dib)
        return 0;


    // Convert non-32 bit images
    if ( FreeImage_GetBPP( dib ) != 32 )
    {
        FIBITMAP* oldDib = dib;
        dib = FreeImage_ConvertTo32Bits( oldDib );
        FreeImage_Unload( oldDib );
    }

    //retrieve the image data
    bits = FreeImage_GetBits(dib);
    //get the image width and height
    width = FreeImage_GetWidth(dib);
    height = FreeImage_GetHeight(dib);
    //if this somehow one of these failed (they shouldn't), return failure
    if((bits == 0) || (width == 0) || (height == 0))
        return 0;

    if(width > VIENNA_MAX_TEXTURE_SIZE || height > VIENNA_MAX_TEXTURE_SIZE) {
        float maxSize = (float) std::max(width, height);
        float factor = VIENNA_MAX_TEXTURE_SIZE / maxSize;
        width = std::min(2048u, (unsigned int) (width * factor));
        height = std::min(2048u, (unsigned int) (height * factor));

        FIBITMAP* oldDib = dib;
        dib = FreeImage_Rescale(oldDib, width, height, FILTER_LANCZOS3);
        FreeImage_Unload(oldDib);
    }
    if(scale) {
        int maxSize = std::max(width, height);
        int targetSize = 256;
        if(maxSize > targetSize) targetSize = 512;
        if(maxSize > targetSize) targetSize = 1024;
        if(maxSize > targetSize) targetSize = 2048;
		width = targetSize;
		height = targetSize;
        FIBITMAP* oldDib = dib;
        dib = FreeImage_Rescale(oldDib, targetSize, targetSize, FILTER_CATMULLROM);
        FreeImage_Unload(oldDib);
    }

    FIBITMAP* smallDib = nullptr;
    if(smallTexData != nullptr) {
        smallDib = FreeImage_Rescale(dib, width/VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR, height/VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR, FILTER_LANCZOS3);
        BYTE* smallBits = FreeImage_GetBits(smallDib);

        GLbyte* destination= new GLbyte[width / VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR * height / VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR * 4];

        for ( unsigned int i = 0; i < width / VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR; ++i ) {
          for ( unsigned int j = 0; j < height / VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR; ++j ) {

            //unsigned int ppm_index = ( (height-j-1)*width + i )*3;
            unsigned int buf_index = ( j * width / VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR + i )*4;
            destination[ buf_index + 0 ] = smallBits [buf_index + FI_RGBA_RED];
            destination[ buf_index + 1 ] = smallBits [buf_index + FI_RGBA_GREEN];
            destination[ buf_index + 2 ] = smallBits [buf_index + FI_RGBA_BLUE];
            destination[ buf_index + 3 ] = smallBits [buf_index + FI_RGBA_ALPHA];
          }
        }

        *smallTexData = destination;
        //Free FreeImage's copy of the data
        FreeImage_Unload(smallDib);
    }

	bits = FreeImage_GetBits(dib);
    *widthp = width;
    *heightp = height;

    GLbyte* destination = new GLbyte[width * height * 4];

    for ( unsigned int i = 0; i < width; ++i ) {
      for ( unsigned int j = 0; j < height; ++j ) {

        //unsigned int ppm_index = ( (height-j-1)*width + i )*3;
        unsigned int buf_index = ( j * width + i )*4;
        destination[ buf_index + 0 ] = bits [buf_index + FI_RGBA_RED];
        destination[ buf_index + 1 ] = bits [buf_index + FI_RGBA_GREEN];
        destination[ buf_index + 2 ] = bits [buf_index + FI_RGBA_BLUE];
        destination[ buf_index + 3 ] = bits [buf_index + FI_RGBA_ALPHA];

//            buffer_data[ buf_index + 0 ] = raster()[ ppm_index + 0 ];
//            buffer_data[ buf_index + 1 ] = raster()[ ppm_index + 1 ];
//            buffer_data[ buf_index + 2 ] = raster()[ ppm_index + 2 ];
//            if (linearize_gamma) {
//              buffer_data[ buf_index + 0 ] = srgb2linear[buffer_data[ buf_index + 0 ]];
//              buffer_data[ buf_index + 1 ] = srgb2linear[buffer_data[ buf_index + 1 ]];
//              buffer_data[ buf_index + 2 ] = srgb2linear[buffer_data[ buf_index + 2 ]];
//            }
//            buffer_data[ buf_index + 3 ] = 255;
      }
    }

    //Free FreeImage's copy of the data
    FreeImage_Unload(dib);
    return destination;
//    return 0;
}

// based on OpenGL SuperBible Fith Edition by Wright, Haemel, Sellers and Lipchak
GLuint TextureManager::load2dTexture(const std::string& path)
{
    unsigned int width, height;
    GLbyte* bytes = readTextureFromDisk(path, &width, &height);
    if(bytes == 0) {
        std::cerr << "TextureManager::load2dTexture: Failed to load image: " + path << std::endl;
        return -1;
    }

    GLuint textureHandle;

    glGenTextures(1, &textureHandle);
    glBindTexture(GL_TEXTURE_2D, textureHandle);

    // Set up texture maps
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR/*_MIPMAP_LINEAR*/);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    // copy to gpu
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bytes);

    delete[] bytes;

    //generate mipmaps
    glGenerateMipmap(GL_TEXTURE_2D);

    //unbind the texture
    glBindTexture(GL_TEXTURE_2D, 0);

    return textureHandle;
}

// based on OpenGL SuperBible Fith Edition by Wright, Haemel, Sellers and Lipchak
GLuint TextureManager::loadCubeTexture(const std::string& posXpath,
                                       const std::string& negXpath,
                                       const std::string& posYpath,
                                       const std::string& negYpath,
                                       const std::string& posZpath,
                                       const std::string& negZpath)
{
    GLuint cubeTextureHandle;

    // Six sides of a cube map
    const std::string szCubeFaces[6] = { posXpath, negXpath, posYpath, negYpath, posZpath, negZpath};

    GLenum  cube[6] = { GL_TEXTURE_CUBE_MAP_POSITIVE_X,
                        GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
                        GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
                        GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
                        GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
                        GL_TEXTURE_CUBE_MAP_NEGATIVE_Z };

    glGenTextures(1, &cubeTextureHandle);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubeTextureHandle);

    // Set up texture maps
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    // Load Cube Map images
    for(int i = 0; i < 6; i++)
    {
        // Load this texture map
        unsigned int width, height;
        GLbyte* bytes = readTextureFromDisk(szCubeFaces[i], &width, &height);
        if(bytes == 0) {
            std::cerr << "TextureManager::load2dTexture: Failed to load image: " + szCubeFaces[i] << std::endl;
            return -1;
        }
        glTexImage2D(cube[i], 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bytes);

        delete[] bytes;
    }

    //generate mipmaps
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
    //unbind the texture
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    return cubeTextureHandle;
}

