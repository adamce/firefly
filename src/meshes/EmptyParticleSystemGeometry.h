#ifndef EMPTYPARTICLESYSTEMGEOMETRY_H
#define EMPTYPARTICLESYSTEMGEOMETRY_H

#include <iostream>

#include <GL/glew.h>

#include "MeshVertex.h"
#include "shaders/Shader.h"
#include <string.h>
#include "auxiliary/randomNumber.h"

void setupEmptyParticleSystemGeometry(MeshVertex** p_vertexDataPointer, GLsizeiptr* p_vertexDataSize, GLuint** p_indexDataPointer, GLsizeiptr* p_indexDataSize, int* p_numElements, int* p_numVerticesPerElement) {
    MeshVertex* vertices;
    vertices = new MeshVertex[Shader::NUM_VERTICES_IN_PARTICLE_SYSTEM];
    for(unsigned int i=0; i<Shader::NUM_VERTICES_IN_PARTICLE_SYSTEM; i++) {
        vertices[i].setPosition(0, 0, 0);
        vertices[i].setNormal(0, 0, 0);
        vertices[i].setTextureCoords(-10, -10);
    }
    for (unsigned int i=0; i<Shader::NUM_GENERATOR_VERTICES_IN_PARTICLE_SYSTEM; i++) {
        vertices[i].setTextureCoords(0, -5);
        float rndDist = 0.1f;
        vertices[i].setPosition(randomNumber(-rndDist, rndDist), randomNumber(-rndDist, rndDist), randomNumber(-rndDist, rndDist));
        vertices[i].setNormal(randomNumber(-1, 1), randomNumber(-1, 1), randomNumber(-1, 1));
    }

    GLuint* indices;
    indices = new GLuint[Shader::NUM_VERTICES_IN_PARTICLE_SYSTEM];

    for(unsigned int i=0; i<Shader::NUM_VERTICES_IN_PARTICLE_SYSTEM; i++) {
        indices[i] = i;
    }

    *p_vertexDataPointer = vertices;
    *p_vertexDataSize = Shader::NUM_VERTICES_IN_PARTICLE_SYSTEM*sizeof(MeshVertex);
    *p_indexDataPointer = indices;
    *p_indexDataSize = Shader::NUM_VERTICES_IN_PARTICLE_SYSTEM*sizeof(GLuint);
    *p_numElements = Shader::NUM_GENERATOR_VERTICES_IN_PARTICLE_SYSTEM;
    *p_numVerticesPerElement = 1;
}


#endif // SPACEDUSTGEOMETRY_H
