#ifndef SKYBOXGEOMETRY_H
#define SKYBOXGEOMETRY_H

#include "MeshVertex.h"

void setupSkyboxGeometry(MeshVertex** p_vertexDataPointer, GLsizeiptr* p_vertexDataSize, GLuint** p_indexDataPointer, GLsizeiptr* p_indexDataSize, int* p_numElements, int* p_numVerticesPerElement) {

    GLfloat radius = 500.0f;
    int numTriangles = 12;
    int vertexCount= 8;
    // this are the vertices of a very simple space ship, created by hand.
    MeshVertex* vertices = new MeshVertex[vertexCount];
    vertices[0].setValues(radius, radius, radius);
    vertices[1].setValues(-radius, radius, radius);
    vertices[2].setValues(-radius, radius, -radius);
    vertices[3].setValues(radius, radius, -radius);
    vertices[4].setValues(radius, -radius, radius);
    vertices[5].setValues(-radius, -radius, radius);
    vertices[6].setValues(-radius, -radius, -radius);
    vertices[7].setValues(radius, -radius, -radius);

    GLuint indices[] ={
        0,5,1,  //back
        0,4,5,  //back
        3,2,6,  //front
        3,6,7,  //front
        2,1,5,  //left
        2,5,6,  //left
        0,3,7,  //right
        0,7,4,  //right
        0,1,2,  //top
        0,2,3,  //top
        7,6,5,  //bottom
        7,5,4   //bottom
    };

    // we need to allocate these indices on the heap, but don't want to address every index separately. so we create them on the stack and then copy to the heap.
    GLuint* indicesOnHeap = new GLuint[numTriangles*3];
    for(int i=0; i<numTriangles*3; i++) {
        indicesOnHeap[i] = indices[i];
    }

    *p_vertexDataPointer = vertices;
    *p_vertexDataSize = vertexCount*sizeof(MeshVertex);
    *p_indexDataPointer = indicesOnHeap;
    *p_indexDataSize = numTriangles*3*sizeof(GLuint);
    *p_numElements = numTriangles;
    *p_numVerticesPerElement = 3;
}

#endif // SKYBOXGEOMETRY_H
