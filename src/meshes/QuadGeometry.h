#ifndef QUADGEOMETRY_H
#define QUADGEOMETRY_H

#include "MeshVertex.h"


void setupQuadGeometry(MeshVertex** p_vertexDataPointer, GLsizeiptr* p_vertexDataSize, GLuint** p_indexDataPointer, GLsizeiptr* p_indexDataSize, int* p_numElements, int* p_numVerticesPerElement) {

    GLfloat radius = 1.f;
    MeshVertex vertices[] = {
        MeshVertex(-radius, -radius, 0),
        MeshVertex(-radius, radius, 0),
//         MeshVertex(0, radius, 0), // these two entries are for half screen
//         MeshVertex(0, -radius, 0)
		MeshVertex(radius, radius, 0), // these two for full screen
		MeshVertex(radius, -radius, 0)
	};

    vertices[0].setTextureCoords(0, 0);
    vertices[1].setTextureCoords(0, 1);
    vertices[2].setTextureCoords(1, 1);
    vertices[3].setTextureCoords(1, 0);

	GLuint indices[] ={
		2,1,0,
		3,2,0
	};
	int numTriangles = 2;
	int vertexCount= 4;

    // we need to allocate these indices and vertices on the heap, but don't want to address every index separately. so we create them on the stack and then copy to the heap.
    GLuint* indicesOnHeap = new GLuint[numTriangles*3];
    for(int i=0; i<numTriangles*3; i++) {
        indicesOnHeap[i] = indices[i];
    }
    MeshVertex* verticesOnHeap = new MeshVertex[vertexCount];
    for(int i=0; i<vertexCount; i++) {
        verticesOnHeap[i] = vertices[i];
    }


    *p_vertexDataPointer = verticesOnHeap;
    *p_vertexDataSize = vertexCount*sizeof(MeshVertex);
    *p_indexDataPointer = indicesOnHeap;
    *p_indexDataSize = numTriangles*3*sizeof(GLuint);
    *p_numElements = numTriangles;
    *p_numVerticesPerElement = 3;
}




#endif // QUADGEOMETRY_H
