#ifndef TEXTUREMANAGER_H
#define TEXTUREMANAGER_H

#include <string>
#include <GL/glew.h>
#include <map>
#include <vector>

#include "MaterialDescription.h"

class TextureManager
{
	TextureManager(void);
	static TextureManager* singletonPointer;
public:
	~TextureManager(void);
	static TextureManager* instance();

	GLuint get2DTexture(const std::string& path);
    GLuint getCubeTexture(
        const std::string& posXpath,
        const std::string& negXpath,
        const std::string& posYpath,
        const std::string& negYpath,
        const std::string& posZpath,
        const std::string& negZpath);

    void openGlUpload();
    TextureDescription loadTexture(std::string path);
    void bindTextures();
    void switchFiltering(int mode);

    const std::vector<std::pair<TextureDescription, GLbyte*> >& textureArray256() const { return m_texutreArray256; }
    const std::vector<std::pair<TextureDescription, GLbyte*> >& textureArray512() const { return m_texutreArray512; }
    const std::vector<std::pair<TextureDescription, GLbyte*> >& textureArray1024() const { return m_texutreArray1024; }
    const std::vector<std::pair<TextureDescription, GLbyte*> >& textureArray2048() const { return m_texutreArray2048; }

    const std::vector<std::pair<TextureDescription, GLbyte*> >& textureArray256small() const { return m_texutreArray256small; }
    const std::vector<std::pair<TextureDescription, GLbyte*> >& textureArray512small() const { return m_texutreArray512small; }
    const std::vector<std::pair<TextureDescription, GLbyte*> >& textureArray1024small() const { return m_texutreArray1024small; }
    const std::vector<std::pair<TextureDescription, GLbyte*> >& textureArray2048small() const { return m_texutreArray2048small; }

private:
    void openGlUpload(std::vector<std::pair<TextureDescription, GLbyte*> >* textureArray, GLuint tex, GLuint size, int texNo);
    GLbyte* readTextureFromDisk(std::string path, unsigned int* width, unsigned int* height, bool scale = false, GLbyte **smallTexData = nullptr) const;
    GLuint load2dTexture(const std::string& path);
    GLuint loadCubeTexture(
        const std::string& posXpath,
        const std::string& negXpath,
        const std::string& posYpath,
        const std::string& negYpath,
        const std::string& posZpath,
        const std::string& negZpath);

    std::map<std::string, TextureDescription> m_pathDescMap;
    std::vector<std::pair<TextureDescription, GLbyte*> > m_texutreArray256;
    std::vector<std::pair<TextureDescription, GLbyte*> > m_texutreArray512;
    std::vector<std::pair<TextureDescription, GLbyte*> > m_texutreArray1024;
    std::vector<std::pair<TextureDescription, GLbyte*> > m_texutreArray2048;

    std::vector<std::pair<TextureDescription, GLbyte*> > m_texutreArray256small;
    std::vector<std::pair<TextureDescription, GLbyte*> > m_texutreArray512small;
    std::vector<std::pair<TextureDescription, GLbyte*> > m_texutreArray1024small;
    std::vector<std::pair<TextureDescription, GLbyte*> > m_texutreArray2048small;
    std::map<std::string, GLuint> m_pathToHandleMapper;
    GLuint m_textureArrayIds[4];
};

#endif // TEXTUREMANAGER_H
