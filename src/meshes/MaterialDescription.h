#ifndef MATERIALDESCRIPTION_H
#define MATERIALDESCRIPTION_H

#define VIENNA_MATERIAL_TYPE_TEXTURED 1
#define VIENNA_MATERIAL_TYPE_FLAT_COLOUR 2
#define VIENNA_MATERIAL_TYPE_MIRROR 3
#define VIENNA_MATERIAL_TYPE_MACRO 4
#define VIENNA_MATERIAL_TYPE_GLOWING 5

//#include <cuda/helper_math.h>

//struct TextureDescription { // 8 bytes  // hehe, there is no short or byte in glsl
//    short textureIndex; // 0-512    // 9 bit
//    short width;        // 0-2048   // 11 bit
//    short height;       // 0-2048   // 11 bit
//    unsigned char textureArrayNo;  // atm 0-4  // 2 bit
//    unsigned char padding;          // sum 33 bit: if we cut texture index to 8 bit, we could reduce to 32 bit -> 4 bytes
//};

//// if you add or remove members, remember to update CudaDataManager::addMaterialDescription
//struct MaterialDescription {
//    TextureDescription normalMapTexture, diffuseTexture;   //16 bytes
//    short phongExponent;
//    unsigned char type;
//    unsigned char padding;
//};  // 20 bytes


//##### note that the same struct in glsl also needs update #####
struct TextureDescription { // hehe, there is no short or byte in glsl // but we can use a smaller struct for CUDA!! But this will be a separate struct since we are addressing ints
    unsigned int textureIndex;
    unsigned int width;
    unsigned int height;
    unsigned int textureArrayNo;
};              // 16 bytes

//##### if you add or remove members, remember to update DataManager::addMaterialDescription ######
//##### note that the same struct in glsl and the uniform update code also needs update #####
//##### and finally it needs update in the mesh loading method, but there should be compile time errors for that #####
//##### for now the opengl shader can load only unsigned ints #####
struct MaterialDescription {
    TextureDescription diffuseTexture, normalMapTexture; //32 bytes
    unsigned int diffuseRGBA;    // A being a boost for bloom / brighter than white.
    float factor;
    unsigned int type;
};  // 40 bytes

#endif // MATERIALDESCRIPTION_H
