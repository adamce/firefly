#ifndef SPACEDUSTGEOMETRY_H
#define SPACEDUSTGEOMETRY_H

#include <iostream>

#include <GL/glew.h>

#include "MeshVertex.h"
#include "shaders/Shader.h"
#include <string.h>
#include "auxiliary/randomNumber.h"


typedef MeshVertex MyVertex;

void setupSpaceDustGeometry(MeshVertex** p_vertexDataPointer, GLsizeiptr* p_vertexDataSize, GLuint** p_indexDataPointer, GLsizeiptr* p_indexDataSize, int* p_numElements, int* p_numVerticesPerElement) {
    float x_from = -100;
    float x_to = 100;
    float y_from= -100;
    float y_to = 100;
    float z_from = -100;
    float z_to = 100;

    MyVertex* vertices;
    int numVertices = 20000;
    vertices = new MyVertex[numVertices];

    GLuint* indices;
    indices = new GLuint[numVertices];

    for(int i=0; i<numVertices; i++) {
        vertices[i].setValues(randomNumber(x_from, x_to), randomNumber(y_from, y_to), randomNumber(z_from, z_to));
        vertices[i].setNormal(randomNumber(-1.0f, 1.0f), randomNumber(-1.0f, 1.0f), randomNumber(-1.0f, 1.0f));
        vertices[i].setTextureCoords(randomNumber(0.0f, 10.0f), randomNumber(0.0f, 1.0f));
    }

    for(int i=0; i<numVertices; i++) {
        indices[i] = i;
    }

    *p_vertexDataPointer = vertices;
    *p_vertexDataSize = numVertices*sizeof(MeshVertex);
    *p_indexDataPointer = indices;
    *p_indexDataSize = numVertices*sizeof(GLuint);
    *p_numElements = numVertices;
    *p_numVerticesPerElement = 1;
}


#endif // SPACEDUSTGEOMETRY_H
