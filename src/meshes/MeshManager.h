#ifndef MESHMANAGER_H
#define MESHMANAGER_H

#include <map>
#include <vector>

#include "Mesh.h"

enum MeshType {ShipMesh,
               EmptyMesh,
               SkyboxMesh,
               SpaceDustMesh,
               FlareMesh,
               LowPolySphereMesh,
               ScreenQuadMesh
};

class MeshManager
{
	MeshManager();
	MeshManager(MeshManager const&){}
	~MeshManager();
	MeshManager& operator=(MeshManager const&){return *this;}
	static MeshManager* singletonPointer;
public:
	static MeshManager* instance();
	Mesh* loadMesh(MeshType type);
    Mesh* loadMesh(const std::string& filename);
	std::map<GLuint, glm::vec3> skyboxes() const;
	void switchSkybox();
	glm::vec3 getCurrentSunPosition() const;
    std::string basePath() const { return m_basePath; }
private:
    std::string m_basePath;
    std::map<std::string, Mesh*> m_meshes;

	//std::map<MeshType, const Mesh*> m_meshes;
	std::map<GLuint, glm::vec3> m_skyboxes;
	std::map<GLuint, glm::vec3>::iterator m_skyboxesIter;
};

#endif // MESHMANAGER_H
