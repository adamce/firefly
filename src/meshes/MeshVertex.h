/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MESHVERTEX_H
#define MESHVERTEX_H

#include <GL/glew.h>
#include <iostream>
#include <glm/glm.hpp>

struct MeshVertex
{
    MeshVertex(GLfloat x_=0.f, GLfloat y_=0.f, GLfloat z_=0.f,
               GLfloat r_=1.f, GLfloat g_=1.f, GLfloat b_=1.f,
               GLfloat nx_=0.f, GLfloat ny_=0.f, GLfloat nz_=1.f) :
                    x(x_), y(y_), z(z_), w(1.0),
                    nx(nx_), ny(ny_), nz(nz_), nw(1.0),
                    s0(0.f), t0(0.f),
                    materialIndex(0)
               {}

               void setValues(GLfloat x_=0.f, GLfloat y_=0.f, GLfloat z_=0.f,
                              GLfloat r_=1.f, GLfloat g_=1.f, GLfloat b_=1.f,
                              GLfloat nx_=0.f, GLfloat ny_=0.f, GLfloat nz_=1.f,
                              GLuint materialIndex = 0)
               {
                   x=x_; y=y_; z=z_;
                   nx=nx_; ny=ny_; nz=nz_;
                   this->materialIndex = materialIndex;
               }

               void setPosition(GLfloat x_=0.f, GLfloat y_=0.f, GLfloat z_=0.f)
               { x=x_; y=y_; z=z_; }

               void setNormal(GLfloat nx_, GLfloat ny_, GLfloat nz_)
               { nx=nx_; ny=ny_; nz=nz_; }

               void setTextureCoords(GLfloat s0_,GLfloat t0_)
               { s0=s0_; t0=t0_;}

               void setMaterialIndex(GLuint value) {
                   materialIndex = value;
               }

               glm::vec3 position() const {
                   return glm::vec3(x, y, z);
               }

               glm::vec4 position4() const {
                   return glm::vec4(x, y, z, w);
               }

               glm::vec3 normal() const {
                   return glm::vec3(nx, ny, nz);
               }

               glm::vec2 texCoords() const {
                   return glm::vec2(s0, t0);
               }

               // if you add something here, remember to also add it to all particle shaders, so that transform feedback works.
               // new format for gdebugger support is T2F_C4F_N3F_V3F
               GLfloat x, y, z, w;        //Vertex
               GLfloat nx, ny, nz, nw;     //Normal
               GLfloat s0, t0;         //Texcoord0, vertex param 1 and 2
               GLint materialIndex, p;
               //    float s1, t1;         //Texcoord1
               //    float s2, t2;         //Texcoord2
               //    float padding[4];
};

std::ostream& operator<<(std::ostream &stream, const MeshVertex& v);

#endif // MESHVERTEX_H
