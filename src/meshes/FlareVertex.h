#ifndef FLAREVERTEX_H
#define FLAREVERTEX_H

#include <GL/glew.h>
#include <iostream>

struct FlareVertex
{
    FlareVertex(GLfloat x_=0., GLfloat y_=0., GLfloat z_=0., GLfloat size_= 1., GLint type_ = 1, GLfloat r_=1, GLfloat g_=1, GLfloat b_=1, GLfloat a_=1) :
               x(x_), y(y_), z(z_),
               size(size_), type(type_) {}

               void setValues(GLfloat x_=0., GLfloat y_=0., GLfloat z_=0.,
                              GLfloat size_= 1., GLint type_ = 1)
               {
                   x=x_;
				   y=y_;
				   z=z_;
					size=size_;
					type=type_;
               }
			   void setPosition(GLfloat x_=0., GLfloat y_=0., GLfloat z_=0.)
			   {
				   x=x_;
				   y=y_;
				   z=z_;
			   }

               void setSize(GLfloat size_)
			   {
				   size=size_;
			   }

			   void setColor(GLfloat r_, GLfloat g_, GLfloat b_, GLfloat a_=1)
               {
                   r=r_;
                   g=g_;
                   b=b_;
                   a=a_;
               }

			   void setType(GLint type_)
			   {
				   type=type_;
			   }



               GLfloat x, y, z;        //Vertex
			   GLfloat size;
			   GLint type;	//used to access texture array
			   GLfloat r, g, b, a;

};

std::ostream& operator<<(std::ostream &stream, const FlareVertex& v);

#endif // FLAREVERTEX_H