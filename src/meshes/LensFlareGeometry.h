#ifndef LENSFLAREGEOMETRY_H
#define LENSFLAREGEOMETRY_H

#include <iostream>
#include <stdlib.h>

#include <GL/glew.h>

#include "FlareVertex.h"
#include "shaders/Shader.h"
#include "auxiliary/randomNumber.h"

void setupLensFlareGeometry(GLuint* vaoId, int* numElements, int* numVerticesPerElement) {


	float x_from= -1.0f;

	FlareVertex* vertices;
	int numVertices = 10;
	vertices = new FlareVertex[numVertices];

	GLuint* indices;
	indices = new GLuint[numVertices];

    float delta_x = 2.0f/(((float) numVertices)-1.f);


	for(int i=0; i<numVertices; i++) {
		vertices[i].setPosition(x_from, 0.0f , 0.0f);
        vertices[i].setColor(randomNumber(0.f, 1.f), randomNumber(0.f, 1.f), randomNumber(0.f, 1.f), randomNumber(0.8f, 1.f));
		x_from += delta_x;
	}

	for(int i=0; i<numVertices; i++) {
		indices[i] = i;
	}

	//set sizes
	vertices[0].setSize(1.0f);
	vertices[1].setSize(0.9f);
	vertices[2].setSize(0.2f);
	vertices[3].setSize(0.3f);
	vertices[4].setSize(0.6f);
	vertices[5].setSize(0.1f);
	vertices[6].setSize(0.4f);
	vertices[7].setSize(0.3f);
	vertices[8].setSize(0.2f);
	vertices[9].setSize(0.6f);

	//set types
	vertices[0].setType(1);
	vertices[1].setType(1);
	vertices[2].setType(0);
	vertices[3].setType(2);
	vertices[4].setType(1);
	vertices[5].setType(1);
	vertices[6].setType(2);
	vertices[7].setType(0);
	vertices[8].setType(1);
	vertices[9].setType(1);

	/* Allocate and assign a Vertex Array Object to our handle */
	glGenVertexArrays(1, vaoId);

	/* Bind our Vertex Array Object as the current used object */
	glBindVertexArray(*vaoId);

    GLuint vboId, iboId;
	glGenBuffers(1, &vboId);
	glGenBuffers(1, &iboId);

	/* Bind our VBO and IBO as being the active buffer and storing vertex attributes and indices*/
	glBindBuffer(GL_ARRAY_BUFFER, vboId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iboId);

	/* Copy the vertex data from vertices and indices to our buffer */
	glBufferData(GL_ARRAY_BUFFER, numVertices*sizeof(FlareVertex), vertices, GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numVertices*sizeof(GLuint), indices, GL_STATIC_DRAW);


    *numElements = numVertices;
    *numVerticesPerElement = 1;

	delete[] vertices;
	delete[] indices;

	Shader::setupFlareAttribPointers();
	glBindVertexArray(0);
}


#endif // LENSFLAREGEOMETRY_H
