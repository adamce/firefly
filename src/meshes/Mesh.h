#ifndef MESH_H
#define MESH_H

#include <iostream>
#include <set>

#include <GL/glew.h>
#define GLM_FORCE_RADIANS
#include "glm/glm.hpp"

class btGImpactMeshShape;
class btCollisionShape;
struct MeshVertex;
struct MaterialDescription;

class Mesh
{
protected:
    /// must be called only by MeshManager.
    Mesh(GLenum drawMode);
    virtual ~Mesh();

    /**
     * these methods must be called only by MeshManager
     */
    void loadFromFunction(void (*geometrySetupFunction)(MeshVertex**, GLsizeiptr*, GLuint**, GLsizeiptr*, int*, int*));
    void loadFromFunction(void (*geometrySetupFunction)(GLuint* vaoId, int* numElements, int* numVerticesPerElement));
    void loadFromFile(std::string path, glm::vec4 specialDiffuseColour = glm::vec4(-1));
public:
    void activate();
    virtual void draw(const glm::mat4& modelMatrix, const glm::mat4& viewProjectionMatrix);

	GLuint* cubeTextureHandle();
	GLuint* texture2dHandle();
    btCollisionShape* collisionShape() const;
	float boundingSphereRadius() const;
	void switchTextureFiltering(int value) const;
	glm::vec3 boundingSphereCenter() const;
	bool hasBoundingSphere() const;
	void setCubeTextureHandle(GLuint id);
    int numElements() const;
    void setNumElements(int num); // used by particle systems
    GLsizeiptr vertexDataSize() const { return m_vertexDataSize; }

    const MeshVertex* vertexData() const { return m_vertexDataPointer; }
    int numberOfVertices() const { return m_numVertices; }
    const GLuint* indexData() const { return m_indexDataPointer; }
    int numberOfTriangles() const { return m_drawMode == GL_TRIANGLES ? m_numElements : 0; }

    GLuint vaoId() const { return m_vaoId; }
    GLuint vboId() const { return m_vboId; }
    GLuint iboId() const { return m_iboId; }


protected:
    bool useBufferedGeometry();
    void setupData(MeshVertex* vertexDataPointer, GLsizeiptr vertexDataSize, int numVertices, GLuint* indexDataPointer, GLsizeiptr indexDataSize, int numElements, int numVerticesPerElement);
	void setupBoundingSphere(MeshVertex* vertexDataPointer, int numVertices);
    void loadMyMaterialFileParams(MaterialDescription* myMat, const std::string& stdName);
// protected:
    GLenum m_drawMode;
    GLuint m_vaoId, m_iboId, m_vboId;
	GLuint m_cubeTextureHandle;
	GLuint m_texture2dHandle;
	GLuint m_texture2dHandle_1;
	std::set<GLuint> m_texture2DHandles;

    MeshVertex* m_vertexDataPointer;
    GLuint* m_indexDataPointer;
    GLsizeiptr m_vertexDataSize;
    GLsizeiptr m_indexDataSize;
    int m_numElements;
    int m_numVerticesPerElement;
    int m_numVertices = 0;

	glm::vec3 m_boundingSphereCenter;
	float m_boundingSphereRadius;

    bool m_meshSupportsUnbufferedGeometry;

	bool m_hasBoundingSphere;

    btGImpactMeshShape* m_collisionShape;

    friend class MeshManager;
    //very dirty code, off gas beam needs to create a separate mesh, because the vertices are changed on the graphics card.
    friend class OffGasBeam;
};

#endif // MESH_H
