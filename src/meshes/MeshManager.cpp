#include "MeshManager.h"

#include <iostream>

#include "Mesh.h"
#include "TextureManager.h"
#include "SpaceDustGeometry.h"
#include "LensFlareGeometry.h"
#include "QuadGeometry.h"
#include "SkyboxGeometry.h"
#include "EmptyParticleSystemGeometry.h"
#include "auxiliary/ApplicationConstants.h"


MeshManager* MeshManager::singletonPointer = 0;

MeshManager::MeshManager() : m_basePath(VIENNA_MODEL_BASE_PATH)
{
	TextureManager* textureManager = TextureManager::instance();

	GLuint skyboxHandle = textureManager->getCubeTexture("../data/textures/skybox/violentdays_ft.tga",
                                                         "../data/textures/skybox/violentdays_bk.tga",
                                                         "../data/textures/skybox/violentdays_dn.tga",
                                                         "../data/textures/skybox/violentdays_up.tga",
                                                         "../data/textures/skybox/violentdays_rt.tga",
                                                         "../data/textures/skybox/violentdays_lf.tga");

	GLuint skyboxHandle2 = textureManager->getCubeTexture("../data/textures/skybox/stormydays_ft.tga",
                                                         "../data/textures/skybox/stormydays_bk.tga",
                                                         "../data/textures/skybox/stormydays_dn.tga",
                                                         "../data/textures/skybox/stormydays_up.tga",
                                                         "../data/textures/skybox/stormydays_rt.tga",
                                                         "../data/textures/skybox/stormydays_lf.tga");

	GLuint skyboxHandle3 = textureManager->getCubeTexture("../data/textures/skybox/my_sky_1024_right1.tga",
                                                         "../data/textures/skybox/my_sky_1024_left2.tga",
														 "../data/textures/skybox/my_sky_1024_bottom4.tga",
														 "../data/textures/skybox/my_sky_1024_top3.tga",
														 "../data/textures/skybox/my_sky_1024_front5.tga",
														 "../data/textures/skybox/my_sky_1024_back6.tga");

	//violendays sunpos (150,-118,225)
	glm::vec3 sunpos = glm::vec3(337.436, -189.451, 319.029);
	m_skyboxes.insert(std::pair<GLuint, glm::vec3>(skyboxHandle , sunpos));
	m_skyboxes.insert(std::pair<GLuint, glm::vec3>(skyboxHandle2 , sunpos));
	//my_sky sun pos(-11,-10, 430)
	sunpos = glm::vec3(-11,-10, 430);
	m_skyboxes.insert(std::pair<GLuint, glm::vec3>(skyboxHandle3 , sunpos));

	m_skyboxesIter = m_skyboxes.begin();
}

MeshManager::~MeshManager()
{
    for (std::map<std::string, Mesh*>::iterator iter = m_meshes.begin(); iter != m_meshes.end(); iter++) {
        delete iter->second;
    }
}

MeshManager* MeshManager::instance() {
    if(singletonPointer==0)
        singletonPointer=new MeshManager();
    return singletonPointer;
}

Mesh* MeshManager::loadMesh(MeshType type) {
    std::string generatedName = "MeshType=" + type;
    if(m_meshes.count(generatedName)) {
        return m_meshes.at(generatedName);
    }

    Mesh* newMesh;

    TextureManager* textureManager = TextureManager::instance();

	GLuint skyboxHandle = (*m_skyboxesIter).first;

    switch(type) {
        case ShipMesh:
            newMesh = new Mesh(GL_TRIANGLES);
            newMesh->loadFromFile(m_basePath + "franklin2.obj");
            newMesh->m_cubeTextureHandle = skyboxHandle;
            break;
        case SkyboxMesh:
            newMesh = new Mesh(GL_TRIANGLES);
            newMesh->loadFromFunction(&setupSkyboxGeometry);
            newMesh->m_cubeTextureHandle = skyboxHandle;
            break;
        case FlareMesh:
            newMesh = new Mesh(GL_POINTS);
            newMesh->loadFromFunction(setupLensFlareGeometry);
            newMesh->m_texture2DHandles.insert(textureManager->get2DTexture("../data/textures/lensflare/flare0_0.tga"));
            newMesh->m_texture2DHandles.insert(textureManager->get2DTexture("../data/textures/lensflare/flare1_1.tga"));
            newMesh->m_texture2DHandles.insert(textureManager->get2DTexture("../data/textures/lensflare/flare2_2.tga"));
            break;
        case SpaceDustMesh:
            newMesh = new Mesh(GL_POINTS);
            newMesh->loadFromFunction(&setupSpaceDustGeometry);
			newMesh->m_texture2DHandles.insert(textureManager->get2DTexture("../data/textures/dust_02.tga"));
            break;
		case LowPolySphereMesh:
            newMesh = new Mesh(GL_TRIANGLES);
            newMesh->loadFromFile(m_basePath + "lowPolySphere.obj");
            break;
        case ScreenQuadMesh:
            newMesh = new Mesh(GL_TRIANGLES);
            newMesh->loadFromFunction(&setupQuadGeometry);
            break;
        default:
            std::cerr << "MeshManager::loadMesh: Trying to load an unknown mesh!" << std::endl;
            break;
    }

    m_meshes.insert(std::pair<std::string, Mesh*>(generatedName, newMesh));

    return newMesh;
}

Mesh *MeshManager::loadMesh(const std::string &filename)
{
    if(m_meshes.count(filename)) {
        return m_meshes.at(filename);
    }

    Mesh* newMesh;

    GLuint skyboxHandle = (*m_skyboxesIter).first;

    newMesh = new Mesh(GL_TRIANGLES);
    newMesh->loadFromFile(m_basePath + filename);

    newMesh->m_cubeTextureHandle = skyboxHandle;

    m_meshes.insert(std::pair<std::string, Mesh*>(filename, newMesh));

    return newMesh;
}

std::map<GLuint, glm::vec3> MeshManager::skyboxes() const
{
	return m_skyboxes;
}

void MeshManager::switchSkybox()
{

		m_skyboxesIter++;
	if(m_skyboxesIter == m_skyboxes.end())
		m_skyboxesIter = m_skyboxes.begin();

    for(std::map<std::string, Mesh*>::iterator iter = m_meshes.begin(); iter != m_meshes.end(); iter++){
		(*iter).second->setCubeTextureHandle((*m_skyboxesIter).first);
	}
}

glm::vec3 MeshManager::getCurrentSunPosition() const
{
	return (*m_skyboxesIter).second;
}

