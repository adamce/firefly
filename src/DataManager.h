#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include <vector>
#include <utility>
#include "meshes/MaterialDescription.h"

class DataManager
{
    DataManager();
    DataManager(DataManager const&){}
    DataManager& operator=(DataManager const&){return *this;}
    static DataManager* singletonPointer;
public:
    static DataManager* instance();
    int addMaterialDescription(const MaterialDescription& description);
    const std::vector<MaterialDescription>& getMaterialDescriptions() const { return m_materialDescriptions; }
private:
    std::vector<MaterialDescription> m_materialDescriptions;
};

#endif // DATAMANAGER_H
