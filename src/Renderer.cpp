/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Renderer.h"

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <chrono>
#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/gl.h>

#include <FreeImage.h>

#include <btBulletDynamicsCommon.h>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Application.h"
#include "ObjectManager.h"
#include "Framebuffer.h"
#include "shaders/ShaderManager.h"
#include "shaders/Shader.h"
#include "objects/Object.h"
#include "objects/Ship.h"
#include "objects/Camera.h"
#include "objects/LensFlare.h"
#include "meshes/MeshManager.h"
#include "meshes/TextureManager.h"
#include "cuda/Cuda.h"
#include "renderPasses/MainRenderPass.h"
#include "renderPasses/PostProcessPass.h"
#include "renderPasses/ParticleSystemsPass.h"
#include "renderPasses/DebugDrawPass.h"
#include "renderPasses/ShadowMapPass.h"
#include "auxiliary/ApplicationConstants.h"
#include "auxiliary/IngameGui.h"
#include "auxiliary/ViewPlane.h"
#include "auxiliary/OpenGlDebug.h"
#include "auxiliary/HelperOperations.h"

using std::cout;
using std::endl;

Renderer::Renderer()
{
    m_useVSync = false;
}

Renderer::~Renderer() {
    glfwDestroyWindow(m_windowHandle);
    for(RenderPass* rp : m_renderPasses) {
        delete rp;
    }
    delete m_resultFramebuffer;
}

void VIENNA_STDCALL resizeFun(GLFWwindow*, int w, int h)
{
    Application::instance()->renderer()->resize(w, h);
}

void VIENNA_STDCALL mouseButtonFun(GLFWwindow*, int button, int action, int mods)
{
    double x, y;
    if(action != GLFW_PRESS || button != GLFW_MOUSE_BUTTON_RIGHT)
        return;
    glfwGetCursorPos(Application::instance()->renderer()->glfwWindowHandle(), &x, &y);
    Application::instance()->renderer()->cudaManager()->setMouseDebugPos((int) std::floor(x), (int) std::floor(y));
}

void Renderer::init() {
    m_width = VIENNA_DEFAULT_WINDOW_WIDTH;
    m_height = VIENNA_DEFAULT_WINDOW_HEIGHT; // will be overwritten
    glfwInit();

    // ## Open an OpenGL window
    glewExperimental = GL_TRUE;
#ifdef VIENNA_DEBUG
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#else
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    m_windowHandle = glfwCreateWindow ( m_width, m_height, "Firefly", 0, 0);
    if (m_windowHandle == 0) {
        std::cerr << "Could not create window" << std::endl;
        glfwTerminate();
        exit(1);
    }
    glfwMakeContextCurrent(m_windowHandle);

    GLenum err = glewInit();

    if ( err != GLEW_OK ) {
        std::cerr << "GLEW Error: " << glewGetErrorString ( err );
        exit ( 1 );
    }

    int glVersion[3] = {-1, -1, -1}; // Set some default values for the version
    glVersion[0] = glfwGetWindowAttrib(m_windowHandle, GLFW_CONTEXT_VERSION_MAJOR);
    glVersion[1] = glfwGetWindowAttrib(m_windowHandle, GLFW_CONTEXT_VERSION_MINOR);
    glVersion[2] = glfwGetWindowAttrib(m_windowHandle, GLFW_CONTEXT_REVISION);
    std::cout << "Using OpenGL: " << glVersion[0] << "." << glVersion[1] << "." << glVersion[2] << std::endl;
//    GLint data;
//    glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &data);
//    std::cout << "GL_MAX_ARRAY_TEXTURE_LAYERS: " << data << std::endl;

#if VIENNA_DEBUG
    addOpenGlDebugCallback();
    glEnable(GL_POINT_SPRITE);  //needed for compat profile
#endif

    // ## set some defaults for opengl
//     glfwSwapInterval(1); // vsync on
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_PROGRAM_POINT_SIZE);
     glCullFace(GL_BACK);
     glFrontFace(GL_CCW);



    m_resultFramebuffer = new Framebuffer();

    m_mainRenderPass = new MainRenderPass(m_resultFramebuffer, &m_shadowMapPasses);
    m_postProcessPass = new PostProcessPass(m_resultFramebuffer);
    m_particleSystemsPass = new ParticleSystemsPass(m_resultFramebuffer);
    m_debugDrawPass = new DebugDrawPass(m_resultFramebuffer);
    m_renderPasses.insert(m_mainRenderPass);
    m_renderPasses.insert(m_postProcessPass);
    m_renderPasses.insert(m_particleSystemsPass);
    m_renderPasses.insert(m_debugDrawPass);

    for(RenderPass* rp : m_renderPasses) {
        rp->init();
    }
	
    resize(m_width, m_height); //generates buffers, sets viewports for cameras etc

    glfwSetWindowSizeCallback(m_windowHandle, &resizeFun);
    glfwSetMouseButtonCallback(m_windowHandle, &mouseButtonFun);
}

void Renderer::initSceneOld() {
	std::cout << "Renderer::initScene()" << std::endl;
    glClearColor(0.,0.,0.,1.);
}

void Renderer::applicationInitFinished()
{
	for (Light* light : ObjectManager::instance()->lights()) {
        ShadowMapPass* lightPass = new ShadowMapPass(light);
		lightPass->init();
        m_shadowMapPasses.push_back(lightPass);
		m_renderPasses.insert(lightPass);
    }

    for(RenderPass* rp : m_renderPasses) {
        rp->applicationInitFinished();
    }
}

void Renderer::resize(int w, int h) {

    m_width = w;
    m_height = h;

    if(Application::instance()->camera()!=0)
        Application::instance()->camera()->setViewport(w, h);

//    std::cout << "resized: " << w << "/" << h << std::endl;

    for(RenderPass* rp : m_renderPasses) {
        rp->screenResize(w, h);
    }
}

void Renderer::renderScene(Camera *camera)
{
    m_trianglesDrawn=0;

#ifdef VIENNA_DEBUG
    auto begin = std::chrono::high_resolution_clock::now();
    auto interim1 = begin;
#endif

    for(ShadowMapPass* lightPass : m_shadowMapPasses) {
        lightPass->render();
    }
#ifdef VIENNA_DEBUG
    glFinish();
    auto interim2 = std::chrono::high_resolution_clock::now();
    Application::instance()->addTiming("1. shadowMaps", std::chrono::duration_cast<VIENNA_TIMING_PRECISION>(interim2 - interim1));
    interim1 = interim2;
#endif

    m_mainRenderPass->render();
#ifdef VIENNA_DEBUG
    glFinish();
    interim2 = std::chrono::high_resolution_clock::now();
    Application::instance()->addTiming("2. main render", std::chrono::duration_cast<VIENNA_TIMING_PRECISION>(interim2 - interim1));
    interim1 = interim2;
#endif

    m_debugDrawPass->render();
#ifdef VIENNA_DEBUG
    glFinish();
    interim2 = std::chrono::high_resolution_clock::now();
    Application::instance()->addTiming("3. debug draw", std::chrono::duration_cast<VIENNA_TIMING_PRECISION>(interim2 - interim1));
    interim1 = interim2;
#endif

    m_particleSystemsPass->render();
#ifdef VIENNA_DEBUG
    glFinish();
    interim2 = std::chrono::high_resolution_clock::now();
    Application::instance()->addTiming("4. particle systems", std::chrono::duration_cast<VIENNA_TIMING_PRECISION>(interim2 - interim1));
    interim1 = interim2;
#endif

    m_postProcessPass->render();
#ifdef VIENNA_DEBUG
    glFinish();
    interim2 = std::chrono::high_resolution_clock::now();
    Application::instance()->addTiming("5. post process", std::chrono::duration_cast<VIENNA_TIMING_PRECISION>(interim2 - interim1));
    interim1 = interim2;
#endif

    Framebuffer::unbindFromDrawing();
    m_resultFramebuffer->bindForReading();

//    ShadowMapPass* first = *(m_shadowMapPasses.begin());
//    first->shadowMap()->bindForReading();
//    glBlitFramebuffer(0, 0, 1024, 1024,
//                      0, 0, m_width, m_height, GL_COLOR_BUFFER_BIT, GL_NEAREST);

    glBlitFramebuffer(0, 0, m_width, m_height,
                      0, 0, m_width, m_height, GL_COLOR_BUFFER_BIT, GL_NEAREST);

    Framebuffer::unbindFromReading();

    glfwSwapBuffers(glfwWindowHandle());    // afair this call is blocking, we could move that to the main loop. this way we could probably hide a bit of latency
                                            // as previous gl calls are already in the command buffer waiting for execution.


#ifdef VIENNA_DEBUG
    glFinish();
    interim2 = std::chrono::high_resolution_clock::now();
    Application::instance()->addTiming("6. blit and swap", std::chrono::duration_cast<VIENNA_TIMING_PRECISION>(interim2 - interim1));
    Application::instance()->addTiming("0. total rendering", std::chrono::duration_cast<VIENNA_TIMING_PRECISION>(interim2 - begin));
#endif
}

void Renderer::destroyScene() {
    glfwTerminate();
}

void Renderer::switchUsingVSync()
{
    m_useVSync = !m_useVSync;

    std::string message = "Switched VSync ";
    message+= m_useVSync?"on.":"off.";
    Application::instance()->ingameGui()->postMessage(message);

    if(m_useVSync)
        glfwSwapInterval(1);
    else
        glfwSwapInterval(0);
}

void Renderer::writeFrontBufferToFile(std::string filename)
{
    glReadBuffer(GL_FRONT_LEFT);
    unsigned char* data = new unsigned char[m_width * m_height * 3];

    glReadPixels(0, 0, m_width, m_height, GL_RGB, GL_UNSIGNED_BYTE, data);

    FIBITMAP* bitmap = FreeImage_Allocate(m_width, m_height, 24);
    BYTE* bits(0);
    bits = FreeImage_GetBits(bitmap);
    for(int x = 0; x < m_width; x++) {
        for (int y = 0; y < m_height; y++) {
            RGBQUAD colour;
            colour.rgbRed   = (BYTE)(data[(y * m_width + x)*3 + 0]);
            colour.rgbGreen = (BYTE)(data[(y * m_width + x)*3 + 1]);
            colour.rgbBlue  = (BYTE)(data[(y * m_width + x)*3 + 2]);
//            colour.rgbRed = (BYTE) (128);
//            colour.rgbGreen = (BYTE) (255);
//            colour.rgbBlue = (BYTE)(120);
            FreeImage_SetPixelColor(bitmap, x, y, &colour);
        }
    }

    bool retVal = FreeImage_Save(FIF_JPEG, bitmap, (filename + ".jpg").c_str(), JPEG_QUALITYSUPERB);
    FreeImage_Unload(bitmap);

    delete[] data;
}

void Renderer::addTrianglesToDrawnCounter(unsigned int num)
{
    m_trianglesDrawn+=num;
}

unsigned int Renderer::trianglesDrawn() const
{
    return m_trianglesDrawn;
}

Cuda *Renderer::cudaManager() const
{
     return m_mainRenderPass->cudaManager();
}

