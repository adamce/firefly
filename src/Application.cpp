/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Application.h"


#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <chrono>
#include <btBulletDynamicsCommon.h>

#define GLM_FORCE_RADIANS
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "objects/Ship.h"
#include "objects/Camera.h"
#include "Renderer.h"
#include "ObjectManager.h"
#include "cuda/Cuda.h"
#include "meshes/TextureManager.h"
#include "shaders/ShaderManager.h"
#include "shaders/DeferredGeometryShader.h"
#include "shaders/Shader.h"
#include "objects/Light.h"
#include "objects/Skybox.h"
#include "objects/SpaceDust.h"
#include "objects/LensFlare.h"
#include "auxiliary/ApplicationConstants.h"
#include "auxiliary/IngameGui.h"
#include "auxiliary/randomNumber.h"
#include "auxiliary/GLDebugDrawer.h"
#include "auxiliary/ShortcutInputHandler.h"
#include "auxiliary/CameraInputHandler.h"
#include "bvh/BvhManager.h"

Application* Application::singletonPointer = 0;
float randomNumber(float lowerBound, float upperBound);

Application::Application() : m_shaderManager(new ShaderManager()), m_renderer(new Renderer()), m_bvhManager(new BvhManager())
{
    m_camera1 = 0;
    m_ingameGui = 0;
    m_bulletDebugDrawing = false;
    m_environmentMapping = true;
    m_gamePaused = false;
    m_useUnbuffredGeometry = false;
	m_useLinearFiltering = true;
	m_useMipMapping = true;
    m_useJetStreams = false;
}

Application::~Application()
{

    delete m_objectManager;

    delete m_shaderManager;
    delete m_renderer;
    delete m_ingameGui;

    delete m_cameraInputHandler;
	ShortcutInputHandler::disableShortcuts();
    delete m_soundManager;
}

Application* Application::instance() {
    if(singletonPointer==0)
        singletonPointer=new Application();
    return singletonPointer;
}

void Application::init()
{
    initBulletEngine();
    initSoundEngine();

	m_environmentMapping = true;

    //camera1 VOR der renderinit allokieren, weils sonst eine access violation beim window resizen gibt
    m_camera1 = new Camera();
    m_camera1->readAnimFromFile("cameraAnimTest.dae");
//    m_camera1->readAnimFromFile("light1Anim.dae");
    m_renderer->init();
    m_renderer->initSceneOld();
    m_cameraInputHandler = new CameraInputHandler(m_camera1);
    ShortcutInputHandler::enableShortcuts();

    m_objectManager = new ObjectManager();

    m_ingameGui = new IngameGui();

    // dirty code: set the window size for the viewports. this is not changing the actual windowsize, just the viewport in the cameras
    m_renderer->resize(VIENNA_DEFAULT_WINDOW_WIDTH, VIENNA_DEFAULT_WINDOW_HEIGHT);

    TextureManager::instance()->openGlUpload();
    updateTextureMode();
    TextureManager::instance()->bindTextures();

    m_soundManager->playSound(SoundType::BackgroundMusic);

    m_bvhManager->createBvh();

    m_renderer->applicationInitFinished();
}

void Application::update(float time)
{
#ifdef VIENNA_DEBUG
    auto begin = std::chrono::high_resolution_clock::now();
#endif
    if(!m_gamePaused) {
        handleInput(time);
        m_camera1->update(time);
		m_ingameGui->update(time);
        m_objectManager->update(time);

		m_cumulatedTime += time;
		//if (m_cumulatedTime > 18.f)
		{
			//m_dynamicsWorld->stepSimulation(time);
			//see http://bulletphysics.org/mediawiki-1.5.8/index.php/Stepping_The_World#What_do_the_parameters_to_btDynamicsWorld::stepSimulation_mean.3F
			m_dynamicsWorld->stepSimulation(time, 7);
		}

        m_bvhManager->updateSceneAccelerationStructure();
    }
    else {
        handleInput(time);
    }
#ifdef VIENNA_DEBUG
    auto end = std::chrono::high_resolution_clock::now();
    addTiming("# Application::update()", std::chrono::duration_cast<VIENNA_TIMING_PRECISION>(end - begin));
#endif
}

void Application::drawWithDefaultCamera(Object *object)
{
    objectManager()->drawObject(object, m_camera1);
}


void Application::handleInput(float time)
{
	if(m_gamePaused) {
        return;
    }

    m_cameraInputHandler->handleInput(time);
}

ShaderManager* Application::shaderManager() const
{
    return m_shaderManager;
}

ObjectManager *Application::objectManager() const
{
    return m_objectManager;
}

Renderer* Application::renderer() const
{
    return m_renderer;
}

Camera* Application::camera() const
{
    return m_camera1;
}

BvhManager *Application::bvhManager() const
{
    return m_bvhManager;
}

IngameGui* Application::ingameGui() const
{
    return m_ingameGui;
}

btDiscreteDynamicsWorld* Application::dynamicsWorld() const
{
    return m_dynamicsWorld;
}

void Application::switchEnvironmentMapping()
{
    m_environmentMapping = !m_environmentMapping;
    std::string message = "Switched environment mapping ";
    message+= m_environmentMapping?"on.":"off.";
    ingameGui()->postMessage(message);
}

void Application::switchPaused()
{
    m_gamePaused = !m_gamePaused;
    std::string message = "Switched pause ";
    message+= m_gamePaused?"on.":"off.";
    ingameGui()->postMessage(message);
}


void Application::switchBufferMode()
{
    m_useUnbuffredGeometry = !m_useUnbuffredGeometry;
    std::string message = "Switched unbuffered geometry ";
    message+= m_useUnbuffredGeometry?"on.":"off.";
    ingameGui()->postMessage(message);
}

void Application::switchMipMapping()
{
	m_useMipMapping = !m_useMipMapping;
	std::string message = "Switched MipMapping ";
    message+= m_useMipMapping?"on.":"off.";
    ingameGui()->postMessage(message);
	updateTextureMode();
}

void Application::switchTextureFiltering()
{
	m_useLinearFiltering = !m_useLinearFiltering;
	std::string message = "Switched Linear Filtering ";
    message+= m_useLinearFiltering?"on.":"off.";
    ingameGui()->postMessage(message);
	updateTextureMode();
}

void Application::switchSkybox()
{
	MeshManager* mMan = MeshManager::instance();
	mMan->switchSkybox();
	glm::vec3 sunpos = mMan->getCurrentSunPosition();
	//update sun
    m_objectManager->m_sunlight->setPosition(sunpos.x, sunpos.y, sunpos.z);
    m_objectManager->m_sun->setPosition(sunpos.x, sunpos.y, sunpos.z);
}

void Application::addTiming(const std::string &name, VIENNA_TIMING_PRECISION time)
{
    if(m_measuredIterations.count(name) == 0) {
        m_measuredIterations.insert(std::pair<std::string, int>(name, 1));
        m_measuredTime.insert(std::pair<std::string, double>(name, (double) time.count()));
        return;
    }
    int i = m_measuredIterations.at(name) + 1;

    if(i > VIENNA_TIMING_SAMPLES) i = VIENNA_TIMING_SAMPLES;

    double factor = (((double) i) - 1.0) / ((double) i);
    double t = m_measuredTime.at(name) * factor;
    t += (1.0 - factor) * ((double) time.count());

    m_measuredIterations.at(name) = i;
    m_measuredTime.at(name) = t;
}


void Application::updateTextureMode()
{
    if(m_useLinearFiltering){
        if(m_useMipMapping){
            //GL_LINEAR_MIPMAP_LINEAR
            TextureManager::instance()->switchFiltering(5);
        }
        else{
            //GL_LINEAR
            TextureManager::instance()->switchFiltering(1);
        }
    }
    else{
        if(m_useMipMapping){
            //GL_NEAREST_MIPMAP_NEAREST
            TextureManager::instance()->switchFiltering(2);
        }
        else{
            //GL_NEAREST
            TextureManager::instance()->switchFiltering(0);
        }
    }

    for(auto iter = m_objectManager->m_objects.begin(); iter != m_objectManager->m_objects.end(); iter++)
        {
			if(m_useLinearFiltering){
				if(m_useMipMapping){
					//GL_LINEAR_MIPMAP_LINEAR
					(*iter)->switchTextureFiltering(5);
				}
				else{
					//GL_LINEAR
					(*iter)->switchTextureFiltering(1);
				}
			}
			else{
				if(m_useMipMapping){
					//GL_NEAREST_MIPMAP_NEAREST
					(*iter)->switchTextureFiltering(2);
				}
				else{
					//GL_NEAREST
					(*iter)->switchTextureFiltering(0);
				}
			}
        }
}

bool Application::useUnbuffredGeometry()
{
    return m_useUnbuffredGeometry;
}

void Application::initBulletEngine()
{
    ///collision configuration contains default setup for memory, collision setup
    m_collisionConfiguration = new btDefaultCollisionConfiguration();
    //  m_collisionConfiguration->setConvexConvexMultipointIterations();

    ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    m_dispatcher = new  btCollisionDispatcher(m_collisionConfiguration);
    m_dispatcher->registerCollisionCreateFunc(BOX_SHAPE_PROXYTYPE,BOX_SHAPE_PROXYTYPE,m_collisionConfiguration->getCollisionAlgorithmCreateFunc(CONVEX_SHAPE_PROXYTYPE,CONVEX_SHAPE_PROXYTYPE));

    m_broadphase = new btDbvtBroadphase();

    ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    btSequentialImpulseConstraintSolver* sol = new btSequentialImpulseConstraintSolver;
    m_solver = sol;

    m_dynamicsWorld = new btDiscreteDynamicsWorld(m_dispatcher,m_broadphase,m_solver,m_collisionConfiguration);
//     m_dynamicsWorld ->setDebugDrawer(&sDebugDrawer);
    m_dynamicsWorld->getSolverInfo().m_splitImpulse=true;
    m_dynamicsWorld->getSolverInfo().m_numIterations = 20;

    m_dynamicsWorld->setGravity(btVector3(0,-9.81f,0));

   GLDebugDrawer* dbgdrawer = new GLDebugDrawer();
   m_dynamicsWorld->setDebugDrawer(dbgdrawer);
   dbgdrawer->setDebugMode(/*btIDebugDraw::DBG_DrawAabb|*/btIDebugDraw::DBG_DrawWireframe/*|btIDebugDraw::DBG_DrawContactPoints*/);
}

void Application::initSoundEngine()
{
    m_soundManager = new FmodSoundManager();
}

FmodSoundManager* Application::soundManager() const
{
	return m_soundManager;
}
