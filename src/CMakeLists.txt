project(firefly)
cmake_minimum_required(VERSION 2.6)
cmake_policy(SET CMP0015 OLD)

#find_package(Qt4 REQUIRED)
find_package(OpenGL REQUIRED)
find_package(CUDA REQUIRED)

#include_directories(${CMAKE_SYSTEM_INCLUDE_PATH} ${QT_INCLUDES} ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR} ../include)

if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
  link_directories(/usr/lib /usr/local/lib ../linlib/lib)

  # otherwise some bullet internal headers don't find friends..
  include_directories(/usr/local/include/bullet /usr/include/bullet ${CMAKE_CURRENT_SOURCE_DIR}/../linlib/include /usr/local/cuda/include)
else()
  #windows
  include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../winlib/include ${CMAKE_CURRENT_SOURCE_DIR}/../winlib/include/bullet)
  link_directories(${CMAKE_CURRENT_SOURCE_DIR}/../winlib/lib)
endif()

set(project_SRCS
#main
main.cpp
Application.cpp                         Application.h
Renderer.cpp 		                Renderer.h
Framebuffer.cpp 	                Framebuffer.h
DataManager.cpp		                DataManager.h
ObjectManager.cpp                       ObjectManager.h

#render passes
renderPasses/RenderPass.cpp             renderPasses/RenderPass.h
renderPasses/MainRenderPass.cpp         renderPasses/MainRenderPass.h
renderPasses/PostProcessPass.cpp        renderPasses/PostProcessPass.h
renderPasses/ParticleSystemsPass.cpp    renderPasses/ParticleSystemsPass.h
renderPasses/DebugDrawPass.cpp          renderPasses/DebugDrawPass.h
renderPasses/ShadowMapPass.cpp          renderPasses/ShadowMapPass.h

#cuda stuff
cuda/Cuda.h				cuda/Cuda.cu
cuda/DeferredRenderKernel.h
cuda/RayTraceKernel.h
cuda/IndirectLightFilterKernel.h
cuda/IndirectLightFilterKernel2.h
cuda/CommonDefs.h
cuda/CommonStructs.h
cuda/GpuMemoryData.h
cuda/HelperMath.h
cuda/BvhHelperStructs.h
cuda/InitRandomNumbersKernel.h
cuda/cuHelM.h

#bounding volume hierarchy builder
bvh/BvhManager.cpp                      bvh/BvhManager.h
bvh/Util.cpp                            bvh/Util.hpp
bvh/nv/Hash.cpp                         bvh/nv/Hash.hpp
bvh/nv/Sort.cpp                         bvh/nv/Sort.hpp
bvh/nv/Math.hpp
bvh/nv/Defs.hpp

bvh/scene/SceneAccelerationStructure.cpp
                                        bvh/scene/SceneAccelerationStructure.h
bvh/scene/SerialisedSceneAccelerationStructure.cpp
                                        bvh/scene/SerialisedSceneAccelerationStructure.h

bvh/triangle/SerialisedBVH.cpp          bvh/triangle/SerialisedBVH.h
bvh/triangle/TriangleBvhHolder.cpp      bvh/triangle/TriangleBvhHolder.h
bvh/triangle/BVH.cpp                    bvh/triangle/BVH.hpp
bvh/triangle/BVHNode.cpp                bvh/triangle/BVHNode.hpp
bvh/triangle/Platform.cpp               bvh/triangle/Platform.hpp
bvh/triangle/SplitBVHBuilder.cpp        bvh/triangle/SplitBVHBuilder.hpp
bvh/triangle/FireflyBVHBuilder.cpp     bvh/triangle/FireflyBVHBuilder.h
bvh/triangle/FireflyHelperClasses.cpp  bvh/triangle/FireflyHelperClasses.h
bvh/triangle/TriangleSoup.cpp           bvh/triangle/TriangleSoup.h

#shaders
shaders/ShaderManager.cpp               shaders/ShaderManager.h
shaders/Shader.cpp                      shaders/Shader.h
shaders/DeferredGeometryShader.cpp	shaders/DeferredGeometryShader.h
shaders/DeferredSkyBoxShader.cpp	shaders/DeferredSkyBoxShader.h

#auxiliary
auxiliary/FmodSoundManager.cpp 		auxiliary/FmodSoundManager.h
auxiliary/CameraInputHandler.cpp 	auxiliary/CameraInputHandler.h
auxiliary/ShortcutInputHandler.cpp 	auxiliary/ShortcutInputHandler.h
auxiliary/IngameGui.cpp 		auxiliary/IngameGui.h
auxiliary/GLDebugDrawer.cpp 		auxiliary/GLDebugDrawer.h
auxiliary/randomNumber.cpp 		auxiliary/randomNumber.h
auxiliary/ViewPlane.h
auxiliary/OpenGlDebug.h
auxiliary/GlmHelper.cpp                 auxiliary/GlmHelper.h
auxiliary/HelperOperations.h            auxiliary/HelperOperations.cpp
auxiliary/Interpolator.h                auxiliary/Interpolator.cpp
auxiliary/ObjectAnimator.h              auxiliary/ObjectAnimator.cpp
auxiliary/ApplicationConstants.h
auxiliary/ApplicationConstantsCuda.h

#objects
objects/Object.cpp                      objects/Object.h
objects/Camera.cpp                      objects/Camera.h
objects/Sztuczka.cpp                    objects/Sztuczka.h
objects/Skybox.cpp                      objects/Skybox.h
objects/Light.cpp                       objects/Light.h
objects/SpaceDust.cpp                   objects/SpaceDust.h
objects/LensFlare.cpp                   objects/LensFlare.h

#physical objects
objects/PhysicalObject.cpp              objects/PhysicalObject.h
objects/SteerableObject.cpp             objects/SteerableObject.h
objects/Ship.cpp                        objects/Ship.h

#meshes
meshes/MeshManager.cpp                  meshes/MeshManager.h
meshes/Mesh.cpp                         meshes/Mesh.h
meshes/MeshVertex.cpp                   meshes/MeshVertex.h
meshes/FlareVertex.cpp                  meshes/FlareVertex.h
meshes/TextureManager.cpp               meshes/TextureManager.h
meshes/QuadGeometry.h
meshes/SpaceDustGeometry.h
meshes/LensFlareGeometry.h
meshes/SkyboxGeometry.h
meshes/EmptyParticleSystemGeometry.h
meshes/MaterialDescription.h

)

#shaders
file(GLOB RES_FILES
../data/shader/DeferredGeometryShader.frag
../data/shader/DeferredGeometryShader.vert
../data/shader/DeferredSkyBoxShader.frag
../data/shader/LensFlareShader.frag
../data/shader/LensFlareShader.vert
../data/shader/PostProcessBloom.frag
../data/shader/PostProcessBloom.vert
../data/shader/shader.frag
../data/shader/shader.vert
../data/shader/ShadowMap.frag
../data/shader/ShadowMap.vert
../data/shader/SpaceDustShader.frag
../data/shader/SpaceDustShader.vert
../data/shader/viennaPack.glsl
)

#if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
#  configure_file(./shader.frag ./shader.frag)
#  configure_file(./shader.vert ./shader.vert)
#else()
#  configure_file(./shader.frag ./Debug/shader.frag)
#  configure_file(./shader.vert ./Debug/shader.vert)
#  configure_file(./shader.frag ./Release/shader.frag)
#  configure_file(./shader.vert ./Release/shader.vert)
#endif()

#set(CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS} -O3 --use_fast_math -gencode arch=compute_20,code=sm_21 --maxrregcount 32)
#set(CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS} "--use_fast_math -gencode arch=compute_20,code=sm_20 -lineinfo -G")   #-G for cuda debugger
#set(CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS} "--use_fast_math -gencode arch=compute_20,code=sm_21 -lineinfo")
set(CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS} "--use_fast_math -gencode arch=compute_20,code=sm_21 -lineinfo --maxrregcount 32")


if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
   set(CMAKE_CXX_FLAGS "-D VIENNA_DEBUG -D VIENNA_LINUX -std=c++11")
#   set(CMAKE_CXX_FLAGS "-D VIENNA_LINUX -std=c++11")
   set(CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS} " -std=c++11")
else()
    #add_definitions(/DVIENNA_DEBUG)
    add_definitions(/DVIENNA_WINDOWS)
    SET( CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS}" )
endif()

#qt4_automoc(${project_SRCS})
#add_executable(firefly ${project_SRCS})
cuda_add_executable(firefly  ${RES_FILES} ${project_SRCS})

if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
    set(LIBS ${LIBS} X11 Xxf86vm Xi GL glfw3 GLEW Xrandr pthread assimp BulletDynamics BulletCollision LinearMath fmodex64 freeimage gsl gslcblas ${CUDA_curand_LIBRARY})
	target_link_libraries(firefly ${LIBS})
else()
        set(LIBS ${LIBS} OpenGL32 glfw3 GLEW32 assimp fmodex64_vc FreeImage gsl cblas ${CUDA_curand_LIBRARY})
	target_link_libraries(firefly ${LIBS} debug BulletDynamics_Debug debug BulletCollision_Debug debug LinearMath_Debug)
	target_link_libraries(firefly ${LIBS} optimized BulletDynamics optimized BulletCollision optimized LinearMath)
	target_link_libraries(firefly ${LIBS} general BulletDynamics general BulletCollision general LinearMath)
endif()


