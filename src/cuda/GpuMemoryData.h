/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GPUMEMORYDATA_H
#define GPUMEMORYDATA_H

#include "CommonStructs.h"
#include "meshes/MaterialDescription.h"
#include <curand_kernel.h>

surface<void,  cudaSurfaceType2D> g_geometry0SurfRef;
//surface<void,  cudaSurfaceType2D> g_geometry1SurfRef;
surface<void,  cudaSurfaceType2D> g_resultSurfRef;
texture<float2,  cudaTextureType2D,  cudaReadModeElementType> g_shadowMapsTexRef0;
texture<float2,  cudaTextureType2D,  cudaReadModeElementType> g_shadowMapsTexRef1;
texture<float2,  cudaTextureType2D,  cudaReadModeElementType> g_shadowMapsTexRef2;
texture<float2,  cudaTextureType2D,  cudaReadModeElementType> g_shadowMapsTexRef3;
texture<float2,  cudaTextureType2D,  cudaReadModeElementType> g_shadowMapsTexRef4;
texture<float2,  cudaTextureType2D,  cudaReadModeElementType> g_shadowMapsTexRef5;
texture<float2,  cudaTextureType2D,  cudaReadModeElementType> g_shadowMapsTexRef6;
texture<float2,  cudaTextureType2D,  cudaReadModeElementType> g_shadowMapsTexRef7;
texture<float2,  cudaTextureType2D,  cudaReadModeElementType> g_shadowMapsTexRef8;
texture<float2,  cudaTextureType2D,  cudaReadModeElementType> g_shadowMapsTexRef9;

texture<uchar4,  cudaTextureType2DLayered,  cudaReadModeNormalizedFloat> gt_textures256;
texture<uchar4,  cudaTextureType2DLayered,  cudaReadModeNormalizedFloat> gt_textures512;
texture<uchar4,  cudaTextureType2DLayered,  cudaReadModeNormalizedFloat> gt_textures1024;
texture<uchar4,  cudaTextureType2DLayered,  cudaReadModeNormalizedFloat> gt_textures2048;

texture<uint2,   cudaTextureType2D, cudaReadModeElementType> gt_rayCastResults;

__device__ MaterialDescription* g_materials;
__device__ CudaUniforms* g_uniforms;
__device__ CudaLight* g_lights;
__device__ int g_lightsCount;

__device__ CudaRTResult* gd_rayCastResults1;
__device__ CudaRTResult* gd_rayCastResults2;
__device__ size_t gd_rayCastResults1_pitch;
__device__ size_t gd_rayCastResults2_pitch;
__device__ uint* gd_indirectLight;

//__device__ curandStateSobol32_t* gd_sobolRandomNumbersStates;
__device__ curandStateScrambledSobol32_t* gd_sobolRandomNumbersStates;
__device__ curandStateXORWOW* gd_randomNumberStates;


// nvidia derived code starts
/*
 *  Copyright (c) 2014 Adam Celarek
 *  Copyright (c) 2009-2011, NVIDIA Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of NVIDIA Corporation nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Linear textures wrapping the corresponding parameter arrays.
texture<float4, cudaTextureType1D, cudaReadModeElementType> t_nodesA;
texture<float4, cudaTextureType1D, cudaReadModeElementType> t_trisA;        // woop
__device__ float4* gd_trisA;

// deleted t_nodesB-D and t_trisB-D, because we don't support SOA
texture<int, cudaTextureType1D, cudaReadModeElementType>   t_triIndices;    // indices of triangles
__device__ int* gd_triIndices;

#define NODES_ARRAY_OF_STRUCTURES           // Define for AOS, comment out for SOA. // we don't support SOA in neither case, because of putting several bvhes into one buffer
#define TRIANGLES_ARRAY_OF_STRUCTURES       // Define for AOS, comment out for SOA. // and therefore we would need to split in a very complicated way

#define LOAD_BALANCER_BATCH_SIZE        96  // Number of rays to fetch at a time. Must be a multiple of 32.
#define STACK_SIZE                      64  // Size of the traversal stack in local memory.
#define LOOP_NODE                       100 // Nodes: 1 = if, 100 = while.
#define LOOP_TRI                        100 // Triangles: 1 = if, 100 = while.

// nvidia derived code ends

__device__ unsigned char gd_sceneAccelerationNodeCount;
__device__ SceneAccelerationNode* gd_serialisedSceneAccelerationNodes;
__device__ int3* gd_vertexIndices;
__device__ float3* gd_positions;
__device__ float3* gd_normals;
__device__ float2* gd_texCoords;
__device__ int* gd_triangleMaterials;

#endif // GPUMEMORYDATA_H
