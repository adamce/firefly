/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEFERRED_RENDER_KERNEL_H
#define DEFERRED_RENDER_KERNEL_H

#include <cuda_runtime.h>

#include "CommonDefs.h"
#include "GpuMemoryData.h"
#include "auxiliary/ApplicationConstantsCuda.h"

__device__ __forceinline__
CudaRTResult getRtResultForPixel(int x, int y) {
    if(y >= 0 && x >= 0 && y < g_uniforms->height / 2 && x < g_uniforms->width / 2) {
        CudaRTResult tmp;
        tmp.data = tex2D(gt_rayCastResults, ((float) x) + 0.5f, ((float) y) + 0.5f);
        return tmp;
//        return gd_rayCastResults[y * (gd_rayCastResults_pitch / sizeof(CudaRTResult)) + x];
    }
    else {
        CudaRTResult tmp;
        tmp.access.normal = make_char3(0, 0, 0);
        tmp.access.irradiance = make_uchar3(0, 0, 0);
        tmp.access.m_dist = (unsigned short) -1;
        return tmp;
    }
}

__device__ __forceinline__ void addWeightedIndirectLight(float4* destination, int x, int y, float dist, float3 normal) {
    CudaRTResult currRes = getRtResultForPixel(x, y);
    float localDist = currRes.access.dist();
    float3 localNormal = convertTo_float3(currRes.access.normal);
    float3 localIrr = convertTo_float3(currRes.access.irradiance);
    float weight = fmaxf(0.f, dot(normal, localNormal)) * fmaxf((1.f - fabs(localDist - dist)), 0.05f);
    float4 tmp = make_float4(localIrr, 1.f);
    tmp *= weight;
    *destination += tmp;
}


__global__
void deferredRenderKernel()
{
    int x = (int) blockIdx.x*blockDim.x + threadIdx.x;
    int y = (int) blockIdx.y*blockDim.y + threadIdx.y;
    if(x >= g_uniforms->width) return;
    if(y >= g_uniforms->height) return;

    uint4 geomData0;
    surf2Dread(&geomData0, g_geometry0SurfRef, x*sizeof(uint4), y);
//    surf2Dread(&geomData1, g_geometry1SurfRef, x*sizeof(uint4), y);

    float4 pos;
    float3 normal, textureColour;
    float dist;
    int materialIndex;

    unpackGeomData(geomData0, x, y, &pos, &materialIndex, &normal, &textureColour, &dist);

#ifdef VIENNA_DEBUG_CUDA_PIXEL       // costs ~50 msec on 800*600
//    if(g_uniforms->mouseDebugX == x && g_uniforms->mouseDebugY == y) {
//        printf("pos: %2.2f/%2.2f/%2.2f normal: %2.2f/%2.2f/%2.2f texC: %2.2f/%2.2f/%2.2f, mat: %d\n",
//               pos.x, pos.y, pos.z, normal.x, normal.y, normal.z, textureColour.x, textureColour.y, textureColour.z, materialIndex);
//    }
#endif

    float3 outColour = make_float3(0.f, 1.f, 1.f);

//    outColour = glm::vec4(normal, 0.f);

    if(materialIndex == 1000000) {
        outColour = make_float3(textureColour.x, textureColour.y, textureColour.z) * VIENNA_HDR_SCALING_FACTOR*1.5f;    // alpha channel was/is used as a boost for bloom
    }
    else if (materialIndex < VIENNA_MAX_MATERIAL_COUNT && materialIndex >= 0) {
        MaterialDescription& mat = g_materials[materialIndex];
//        if(mat.type == VIENNA_MATERIAL_TYPE_TEXTURED || mat.type == VIENNA_MATERIAL_TYPE_FLAT_COLOUR) {
        {
            outColour = shade(pos, textureColour, normal);
        }
        if(mat.type == VIENNA_MATERIAL_TYPE_GLOWING) {
            outColour += textureColour * mat.factor;
        }
    }
    else {
        outColour = make_float3(1.f, 0.f, 1.f);
    }

//    float4 indirectLight = uintToRgbaFloat(gd_indirectLight[y * g_uniforms->width + x]);
    float4 indirectLight = make_float4(0);
    addWeightedIndirectLight(&indirectLight, x/2,   y/2,   dist, normal);

    addWeightedIndirectLight(&indirectLight, x/2+1, y/2,   dist, normal);
    addWeightedIndirectLight(&indirectLight, x/2,   y/2+1, dist, normal);
    addWeightedIndirectLight(&indirectLight, x/2-1, y/2,   dist, normal);
    addWeightedIndirectLight(&indirectLight, x/2,   y/2+1, dist, normal);

    indirectLight /= indirectLight.w;
    indirectLight.w = 0.f;


//    outColour = make_float4(normal);
//    outColour = make_float4(length(make_float3(pos - g_uniforms->cameraPosition))*0.02f);
//    outColour = make_float4(pos.w * 0.02f);
//    outColour = make_float4(gd_rayCastResults[y * g_uniforms->width + x].hitPosRadiation);
#ifndef VIENNA_DEBUG_TRACE_FIRST_RAY
    outColour = outColour + make_float3(indirectLight) * textureColour;
//    outColour = make_float3(indirectLight);
#else
    outColour = make_float3(indirectLight);
#endif

    {
        float3 colourScaled = outColour;
        float luminosity = dot(colourScaled, make_float3(0.2126f, 0.7152f, 0.0722f));

        // ungamma
        colourScaled = pow(colourScaled, 2.2f);

        colourScaled = colourScaled * 0.5f;// (1.f / VIENNA_HDR_SCALING_FACTOR);

        float3 colourSq = colourScaled * colourScaled;
        const float white = 3.5f;
        const float factorBelow = 1.0f;

        float3 reinhard = colourScaled * (1.f + colourScaled / (white*white) ) / (factorBelow+colourScaled);
        float3 colourCu = colourSq * colourScaled;

        colourScaled = /*-1.0f * colourSq + 1.0f * colourCu +*/ 0.7f * reinhard + 0.2f * sqrtf(colourScaled);
        // gamma
        outColour = pow(colourScaled, 1.0f/2.2f);


    }

//    outColour.w = gd_rayCastResults[y * g_uniforms->width + x].tmp;
//    outColour.w = 0.f;

    uchar4 data = rgbFloatToUChar4(outColour);

//    data = gd_rayCastResults[y * g_uniforms->width + x].hitPosRadiation;

    surf2Dwrite(data, g_resultSurfRef, x*4, y);
}


#endif //DEFERRED_RENDER_KERNEL_H
