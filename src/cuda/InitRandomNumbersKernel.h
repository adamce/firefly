#ifndef INITRANDOMNUMBERSKERNEL_H
#define INITRANDOMNUMBERSKERNEL_H

#include <curand_kernel.h>
#include "GpuMemoryData.h"

__global__ void initRandomNumbersKernel(curandDirectionVectors32_t *const randDirections, const int y, unsigned int* scrambleRands, unsigned int* randomNumbers)
{
    int x = (int) blockIdx.x*blockDim.x + threadIdx.x;
//    int y = (int) blockIdx.y*blockDim.y + threadIdx.y;
    if(x >= g_uniforms->width) return;
//    if(y >= g_uniforms->height) return;

    unsigned int threadId = y * g_uniforms->width + x;
    unsigned int offset = g_uniforms->width * g_uniforms->height;

    //curand_init(1234, threadId, 0, &gd_randomNumberStates[threadId]);

	//curand_init(randDirections[0], d_scrambleRands[0], threadId * (g_uniforms->width * g_uniforms->height), &gd_sobolRandomNumbersStates[threadId]);
	//curand_init(randDirections[1], d_scrambleRands[1], threadId * (g_uniforms->width * g_uniforms->height), &gd_sobolRandomNumbersStates[threadId + offset]);
    curand_init(randDirections[0], scrambleRands[0], randomNumbers[threadId], &gd_sobolRandomNumbersStates[threadId]);
    curand_init(randDirections[1], scrambleRands[1], randomNumbers[threadId], &gd_sobolRandomNumbersStates[threadId + offset]);

}

#endif // INITRANDOMNUMBERSKERNEL_H
