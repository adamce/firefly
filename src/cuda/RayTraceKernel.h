/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * Ray tracing Code:
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 * Copyright (c) 2009-2011, NVIDIA Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NVIDIA Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
   "Persistent while-while kernel" used in:

   "Understanding the Efficiency of Ray Traversal on GPUs",
   Timo Aila and Samuli Laine,
   Proc. High-Performance Graphics 2009
*/

#ifndef RAYTRACEKERNEL_H
#define RAYTRACEKERNEL_H

#include "CommonDefs.h"
#include "CommonStructs.h"
#include "GpuMemoryData.h"
#include "auxiliary/ApplicationConstantsCuda.h"

#define FETCH_GLOBAL(NAME, IDX, TYPE) ((const TYPE*) gd_ ## NAME)[IDX]
#define FETCH_TEXTURE(NAME, IDX, TYPE) tex1Dfetch(t_ ## NAME, IDX)

#ifdef __CUDACC__

template <class T> __device__ __forceinline__ void swap(T& a,T& b)
{
    T t = a;
    a = b;
    b = t;
}

__device__ __forceinline__ float min4(float a, float b, float c, float d)
{
    return fminf(fminf(fminf(a, b), c), d);
}

__device__ __forceinline__ float max4(float a, float b, float c, float d)
{
    return fmaxf(fmaxf(fmaxf(a, b), c), d);
}
#endif

enum
{
    MaxBlockHeight      = 6,            // Upper bound for blockDim.y.
    EntrypointSentinel  = 0x76543210,   // Bottom-most stack entry, indicating the end of traversal.
};

struct RayStruct
{
    float   idirx;  // 1.0f / ray.direction.x
    float   idiry;  // 1.0f / ray.direction.y
    float   idirz;  // 1.0f / ray.direction.z
    float   tmin;   // ray.tmin
    float3 orig;

//    float   dummy;  // Padding to avoid bank conflicts.
};

struct HitStruct1 {
    float2 hitUv;
    int hitTriAddr;
};

struct HitStruct2 {
    float3 hitDiffuseColour;
    float3 hitNormal;
    float glowFactor;
};

__device__ float3 rtForColour(float4 origin, float3 dir) {
//    __shared__ HitStruct1 sharedHitStruct1[VIENNA_CUDA_BLOCK_WIDTH_RT*VIENNA_CUDA_BLOCK_HEIGHT_RT];
//    __shared__ HitStruct2 sharedHitStruct2[VIENNA_CUDA_BLOCK_WIDTH_RT*VIENNA_CUDA_BLOCK_HEIGHT_RT];
//    HitStruct1* auxHitStruct1 = sharedHitStruct1 + threadIdx.x + (blockDim.x * threadIdx.y);
//    HitStruct2* auxHitStruct2 = sharedHitStruct2 + threadIdx.x + (blockDim.x * threadIdx.y);
    HitStruct1 hitStr1NotShared;
    HitStruct2 hitStr2NotShared;
    HitStruct1* auxHitStruct1 = &hitStr1NotShared;
    HitStruct2* auxHitStruct2 = &hitStr2NotShared;

    float hitT = 1000000000.f;      // t-value of the closest intersection.
    auxHitStruct1->hitTriAddr = -1;
    auxHitStruct2->hitDiffuseColour = make_float3(0.0f);

//    RayStruct notShared;
//    RayStruct* auxRayStruct = &notShared;
    __shared__ RayStruct sharedRayStruct[VIENNA_CUDA_BLOCK_WIDTH_RT*VIENNA_CUDA_BLOCK_HEIGHT_RT];
    RayStruct* auxRayStruct = sharedRayStruct + threadIdx.x + (blockDim.x * threadIdx.y);


    for(int scAccelStructNo = 0; scAccelStructNo < gd_sceneAccelerationNodeCount; scAccelStructNo++) {
    // Temporary data stored in shared memory to reduce register pressure.

        // Traversal stack in CUDA thread-local memory.
        int traversalStack[STACK_SIZE];

        // Live state during traversal, stored in registers.

//        float  origx, origy, origz;    // Ray origin.
//        float3  orig;    // Ray origin.
        int     stackPtr;               // Current position in traversal stack.
        int     nodeAddr;               // Current internal node.
        int     triAddr;                // Start of a pending triangle list.
        int     triAddr2;               // End of a pending triangle list.


        {
            mat4 worldToStructMatrix = gd_serialisedSceneAccelerationNodes[scAccelStructNo].worldToStructMatrix;
            float3 d = make_mat3(worldToStructMatrix) * dir;

//            origx = origin.x;
//            origy = origin.y;
//            origz = origin.z;
            auxRayStruct->orig = transformHPoint3f(worldToStructMatrix, origin);

    //                aux->tmin = o.w;
            auxRayStruct->tmin = 0.00001f;

            float ooeps = exp2f(-80.0f); // Avoid div by zero.
            auxRayStruct->idirx = 1.0f / (fabsf(d.x) > ooeps ? d.x : copysignf(ooeps, d.x));
            auxRayStruct->idiry = 1.0f / (fabsf(d.y) > ooeps ? d.y : copysignf(ooeps, d.y));
            auxRayStruct->idirz = 1.0f / (fabsf(d.z) > ooeps ? d.z : copysignf(ooeps, d.z));
    //                traversalStack[STACK_SIZE + 2] = rayidx; // Spill.

//            {
//                float3 bbmin = gd_serialisedSceneAccelerationNodes[scAccelStructNo].bbMin;
//                float3 bbmax = gd_serialisedSceneAccelerationNodes[scAccelStructNo].bbMax;

//                float oodx  = auxRayStruct->orig.x * auxRayStruct->idirx;
//                float oody  = auxRayStruct->orig.y * auxRayStruct->idiry;
//                float oodz  = auxRayStruct->orig.z * auxRayStruct->idirz;

//                // Intersect the ray against the bounding box of the rt structure
//                RayStruct tmp = *auxRayStruct;
//                float c0lox = bbmin.x * tmp.idirx - oodx;
//                float c0hix = bbmax.x * tmp.idirx - oodx;
//                float c0loy = bbmin.y * tmp.idiry - oody;
//                float c0hiy = bbmax.y * tmp.idiry - oody;
//                float c0loz = bbmin.z * tmp.idirz - oodz;
//                float c0hiz = bbmax.z * tmp.idirz - oodz;
//                float c0min = max4(fminf(c0lox, c0hix), fminf(c0loy, c0hiy), fminf(c0loz, c0hiz), tmp.tmin);
//                float c0max = min4(fmaxf(c0lox, c0hix), fmaxf(c0loy, c0hiy), fmaxf(c0loz, c0hiz), hitT);

//                // don't trace if the ray is not intersecting the bounding box.
//                if(!(c0max >= c0min)) continue;

//            }

            // Setup traversal.
            traversalStack[0] = EntrypointSentinel; // Bottom-most entry.
            stackPtr = 0;
            nodeAddr = 0;   // Start from the root.
            triAddr  = 0;   // No pending triangle list.
            triAddr2 = 0;
    //                STORE_RESULT(rayidx, -1, 0.0f); // No triangle intersected so far.
    //                hitT     = d.w; // tmax
        }

        // Traversal loop.

        do
        {
            // Traverse internal nodes.
            float oodx  = auxRayStruct->orig.x * auxRayStruct->idirx;
            float oody  = auxRayStruct->orig.y * auxRayStruct->idiry;
            float oodz  = auxRayStruct->orig.z * auxRayStruct->idirz;

            for (int i = LOOP_NODE - 1; i >= 0 && nodeAddr >= 0 && nodeAddr != EntrypointSentinel; i--)
            {
                // Fetch AABBs of the two child nodes.

                //AOS
                float4 n0xy = FETCH_TEXTURE(nodesA, gd_serialisedSceneAccelerationNodes[scAccelStructNo].nodesOffset + nodeAddr*4+0, float4);  // (c0.lo.x, c0.hi.x, c0.lo.y, c0.hi.y)
                float4 n1xy = FETCH_TEXTURE(nodesA, gd_serialisedSceneAccelerationNodes[scAccelStructNo].nodesOffset + nodeAddr*4+1, float4);  // (c1.lo.x, c1.hi.x, c1.lo.y, c1.hi.y)
                float4 nz   = FETCH_TEXTURE(nodesA, gd_serialisedSceneAccelerationNodes[scAccelStructNo].nodesOffset + nodeAddr*4+2, float4);  // (c0.lo.z, c0.hi.z, c1.lo.z, c1.hi.z)
                float4 cnodes=FETCH_TEXTURE(nodesA, gd_serialisedSceneAccelerationNodes[scAccelStructNo].nodesOffset + nodeAddr*4+3, float4);


                // Intersect the ray against the child nodes.

                RayStruct tmp = *auxRayStruct;
                float c0lox = n0xy.x * tmp.idirx - oodx;
                float c0hix = n0xy.y * tmp.idirx - oodx;
                float c0loy = n0xy.z * tmp.idiry - oody;
                float c0hiy = n0xy.w * tmp.idiry - oody;
                float c0loz = nz.x   * tmp.idirz - oodz;
                float c0hiz = nz.y   * tmp.idirz - oodz;
                float c1loz = nz.z   * tmp.idirz - oodz;
                float c1hiz = nz.w   * tmp.idirz - oodz;
                float c0min = max4(fminf(c0lox, c0hix), fminf(c0loy, c0hiy), fminf(c0loz, c0hiz), tmp.tmin);
                float c0max = min4(fmaxf(c0lox, c0hix), fmaxf(c0loy, c0hiy), fmaxf(c0loz, c0hiz), hitT);
                float c1lox = n1xy.x * tmp.idirx - oodx;
                float c1hix = n1xy.y * tmp.idirx - oodx;
                float c1loy = n1xy.z * tmp.idiry - oody;
                float c1hiy = n1xy.w * tmp.idiry - oody;
                float c1min = max4(fminf(c1lox, c1hix), fminf(c1loy, c1hiy), fminf(c1loz, c1hiz), tmp.tmin);
                float c1max = min4(fmaxf(c1lox, c1hix), fmaxf(c1loy, c1hiy), fmaxf(c1loz, c1hiz), hitT);

                // Decide where to go next.

                bool traverseChild0 = (c0max >= c0min);
                bool traverseChild1 = (c1max >= c1min);

                nodeAddr           = __float_as_int(cnodes.x);      // stored as int
                int nodeAddrChild1 = __float_as_int(cnodes.y);      // stored as int

                // One child was intersected => go there.

                if(traverseChild0 != traverseChild1)
                {
                    if (traverseChild1)
                        nodeAddr = nodeAddrChild1;
                }
                else
                {
                    // Neither child was intersected => pop.

                    if (!traverseChild0)
                    {
                        nodeAddr = traversalStack[stackPtr];
                        --stackPtr;
                    }

                    // Both children were intersected => push the farther one.

                    else
                    {
                        if(c1min < c0min)
                            swap(nodeAddr, nodeAddrChild1);
                        ++stackPtr;
                        traversalStack[stackPtr] = nodeAddrChild1;
                    }
                }
            }

            // Current node is a leaf => fetch the start and end of the triangle list.

            if (nodeAddr < 0 && triAddr >= triAddr2)
            {
                float4 leaf=FETCH_TEXTURE(nodesA, gd_serialisedSceneAccelerationNodes[scAccelStructNo].nodesOffset + (-nodeAddr-1)*4+3, float4);
                triAddr  = __float_as_int(leaf.x); // stored as int
                triAddr2 = __float_as_int(leaf.y); // stored as int

                // Pop.

                nodeAddr = traversalStack[stackPtr];
                --stackPtr;
            }

            // Intersect the ray against each triangle using Sven Woop's algorithm.

            for (int i = LOOP_TRI - 1; i >= 0 && triAddr < triAddr2; triAddr++, i--)
            {
                // Compute and check intersection t-value.

                float4 v00 = FETCH_GLOBAL(trisA, triAddr*4+0, float4);
                float4 v11 = FETCH_GLOBAL(trisA, triAddr*4+1, float4);

                RayStruct tmp = *auxRayStruct;
                float dirx  = 1.0f / tmp.idirx;
                float diry  = 1.0f / tmp.idiry;
                float dirz  = 1.0f / tmp.idirz;

                float Oz = v00.w - tmp.orig.x*v00.x - tmp.orig.y*v00.y - tmp.orig.z*v00.z;
                float invDz = 1.0f / (dirx*v00.x + diry*v00.y + dirz*v00.z);
                float t = Oz * invDz;

                if (t > tmp.tmin && t < hitT)
                {
                    // Compute and check barycentric u.

                    float Ox = v11.w + tmp.orig.x*v11.x + tmp.orig.y*v11.y + tmp.orig.z*v11.z;
                    float Dx = dirx*v11.x + diry*v11.y + dirz*v11.z;
                    float u = Ox + t*Dx;

                    if (u >= 0.0f)
                    {
                        // Compute and check barycentric v.
                        float4 v22 = FETCH_GLOBAL(trisA, triAddr*4+2, float4);
                        float Oy = v22.w + tmp.orig.x*v22.x + tmp.orig.y*v22.y + tmp.orig.z*v22.z;
                        float Dy = dirx*v22.x + diry*v22.y + dirz*v22.z;
                        float v = Oy + t*Dy;

                        if (v >= 0.0f && u + v <= 1.0f)
                        {
                            // Record intersection.
                            hitT = t;
                            HitStruct1 tmp;
                            tmp.hitTriAddr = triAddr;
                            tmp.hitUv.x = u;
                            tmp.hitUv.y = v;
                            *auxHitStruct1 = tmp;
//                            auxHitStruct1->hitTriAddr = triAddr;
//                            auxHitStruct1->hitUv.x = u;
//                            auxHitStruct1->hitUv.y = v;
                        }
                    }
                }
            } // triangle
        } while (nodeAddr != EntrypointSentinel || triAddr < triAddr2); // traversal


        int hitTriAddr = auxHitStruct1->hitTriAddr;
        if(hitT < 100000.f && hitTriAddr >= 0) {
            int3& vertexIndices = gd_vertexIndices[gd_triIndices[hitTriAddr]];
            mat4 worldToStructMatrix4 = gd_serialisedSceneAccelerationNodes[scAccelStructNo].worldToStructMatrix;
            float3 normal = make_float3(0.0f);
            float2 hitUv = auxHitStruct1->hitUv;
            normal += (gd_normals[vertexIndices.x]) * hitUv.x;
            mat3 worldToStructMatrix = make_transposedMat3(worldToStructMatrix4);
            normal += (gd_normals[vertexIndices.y]) * hitUv.y;
            normal += (gd_normals[vertexIndices.z]) * (1.f - hitUv.x - hitUv.y);
            MaterialDescription* mat = g_materials + gd_triangleMaterials[gd_triIndices[hitTriAddr]];
            auxHitStruct2->hitNormal = worldToStructMatrix * normal;
            auxHitStruct1->hitTriAddr = (-hitTriAddr) - 1;
            auxHitStruct2->glowFactor = 0;

            if(mat->type == VIENNA_MATERIAL_TYPE_TEXTURED) {  //VIENNA_MATERIAL_TYPE_TEXTURED
                int3& vertexIndices = gd_vertexIndices[gd_triIndices[hitTriAddr]];
                float2 texCoords = make_float2(0.f);
                texCoords += gd_texCoords[vertexIndices.x] * hitUv.x;
                texCoords += gd_texCoords[vertexIndices.y] * hitUv.y;
                texCoords += gd_texCoords[vertexIndices.z] * (1.f - hitUv.x - hitUv.y);
                auxHitStruct2->hitDiffuseColour = tex2dOurTex(mat->diffuseTexture, texCoords.x, texCoords.y);
            }
            else if(mat->type == VIENNA_MATERIAL_TYPE_FLAT_COLOUR || mat->type == VIENNA_MATERIAL_TYPE_MACRO) {
                auxHitStruct2->hitDiffuseColour = make_float3(uintToRgbaFloat(mat->diffuseRGBA));
            }
            else if(mat->type == VIENNA_MATERIAL_TYPE_GLOWING) {
                auxHitStruct2->hitDiffuseColour = make_float3(uintToRgbaFloat(mat->diffuseRGBA));
                auxHitStruct2->glowFactor = mat->factor;
            }
//            else {
//                hitDiffuseColour = make_float4(0.0f);
//            }
        }
    }

//    float4 colour = make_float4(hitNormal, 0.f);
    origin.w = 1.f; // not sure if necessary.
    float3 colour = VIENNA_BG_COLOUR * VIENNA_HDR_SCALING_FACTOR;

    if(hitT < 100000.f) {

        float3 normal = auxHitStruct2->hitNormal;

        float4 tmpPos = origin + make_float4(dir * (hitT - 0.005f), 0.f);
        tmpPos.x = tmpPos.x + normal.x * 0.014f;
        tmpPos.y = tmpPos.y + normal.y * 0.014f;
        tmpPos.z = tmpPos.z + normal.z * 0.014f;

        colour = shade_rt(tmpPos, auxHitStruct2->hitDiffuseColour, normal);
        colour += auxHitStruct2->hitDiffuseColour * auxHitStruct2->glowFactor;
    }

//    colour.w = hitT;
    return colour;
}

__global__
void rayTraceKernel()
{
    int x = (int) blockIdx.x*blockDim.x + threadIdx.x;
    int y = (int) blockIdx.y*blockDim.y + threadIdx.y;
    x *= 2;
    y *= 2;

//    printf("pixel %d / %d \n", x, y);
    if(x >= g_uniforms->width) return;
    if(y >= g_uniforms->height) return;


    uint4 geomData0;
    surf2Dread(&geomData0, g_geometry0SurfRef, x*sizeof(uint4), y);
//    surf2Dread(&geomData1, g_geometry1SurfRef, x*sizeof(uint4), y);

    float4 pos;
    float3 normal, textureColour;
    int materialIndex;
    float dist;

    unpackGeomData(geomData0, x, y, &pos, &materialIndex, &normal, &textureColour, &dist);
    UnpackToCharUnion unpacker;
    unpacker.val = geomData0.y;
//    gd_rayCastResults[y * g_uniforms->width + x].access.normal = make_char3(unpacker.chars);
//    gd_rayCastResults[y * g_uniforms->width + x].access.setDist(__int_as_float(geomData0.x));
    CudaRTResult* rcResultData = (CudaRTResult*) (((char*)gd_rayCastResults1) + (y/2) * gd_rayCastResults1_pitch + sizeof(CudaRTResult) * x / 2);
    rcResultData->access.normal = make_char3(unpacker.chars);

    float distFactor = 1.f;
    if(g_materials[materialIndex].type == VIENNA_MATERIAL_TYPE_MACRO) {
        distFactor = 100.f;
    }
    rcResultData->access.setDist(dist * distFactor);

#ifndef VIENNA_DEBUG_TRACE_FIRST_RAY
    if(materialIndex == 1000000) {
//        gd_rayCastResults[y * g_uniforms->width + x].access.irradiance = make_uchar3(0,0,0);
        rcResultData->access.irradiance = make_uchar3(0,0,0);
        return;
    }
#endif



    pos.x = pos.x + normal.x * 0.0003f;
    pos.y = pos.y + normal.y * 0.0003f;
    pos.z = pos.z + normal.z * 0.0003f;

    __shared__ float3 sharedColour[VIENNA_CUDA_BLOCK_WIDTH_RT*VIENNA_CUDA_BLOCK_HEIGHT_RT];
    float3* auxColour = sharedColour + threadIdx.x + (blockDim.x * threadIdx.y);
//    float3 colour = make_float3(0.f);
    *auxColour = make_float3(0.f);

#ifdef VIENNA_DEBUG_TRACE_FIRST_RAY
    float xf = (float) x;
    xf /= (float) g_uniforms->width;
    xf = xf * 2.f - 1.f;
    float yf = y;
    yf /= (float) g_uniforms->height;
    yf = yf * 2.f - 1.f;

    float3 u = make_float3(g_uniforms->cameraUVW.c0);
    float3 v = make_float3(g_uniforms->cameraUVW.c1);
    float3 w = make_float3(g_uniforms->cameraUVW.c2);


    float3 dir = w + xf*u + yf*v;
    dir = normalize(dir);
    float4 origin = g_uniforms->cameraPosition;
    *auxColour = rtForColour(origin, dir);

#else
    for(int i = 0; i < VIENNA_SECONDARY_RAYS; i++) {
        float3 dir;
//        float2 randomNumber = make_randFloat2();
        unsigned int threadId = y * g_uniforms->width + x;
        unsigned int offset = g_uniforms->width * g_uniforms->height;
        float randX = curand_uniform(&gd_sobolRandomNumbersStates[threadId]);
//        randX *= 1.f / ((float) VIENNA_SECONDARY_RAYS) + (i%2) * 0.5f;
        mat3 onb = make_onbMat3(normal);
        float radius = sqrtf(randX);
        float randY = curand_uniform(&gd_sobolRandomNumbersStates[threadId+offset]);
        randY *= 1.f / ((float) VIENNA_SECONDARY_RAYS);
        randY += i * 1.f / ((float) VIENNA_SECONDARY_RAYS);

        float phi = 2.0f * 3.14159265358979f * randY;
        dir.x = radius * cosf(phi);
        dir.y = radius * sinf(phi);

        // Project up to hemisphere.
        dir.z = sqrtf(fmaxf(0.0f, 1.0f - dir.x * dir.x - dir.y * dir.y));
    //    onb = transposed(onb);
        dir = onb * dir;

        *auxColour += rtForColour(pos, dir);
    }
    *auxColour *= 1.f / ((float) VIENNA_SECONDARY_RAYS);
#endif

    //float4 colour = make_float4(hitT*0.05f, hitT * 0.025f, hitT * 0.001f, 0.f);
    //gd_rayCastResults[y * g_uniforms->width + x].access.irradiance = rgbaFloatToUChar3(colour);
    rcResultData->access.irradiance = rgbFloatToUChar3(*auxColour);
}

#endif // RAYTRACEKERNEL_H
