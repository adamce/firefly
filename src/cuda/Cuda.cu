/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Cuda.h"
#include <stdio.h>

#include <GL/glew.h>
#include <GL/gl.h>

#include <cstdio>
#include <iostream>
#include <chrono>
#include <cstdlib>

#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#include <curand_kernel.h>

#include "Application.h"
#include "Framebuffer.h"
#include "DataManager.h"
#include "Renderer.h"
#include "objects/Camera.h"
#include "objects/Light.h"
#include "meshes/TextureManager.h"
#include "bvh/BvhManager.h"
#include "bvh/scene/SerialisedSceneAccelerationStructure.h"
#include "renderPasses/ShadowMapPass.h"
#include "auxiliary/ApplicationConstantsCuda.h"

#include "GpuMemoryData.h"
#include "CommonStructs.h"
#include "DeferredRenderKernel.h"
#include "RayTraceKernel.h"
#include "InitRandomNumbersKernel.h"
#include "IndirectLightFilterKernel.h"

cudaGraphicsResource* g_geometry0GraphicsRes;
cudaGraphicsResource* g_geometry1GraphicsRes;
cudaGraphicsResource* g_resultGraphicsRes;
//cudaGraphicsResource* g_shadowMapRes0;
cudaGraphicsResource* g_shadowMapReses[VIENNA_MAX_LIGHT_COUNT];

cudaArray* g_geometryTexture0Array;
cudaArray* g_geometryTexture1Array;
cudaArray* g_resultArray;
cudaArray* g_shadowMapArray0;
cudaArray* g_shadowMapArrays[VIENNA_MAX_LIGHT_COUNT];

//texture<int4,  cudaTextureType2D,  cudaReadModeElementType> texRefGeometry0;
//texture<uint4,  cudaTextureType2D,  cudaReadModeElementType> texRefGeometry1;


Cuda::Cuda()
{
    cudaSetDevice(0);

    m_mouseDebugPosX = -1;
    m_mouseDebugPosY = -1;

    initUniforms();
    resize(1, 1);
}

Cuda::~Cuda()
{
    for (int i=0; i<m_mappedShadowMaps; i++) {
        handleCudaError(cudaGraphicsUnregisterResource(g_shadowMapReses[i]));
//        handleCudaError(cudaGraphicsUnregisterResource(g_shadowMapRes0));
    }
    cudaUnbindTexture(gt_textures256);
    cudaUnbindTexture(gt_textures512);
    cudaUnbindTexture(gt_textures1024);
    cudaUnbindTexture(gt_textures2048);

    handleCudaError(cudaFreeArray(ma_texture256));
    handleCudaError(cudaFreeArray(ma_texture512));
    handleCudaError(cudaFreeArray(ma_texture1024));
    handleCudaError(cudaFreeArray(ma_texture2048));

//    handleCudaError(cudaFreeMipmappedArray(ma_texture256mm));
//    handleCudaError(cudaFreeMipmappedArray(ma_texture512mm));
//    handleCudaError(cudaFreeMipmappedArray(ma_texture1024mm));
//    handleCudaError(cudaFreeMipmappedArray(ma_texture2048mm));

    handleCudaError(cudaUnbindTexture(t_nodesA));
    handleCudaError(cudaUnbindTexture(t_trisA));
    handleCudaError(cudaUnbindTexture(t_triIndices));
    handleCudaError(cudaFree(md_nodesA));
    handleCudaError(cudaFree(md_trisA));
    handleCudaError(cudaFree(md_triIndices));

    handleCudaError(cudaFree(md_serialisedSceneAccelerationNodes));
    handleCudaError(cudaFree(md_vertexIndices));
    handleCudaError(cudaFree(md_positions));
    handleCudaError(cudaFree(md_normals));
    handleCudaError(cudaFree(md_texCoords));
    handleCudaError(cudaFree(md_triangleMaterials));

    handleCudaError(cudaFree(md_uniforms));
    handleCudaError(cudaFree(md_cudaLights));

    handleCudaError(cudaFree(md_rayCastResults1));
    handleCudaError(cudaFree(md_rayCastResults2));
    handleCudaError(cudaFree(md_indirectLight));

    handleCudaError(cudaFree(md_sobolRandomNumbersStates));
    handleCudaError(cudaFree(md_randomNumberStates));
    handleCudaError(cudaFree(md_randomNumberDirections));

    deInitConstData();
    deInitFramebuffers();
    cudaDeviceReset();
}

void Cuda::initFramebuffers(Framebuffer* geometryFramebuffer, Framebuffer* resultFramebuffer)
{
    m_geometryFB = geometryFramebuffer;
    m_resultFB = resultFramebuffer;

    FramebufferTexture* tex0 = geometryFramebuffer->getTex0();
//    FramebufferTexture* tex1 = geometryFramebuffer->getTex1();
    FramebufferTexture* texR = resultFramebuffer->getTex0();

//    handleCudaError(cudaGraphicsGLRegisterImage(&geometry0GraphicsRes, tex0->textureId(), tex0->target(), cudaGraphicsRegisterFlagsReadOnly));
//    handleCudaError(cudaGraphicsGLRegisterImage(&geometry1GraphicsRes, tex1->textureId(), tex1->target(), cudaGraphicsRegisterFlagsReadOnly));

    handleCudaError(cudaGraphicsGLRegisterImage(&g_geometry0GraphicsRes, tex0->textureId(), tex0->target(), cudaGraphicsRegisterFlagsSurfaceLoadStore));
//    handleCudaError(cudaGraphicsGLRegisterImage(&g_geometry1GraphicsRes, tex1->textureId(), tex1->target(), cudaGraphicsRegisterFlagsSurfaceLoadStore));
    handleCudaError(cudaGraphicsGLRegisterImage(&g_resultGraphicsRes, texR->textureId(), texR->target(), cudaGraphicsRegisterFlagsSurfaceLoadStore));
}

void Cuda::deInitFramebuffers()
{
    handleCudaError(cudaGraphicsUnregisterResource(g_geometry0GraphicsRes));
//    handleCudaError(cudaGraphicsUnregisterResource(g_geometry1GraphicsRes));
    handleCudaError(cudaGraphicsUnregisterResource(g_resultGraphicsRes));
}

void Cuda::resize(int width, int height)
{
    handleCudaError(cudaFree(md_rayCastResults1));
//    handleCudaError(cudaMalloc((void**)&md_rayCastResults, sizeof(CudaRTResult) * width * height));
    handleCudaError(cudaMallocPitch((void**)&md_rayCastResults1, &m_rayCastResults1_pitch, sizeof(CudaRTResult) * width, height));
    handleCudaError(cudaMemcpyToSymbol(gd_rayCastResults1, &md_rayCastResults1, sizeof(CudaRTResult*)));
    handleCudaError(cudaMemcpyToSymbol(gd_rayCastResults1_pitch, &m_rayCastResults1_pitch, sizeof(size_t)));

    handleCudaError(cudaFree(md_rayCastResults2));
//    handleCudaError(cudaMalloc((void**)&md_rayCastResults, sizeof(CudaRTResult) * width * height));
    handleCudaError(cudaMallocPitch((void**)&md_rayCastResults2, &m_rayCastResults2_pitch, sizeof(CudaRTResult) * width, height));
    handleCudaError(cudaMemcpyToSymbol(gd_rayCastResults2, &md_rayCastResults2, sizeof(CudaRTResult*)));
    handleCudaError(cudaMemcpyToSymbol(gd_rayCastResults2_pitch, &m_rayCastResults2_pitch, sizeof(size_t)));

    handleCudaError(cudaFree(md_indirectLight));
    handleCudaError(cudaMalloc((void**)&md_indirectLight, sizeof(uchar4) * width * height));
    handleCudaError(cudaMemcpyToSymbol(gd_indirectLight, &md_indirectLight, sizeof(uchar4*)));

    initRandomNumbers(width, height);
}


void Cuda::deferredRender()
{
    handleCudaError(cudaGraphicsMapResources(1, &g_geometry0GraphicsRes));
//    handleCudaError(cudaGraphicsMapResources(1, &g_geometry1GraphicsRes));
    handleCudaError(cudaGraphicsMapResources(1, &g_resultGraphicsRes));

    handleCudaError(cudaGraphicsSubResourceGetMappedArray(&g_geometryTexture0Array, g_geometry0GraphicsRes, 0, 0));
//    handleCudaError(cudaGraphicsSubResourceGetMappedArray(&g_geometryTexture1Array, g_geometry1GraphicsRes, 0, 0));
    handleCudaError(cudaGraphicsSubResourceGetMappedArray(&g_resultArray, g_resultGraphicsRes, 0, 0));


//	texRefGeometry0.filterMode     = cudaFilterModePoint;
//    texRefGeometry0.normalized     = false;
//	texRefGeometry1.filterMode     = cudaFilterModePoint;
//    texRefGeometry1.normalized     = false;

//    handleCudaError(cudaBindTextureToArray(texRefGeometry0, geometryTexture0Array));
//    handleCudaError(cudaBindTextureToArray(texRefGeometry1, geometryTexture1Array));
    handleCudaError(cudaBindSurfaceToArray(g_geometry0SurfRef, g_geometryTexture0Array));
//    handleCudaError(cudaBindSurfaceToArray(g_geometry1SurfRef, g_geometryTexture1Array));
    handleCudaError(cudaBindSurfaceToArray(g_resultSurfRef, g_resultArray));


    for (int i=0; i<m_mappedShadowMaps; i++) {
        handleCudaError(cudaGraphicsMapResources(1, &g_shadowMapReses[i]));
        handleCudaError(cudaGraphicsSubResourceGetMappedArray(&g_shadowMapArrays[i], g_shadowMapReses[i], 0, 0));
    }
    if (m_mappedShadowMaps > 0) {
        handleCudaError(cudaBindTextureToArray(g_shadowMapsTexRef0, g_shadowMapArrays[0]));
    }
    if(m_mappedShadowMaps > 1)
        handleCudaError(cudaBindTextureToArray(g_shadowMapsTexRef1, g_shadowMapArrays[1]));
    if(m_mappedShadowMaps > 2)
        handleCudaError(cudaBindTextureToArray(g_shadowMapsTexRef2, g_shadowMapArrays[2]));
    if(m_mappedShadowMaps > 3)
        handleCudaError(cudaBindTextureToArray(g_shadowMapsTexRef3, g_shadowMapArrays[3]));
    if(m_mappedShadowMaps > 4)
        handleCudaError(cudaBindTextureToArray(g_shadowMapsTexRef4, g_shadowMapArrays[4]));
    if(m_mappedShadowMaps > 5)
        handleCudaError(cudaBindTextureToArray(g_shadowMapsTexRef5, g_shadowMapArrays[5]));
    if(m_mappedShadowMaps > 6)
        handleCudaError(cudaBindTextureToArray(g_shadowMapsTexRef6, g_shadowMapArrays[6]));
    if(m_mappedShadowMaps > 7)
        handleCudaError(cudaBindTextureToArray(g_shadowMapsTexRef7, g_shadowMapArrays[7]));
    if(m_mappedShadowMaps > 8)
        handleCudaError(cudaBindTextureToArray(g_shadowMapsTexRef8, g_shadowMapArrays[8]));
    if(m_mappedShadowMaps > 9)
        handleCudaError(cudaBindTextureToArray(g_shadowMapsTexRef9, g_shadowMapArrays[9]));

    updateLights();
    updateSceneBvh();

    int width = m_resultFB->width();
    int height = m_resultFB->height();

    CudaUniforms hostUniforms;
    hostUniforms.width = width;
    hostUniforms.height = height;
    hostUniforms.mouseDebugX = m_mouseDebugPosX;
    hostUniforms.mouseDebugY = height - m_mouseDebugPosY;
    hostUniforms.cameraInvVPMatrix = make_mat4(Application::instance()->camera()->inverseViewProjectionMatrix());
    hostUniforms.cameraUVW= make_mat4(Application::instance()->camera()->UVW());
    hostUniforms.cameraPosition = make_float4(glm::vec4(Application::instance()->camera()->position(), 1.f));
    m_mouseDebugPosX = -1;
    m_mouseDebugPosY = -1;



    handleCudaError(cudaMemcpy(md_uniforms, &hostUniforms, sizeof(CudaUniforms), cudaMemcpyHostToDevice));




#ifdef VIENNA_DEBUG
    handleCudaError(cudaDeviceSynchronize());
    auto begin = std::chrono::high_resolution_clock::now();
#endif

    dim3 threadsPerBlock(VIENNA_CUDA_BLOCK_WIDTH_RT, VIENNA_CUDA_BLOCK_HEIGHT_RT);
    dim3 numBlocks((width/2) / VIENNA_CUDA_BLOCK_WIDTH_RT, (height/2) / VIENNA_CUDA_BLOCK_HEIGHT_RT);
    if((width/2) % VIENNA_CUDA_BLOCK_WIDTH_RT > 0) numBlocks.x++;
    if((height/2) % VIENNA_CUDA_BLOCK_HEIGHT_RT > 0) numBlocks.y++;

    g_shadowMapsTexRef0.filterMode     = cudaFilterModePoint;
    g_shadowMapsTexRef1.filterMode     = cudaFilterModePoint;
    g_shadowMapsTexRef2.filterMode     = cudaFilterModePoint;
    g_shadowMapsTexRef3.filterMode     = cudaFilterModePoint;
    g_shadowMapsTexRef4.filterMode     = cudaFilterModePoint;
    g_shadowMapsTexRef5.filterMode     = cudaFilterModePoint;
    g_shadowMapsTexRef6.filterMode     = cudaFilterModePoint;
    g_shadowMapsTexRef7.filterMode     = cudaFilterModePoint;
    g_shadowMapsTexRef8.filterMode     = cudaFilterModePoint;
    g_shadowMapsTexRef9.filterMode     = cudaFilterModePoint;
    rayTraceKernel<<<numBlocks, threadsPerBlock>>>();

#ifdef VIENNA_DEBUG
    handleCudaError(cudaDeviceSynchronize());
    auto interim = std::chrono::high_resolution_clock::now();
    Application::instance()->addTiming("2.3.1 rayTraceKernel", std::chrono::duration_cast<VIENNA_TIMING_PRECISION>(interim - begin));
    begin = interim;
#endif

    threadsPerBlock = dim3(VIENNA_CUDA_BLOCK_WIDTH_INDIR_FILTER, VIENNA_BLOCK_HEIGHT_INDIR_FILTER);
    numBlocks = dim3((width/2) / VIENNA_CUDA_BLOCK_WIDTH_INDIR_FILTER, (height/2) / VIENNA_BLOCK_HEIGHT_INDIR_FILTER);
    if((width/2) % VIENNA_CUDA_BLOCK_WIDTH_INDIR_FILTER > 0) numBlocks.x++;
    if((height/2) % VIENNA_BLOCK_HEIGHT_INDIR_FILTER > 0) numBlocks.y++;
    threadsPerBlock.y = VIENNA_BLOCK_HEIGHT_INDIR_FILTER/2;

    cudaChannelFormatDesc chafd = cudaCreateChannelDesc(32, 32, 0, 0, cudaChannelFormatKindUnsigned);
    gt_rayCastResults.normalized = false;
    gt_rayCastResults.filterMode = cudaFilterModePoint;
//    handleCudaError(cudaBindTexture2D(nullptr, &gt_rayCastResults, md_rayCastResults, &chafd, width, height, width * sizeof(CudaRTResult)));
    handleCudaError(cudaBindTexture2D(nullptr, &gt_rayCastResults, md_rayCastResults1, &chafd, width, height, m_rayCastResults1_pitch));

    indirectLightFilterKernel<<<numBlocks, threadsPerBlock>>>(0);
    handleCudaError(cudaUnbindTexture(&gt_rayCastResults));
    handleCudaError(cudaBindTexture2D(nullptr, &gt_rayCastResults, md_rayCastResults2, &chafd, width, height, m_rayCastResults2_pitch));

    indirectLightFilterKernel<<<numBlocks, threadsPerBlock>>>(1);

    handleCudaError(cudaBindTexture2D(nullptr, &gt_rayCastResults, md_rayCastResults1, &chafd, width, height, m_rayCastResults1_pitch));


#ifdef VIENNA_DEBUG
    handleCudaError(cudaDeviceSynchronize());
    interim = std::chrono::high_resolution_clock::now();
    Application::instance()->addTiming("2.3.2 indirectLightFilterKe", std::chrono::duration_cast<VIENNA_TIMING_PRECISION>(interim - begin));
    begin = interim;
#endif

    threadsPerBlock = dim3(VIENNA_CUDA_BLOCK_WIDTH_DEFERRED, VIENNA_CUDA_BLOCK_HEIGHT_DEFERRED);
    numBlocks = dim3(width / VIENNA_CUDA_BLOCK_WIDTH_DEFERRED, height / VIENNA_CUDA_BLOCK_HEIGHT_DEFERRED);
    if(width % VIENNA_CUDA_BLOCK_WIDTH_DEFERRED > 0) numBlocks.x++;
    if(height % VIENNA_CUDA_BLOCK_HEIGHT_DEFERRED > 0) numBlocks.y++;

    g_shadowMapsTexRef0.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef1.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef2.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef3.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef4.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef5.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef6.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef7.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef8.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef9.filterMode     = cudaFilterModeLinear;
    deferredRenderKernel<<<numBlocks, threadsPerBlock>>>();

#ifdef VIENNA_DEBUG
    handleCudaError(cudaDeviceSynchronize());
    interim = std::chrono::high_resolution_clock::now();
    Application::instance()->addTiming("2.3.3 deferredRenderKernel", std::chrono::duration_cast<VIENNA_TIMING_PRECISION>(interim - begin));
    begin = interim;
#endif

    handleCudaError(cudaUnbindTexture(&gt_rayCastResults));

	for (int i = 0; i<m_mappedShadowMaps; i++) {
//        handleCudaError(cudaGraphicsUnmapResources(1, &g_shadowMapRes0));
        handleCudaError(cudaGraphicsUnmapResources(1, &g_shadowMapReses[i]));
	}

    handleCudaError(cudaGraphicsUnmapResources(1, &g_geometry0GraphicsRes));
//    handleCudaError(cudaGraphicsUnmapResources(1, &g_geometry1GraphicsRes));
    handleCudaError(cudaGraphicsUnmapResources(1, &g_resultGraphicsRes));

#ifdef VIENNA_DEBUG
    handleCudaError(cudaDeviceSynchronize());
#endif
}

void Cuda::updateShadowMaps(const std::vector<ShadowMapPass *>* shadowMapPasses)
{
    m_shadowMapPasses = shadowMapPasses;
    handleCudaError(cudaFree(md_cudaLights));

    for (int i=0; i<m_mappedShadowMaps; i++) {
        handleCudaError(cudaGraphicsUnregisterResource(g_shadowMapReses[i]));
//        handleCudaError(cudaGraphicsUnregisterResource(g_shadowMapRes0));
    }
    m_mappedShadowMaps = 0;

    std::vector<CudaLight> cudaLights;
    for(ShadowMapPass* shmp : (*shadowMapPasses)) {
        cudaLights.push_back(CudaLight(shmp->light()));
        FramebufferTexture* tex = shmp->depthBuffer();
        handleCudaError(cudaGraphicsGLRegisterImage(&g_shadowMapReses[m_mappedShadowMaps], tex->textureId(), tex->target(), cudaGraphicsRegisterFlagsReadOnly));
//        handleCudaError(cudaGraphicsGLRegisterImage(&g_shadowMapRes0, tex->textureId(), tex->target(), cudaGraphicsRegisterFlagsReadOnly));
        m_mappedShadowMaps++;
        if(m_mappedShadowMaps >= VIENNA_MAX_LIGHT_COUNT) break;
    }

    size_t lightCount = cudaLights.size();
    // allocate memory on the device for our data
    handleCudaError(cudaMalloc((void**)&md_cudaLights, sizeof(CudaLight) * lightCount));

    // copy our data into the allocated memory
    handleCudaError(cudaMemcpy(md_cudaLights, &cudaLights.front(), sizeof(CudaLight) * lightCount, cudaMemcpyHostToDevice));

    // copy the pointer to our data into the global __device__ variable.
    handleCudaError(cudaMemcpyToSymbol(g_lights, &md_cudaLights, sizeof(CudaLight*)));

    // copy the size
    handleCudaError(cudaMemcpyToSymbol(g_lightsCount, &lightCount, 1));


    g_shadowMapsTexRef0.normalized     = true;
    g_shadowMapsTexRef1.normalized     = true;
    g_shadowMapsTexRef2.normalized     = true;
    g_shadowMapsTexRef3.normalized     = true;
    g_shadowMapsTexRef4.normalized     = true;
    g_shadowMapsTexRef5.normalized     = true;
    g_shadowMapsTexRef6.normalized     = true;
    g_shadowMapsTexRef7.normalized     = true;
    g_shadowMapsTexRef8.normalized     = true;
    g_shadowMapsTexRef9.normalized     = true;

    g_shadowMapsTexRef0.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef1.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef2.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef3.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef4.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef5.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef6.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef7.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef8.filterMode     = cudaFilterModeLinear;
    g_shadowMapsTexRef9.filterMode     = cudaFilterModeLinear;
}

BVHLayout Cuda::bvhLayout()
{
    return BVHLayout_AOS_AOS;
}

void Cuda::updateLights()
{
    std::vector<CudaLight> cudaLights;
    int i=0;
    for(ShadowMapPass* shmp : (*m_shadowMapPasses)) {
        cudaLights.push_back(CudaLight(shmp->light()));
        i++;
        if(i >= VIENNA_MAX_LIGHT_COUNT) break;
    }

    size_t lightCount = cudaLights.size();

    // copy our data into the allocated memory
    handleCudaError(cudaMemcpy(md_cudaLights, &cudaLights.front(), sizeof(CudaLight) * lightCount, cudaMemcpyHostToDevice));

    // copy the pointer to our data into the global __device__ variable.
//    cudaMemcpyToSymbol(g_lights, &m_deviceCudaLights, sizeof(CudaLight*));

    // copy the size
    //    cudaMemcpyToSymbol(g_lightsCount, &lightCount, 1);
}

void Cuda::updateSceneBvh()
{
    const SerialisedSceneAccelerationStructure* ssas = Application::instance()->bvhManager()->serialisedSceneAccelerationStructure();
    handleCudaError(cudaMemcpy(md_serialisedSceneAccelerationNodes, ssas->m_serialisedSceneAccelerationNodes.data(),
                               sizeof(SceneAccelerationNode) * ssas->m_serialisedSceneAccelerationNodes.size(), cudaMemcpyHostToDevice));
}

void Cuda::initUniforms()
{
    handleCudaError(cudaMalloc((void**)&md_uniforms, sizeof(CudaUniforms) ));
    handleCudaError(cudaMemcpyToSymbol(g_uniforms, &md_uniforms, sizeof(CudaUniforms*)));
}

void Cuda::initConstData()
{
    DataManager* dataManager = DataManager::instance();
    const std::vector<MaterialDescription>& materials = dataManager->getMaterialDescriptions();

    size_t materialCount = materials.size();
    if(materialCount > VIENNA_MAX_MATERIAL_COUNT) materialCount = VIENNA_MAX_MATERIAL_COUNT;

    // allocate memory on the device for our data
    handleCudaError(cudaMalloc((void**)&md_materialDescriptions, sizeof(MaterialDescription) * materialCount));

    // copy our data into the allocated memory
    handleCudaError(cudaMemcpy(md_materialDescriptions, materials.data(), sizeof(MaterialDescription) * materialCount, cudaMemcpyHostToDevice));

    // copy the pointer to our data into the global __device__ variable.
    handleCudaError(cudaMemcpyToSymbol(g_materials, &md_materialDescriptions, sizeof(MaterialDescription*)));

    initBvh();
    initTextures();
}

void copyTextureData(cudaArray* deviceTexture, cudaExtent size, const std::vector<std::pair<TextureDescription, GLbyte*> >& hostTextures) {
    uchar4* h_data = new uchar4[size.width * size.height * size.depth];

    unsigned int i = 0;
    for(std::pair<TextureDescription, GLbyte*> pair : hostTextures) {
        memcpy(h_data + size.width * size.height * i,
               pair.second,
               pair.first.width * pair.first.height * sizeof(uchar4));
        i++;
    }

    cudaMemcpy3DParms copyParams = {0};
    copyParams.srcPtr       = make_cudaPitchedPtr(h_data, size.width * sizeof(uchar4), size.width, size.height);
    copyParams.dstArray     = deviceTexture;
    copyParams.extent       = make_cudaExtent(size.width/* * sizeof(uchar4)*/, size.height, size.depth);
    copyParams.dstPos       = make_cudaPos(0, 0, 0);
    copyParams.kind         = cudaMemcpyHostToDevice;
    Cuda::handleCudaError(cudaMemcpy3D(&copyParams));

    delete h_data;
}

//void copyTextureData(cudaMipmappedArray* deviceTexture, cudaExtent size, const std::vector<std::pair<TextureDescription, GLbyte*> >& hostTextures) {
//    cudaArray* level0;
//    Cuda::handleCudaError(cudaGetMipmappedArrayLevel(&level0, deviceTexture, 0));

//    uchar4* h_data = new uchar4[size.width * size.height * size.depth];

//	for (int i = 0; i < size.width * size.height * size.depth; i++) h_data[i] = make_uchar4(0, 0, 0, 0);

//	/*
//    unsigned int i = 0;
//    for(std::pair<TextureDescription, GLbyte*> pair : hostTextures) {
//        memcpy(h_data + size.width*size.height*i,
//               pair.second,
//               pair.first.width * pair.first.height * sizeof(uchar4));
//        i++;
//    }*/

//    cudaMemcpy3DParms copyParams = {0};
//    copyParams.srcPtr       = make_cudaPitchedPtr(h_data, size.width * sizeof(uchar4), size.width, size.height);
//    copyParams.dstArray     = level0;
//    copyParams.extent       = make_cudaExtent(size.width/* * sizeof(uchar4)*/, size.height, size.depth);
//    copyParams.dstPos       = make_cudaPos(0, 0, 0);
//    copyParams.kind         = cudaMemcpyHostToDevice;
//    Cuda::handleCudaError(cudaMemcpy3D(&copyParams));

//    delete h_data;
//}

//void copyTextureData(cudaMipmappedArray* deviceMipTexture, cudaArray* deviceTexture, cudaExtent size) {
//    cudaArray* level0;
//    Cuda::handleCudaError(cudaGetMipmappedArrayLevel(&level0, deviceMipTexture, 0));

//    cudaMemcpy3DParms copyParams = {0};
//    copyParams.srcArray     = deviceTexture;
//    copyParams.dstArray     = level0;
//    copyParams.extent       = make_cudaExtent(size.width/* * sizeof(uchar4)*/, size.height, size.depth);
//    copyParams.dstPos       = make_cudaPos(0, 0, 0);
//    copyParams.kind         = cudaMemcpyDeviceToDevice;
//    Cuda::handleCudaError(cudaMemcpy3D(&copyParams));
//}

void Cuda::initTextures()
{
    TextureManager* texManager = TextureManager::instance();
    cudaChannelFormatDesc desc = cudaCreateChannelDesc<uchar4>();

    cudaExtent size = make_cudaExtent(256/VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR, 256/VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR, texManager->textureArray256small().size());
	if (size.depth > 0) {
		handleCudaError(cudaMalloc3DArray(&ma_texture256, &desc, size, cudaArrayLayered));
        copyTextureData(ma_texture256, size, texManager->textureArray256small());
        cudaBindTextureToArray(gt_textures256, ma_texture256);
	}

    size = make_cudaExtent(512/VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR, 512/VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR, texManager->textureArray512small().size());
	if (size.depth > 0) {
		handleCudaError(cudaMalloc3DArray(&ma_texture512, &desc, size, cudaArrayLayered));
        copyTextureData(ma_texture512, size, texManager->textureArray512small());
        cudaBindTextureToArray(gt_textures512, ma_texture512);
	}

    size = make_cudaExtent(1024/VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR, 1024/VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR, texManager->textureArray1024small().size());
	if (size.depth > 0) {
		handleCudaError(cudaMalloc3DArray(&ma_texture1024, &desc, size, cudaArrayLayered));
        copyTextureData(ma_texture1024, size, texManager->textureArray1024small());
        cudaBindTextureToArray(gt_textures1024, ma_texture1024);
	}

    size = make_cudaExtent(2048/VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR, 2048/VIENNA_SMALL_TEXTURE_DIVIDE_FACTOR, texManager->textureArray2048small().size());
	if (size.depth > 0) {
		handleCudaError(cudaMalloc3DArray(&ma_texture2048, &desc, size, cudaArrayLayered));
        copyTextureData(ma_texture2048, size, texManager->textureArray2048small());
        cudaBindTextureToArray(gt_textures2048, ma_texture2048);
	}

//    int mipLevelels = 1;
//    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<uchar4>();

//	size = make_cudaExtent(256, 256, texManager->textureArray256().size());
//	if (size.depth > 0) {
//		handleCudaError(cudaMallocMipmappedArray(&ma_texture256mm, &desc, size, mipLevelels, cudaArrayLayered));
//		copyTextureData(ma_texture256mm, ma_texture256, size);
//		//    copyTextureData(ma_texture256mm, size, texManager->textureArray256());
//    cudaBindTextureToMipmappedArray(gt_textures256, ma_texture256mm, channelDesc);
//	}

//	size = make_cudaExtent(512, 512, texManager->textureArray512().size());
//	if (size.depth > 0) {
//		handleCudaError(cudaMallocMipmappedArray(&ma_texture512mm, &desc, size, mipLevelels, cudaArrayLayered));
//		//    copyTextureData(ma_texture512mm, size, texManager->textureArray512());
//		copyTextureData(ma_texture512mm, ma_texture512, size);
//    cudaBindTextureToMipmappedArray(gt_textures512, ma_texture512mm, channelDesc);
//	}

//	size = make_cudaExtent(1024, 1024, texManager->textureArray1024().size());
//	if (size.depth > 0) {
//		handleCudaError(cudaMallocMipmappedArray(&ma_texture1024mm, &desc, size, mipLevelels, cudaArrayLayered));
//		//    copyTextureData(ma_texture1024mm, size, texManager->textureArray1024());
//		copyTextureData(ma_texture1024mm, ma_texture1024, size);
//    cudaBindTextureToMipmappedArray(gt_textures1024, ma_texture1024mm, channelDesc);
//	}

//    size = make_cudaExtent(2048, 2048, texManager->textureArray2048().size());
//	if (size.depth > 0) {
//		handleCudaError(cudaMallocMipmappedArray(&ma_texture2048mm, &desc, size, mipLevelels, cudaArrayLayered));
//	//    copyTextureData(ma_texture2048mm, size, texManager->textureArray2048());
//		copyTextureData(ma_texture2048mm, ma_texture2048, size);
//    cudaBindTextureToMipmappedArray(gt_textures2048, ma_texture2048mm, channelDesc);
//	}


    gt_textures256.normalized = true;
    gt_textures256.filterMode = cudaFilterModeLinear;
    gt_textures256.addressMode[0] = cudaAddressModeWrap;
    gt_textures256.addressMode[1] = cudaAddressModeWrap;

    gt_textures512.normalized = true;
    gt_textures512.filterMode = cudaFilterModeLinear;
    gt_textures512.addressMode[0] = cudaAddressModeWrap;
    gt_textures512.addressMode[1] = cudaAddressModeWrap;

    gt_textures1024.normalized = true;
    gt_textures1024.filterMode = cudaFilterModeLinear;
    gt_textures1024.addressMode[0] = cudaAddressModeWrap;
    gt_textures1024.addressMode[1] = cudaAddressModeWrap;

    gt_textures2048.normalized = true;
    gt_textures2048.filterMode = cudaFilterModeLinear;
    gt_textures2048.addressMode[0] = cudaAddressModeWrap;
    gt_textures2048.addressMode[1] = cudaAddressModeWrap;

}

void Cuda::initBvh()
{
    const SerialisedSceneAccelerationStructure* ssas = Application::instance()->bvhManager()->serialisedSceneAccelerationStructure();

    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float4>();
	handleCudaError(cudaMalloc((void**)&md_nodesA, sizeof(unsigned char) * ssas->m_nodeBuffer.size()));
	handleCudaError(cudaMemcpy(md_nodesA, ssas->m_nodeBuffer.data(), sizeof(unsigned char) * ssas->m_nodeBuffer.size(), cudaMemcpyHostToDevice));
	handleCudaError(cudaBindTexture(nullptr, t_nodesA, md_nodesA, channelDesc, ssas->m_nodeBuffer.size()));

	handleCudaError(cudaMalloc((void**)&md_trisA, sizeof(unsigned char) * ssas->m_triWoopBuffer.size()));
	handleCudaError(cudaMemcpy(md_trisA, ssas->m_triWoopBuffer.data(), sizeof(unsigned char) * ssas->m_triWoopBuffer.size(), cudaMemcpyHostToDevice));
	handleCudaError(cudaBindTexture(nullptr, t_trisA, md_trisA, channelDesc, ssas->m_triWoopBuffer.size()));
    handleCudaError(cudaMemcpyToSymbol(gd_trisA, &md_trisA, sizeof(float4*)));

	handleCudaError(cudaMalloc((void**)&md_triIndices, sizeof(unsigned char) * ssas->m_triIndexBuffer.size()));
	handleCudaError(cudaMemcpy(md_triIndices, ssas->m_triIndexBuffer.data(), sizeof(unsigned char) * ssas->m_triIndexBuffer.size(), cudaMemcpyHostToDevice));
    channelDesc = cudaCreateChannelDesc<int>();
	handleCudaError(cudaBindTexture(nullptr, t_triIndices, md_triIndices, channelDesc, ssas->m_triIndexBuffer.size()));
    handleCudaError(cudaMemcpyToSymbol(gd_triIndices, &md_triIndices, sizeof(int*)));

	unsigned char sceneAccelerationNodeCount = (unsigned char)ssas->m_serialisedSceneAccelerationNodes.size();
	handleCudaError(cudaMemcpyToSymbol(gd_sceneAccelerationNodeCount, &sceneAccelerationNodeCount, sizeof(unsigned char)));

    handleCudaError(cudaMalloc((void**)&md_serialisedSceneAccelerationNodes, sizeof(SceneAccelerationNode) * ssas->m_serialisedSceneAccelerationNodes.size()));
    handleCudaError(cudaMemcpy(md_serialisedSceneAccelerationNodes, ssas->m_serialisedSceneAccelerationNodes.data(),
               sizeof(SceneAccelerationNode) * ssas->m_serialisedSceneAccelerationNodes.size(), cudaMemcpyHostToDevice));
    handleCudaError(cudaMemcpyToSymbol(gd_serialisedSceneAccelerationNodes, &md_serialisedSceneAccelerationNodes, sizeof(SceneAccelerationNode*)));

    handleCudaError(cudaMalloc((void**)&md_vertexIndices, sizeof(int3) * ssas->m_vertexIndices.size()));
    handleCudaError(cudaMemcpy(md_vertexIndices, ssas->m_vertexIndices.data(), sizeof(int3) * ssas->m_vertexIndices.size(), cudaMemcpyHostToDevice));
    handleCudaError(cudaMemcpyToSymbol(gd_vertexIndices, &md_vertexIndices, sizeof(int3*)));

    handleCudaError(cudaMalloc((void**)&md_positions, sizeof(float3) * ssas->m_positions.size()));
    handleCudaError(cudaMemcpy(md_positions, ssas->m_positions.data(), sizeof(float3) * ssas->m_positions.size(), cudaMemcpyHostToDevice));
    handleCudaError(cudaMemcpyToSymbol(gd_positions, &md_positions, sizeof(float3*)));

    handleCudaError(cudaMalloc((void**)&md_normals, sizeof(float3) * ssas->m_normals.size()));
    handleCudaError(cudaMemcpy(md_normals, ssas->m_normals.data(), sizeof(float3) * ssas->m_normals.size(), cudaMemcpyHostToDevice));
    handleCudaError(cudaMemcpyToSymbol(gd_normals, &md_normals, sizeof(float3*)));

    handleCudaError(cudaMalloc((void**)&md_texCoords, sizeof(float2) * ssas->m_texCoords.size()));
    handleCudaError(cudaMemcpy(md_texCoords, ssas->m_texCoords.data(), sizeof(float2) * ssas->m_texCoords.size(), cudaMemcpyHostToDevice));
    handleCudaError(cudaMemcpyToSymbol(gd_texCoords, &md_texCoords, sizeof(float2*)));

    handleCudaError(cudaMalloc((void**)&md_triangleMaterials, sizeof(int) * ssas->m_triangleMaterials.size()));
    handleCudaError(cudaMemcpy(md_triangleMaterials, ssas->m_triangleMaterials.data(), sizeof(int) * ssas->m_triangleMaterials.size(), cudaMemcpyHostToDevice));
    handleCudaError(cudaMemcpyToSymbol(gd_triangleMaterials, &md_triangleMaterials, sizeof(int*)));
}

void Cuda::initRandomNumbers(int w, int h)
{
    handleCudaError(cudaFree(md_sobolRandomNumbersStates));
    handleCudaError(cudaMalloc((void**)&md_sobolRandomNumbersStates, sizeof(curandStateScrambledSobol32_t) * w * h * 2));
    handleCudaError(cudaMemcpyToSymbol(gd_sobolRandomNumbersStates, &md_sobolRandomNumbersStates, sizeof(curandStateScrambledSobol32_t*)));
//    handleCudaError(cudaMalloc((void**)&md_sobolRandomNumbersStates, sizeof(curandStateSobol32_t) * w * h * 2));
//    handleCudaError(cudaMemcpyToSymbol(gd_sobolRandomNumbersStates, &md_sobolRandomNumbersStates, sizeof(curandStateSobol32_t*)));

    handleCudaError(cudaFree(md_randomNumberStates));
    handleCudaError(cudaMalloc((void**)&md_randomNumberStates, sizeof(curandStateXORWOW) * w * h));
    handleCudaError(cudaMemcpyToSymbol(gd_randomNumberStates, &md_randomNumberStates, sizeof(curandStateXORWOW*)));


    handleCudaError(cudaFree(md_randomNumberDirections));
    handleCudaError(cudaMalloc((void**)&md_randomNumberDirections, sizeof(curandDirectionVectors32_t) * 2));
    curandDirectionVectors32_t* h_directions;
    curandStatus_t curandStatus = curandGetDirectionVectors32(&h_directions, CURAND_DIRECTION_VECTORS_32_JOEKUO6);
    if(curandStatus != CURAND_STATUS_SUCCESS) {
        std::cerr << "Cuda::initRandomNumbers() curandGetDirectionVectors32 didn't return status CURAND_STATUS_SUCCESS" << std::endl;
    }

    handleCudaError(cudaMemcpy(md_randomNumberDirections, h_directions, sizeof(curandDirectionVectors32_t) * 2, cudaMemcpyHostToDevice));

    unsigned int* randomNumbers = new unsigned int[w*h];
    for (int i=0; i<w; i++) {
        for (int j=0; j<h; j++) {
            int rnd = std::rand() % (w*h);
            randomNumbers[j*w + i] = static_cast<unsigned int>(rnd);
        }
    }
    unsigned int* d_randomNumbers;
    handleCudaError(cudaMalloc((void**)&d_randomNumbers, sizeof(unsigned int) * w * h));
    handleCudaError(cudaMemcpy(d_randomNumbers, randomNumbers, sizeof(unsigned int) * w * h, cudaMemcpyHostToDevice));

    unsigned int* scrambleConstants = new unsigned int[w*h];
    curandStatus = curandGetScrambleConstants32(&scrambleConstants);
	if (curandStatus != CURAND_STATUS_SUCCESS) {
		std::cerr << "Cuda::initRandomNumbers() curandGetDirectionVectors32 didn't return status CURAND_STATUS_SUCCESS" << std::endl;
	}

    unsigned int* d_scrambleConstants;
    handleCudaError(cudaMalloc((void**)&d_scrambleConstants, sizeof(unsigned int) * 2));
    handleCudaError(cudaMemcpy(d_scrambleConstants, scrambleConstants, sizeof(unsigned int) * 2, cudaMemcpyHostToDevice));

    CudaUniforms hostUniforms;
    hostUniforms.width = w;
    hostUniforms.height = h;
    handleCudaError(cudaMemcpy(md_uniforms, &hostUniforms, sizeof(CudaUniforms), cudaMemcpyHostToDevice));

//    dim3 threadsPerBlock(VIENNA_CUDA_BLOCK_WIDTH, VIENNA_CUDA_BLOCK_HEIGHT);
//    dim3 numBlocks(w / VIENNA_CUDA_BLOCK_WIDTH, h / VIENNA_CUDA_BLOCK_HEIGHT);
    dim3 threadsPerBlock(VIENNA_CUDA_BLOCK_WIDTH_DEFERRED);
    dim3 numBlocks(w / VIENNA_CUDA_BLOCK_WIDTH_DEFERRED);
//    dim3 numBlocks(100, 100);

    if(w % VIENNA_CUDA_BLOCK_WIDTH_DEFERRED > 0) numBlocks.x++;
//    if(h % VIENNA_CUDA_BLOCK_HEIGHT_DEFERRED > 0) numBlocks.y++;

	for (int y = 0; y < h; y++) {
        initRandomNumbersKernel << <numBlocks, threadsPerBlock >> >(md_randomNumberDirections, y, d_scrambleConstants, d_randomNumbers);
		handleCudaError(cudaDeviceSynchronize());
        glfwSwapBuffers(Application::instance()->renderer()->glfwWindowHandle());
	}

    handleCudaError(cudaFree(d_scrambleConstants));
    //delete[] scrambleRands;

}

void Cuda::deInitConstData()
{
    cudaFree(md_materialDescriptions);
}

void Cuda::handleCudaError(cudaError_t error)
{
    if(error != cudaSuccess) {
        std::cerr << "CUDA error: " << cudaGetErrorString(error) << std::endl;
    }
}


