/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CUHELM_H
#define CUHELM_H

#include <cuda_runtime.h>
#include <glm/glm.hpp>

#include "HelperMath.h"

union UnpackToShortUnion{
    unsigned int a;
    struct {
        short a, b;
    } myShorts;
};
union UnpackToUCharUnion{
    unsigned int val;
    uchar4 uchars;
};
union UnpackToCharUnion{
    unsigned int val;
    char4 chars;
};

#ifndef __CUDA_ARCH__
__host__ inline float cuhelm_host_saturatef(float val) {
    if(val >= 0.f && val <= 1.f) return val;
    else if (val > 1.f) return 1.f;
    else return 0.f;
}
#define cuhelm_saturatef cuhelm_host_saturatef
#else
#define cuhelm_saturatef __saturatef
#endif

// small helper functions (step, min, max etc would go here)
__forceinline__ __host__ __device__ float step(float edge, float x) { return x < edge ? 0.f : 1.f; }

__inline__ __host__ __device__ float2 step(float edge, float2 x) { return make_float2(step(edge, x.x), step(edge, x.y)); }
__inline__ __host__ __device__ float3 step(float edge, float3 x) { return make_float3(step(edge, x.x), step(edge, x.y), step(edge, x.z)); }
__inline__ __host__ __device__ float4 step(float edge, float4 x) { return make_float4(step(edge, x.x), step(edge, x.y), step(edge, x.z), step(edge, x.w)); }

__inline__ __host__ __device__ float2 step(float2 edge, float2 x) { return make_float2(step(edge.x, x.x), step(edge.y, x.y)); }
__inline__ __host__ __device__ float3 step(float3 edge, float3 x) { return make_float3(step(edge.x, x.x), step(edge.y, x.y), step(edge.z, x.z)); }
__inline__ __host__ __device__ float4 step(float4 edge, float4 x) { return make_float4(step(edge.x, x.x), step(edge.y, x.y), step(edge.z, x.z), step(edge.w, x.w)); }

__forceinline__ __host__ __device__ float linstep(float lowEdge, float highEdge, float x) {
    return clamp((x - lowEdge) / (highEdge - lowEdge), 0.0f, 1.0f);
}

__inline__ __host__ __device__ float2 pow(float2 bases, float exponent) {
    return make_float2(pow(bases.x, exponent), pow(bases.y, exponent));
}
__inline__ __host__ __device__ float3 pow(float3 bases, float exponent) {
    return make_float3(pow(bases.x, exponent), pow(bases.y, exponent), pow(bases.z, exponent));
}
__inline__ __host__ __device__ float4 pow(float4 bases, float exponent) {
    return make_float4(pow(bases.x, exponent), pow(bases.y, exponent), pow(bases.z, exponent), pow(bases.w, exponent));
}

__inline__ __host__ __device__ float2 sqrtf(float2 values) {
    return make_float2(sqrtf(values.x), sqrtf(values.y));
}
__inline__ __host__ __device__ float3 sqrtf(float3 values) {
    return make_float3(sqrtf(values.x), sqrtf(values.y), sqrtf(values.z));
}
__inline__ __host__ __device__ float4 sqrtf(float4 values) {
    return make_float4(sqrtf(values.x), sqrtf(values.y), sqrtf(values.z), sqrtf(values.w));
}
// additional constructors
static __inline__ __host__ __device__ uchar4 make_uchar4(float4 v)
{
  uchar4 t; t.x = v.x; t.y = v.y; t.z = v.z; t.w = v.w; return t;
}
static __inline__ __host__ __device__ char3 make_char3(char4 v)
{
    char3 t; t.x = v.x; t.y = v.y; t.z = v.z; return t;
}
static __inline__ __host__ __device__ uchar3 make_uchar3(uchar4 v)
{
    uchar3 t; t.x = v.x; t.y = v.y; t.z = v.z; return t;
}

inline __host__ __device__ float2 make_float2(float4 a)
{
    return make_float2(a.x, a.y);
}


// additional operators
inline __host__ __device__ uchar4 operator+(uchar4 a, uchar4 b)
{
    return make_uchar4(a.x + b.x, a.y + b.y, a.z + b.z,  a.w + b.w);
}
inline __host__ __device__ void operator+=(uchar4 &a, uchar4 b)
{
    a.x += b.x;
    a.y += b.y;
    a.z += b.z;
    a.w += b.w;
}


// converting from floatX to ucharX, charX and uint and vice versa
// converting meaning float ranges [0, 1] or [-1, 1] to char ranges [0, 255] or [-127, 127] respectively.
__host__ __device__ __forceinline__ float4 convertTo_float4(char4 v) {
    return make_float4(v.x, v.y, v.z, v.w) * (1.f / 127.f);
}

__host__ __device__ __forceinline__ float3 convertTo_float3(char4 v) {
    return make_float3(v.x, v.y, v.z) * (1.f / 127.f);
}

__host__ __device__ __forceinline__ float4 convertTo_float4(uchar4 v) {
    return make_float4(v.x, v.y, v.z, v.w) * (1.f / 255.f);
}

__host__ __device__ __forceinline__ float3 convertTo_float3(uchar4 v) {
    return make_float3(v.x, v.y, v.z) * (1.f / 255.f);
}
__host__ __device__ __forceinline__ float3 convertTo_float3(char3 v) {
    return make_float3(v.x, v.y, v.z) * (1.f / 127.f);
}
__host__ __device__ __forceinline__ float3 convertTo_float3(uchar3 v) {
    return make_float3(v.x, v.y, v.z) * (1.f / 255.f);
}


__host__ __device__ __forceinline__ uchar4 convertTo_uchar4(float4 rgba)
{
    rgba.x = cuhelm_saturatef(rgba.x);   // clamp to [0.0, 1.0]
    rgba.y = cuhelm_saturatef(rgba.y);
    rgba.z = cuhelm_saturatef(rgba.z);
    rgba.w = cuhelm_saturatef(rgba.w);
    return make_uchar4(rgba.x * 255.0f, rgba.y * 255.0f, rgba.z * 255.0f, rgba.w * 255.0f);
}

__host__ __device__ __forceinline__ uchar4 convertTo_uchar4(float3 rgba)
{
    rgba.x = cuhelm_saturatef(rgba.x);   // clamp to [0.0, 1.0]
    rgba.y = cuhelm_saturatef(rgba.y);
    rgba.z = cuhelm_saturatef(rgba.z);
    return make_uchar4(rgba.x * 255.0f, rgba.y * 255.0f, rgba.z * 255.0f, 0);
}

__host__ __device__ __forceinline__ uint convertTo_uint(uchar4 val)
{
    UnpackToUCharUnion unpacker;
    unpacker.uchars = val;
    return unpacker.val;
}
__host__ __device__ __forceinline__ uint convertTo_uint(uchar3 val)
{
    UnpackToUCharUnion unpacker;
    unpacker.uchars = make_uchar4(val.x, val.y, val.z, 0);
    return unpacker.val;
}

// matrices mat3 and mat4
struct mat4 {
    float4 c0, c1, c2, c3;
};

struct mat3 {
    float3 c0, c1, c2;
};

inline __host__ float3 make_float3(glm::vec3 v) {
    return make_float3(v.x, v.y, v.z);
}
inline __host__ float4 make_float4(glm::vec4 v) {
    return make_float4(v.x, v.y, v.z, v.w);
}
__host__ __device__ __forceinline__ float4 make_float4(uchar4 v) {
    return make_float4(v.x, v.y, v.z, v.w);
}
inline __host__ mat4 make_mat4(const glm::mat4& m) {
    mat4 n;
    n.c0 = make_float4(m[0]);
    n.c1 = make_float4(m[1]);
    n.c2 = make_float4(m[2]);
    n.c3 = make_float4(m[3]);
    return n;
}

__host__ __device__ __forceinline__ mat3 make_mat3(float3 col0, float3 col1, float3 col2) {
    mat3 n;
    n.c0 = col0;
    n.c1 = col1;
    n.c2 = col2;
    return n;
}

__host__ __device__ __forceinline__ mat3 make_mat3(const mat4& m) {
    mat3 n;
    n.c0 = make_float3(m.c0);
    n.c1 = make_float3(m.c1);
    n.c2 = make_float3(m.c2);
    return n;
}

__host__ __device__ __forceinline__ mat3 transposed(const mat3& m) {
    mat3 n;
    n.c0 = make_float3(m.c0.x, m.c1.x, m.c2.x);
    n.c1 = make_float3(m.c0.y, m.c1.y, m.c2.y);
    n.c2 = make_float3(m.c0.z, m.c1.z, m.c2.z);
    return n;
}

__host__ __device__ __forceinline__ mat3 make_transposedMat3(const mat4& m) {
    mat3 n;
    n.c0 = make_float3(m.c0.x, m.c1.x, m.c2.x);
    n.c1 = make_float3(m.c0.y, m.c1.y, m.c2.y);
    n.c2 = make_float3(m.c0.z, m.c1.z, m.c2.z);
    return n;
}

//will create a rotation matrix that rotates (0,0,1) into vec direction
__host__ __device__ __forceinline__ mat3 make_onbMat3(float3 vec) {
    int dim = 0;
    if (fabsf(vec.y) < fabsf(vec.x))
        dim = 1;
    if (fabsf(vec.z) < fabsf(((float*)&vec)[dim]))
        dim = 2;
    float3 helper = vec;
    ((float*)&helper)[dim] = 1.f;

    float3 orthoHelper1 = cross(vec, helper);
    orthoHelper1 = normalize(orthoHelper1);
    float3 orthoHelper2 = cross(vec, orthoHelper1);

    return make_mat3(orthoHelper1, orthoHelper2, vec);
}

inline __device__ __host__ float4 operator * (const mat4& m, float4 v) {
    return make_float4(m.c0.x*v.x + m.c1.x*v.y + m.c2.x*v.z + m.c3.x*v.w,
                       m.c0.y*v.x + m.c1.y*v.y + m.c2.y*v.z + m.c3.y*v.w,
                       m.c0.z*v.x + m.c1.z*v.y + m.c2.z*v.z + m.c3.z*v.w,
                       m.c0.w*v.x + m.c1.w*v.y + m.c2.w*v.z + m.c3.w*v.w);
//    return make_float4(m.r0.x*v.x + m.r0.y*v.y + m.r0.z*v.z + m.r0.w*v.w,
//                       m.r1.x*v.x + m.r1.y*v.y + m.r1.z*v.z + m.r1.w*v.w,
//                       m.r2.x*v.x + m.r2.y*v.y + m.r2.z*v.z + m.r2.w*v.w,
//                       m.r3.x*v.x + m.r3.y*v.y + m.r3.z*v.z + m.r3.w*v.w);
}

inline __device__ __host__ float3 transformHPoint3f(const mat4& m, float4 v) {
    return make_float3(m.c0.x*v.x + m.c1.x*v.y + m.c2.x*v.z + m.c3.x,
                       m.c0.y*v.x + m.c1.y*v.y + m.c2.y*v.z + m.c3.y,
                       m.c0.z*v.x + m.c1.z*v.y + m.c2.z*v.z + m.c3.z);
}

inline __device__ __host__ float3 transformHPoint(const mat4& m, float3 v) {
    return make_float3(m.c0.x*v.x + m.c1.x*v.y + m.c2.x*v.z + m.c3.x,
                       m.c0.y*v.x + m.c1.y*v.y + m.c2.y*v.z + m.c3.y,
                       m.c0.z*v.x + m.c1.z*v.y + m.c2.z*v.z + m.c3.z);
}

inline __device__ __host__ float3 operator * (const mat3& m, float3 v) {
    return make_float3(m.c0.x*v.x + m.c1.x*v.y + m.c2.x*v.z,
                       m.c0.y*v.x + m.c1.y*v.y + m.c2.y*v.z,
                       m.c0.z*v.x + m.c1.z*v.y + m.c2.z*v.z);
}

#endif // CUHELM_H
