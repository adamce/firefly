/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INDIRECTLIGHTFILTERKERNEL_H
#define INDIRECTLIGHTFILTERKERNEL_H

#include "auxiliary/ApplicationConstantsCuda.h"
#include "GpuMemoryData.h"
#include "CommonDefs.h"

__device__ __forceinline__
void getDataForPixel(float3* normal, float3* colour, float* dist, int x, int y) {
    if(y >= 0 && x >= 0 && y < g_uniforms->height && x < g_uniforms->width) {
//        *normal = convertTo_float3(gd_rayCastResults[y * g_uniforms->width + x].access.normal);
//        *colour = convertTo_float3(gd_rayCastResults[y * g_uniforms->width + x].access.irradiance);
//        *dist = gd_rayCastResults[y * g_uniforms->width + x].access.dist();

        CudaRTResult tmp;
        tmp.data = tex2D(gt_rayCastResults, ((float) x) + 0.5f, ((float) y) + 0.5f);
        *normal = convertTo_float3(tmp.access.normal);
        *colour = convertTo_float3(tmp.access.irradiance);
        *dist = tmp.access.dist();

    }
    else {
        *normal = make_float3(0.f, 0.f, 0.f);
        *colour = make_float3(0.f, 0.f, 0.f);
        *dist = 1000000000;
    }
}

__device__ __forceinline__
CudaRTResult getDataForPixel(int x, int y) {
    if(y >= 0 && x >= 0 && y < g_uniforms->height && x < g_uniforms->width) {
        CudaRTResult tmp;
        tmp.data = tex2D(gt_rayCastResults, ((float) x) + 0.5f, ((float) y) + 0.5f);
        return tmp;
//        return gd_rayCastResults[y * (gd_rayCastResults_pitch / sizeof(CudaRTResult)) + x];
    }
    else {
        CudaRTResult tmp;
        tmp.access.normal = make_char3(0, 0, 0);
        tmp.access.irradiance = make_uchar3(0, 0, 0);
        tmp.access.m_dist = -1;
        return tmp;
    }
}

__global__
void indirectLightFilterKernel()
{
    int x = (int) blockIdx.x*blockDim.x + threadIdx.x;
    int y = (int) blockIdx.y*blockDim.y + threadIdx.y;

    int xbase = (int) blockIdx.x*blockDim.x;
    int ybase = (int) blockIdx.y*blockDim.y;

    __shared__ CudaRTResult sharedMem[32][32];

    /*
     ____________________
    |corn|         |corn|
    | er |   top   | er |
    |-------------------|
    |    |         |    |
    |left| centre  |ri  |
    |    |         | ght|
    |----|---------|----|
    |corn| bottom  |corn|
    |____|_________|____|

    centre is of blockDim size
    corners are of INDIRECT_LIGHT_HALF_FILTER_SIZE^2 size
    left and right is blockDim.y * INDIRECT_LIGHT_HALF_FILTER_SIZE
    top and bottom are blockDim.x * INDIRECT_LIGHT_HALF_FILTER_SIZE
    */

//    int offsetX = -VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;
//    int offsetY = -VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;
//    if(threadIdx.x >= VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE)
//        offsetX = VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;
//    if(threadIdx.y >= VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE)
//        offsetY = VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;

//    offsetX += VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;
//    offsetY += VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;

//    CudaRTResult centre, leftRight, topDown, corners;
//    centre = getDataForPixel(x, y);
//    leftRight = getDataForPixel(x + offsetX, y);
//    topDown = getDataForPixel(x, y + offsetY);
//    corners = getDataForPixel(x + offsetX, y + offsetY);

////    //centre
//    sharedMem[threadIdx.x + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE][threadIdx.y + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE].data = centre.data;

////    //left and right
//    sharedMem[threadIdx.x + offsetX][threadIdx.y].data = leftRight.data;

////    //top and bottom
//    sharedMem[threadIdx.x][threadIdx.y + offsetY].data = topDown.data;

////    // corners
//    sharedMem[threadIdx.x + offsetX][threadIdx.y + offsetY].data = corners.data;

    if(threadIdx.x == 0) {
//        for(int j = 0; j<32; j++) {
//            sharedMem[threadIdx.x][j] = getDataForPixel(xb+threadIdx.x-8, yb+j-8);
//        }
//        for(int j = 0; j<32; j++) {
//            sharedMem[threadIdx.x+16][j] = getDataForPixel(xb+threadIdx.x+16-8, yb+j-8);
//        }
        for(int i = 0; i<32; i++) {
            sharedMem[i][threadIdx.y] = getDataForPixel(xbase+i-8, ybase+threadIdx.y-8);
        }
        for(int i = 0; i<32; i++) {
            sharedMem[i][threadIdx.y+16] = getDataForPixel(xbase+i-8, ybase+threadIdx.y+16-8);
        }
    }

    __syncthreads();

    if(x >= g_uniforms->width) return;
    if(y >= g_uniforms->height) return;


    float3 normal, colourSum;
    float weightSum = 0.f;
    float dist;
    colourSum = make_float3(0.f);

//    getDataForPixel(&normal, &colourSum, &dist, x, y);
//    for(int i = - VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE; i <= VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE; i++) {
//        for(int j = - VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE; j <= VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE; j++) {
//            float3 localNormal, localColour;
//            float localDist;
//            getDataForPixel(&localNormal, &localColour, &localDist, x + i, y + j);
//            float weight = dot(normal, localNormal) * fmaxf((1.f - fabs(localDist - dist)), 0.f);
//            if(weight < 0.f) weight = 0.f;
//            weightSum += weight;
//            colourSum += localColour * weight;
//        }
//    }

    int centreIndexX = threadIdx.x + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;
    int centreIndexY = threadIdx.y + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;
    CudaRTResult tmp = sharedMem[centreIndexX][centreIndexY];
    normal = convertTo_float3(tmp.access.normal);
    dist = tmp.access.dist();

    for (int j = 0; j < 32; j++) {
        for (int i = 0; i < 32; i++) {
            if(abs(centreIndexX - i) > 8 || abs(centreIndexY - j) > 8) continue;
            CudaRTResult tmp = sharedMem[i][j];
            float3 localNormal = convertTo_float3(tmp.access.normal);
            float3 localColour = convertTo_float3(tmp.access.irradiance);
            float localDist = tmp.access.dist();
            float weight = dot(normal, localNormal) * fmaxf((1.f - fabs(localDist - dist)), 0.f);

            if(weight < 0.f) weight = 0.f;

            weightSum += weight;
            colourSum += localColour * weight;
        }
    }

//    for(int j=0; j < VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE*2+1; j++) {
//        for(int i=0; i < VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE*2+1; i++) {
////    {{
////            int i = VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;
////            int j = VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;
//            CudaRTResult tmp = sharedMem[i+threadIdx.x][j+threadIdx.y];
//            float3 localNormal = convertTo_float3(tmp.access.normal);
//            float3 localColour = convertTo_float3(tmp.access.irradiance);
//            float localDist = tmp.access.dist();
//            float weight = dot(normal, localNormal) * fmaxf((1.f - fabs(localDist - dist)), 0.f);

//            if(weight < 0.f) weight = 0.f;

//            weightSum += weight;
//            colourSum += localColour * weight;
//        }
//    }

    colourSum *= 1.f / weightSum;

    gd_indirectLight[y * g_uniforms->width + x] = rgbFloatToUInt(colourSum);
//    gd_indirectLight[y * g_uniforms->width + x] = convertTo_uint(sharedMem[threadIdx.x + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE][threadIdx.y + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE].access.irradiance);

}

#endif // INDIRECTLIGHTFILTERKERNEL_H
