/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMMONDEFS_H
#define COMMONDEFS_H

#include "cuHelM.h"
#include "GpuMemoryData.h"
#include "auxiliary/ApplicationConstantsCuda.h"

__device__ __forceinline__ unsigned int rgbaFloatToUInt(float4 rgba)
{
    rgba.x = __saturatef(rgba.x);   // clamp to [0.0, 1.0]
    rgba.y = __saturatef(rgba.y);
    rgba.z = __saturatef(rgba.z);
    rgba.w = __saturatef(rgba.w);
    return ((unsigned int)(rgba.w * 255.0f) << 24) |
           ((unsigned int)(rgba.z * 255.0f) << 16) |
           ((unsigned int)(rgba.y * 255.0f) <<  8) |
           ((unsigned int)(rgba.x * 255.0f));
}

__device__ __forceinline__ unsigned int rgbFloatToUInt(float3 rgba)
{
    rgba.x = __saturatef(rgba.x);   // clamp to [0.0, 1.0]
    rgba.y = __saturatef(rgba.y);
    rgba.z = __saturatef(rgba.z);
    return ((unsigned int)(rgba.z * 255.0f) << 16) |
           ((unsigned int)(rgba.y * 255.0f) <<  8) |
           ((unsigned int)(rgba.x * 255.0f));
}

__device__ __forceinline__ float4 uintToRgbaFloat(unsigned int val)
{
    int r, g, b, a;
    r = val & 255;
    g = (val >> 8) & 255;
    b = (val >> 16) & 255;
    a = (val >> 24) & 255;

    float4 retval = make_float4(r, g, b, a);
    retval *= 1.f/255.0f;
    return retval;
}

__device__ __forceinline__ uchar4 rgbaFloatToUChar4(float4 rgba)
{
    rgba.x = __saturatef(rgba.x);   // clamp to [0.0, 1.0]
    rgba.y = __saturatef(rgba.y);
    rgba.z = __saturatef(rgba.z);
    rgba.w = __saturatef(rgba.w);
    return make_uchar4(rgba.x * 255.0f, rgba.y * 255.0f, rgba.z * 255.0f, rgba.w * 255.0f);
}

__device__ __forceinline__ uchar4 rgbFloatToUChar4(float3 rgb)
{
    rgb.x = __saturatef(rgb.x);   // clamp to [0.0, 1.0]
    rgb.y = __saturatef(rgb.y);
    rgb.z = __saturatef(rgb.z);
    return make_uchar4(rgb.x * 255.0f, rgb.y * 255.0f, rgb.z * 255.0f, 0);
}
__device__ __forceinline__ uchar3 rgbFloatToUChar3(float3 rgb)
{
    rgb.x = __saturatef(rgb.x);   // clamp to [0.0, 1.0]
    rgb.y = __saturatef(rgb.y);
    rgb.z = __saturatef(rgb.z);
    return make_uchar3(rgb.x * 255.0f, rgb.y * 255.0f, rgb.z * 255.0f);
}

__device__ __forceinline__ uchar3 rgbaFloatToUChar3(float4 rgba)
{
    rgba.x = __saturatef(rgba.x);   // clamp to [0.0, 1.0]
    rgba.y = __saturatef(rgba.y);
    rgba.z = __saturatef(rgba.z);
    return make_uchar3(rgba.x * 255.0f, rgba.y * 255.0f, rgba.z * 255.0f);
}

__device__ void unpackGeomData(const uint4& d0, int x, int y, float4* pos, int* materialIndex, float3* normal, float3* textureColour, float* dist)
{
    float xf = (float) (x) + 0.5f;
    xf /= (float) g_uniforms->width;
    xf = xf * 2.f - 1.f;
    float yf = (float) (y) + 0.5f;
    yf /= (float) g_uniforms->height;
    yf = yf * 2.f - 1.f;

    float3 u = make_float3(g_uniforms->cameraUVW.c0);
    float3 v = make_float3(g_uniforms->cameraUVW.c1);
    float3 w = make_float3(g_uniforms->cameraUVW.c2);


    float3 dir = w + xf*u + yf*v;
    dir = normalize(dir);

    float _dist = __int_as_float(d0.x);
    *dist = _dist;
    *pos = make_float4(make_float3(g_uniforms->cameraPosition) + dir * _dist);
    pos->w = 1.f;



    *materialIndex = d0.w;

//    UnpackToShortUnion blub;
//    blub.a = d0.y;
//    float2 enc;
//    enc.x = __half2float(blub.myShorts.a);
//    enc.y = __half2float(blub.myShorts.b);

//    glm::vec2 fenc = enc*4.f - 2.f;
//    float f = glm::dot(fenc,fenc);
//    float g = sqrtf(1.f - f / 4.f);
//    normal->x = fenc.x * g;
//    normal->y = fenc.y * g;
//    normal->z = 1 - f / 2;
//    *normal = glm::normalize(*normal);

    // unpacking of normal taken from http://aras-p.info/texts/CompactNormalStorage.html (Spherical Coordinates)
    // it is really quite slow, spherical -> 2855, unprecise truncate version below->2705
//    float2 ang = enc * 2.f - make_float2(1.0f);
//    if(!(ang.x >= -1.f && ang.x <= 1.f))
//        ang.x = 1.f;

//    float2 scth;
//    sincosf(ang.x * 3.141592653589793238f, &scth.x, &scth.y);
//    float2 scphi = make_float2(sqrtf(1.0f - ang.y*ang.y), ang.y);
//    normal->x = scth.y*scphi.x;
//    normal->y = scth.x*scphi.x;
//    normal->z = scphi.y;


    UnpackToCharUnion charUnpacker;
    charUnpacker.val = d0.y;
    *normal = convertTo_float3(charUnpacker.chars);

    if(textureColour != 0) {
        UnpackToUCharUnion blib;
        blib.val = d0.z;
        blib.uchars.w = 0;
        *textureColour = make_float3(convertTo_float4(blib.uchars));
//        textureColour->x = ((float) blib.myChars.a) / 255.f;
//        textureColour->y = ((float) blib.myChars.b) / 255.f;
//        textureColour->z = ((float) blib.myChars.c) / 255.f;
//        textureColour->w = 0.f;
    }

#ifdef VIENNA_DEBUG_CUDA_PIXEL
    int xi = x;
    int yi = y;
    if(g_uniforms->mouseDebugX == xi && g_uniforms->mouseDebugY == yi) {
        printf("###ray dir: %2.2f/%2.2f/%2.2f ", dir.x, dir.y, dir.z);
        printf("rend pos: %2.2f/%2.2f/%2.2f ", pos->x, pos->y, pos->z);
//        printf("norm enc: %2.2f/%2.2f  norm: %2.2f/%2.2f/%2.2f ", enc.x, enc.y, normal->x, normal->y, normal->z);

        printf("\n");
    }
#endif
}

//static __forceinline__ __device__ float4 tex2DLayeredLod(texture<uchar4, cudaTextureType2DLayered, cudaReadModeNormalizedFloat> t, float x, float y, int layer, float level)
__device__ __forceinline__ float3 tex2dOurTex(const TextureDescription& texDesc, float u, float v) {
//    return tex2DLayered(gt_textures512, u, v, 0);

    switch (texDesc.textureArrayNo) {
    case 0: // 256
        return make_float3(tex2DLayered(gt_textures256, u, v, texDesc.textureIndex));
    case 1: // 512
        return make_float3(tex2DLayered(gt_textures512, u, v, texDesc.textureIndex));
    case 2: // 1024
        return make_float3(tex2DLayered(gt_textures1024, u, v, texDesc.textureIndex));
    case 3: // 2048
        return make_float3(tex2DLayered(gt_textures2048, u, v, texDesc.textureIndex));
//        return tex2DLayeredLod(gt_textures256, u, v, texDesc.textureIndex, 0.f);
//    case 1: // 512
//        return tex2DLayeredLod(gt_textures512, u, v, texDesc.textureIndex, 0.f);
//    case 2: // 1024
//        return tex2DLayeredLod(gt_textures1024, u, v, texDesc.textureIndex, 0.f);
//    case 3: // 2048
//        return tex2DLayeredLod(gt_textures2048, u, v, texDesc.textureIndex, 0.f);
    default:
        return make_float3(1.f);
    }
}

__device__ __forceinline__ float2 tex2dShadowMap(unsigned int shadowMapId, float u, float v) {
    switch (shadowMapId) {
    case 0:
        return tex2D(g_shadowMapsTexRef0, u, v);
    case 1:
        return tex2D(g_shadowMapsTexRef1, u, v);
    case 2:
        return tex2D(g_shadowMapsTexRef2, u, v);
    case 3:
        return tex2D(g_shadowMapsTexRef3, u, v);
    case 4:
        return tex2D(g_shadowMapsTexRef4, u, v);
    case 5:
        return tex2D(g_shadowMapsTexRef5, u, v);
    case 6:
        return tex2D(g_shadowMapsTexRef6, u, v);
    case 7:
        return tex2D(g_shadowMapsTexRef7, u, v);
    case 8:
        return tex2D(g_shadowMapsTexRef8, u, v);
    case 9:
        return tex2D(g_shadowMapsTexRef9, u, v);
    }
    return make_float2(0.f, 0.000001f);
}

__device__ __forceinline__ float sampleShadow(int shadowMapNo, float2 texelCoords, float compareValue) {
    float2 moments = tex2dShadowMap(shadowMapNo, texelCoords.x, texelCoords.y);
//    float sigmaSq = clamp(moments.y - moments.x * moments.x, 0.1f, 100.0f);
    float sigmaSq = fmaxf(0.0005f, moments.y - moments.x * moments.x);


    float denom = compareValue - moments.x;
    float hardShadow = step(compareValue, moments.x);
    denom = fmaxf(denom, 0.f);
    denom *= denom;
    denom += sigmaSq;
    float prob = linstep(0.2f, 1.f, sigmaSq / denom);
    return clamp(prob, hardShadow, 1.f);
}


__device__ __forceinline__ float3 shade(float4 pos, float3 diffuseColour, float3 normal) {
//    float3 outColour = make_float3(diffuseColour.x * 0.05f, diffuseColour.y * 0.05f, diffuseColour.z * 0.05f);
    float3 outColour = make_float3(0.f);
    for(int i=0; i<g_lightsCount; i++) {
        float4 shadingPosInLightSpace = g_lights[i].vpMat * pos;
        if(shadingPosInLightSpace.z < 0.f) continue;
        float viewSpaceZ = shadingPosInLightSpace.w; // this variable is also equal to distance * dot(lookdir, raydir)

        shadingPosInLightSpace /= shadingPosInLightSpace.w;
        if(shadingPosInLightSpace.x < -1.f || shadingPosInLightSpace.x > 1.f) continue;
        if(shadingPosInLightSpace.y < -1.f || shadingPosInLightSpace.y > 1.f) continue;
        float2 shadowTexCoord = make_float2(shadingPosInLightSpace);
        shadowTexCoord *= 1.f/2.f;
        shadowTexCoord += make_float2(0.5f);

//        float2 shadowMap = tex2dShadowMap(i, shadingPosInLightSpace.x/2.f + 0.5f, shadingPosInLightSpace.y/2.f + 0.5f);
//        float shadowMapDepth = shadowMap.x;
//        float shadowMapVariance = shadowMap.y;

//        if(shadowMapDepth + 0.0000f >= viewSpaceZ) { float shadowValue = 1.f;
        float shadowValue = sampleShadow(i, shadowTexCoord, viewSpaceZ);
        shadowValue += sampleShadow(i, shadowTexCoord + make_float2(VIENNA_SHADOW_MAP_SIZE_ONE_OVER, 0), viewSpaceZ);
        shadowValue += sampleShadow(i, shadowTexCoord + make_float2(-VIENNA_SHADOW_MAP_SIZE_ONE_OVER, 0), viewSpaceZ);
        shadowValue += sampleShadow(i, shadowTexCoord + make_float2(0, VIENNA_SHADOW_MAP_SIZE_ONE_OVER), viewSpaceZ);
        shadowValue += sampleShadow(i, shadowTexCoord + make_float2(0, -VIENNA_SHADOW_MAP_SIZE_ONE_OVER), viewSpaceZ);
        shadowValue *= 1.f/5.f;
        {
            float3 lightVec = g_lights[i].pos - make_float3(pos);
            float lightDist = length(lightVec);
            lightVec /= lightDist;

            if(i==0) // constant sun
                lightDist = 1.f;

#ifdef VIENNA_DEBUG_CUDA_PIXEL       // costs ~50 msec on 800*600
            int xi = (int) blockIdx.x*blockDim.x + threadIdx.x;
            int yi = (int) blockIdx.y*blockDim.y + threadIdx.y;
            if(g_uniforms->mouseDebugX == xi && g_uniforms->mouseDebugY == yi) {
                printf("###normal: %2.2f/%2.2f/%2.2f lightVec: %2.2f/%2.2f/%2.2f\n",
                       normal.x, normal.y, normal.z, lightVec.x, lightVec.y, lightVec.z);
            }
#endif

            float f = 100.f;
            if(i==0) f = 2.f;     // sun
            if(i==1) f = 1.0f;      // macro light
            f *= shadowValue * dot(normal, lightVec) / (lightDist*lightDist);
            if(f<0) f = 0.f;
            outColour += diffuseColour*f;
        }
    }
    return outColour;
}

__device__ __forceinline__ float3 shade_rt(float4 pos, float3 diffuseColour, float3 normal) {
//    float3 outColour = make_float3(diffuseColour.x * 0.05f, diffuseColour.y * 0.05f, diffuseColour.z * 0.05f);
    float3 outColour = make_float3(0.f);
    for(int i=0; i<g_lightsCount; i++) {
        float4 shadingPosInLightSpace = g_lights[i].vpMat * pos;
        if(shadingPosInLightSpace.z < 0.f) continue;
        float viewSpaceZ = shadingPosInLightSpace.w; // this variable is also equal to distance * dot(lookdir, raydir)

        shadingPosInLightSpace /= shadingPosInLightSpace.w;
        if(shadingPosInLightSpace.x < -1.f || shadingPosInLightSpace.x > 1.f) continue;
        if(shadingPosInLightSpace.y < -1.f || shadingPosInLightSpace.y > 1.f) continue;
        float2 shadowTexCoord = make_float2(shadingPosInLightSpace);
        shadowTexCoord *= 1.f/2.f;
        shadowTexCoord += make_float2(0.5f);

//        float2 shadowMap = tex2dShadowMap(i, shadingPosInLightSpace.x/2.f + 0.5f, shadingPosInLightSpace.y/2.f + 0.5f);
//        float shadowMapDepth = shadowMap.x;
//        float shadowMapVariance = shadowMap.y;

//        if(shadowMapDepth + 0.0000f >= viewSpaceZ) { float shadowValue = 1.f;
        float shadowValue = sampleShadow(i, shadowTexCoord, viewSpaceZ);
        {
            float3 lightVec = g_lights[i].pos - make_float3(pos);
            float lightDist = length(lightVec);
            lightVec /= lightDist;

            if(i==0) // constant sun
                lightDist = 1.f;

#ifdef VIENNA_DEBUG_CUDA_PIXEL       // costs ~50 msec on 800*600
            int xi = (int) blockIdx.x*blockDim.x + threadIdx.x;
            int yi = (int) blockIdx.y*blockDim.y + threadIdx.y;
            if(g_uniforms->mouseDebugX == xi && g_uniforms->mouseDebugY == yi) {
                printf("###normal: %2.2f/%2.2f/%2.2f lightVec: %2.2f/%2.2f/%2.2f\n",
                       normal.x, normal.y, normal.z, lightVec.x, lightVec.y, lightVec.z);
            }
#endif

            float f = 100.f;
            if(i==0) f = 2.f;     // sun
            if(i==1) f = 1.0f;      // macro light
            f *= shadowValue * dot(normal, lightVec) / (lightDist*lightDist);
            if(f<0) f = 0.f;
            outColour += diffuseColour*f;
        }
    }
    return outColour;
}


#endif // COMMONDEFS_H
