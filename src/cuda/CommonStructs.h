/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMMONSTRUCTS_H
#define COMMONSTRUCTS_H

#include "cuHelM.h"
#include "objects/Light.h"
#include "objects/Camera.h"
//#include "auxiliary/ApplicationConstantsCuda.h"

struct CudaLight {
    __host__ CudaLight(const Light& light) {
        colour = make_float3(light.color());
        vpMat = make_mat4(light.lightCamera()->viewProjectionMatrix());
        pos = make_float3(light.position());
    }
    float3 colour;
    float3 pos;
    mat4 vpMat;
};

struct CudaUniforms {
    unsigned short width;
    unsigned short height;
    int mouseDebugX;
    int mouseDebugY;
    mat4 cameraInvVPMatrix;
    mat4 cameraUVW;
    float4 cameraPosition;
};

struct CudaRayData{
    float3 rayOrigin;
    float3 rayDirection;
    float3 resultColour;
    short2 screenPos;
    float t;
};

union CudaRTResult {
    uint2 data;
    struct Access {
        char3 normal;
        uchar3 irradiance;
        unsigned short m_dist;
        __device__ __forceinline__ float dist() const {
            return ((float) m_dist) * (100.f / 65536.f);
        }

        __device__ __forceinline__ void setDist(float dist) {
            m_dist = (short) min(((int) (dist * (65536.f / 100.f))), 65536);
        }
    } access;
};

struct CudaRayCastResult {
//    float4 hitPos;
    uint normal;
    uchar4 hitPosIrradiance;
    float dist;
};

struct SceneAccelerationNode {
    mat4 worldToStructMatrix;
    float3 bbMin;
    float3 bbMax;
    unsigned int nodesOffset;       // probably woop and index offsets could be attributed to in the nodes itself. then woop and index offset could be removed.
    unsigned int triWoopOffset;     // but since it would be complicated, we leave that for later..
    unsigned int triIndexOffset;    // this offset is for the triangle index. the triangle then points to the vertices (positions, normals etc..)
    unsigned int vertexIndexOffset; // so now we are only moving the index of positions, normals, materials etc in the triangle soup.
    int layout;
};

#endif // COMMONSTRUCTS_H
