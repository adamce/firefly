/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INDIRECTLIGHTFILTERKERNEL_H
#define INDIRECTLIGHTFILTERKERNEL_H

#include "auxiliary/ApplicationConstantsCuda.h"
#include "GpuMemoryData.h"
#include "CommonDefs.h"

__device__ __forceinline__
void getDataForPixel(float3* normal, float3* colour, float* dist, int x, int y) {
    if(y >= 0 && x >= 0 && y < g_uniforms->height && x < g_uniforms->width) {
//        *normal = convertTo_float3(gd_rayCastResults[y * g_uniforms->width + x].access.normal);
//        *colour = convertTo_float3(gd_rayCastResults[y * g_uniforms->width + x].access.irradiance);
//        *dist = gd_rayCastResults[y * g_uniforms->width + x].access.dist();

        CudaRTResult tmp;
        tmp.data = tex2D(gt_rayCastResults, ((float) x) + 0.5f, ((float) y) + 0.5f);
        *normal = convertTo_float3(tmp.access.normal);
        *colour = convertTo_float3(tmp.access.irradiance);
        *dist = tmp.access.dist();

    }
    else {
        *normal = make_float3(0.f, 0.f, 0.f);
        *colour = make_float3(0.f, 0.f, 0.f);
        *dist = 1000000000;
    }
}

__device__ __forceinline__
CudaRTResult getDataForPixel(int x, int y) {
    if(y >= 0 && x >= 0 && y < g_uniforms->height / 2 && x < g_uniforms->width / 2) {
        CudaRTResult tmp;
        tmp.data = tex2D(gt_rayCastResults, ((float) x) + 0.5f, ((float) y) + 0.5f);
        return tmp;
//        return gd_rayCastResults[y * (gd_rayCastResults_pitch / sizeof(CudaRTResult)) + x];
    }
    else {
        CudaRTResult tmp;
        tmp.access.normal = make_char3(0, 0, 0);
        tmp.access.irradiance = make_uchar3(0, 0, 0);
        tmp.access.m_dist = (unsigned short) -1;
        return tmp;
    }
}


#define VIENNA_SHARED_MEM_WIDTH VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE * 2 + VIENNA_CUDA_BLOCK_WIDTH_INDIR_FILTER
#define VIENNA_SHARED_MEM_HEIGHT VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE * 2 + VIENNA_BLOCK_HEIGHT_INDIR_FILTER

__global__
void indirectLightFilterKernel(int phase)
{
    int x = (int) blockIdx.x*blockDim.x + threadIdx.x;
    int y1 = (int) blockIdx.y*blockDim.y*2 + threadIdx.y*2;
    int y2 = (int) blockIdx.y*blockDim.y*2 + threadIdx.y*2+1;

    __shared__ CudaRTResult sharedMem[VIENNA_SHARED_MEM_WIDTH][VIENNA_SHARED_MEM_HEIGHT];


    /*
     ____________________
    |corn|         |corn|
    | er |   top   | er |
    |-------------------|
    |    |         |    |
    |left| centre  |ri  |
    |    |         | ght|
    |----|---------|----|
    |corn| bottom  |corn|
    |____|_________|____|

    centre is of blockDim size
    corners are of INDIRECT_LIGHT_HALF_FILTER_SIZE^2 size
    left and right is blockDim.y * INDIRECT_LIGHT_HALF_FILTER_SIZE
    top and bottom are blockDim.x * INDIRECT_LIGHT_HALF_FILTER_SIZE
    */

//    int offsetX = -VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;
//    int offsetY = -VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;
//    if(threadIdx.x >= VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE)
//        offsetX = VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;
//    if(threadIdx.y >= VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE)
//        offsetY = VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;

//    offsetX += VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;
//    offsetY += VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE;

//    CudaRTResult centre, leftRight, topDown, corners;
//    centre = getDataForPixel(x, y);
//    leftRight = getDataForPixel(x + offsetX, y);
//    topDown = getDataForPixel(x, y + offsetY);
//    corners = getDataForPixel(x + offsetX, y + offsetY);

////    //centre
//    sharedMem[threadIdx.x + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE][threadIdx.y + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE].data = centre.data;

////    //left and right
//    sharedMem[threadIdx.x + offsetX][threadIdx.y].data = leftRight.data;

////    //top and bottom
//    sharedMem[threadIdx.x][threadIdx.y + offsetY].data = topDown.data;

////    // corners
//    sharedMem[threadIdx.x + offsetX][threadIdx.y + offsetY].data = corners.data;

#if VIENNA_SHARED_MEM_WIDTH == 32
    int memPos = threadIdx.y * blockDim.x + threadIdx.x;
    if(memPos < 32) {
        int xb = (int) blockIdx.x*blockDim.x;
        int yb = (int) blockIdx.y*blockDim.y*2;
        for(int j = 0; j<VIENNA_SHARED_MEM_HEIGHT; j++) {
            sharedMem[memPos][j] = getDataForPixel(xb+memPos-VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE, yb+j-VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE);
        }
//        for(int j = 0; j<VIENNA_SHARED_MEM_HEIGHT; j++) {
//            sharedMem[threadIdx.x+16][j] = getDataForPixel(xb+threadIdx.x+16-VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE, yb+j-VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE);
//        }
//        for(int i = 0; i<32; i++) {
//            sharedMem[i][threadIdx.y] = getDataForPixel(xb+i-8, yb+threadIdx.y-8);
//        }
//        for(int i = 0; i<32; i++) {
//            sharedMem[i][threadIdx.y+16] = getDataForPixel(xb+i-8, yb+threadIdx.y+16-8);
//        }
    }
#elif VIENNA_SHARED_MEM_WIDTH == 16
    if(threadIdx.y < 2) {
        int xb = (int) blockIdx.x*blockDim.x;
        int yb = (int) blockIdx.y*blockDim.y*2;
        for(int j = 0; j<VIENNA_SHARED_MEM_HEIGHT; j++) {
            int xIndex = threadIdx.x + threadIdx.y * 8;
            sharedMem[xIndex][j] = getDataForPixel(xb+xIndex-VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE, yb+j-VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE);
        }
    }
#endif

    __syncthreads();

    if(x >= g_uniforms->width) return;
    if(y1 >= g_uniforms->height) return;


    float3 normal1, colourSum1;
    float weightSum1 = 0.f;
    float dist1;
    colourSum1 = make_float3(0.f);
    float3 normal2, colourSum2;
    float weightSum2 = 0.f;
    float dist2;
    colourSum2 = make_float3(0.f);

//    getDataForPixel(&normal, &colourSum, &dist, x, y);
//    for(int i = - VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE; i <= VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE; i++) {
//        for(int j = - VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE; j <= VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE; j++) {
//            float3 localNormal, localColour;
//            float localDist;
//            getDataForPixel(&localNormal, &localColour, &localDist, x + i, y + j);
//            float weight = dot(normal, localNormal) * fmaxf((1.f - fabs(localDist - dist)), 0.f);
//            if(weight < 0.f) weight = 0.f;
//            weightSum += weight;
//            colourSum += localColour * weight;
//        }
//    }

    CudaRTResult tmp = sharedMem[threadIdx.x + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE][threadIdx.y*2 + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE];
    gd_rayCastResults2[y1 * (gd_rayCastResults2_pitch / sizeof(CudaRTResult)) + x].data = tmp.data;
    normal1 = convertTo_float3(tmp.access.normal);
    dist1 = tmp.access.dist();
    tmp = sharedMem[threadIdx.x + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE][threadIdx.y*2 + 1 + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE];
    normal2 = convertTo_float3(tmp.access.normal);
    dist2 = tmp.access.dist();
    gd_rayCastResults2[y2 * (gd_rayCastResults2_pitch / sizeof(CudaRTResult)) + x].data = tmp.data;

    for(int i=0; i < VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE*2+1; i++) {
        for(int j=0; j < VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE*2+2; j++) {
            CudaRTResult tmp = sharedMem[i+threadIdx.x][j+threadIdx.y*2];
            float3 localNormal = convertTo_float3(tmp.access.normal);
            float3 localColour = convertTo_float3(tmp.access.irradiance);
            float localDist = tmp.access.dist();
            float weight1 = dot(normal1, localNormal) * fmaxf((1.f - fabs(localDist - dist1)), 0.f);
            float weight2 = dot(normal2, localNormal) * fmaxf((1.f - fabs(localDist - dist2)), 0.f);

            if(weight1 < 0.f) weight1 = 0.f;
            if(weight2 < 0.f) weight2 = 0.f;

            if(j < VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE * 2 + 1) {
                weightSum1 += weight1;
                colourSum1 += localColour * weight1;
            }
            if(j >= 1) {
                weightSum2 += weight2;
                colourSum2 += localColour * weight2;
            }
        }
    }

    colourSum1 *= 1.f / weightSum1;
    colourSum2 *= 1.f / weightSum2;

#ifdef VIENNA_DEBUG_TRACE_FIRST_RAY
    colourSum1 = convertTo_float3(sharedMem[threadIdx.x + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE][threadIdx.y*2 + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE].access.irradiance);
    colourSum2 = convertTo_float3(sharedMem[threadIdx.x + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE][threadIdx.y*2 + 1 + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE].access.irradiance);
#endif

    if(phase == 0) {
        gd_rayCastResults2[y1 * (gd_rayCastResults2_pitch / sizeof(CudaRTResult)) + x].access.irradiance = rgbFloatToUChar3(colourSum1);
        gd_rayCastResults2[y2 * (gd_rayCastResults2_pitch / sizeof(CudaRTResult)) + x].access.irradiance = rgbFloatToUChar3(colourSum2);
    }
    else {
        gd_rayCastResults1[y1 * (gd_rayCastResults2_pitch / sizeof(CudaRTResult)) + x].access.irradiance = rgbFloatToUChar3(colourSum1);
        gd_rayCastResults1[y2 * (gd_rayCastResults2_pitch / sizeof(CudaRTResult)) + x].access.irradiance = rgbFloatToUChar3(colourSum2);
    }

//    gd_indirectLight[y1 * g_uniforms->width + x] = convertTo_uint(sharedMem[threadIdx.x + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE][threadIdx.y*2 + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE].access.irradiance);
//    gd_indirectLight[y2 * g_uniforms->width + x] = convertTo_uint(sharedMem[threadIdx.x + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE][threadIdx.y*2+1 + VIENNA_INDIRECT_LIGHT_HALF_FILTER_SIZE].access.irradiance);

}

#endif // INDIRECTLIGHTFILTERKERNEL_H
