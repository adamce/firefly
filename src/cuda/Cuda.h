/**
 * Vienna Renderer - Firefly demo for UT-Vienna real-time graphics course
 * (http://cg.tuwien.ac.at/courses/Realtime/VU.html)
 * Copyright (C) 2014 Adam Celarek (www.celarek.at)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CUDA_H
#define CUDA_H

#include <driver_types.h>
#include <unordered_set>
#include <vector>
#include "HelperMath.h"
#include "BvhHelperStructs.h"
#include <curand.h>

class Framebuffer;
class ShadowMapPass;
struct MaterialDescription;
struct CudaLight;
struct CudaUniforms;
struct SceneAccelerationNode;
struct cudaArray;
struct cudaMipmappedArray;
struct curandStateSobol32;
struct curandStateXORWOW;
union CudaRTResult;

class Cuda
{
    Cuda();
    ~Cuda();
    friend class MainRenderPass;
public:
    void initFramebuffers(Framebuffer* geometryFramebuffer, Framebuffer* resultFramebuffer);
    void deInitFramebuffers();
    void resize(int width, int height);
    void initConstData();
    void deInitConstData();
    void deferredRender();
    void updateShadowMaps(const std::vector<ShadowMapPass *> *shadowMapPasses);
    void setMouseDebugPos(int x, int y) { m_mouseDebugPosX = x; m_mouseDebugPosY = y; }

    static BVHLayout bvhLayout();

    static void handleCudaError(cudaError_t error);

protected:
    void updateLights();
    void updateSceneBvh();
    void initUniforms();
    void initTextures();
    void initBvh();
    void initRandomNumbers(int w, int h);

private:
    Framebuffer* m_geometryFB = nullptr;
    Framebuffer* m_resultFB = nullptr;

    MaterialDescription* md_materialDescriptions = nullptr;  // device pointer and cuda not necessarily knows c++11
    CudaLight* md_cudaLights = nullptr;  // device pointer and cuda not necessarily knows c++11
    CudaUniforms* md_uniforms = nullptr;
    int m_mappedShadowMaps = 0;

//    cudaMipmappedArray* ma_texture256mm;
//    cudaMipmappedArray* ma_texture512mm;
//    cudaMipmappedArray* ma_texture1024mm;
//    cudaMipmappedArray* ma_texture2048mm;

    cudaArray* ma_texture256;
    cudaArray* ma_texture512;
    cudaArray* ma_texture1024;
    cudaArray* ma_texture2048;

    //ray tracing data start
    unsigned char* md_nodesA = nullptr;
    unsigned char* md_trisA = nullptr;        // woop
    unsigned char* md_triIndices = nullptr;    // indices of triangles

    SceneAccelerationNode* md_serialisedSceneAccelerationNodes = nullptr;
    int3* md_vertexIndices = nullptr;
    float3* md_positions = nullptr;
    float3* md_normals = nullptr;
    float2* md_texCoords = nullptr;
    int* md_triangleMaterials = nullptr;

    CudaRTResult* md_rayCastResults1 = nullptr;
    CudaRTResult* md_rayCastResults2 = nullptr;
    size_t m_rayCastResults1_pitch;
    size_t m_rayCastResults2_pitch;
    uchar4* md_indirectLight = nullptr;
    //ray tracing data end

    const std::vector<ShadowMapPass *>* m_shadowMapPasses = nullptr;
    curandDirectionVectors32_t* md_randomNumberDirections = nullptr;
    curandStateSobol32* md_sobolRandomNumbersStates = nullptr;
    curandStateXORWOW* md_randomNumberStates = nullptr;

    int m_mouseDebugPosX;
    int m_mouseDebugPosY;

};

#endif // CUDA_H
