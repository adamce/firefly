/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Adam Celarek

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include "Framebuffer.h"
#include "objects/Camera.h"
#include "shaders/Shader.h"

Framebuffer::Framebuffer(FramebufferTexture* colorBuffer, FramebufferTexture* depthBuffer)
{
    m_texture[0] = depthBuffer;
    m_texture[1] = colorBuffer;
    m_texture[2] = 0;

    glGenFramebuffers(1, &m_framebufferId);

    bool useMultisample = false && colorBuffer == depthBuffer && depthBuffer == 0;

    if(m_texture[0] == 0)
        m_texture[0] = new FramebufferTexture(GL_TEXTURE_2D, GL_DEPTH_ATTACHMENT, FramebufferTexture::DepthBuffer, useMultisample);

    if(m_texture[1] == 0)
        m_texture[1] = new FramebufferTexture(GL_TEXTURE_2D, GL_COLOR_ATTACHMENT0, FramebufferTexture::RGBA8, useMultisample);

    m_texture[0]->addParent(this);
    m_texture[1]->addParent(this);

    resize(20, 20);

    glBindFramebuffer(GL_FRAMEBUFFER, m_framebufferId);
    m_texture[0]->useInFramebuffer();
    m_texture[1]->useInFramebuffer();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

Framebuffer::Framebuffer(FramebufferTexture* colorBuffer0, FramebufferTexture* colorBuffer1, FramebufferTexture* depthBuffer)
{
    m_texture[0] = depthBuffer;
    m_texture[1] = colorBuffer0;
    m_texture[2] = colorBuffer1;

	for(int i=0; i<3; i++) {
        if(m_texture[i] != 0)
            m_texture[i]->addParent(this);
    }

    glGenFramebuffers(1, &m_framebufferId);
    resize(20, 20);
    glBindFramebuffer(GL_FRAMEBUFFER, m_framebufferId);

    for(int i=0; i<3; i++) {
        if(m_texture[i] != 0)
            m_texture[i]->useInFramebuffer();
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

Framebuffer::~Framebuffer()
{
    for(int i=0; i<3; i++) {
        if(m_texture[i] == 0) continue;

        m_texture[i]->removeParent(this);
        if(!m_texture[i]->parentCount())
            delete m_texture[i];
    }

    glDeleteFramebuffers(1, &m_framebufferId);
}

void Framebuffer::bindForDrawing(Camera* camera, Shader* shader)
{

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_framebufferId);
    GLenum bufs[2];
    bufs[0] = GL_COLOR_ATTACHMENT0;
    bufs[1] = GL_COLOR_ATTACHMENT1;
    if(m_texture[2] != 0) {
        glDrawBuffers(2, bufs);
    }
    else {
        glDrawBuffers(1, bufs);
    }


    if(camera != 0) {
        camera->setViewport(m_width, m_height);
        camera->activate();
    }

    if(shader != 0)
        shader->setFramebufferSize(m_width, m_height);
}

void Framebuffer::bindForReading()
{
    glBindFramebuffer(GL_READ_FRAMEBUFFER, m_framebufferId);
}

void Framebuffer::copyTo(Framebuffer* other, int channel)
{
    bindForReading();
    other->bindForDrawing();

    GLbitfield mask = 0;
    if((channel & DepthAttachment) == DepthAttachment)
        mask = GL_DEPTH_BUFFER_BIT;
    if((channel & Attach0) == Attach0)
        mask = mask | GL_COLOR_BUFFER_BIT;

    if(mask != 0) {
         glReadBuffer(GL_COLOR_ATTACHMENT0);
         glDrawBuffer(GL_COLOR_ATTACHMENT0);

         glBlitFramebuffer(0, 0, m_width, m_height,
                           0, 0, other->width(), other->height(), mask, GL_NEAREST);
    }

    if((channel & Attach1) == Attach1) {
        mask = GL_COLOR_BUFFER_BIT;
        glReadBuffer(GL_COLOR_ATTACHMENT1);
        glDrawBuffer(GL_COLOR_ATTACHMENT1);

        glBlitFramebuffer(0, 0, m_width, m_height,
                          0, 0, other->width(), other->height(), mask, GL_NEAREST);
    }
    Framebuffer::unbindFromDrawing();
    Framebuffer::unbindFromReading();
}

void Framebuffer::clear(int channel)
{
    bindForDrawing();
    GLbitfield mask = 0;
    if((channel & DepthAttachment) == DepthAttachment)
        mask = GL_DEPTH_BUFFER_BIT;
    if((channel & Attach0) == Attach0)
        mask = mask | GL_COLOR_BUFFER_BIT;

    if(mask != 0) {
         glDrawBuffer(GL_COLOR_ATTACHMENT0);
         glClear(mask);
    }

    if((channel & Attach1) == Attach1) {
        mask = GL_COLOR_BUFFER_BIT;
        glDrawBuffer(GL_COLOR_ATTACHMENT1);
        glClear(mask);
    }
    unbindFromDrawing();
}

void Framebuffer::unbindFromDrawing()
{
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

void Framebuffer::unbindFromReading()
{
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}

void Framebuffer::useTexture(FramebufferTexture *tex)
{
    glBindFramebuffer(GL_FRAMEBUFFER, m_framebufferId);
    tex->useInFramebuffer();
}


void Framebuffer::resize(int width, int height)
{
    m_width = width;
    m_height = height;
    for(int i=0; i<3; i++) {
        if(m_texture[i] == 0) continue;
        if(!m_texture[i]->isFirstParent(this)) continue;
        glBindFramebuffer(GL_FRAMEBUFFER, m_framebufferId);
        m_texture[i]->resize(width, height);
    }

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

void FramebufferTexture::resize(int width, int height)
{
    bool changed = false;
     if(width != m_width) {
        changed = true;
        m_width = width;
     }
     if(height != m_height) {
        changed = true;
        m_height = height;
     }
    if(changed)
        internalResize(m_width, m_height);
}

void FramebufferTexture::removeParent(Framebuffer *parent)
{
    m_parents.erase(std::find(m_parents.begin(), m_parents.end(), parent));
}


FramebufferTexture::FramebufferTexture(GLenum target, GLenum attachmentPoint, Format format, bool useMultisample): m_width(1), m_height(1)
{
    m_format = format;
    m_target = target;
    m_attachmentPoint = attachmentPoint;
    if(m_attachmentPoint == GL_DEPTH_ATTACHMENT) {
        m_internalFormat = GL_DEPTH_COMPONENT32F;
    }
    else {
        m_internalFormat = format;
    }
    m_useMultisample = useMultisample;

    glGenTextures(1, &m_texturebufferId);
//    resize(width, height);
}

FramebufferTexture::~FramebufferTexture()
{
    glDeleteTextures(1, &m_texturebufferId);
}

void FramebufferTexture::internalResize(int width, int height)
{
    // old version, which doesn't need to destroy anything, but has mutable textures

//    if(m_useMultisample) {
//        glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, m_texturebufferId);
//        glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 8, m_internalFormat, width, height, false);
//    }
//    else {
//        glBindTexture(m_target, m_texturebufferId);
//        GLenum format = GL_RGBA;
//        GLenum type = GL_FLOAT;
//        switch (m_format) {
//        case DepthBuffer:
//            format = GL_DEPTH_COMPONENT;
//            type = GL_FLOAT;
//            break;
//        case RGBA8:
//            break;
//        case RGBA32I:
//            format = GL_RGBA_INTEGER;
//            type = GL_INT;
//            break;
//        case RGBA32UI:
//            format = GL_RGBA_INTEGER;
//            type = GL_UNSIGNED_INT;
//            break;
//        }
//        glTexImage2D(m_target, 0, m_internalFormat, width, height, 0, format, type, 0);
//    }

//    glBindTexture(m_target, 0);// just cleanup, shouldn't be needed normaly


    // new version, imutable textures (in size and format) and less parameters for the functions
    glDeleteTextures(1, &m_texturebufferId);
    glGenTextures(1, &m_texturebufferId);

    if(m_useMultisample) {
        glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, m_texturebufferId);
        glTexStorage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 8, m_internalFormat, width, height, false);
    }
    else {
        glBindTexture(m_target, m_texturebufferId);
        glTexStorage2D(m_target, 1, m_internalFormat, width, height);
    }

    useInFramebuffer();
    glBindTexture(m_target, 0);// just cleanup, shouldn't be needed normaly

    for(auto i = m_parents.begin(); i<m_parents.end(); i++) {
        Framebuffer* c = *i;
        c->useTexture(this);
    }
}

void FramebufferTexture::useInFramebuffer()
{
    glBindTexture(m_target, m_texturebufferId);
    glFramebufferTexture(GL_FRAMEBUFFER, m_attachmentPoint, m_texturebufferId, 0);

//    glBindTexture(m_target, m_texturebufferId);
//    glFramebufferTexture2D(GL_FRAMEBUFFER, m_attachmentPoint, m_target, m_texturebufferId, 0);
}


