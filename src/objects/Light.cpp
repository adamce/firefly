#include "Light.h"

#include "Camera.h"
#include "auxiliary/ApplicationConstants.h"

Light::Light(Object *parent) : Object(EmptyMesh), m_parent(parent)
{
    setColor(1.0f, 1.0f, 1.0f);
    m_lightCamera = new Camera(70.f, 0.001f, 50.f, this);
    m_lightCamera->setViewport(VIENNA_SHADOW_MAP_SIZE, VIENNA_SHADOW_MAP_SIZE);
}

Light::~Light()
{
    delete m_lightCamera;
}

void Light::setModelMatrix(const glm::mat4 &matrix)
{
    Object::setModelMatrix(matrix);
    m_lightCamera->setModelMatrix(m_lightCamera->modelMatrix()); // make the view frustum dirty
}

void Light::setColor(GLfloat red, GLfloat green, GLfloat blue)
{
    m_color = glm::vec3(red, green, blue);
}


glm::vec3 Light::color() const
{
    return m_color;
}

glm::mat4 Light::modelMatrix() const
{
    return cumulatedModelMatrix();
}

glm::mat4 Light::cumulatedModelMatrix() const
{
    if(m_parent != nullptr)
        return m_parent->modelMatrix() * Object::modelMatrix();
    else
        return Object::modelMatrix();
}
