#ifndef SZTUCZKA_H
#define SZTUCZKA_H

#include <random>
#include "PhysicalObject.h"

class Sztuczka : public PhysicalObject
{
public:
    Sztuczka(int seed, const glm::vec3& startPos , const std::string& materialFile);
    float genRand();
    void update(float time);

private:
    float m_time = 0.f;
    float m_countDown = 17.f;
    bool m_animated = true;
	std::default_random_engine m_randomNumberGenerator;

	// i read somewhere that the float random number generator is broken on some architectures.
	std::uniform_real_distribution<double> m_randomNumberDistribution;
};

#endif // SZTUCZKA_H
