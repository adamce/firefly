#include "Object.h"

#include <string>

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>       // Output data structure

#define GLM_FORCE_RADIANS
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include "auxiliary/ObjectAnimator.h"
#include "auxiliary/ApplicationConstants.h"

#include "meshes/MeshManager.h"
#include "PhysicalObject.h"


#define BUFFER_OFFSET(i) ((char *)NULL + (i))

Object::Object(MeshType type)
{
    loadMesh(type);
}

Object::Object(const std::string &path)
{
    loadMesh(path);
}

Object::~Object()
{
    delete m_objectAnimator;
}

void Object::loadMesh(MeshType type)
{
    if(type==EmptyMesh)
        m_mesh = 0;
    else
        m_mesh = MeshManager::instance()->loadMesh(type);
}

void Object::loadMesh(const std::string& path)
{
    m_mesh = MeshManager::instance()->loadMesh(path);
}

void Object::activateMeshForDrawing()
{
    if(!m_mesh) {
        std::cerr << "Trying to activate an object without a mesh. this is weird, most probably a bug and I'm going to die.." << std::endl;
    }
    m_mesh->activate();
}

void Object::draw(const glm::mat4& viewProjectionMatrix)
{
    if(!m_mesh) {
        std::cerr << "Trying to draw an object without a mesh. this is weird, most probably a bug and I'm going to die.." << std::endl;
    }
    m_mesh->draw(m_modelMatrix, viewProjectionMatrix);
}

void Object::update(float time)
{
    if(!m_animated) return;
    m_frame++;
    if(m_frame < 10) {
        time = 0;
    }

//    if(m_time > 10.f)
//        m_projectionMatrix = glm::perspective(glm::radians(m_fovy), 1.0f, m_zNear, m_zFar);
//    else
//        m_projectionMatrix = glm::perspective(glm::radians(m_fovy), 1.0f, m_zNear*0.1f, m_zFar*0.1f);

    m_time += time;
//    setPosition(glm::vec3(m_objectAnimator->transformationForTime(m_time)[3]));
    setModelMatrix(m_objectAnimator->transformationForTime(m_time));
}

void Object::setStatic()
{
    m_isStatic = true;
}

glm::mat4 Object::modelMatrix() const
{
    return m_modelMatrix;
}

void Object::setModelMatrix(const glm::mat4 &matrix)
{
    PhysicalObject* pho = dynamic_cast<PhysicalObject*>(this);
    if(m_isStatic && pho == nullptr) {
//        if(pho == nullptr)
        std::cerr << "Object::setModelMatrix(): trying to update model matrix of a static object. I won't let this happen!" << std::endl;
        return;
    }
    m_modelMatrix = matrix;
}

void Object::setPosition(const glm::vec3& position)
{
    glm::mat4 mm = modelMatrix();
    mm[3][0] = position[0];
    mm[3][1] = position[1];
    mm[3][2] = position[2];
    this->setModelMatrix(mm);
}

void Object::setPosition(float x, float y, float z)
{
    setPosition(glm::vec3(x, y, z));
}

void Object::translate(const glm::vec3& direction)
{
    glm::mat4 translation = glm::translate(glm::mat4(), direction);
    setModelMatrix(m_modelMatrix * translation);
}

void Object::scale(float x, float y, float z)
{
    if(dynamic_cast<PhysicalObject*>(this))
        std::cout << "Object::scale(): Warning, scaling an PhysicalObject, this is not supported by bullet, so weird things might happen. " << std::endl;
    glm::mat4 scaling = glm::scale(glm::mat4(), glm::vec3(x, y, z));
    setModelMatrix(m_modelMatrix * scaling);
}

void Object::rotateX(float angle)
{
    glm::mat4 rotation = glm::rotate<float>(glm::mat4(), glm::radians(angle), glm::vec3(1,0,0));
    setModelMatrix(m_modelMatrix * rotation);
}

void Object::rotateY(float angle)
{
    glm::mat4 rotation = glm::rotate<float>(glm::mat4(), glm::radians(angle), glm::vec3(0,1,0));
    setModelMatrix(m_modelMatrix * rotation);
}

void Object::rotateZ(float angle)
{
    glm::mat4 rotation = glm::rotate<float>(glm::mat4(), glm::radians(angle), glm::vec3(0,0,1));
    setModelMatrix(m_modelMatrix * rotation);
}

glm::vec3 Object::position() const
{
    return glm::vec3(m_modelMatrix * glm::vec4(0, 0, 0, 1));
}

Mesh* Object::mesh() const
{
    return m_mesh;
}

void Object::switchTextureFiltering(int value) const
{
	if(m_mesh != 0)
		m_mesh->switchTextureFiltering(value);
}

glm::vec3 toGlmVec(const aiVector3D& aiVec) {
    return glm::vec3(aiVec.x, aiVec.y, aiVec.z);
}
glm::quat toGlmQuat(const aiQuaternion& aiQuat) {
    // conversion in here? very ugly.. but i have little time and i won't use the code anymore.. sorry if you have to read it.
//    return glm::quat(-aiQuat.w, aiQuat.z*0, aiQuat.y*0, -aiQuat.x);
    return glm::quat(aiQuat.w, aiQuat.x, aiQuat.y, aiQuat.z);
}
glm::mat4 toGlmMat4(const aiMatrix4x4& aiMat) {
    glm::mat4 retMat;
    float* retData = & (retMat[0][0]);
    const float* data = &(aiMat[0][0]);
    for(int i=0; i<16; i++) {
        *(retData + i) = *(data + i);
    }
    retMat = glm::transpose(retMat);
    return retMat;
}
glm::mat3 quatToMat3(glm::quat q) {
        float sqw = q.w*q.w;
        float sqx = q.x*q.x;
        float sqy = q.y*q.y;
        float sqz = q.z*q.z;

        glm::mat3 m;

        // invs (inverse square length) is only required if quaternion is not already normalised
        float invs = 1 / (sqx + sqy + sqz + sqw);
        m[0][0] = ( sqx - sqy - sqz + sqw)*invs ; // since sqw + sqx + sqy + sqz =1/invs*invs
        m[1][1] = (-sqx + sqy - sqz + sqw)*invs ;
        m[2][2] = (-sqx - sqy + sqz + sqw)*invs ;

        double tmp1 = q.x*q.y;
        double tmp2 = q.z*q.w;
        m[1][0] = 2.0 * (tmp1 + tmp2)*invs ;
        m[0][1] = 2.0 * (tmp1 - tmp2)*invs ;

        tmp1 = q.x*q.z;
        tmp2 = q.y*q.w;
        m[2][0] = 2.0 * (tmp1 - tmp2)*invs ;
        m[0][2] = 2.0 * (tmp1 + tmp2)*invs ;
        tmp1 = q.y*q.z;
        tmp2 = q.x*q.w;
        m[2][1] = 2.0 * (tmp1 + tmp2)*invs ;
        m[1][2] = 2.0 * (tmp1 - tmp2)*invs ;
        return glm::transpose(m);
}

void Object::readAnimFromFile(std::string path)
{
    path = VIENNA_MODEL_BASE_PATH + path;
    if(path.length() > 0) {
        m_objectAnimator = new ObjectAnimator();
        std::cout << "Object::readAnimFromFile: loading " << path << std::endl;
        Assimp::Importer importer;
        importer.SetPropertyInteger(AI_CONFIG_PP_SBP_REMOVE, aiPrimitiveType_POINT|aiPrimitiveType_LINE);

        const aiScene* scene = importer.ReadFile(path, 0);

        if(!(scene && scene->HasAnimations())) {
            std::cerr << "Object::readAnimFromFile: failed importing file: " << importer.GetErrorString() << std::endl;
            return;
        }

        glm::mat4 conversionTransform;
        conversionTransform[1][1] = 0.f;
        conversionTransform[2][1] = 1.f;
        conversionTransform[2][2] = 0.f;
        conversionTransform[1][2] = -1.f;

        aiAnimation* animation = scene->mAnimations[0];
//        animation.mChannels[0]->mPositionKeys[0];
        if(animation->mNumChannels != 1) {
            std::cout << "Object::readAnimFromFile: unexpected input, numchannels != 1." << std::endl;
        }
        aiNodeAnim* nodeAnim = animation->mChannels[0];
        for(int i=0; i<nodeAnim->mNumPositionKeys; i++) {
            aiVectorKey posKey = nodeAnim->mPositionKeys[i];
            m_objectAnimator->addKeyFrame(posKey.mTime, glm::vec3(conversionTransform * glm::vec4(toGlmVec(posKey.mValue) * 0.01f, 1.f)));
//            m_objectAnimator->addKeyFrame(posKey.mTime * 5.0, toGlmVec(posKey.mValue) * 0.01f);
        }
        for(int i=0; i<nodeAnim->mNumRotationKeys; i++) {
            aiQuatKey rotKey = nodeAnim->mRotationKeys[i];
            glm::quat glmQuat = toGlmQuat(rotKey.mValue);
//            glm::mat4 rotMat = glm::toMat4(glmQuat);
            glm::mat3 rotMat = quatToMat3(glmQuat);
            rotMat = glm::mat3(conversionTransform) * rotMat;
            glmQuat = glm::toQuat(rotMat);
            m_objectAnimator->addKeyFrame(rotKey.mTime, glmQuat);
        }

        m_objectAnimator->build();
        m_animated = true;
        setModelMatrix(m_objectAnimator->transformationForTime(0));
    }
}
