#ifndef SKYBOX_H
#define SKYBOX_H

#include "Object.h"

class Ship;
class Camera;

class Skybox : public Object
{
public:
    Skybox();
    void update(float time);
    void setCamera(Camera* camera);
    void updatePosition();

protected:
    Camera* m_camera;
};

#endif // SKYBOX_H
