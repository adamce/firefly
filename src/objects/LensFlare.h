#ifndef LENSFLARE_H
#define LENSFLARE_H

#include "Object.h"
#include "Camera.h"
#include "Light.h"
#include <set>
#include "glm/glm.hpp"

class Flare;
class Camera;

class LensFlare : public Object
{
public:
    LensFlare(glm::vec4 color);
	~LensFlare();
	void update(float time);
	void setColor(glm::vec4 color);
	void setCamera(Camera* cam);
	void setSun(Object* sun);
	glm::vec4 getColor() const;
	Object* getSun() const;
    void calculateLensflare();
    float glareFactor() const;
private:
    float m_glareFactor;
	glm::vec4 m_color;
	glm::vec3 m_lastaxis;
    Camera* m_camera = nullptr;
    Object* m_sun = nullptr;
};

#endif // LENSFLARE_H
