#include "Sztuczka.h"

#include <random>

Sztuczka::Sztuczka(int seed, const glm::vec3& startPos, const std::string &materialFile)
    : PhysicalObject("sztuczka/sztuczka.obj?" + materialFile, seed>0?1.f:0.f, startPos, true), m_randomNumberDistribution(0.0, 1.0)
{
    if(seed < 0) {
        m_animated = false;
        m_isStatic = true;
    }
    else
        m_randomNumberGenerator.seed(seed);
}

float Sztuczka::genRand()
{
	return (float) m_randomNumberDistribution(m_randomNumberGenerator);
}

void Sztuczka::update(float time)
{
    float randVal = genRand()*0.7f;
	m_time += time;
	m_countDown -= time;
    if (m_time > randVal * 10.f && m_countDown < 0.f && m_animated) {
        m_time = 0.f;

        btBody()->activate();
        btVector3 correction = btVector3(genRand()-0.5f, genRand(), genRand()-0.5f) * 5.f;
        //std::cout << "apply rand impulse: " << correction.x() << "/" << correction.y() << "/" << correction.z() << std::endl;
        btBody()->applyCentralImpulse(correction * mass());

        btVector3 angularVelocity = btVector3(genRand()-0.5f, genRand()-0.5f, genRand()-0.5f) * 2.f;
        btBody()->applyTorqueImpulse(angularVelocity * mass());
    }

    PhysicalObject::update(time);
}
