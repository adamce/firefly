/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PHYSICALOBJECT_H
#define PHYSICALOBJECT_H

#include <LinearMath/btDefaultMotionState.h>
#include <BulletDynamics/Dynamics/btRigidBody.h>

#include "Object.h"


class PhysicalObject : public Object/*, public btDefaultMotionState*/
{
public:
    PhysicalObject(MeshType type, float mass=0.f, bool constructBtShapeAsSimpleBox = false);
    PhysicalObject(const std::string& path, float mass=0.f, const glm::vec3& startPos = glm::vec3(0.f), bool constructBtShapeAsSimpleBox = false);
    PhysicalObject(MeshType type, btCollisionShape* shape, float mass=0.f);
    PhysicalObject(MeshType type, const btRigidBody::btRigidBodyConstructionInfo& cinfo);
    virtual ~PhysicalObject();
    btRigidBody* btBody() const;

    void update(float time);

    virtual void setModelMatrix(const glm::mat4& matrix) override;

private:
    void init(btCollisionShape* shape, float mass=0.f, const glm::vec3& initialPosition = glm::vec3(0.f));

protected:
    btScalar mass() const;

private:
    btMotionState* m_motionState;
    btRigidBody* m_btBody;
};

#endif // PHYSICALOBJECT_H
