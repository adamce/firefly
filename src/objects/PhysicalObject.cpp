/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PhysicalObject.h"

#include <glm/gtc/type_ptr.hpp>
#include <btBulletDynamicsCommon.h>

#include "Application.h"

PhysicalObject::PhysicalObject(MeshType type, float mass, bool constructBtShapeAsSimpleBox) : Object(type), m_btBody(0)
{

    if(constructBtShapeAsSimpleBox) {
        init(new btBoxShape(btVector3(0.5f,0.5f,0.5f)), mass);
    }
    else {
        init(mesh()->collisionShape(), mass);
    }
}


PhysicalObject::PhysicalObject(const std::string& path, float mass, const glm::vec3 &startPos, bool constructBtShapeAsSimpleBox) : Object(path), m_btBody(0)
{
    glm::mat4 mm = modelMatrix();
    mm[3][0] = startPos[0];
    mm[3][1] = startPos[1];
    mm[3][2] = startPos[2];
    Object::setModelMatrix(mm);

    if(constructBtShapeAsSimpleBox) {
        init(new btBoxShape(btVector3(0.5f,0.5f,0.5f)), mass, startPos);
    }
    else {
        init(mesh()->collisionShape(), mass, startPos);
    }
}

PhysicalObject::PhysicalObject(MeshType type, btCollisionShape* shape, float mass): Object(type), m_btBody(0)
{
    init(shape, mass);
}

PhysicalObject::PhysicalObject(MeshType type, const btRigidBody::btRigidBodyConstructionInfo& cinfo): Object(type), m_btBody(new btRigidBody(cinfo))
{
    m_motionState = cinfo.m_motionState;
    Application::instance()->dynamicsWorld()->addRigidBody(m_btBody);
}


PhysicalObject::~PhysicalObject()
{
    Application::instance()->dynamicsWorld()->removeRigidBody(m_btBody);
    delete m_motionState;
    delete m_btBody;
}

btRigidBody* PhysicalObject::btBody() const
{
    return m_btBody;
}


void PhysicalObject::update(float time)
{
    Object::update(time);
    btTransform transform;
    m_motionState->getWorldTransform(transform);
    glm::mat4 newModelMatrix;
    transform.getOpenGLMatrix(&newModelMatrix[0][0]);
    Object::setModelMatrix(newModelMatrix);
}

void PhysicalObject::setModelMatrix(const glm::mat4 &matrix)
{
    Object::setModelMatrix(matrix);

    btTransform transform;
    const glm::mat4& objectMatrix = modelMatrix();  // needed for static objects, untested!
    transform.setFromOpenGLMatrix(&objectMatrix[0][0]);

    m_btBody->setCenterOfMassTransform(transform);
}

void PhysicalObject::init(btCollisionShape* shape, float mass, const glm::vec3 &initialPosition)
{
    btAssert((!shape || shape->getShapeType() != INVALID_SHAPE_PROXYTYPE));

    //rigidbody is dynamic if and only if mass is non zero, otherwise static
    bool isDynamic = (mass != 0.f);

    btVector3 localInertia(0,0,0);
    if (isDynamic)
        shape->calculateLocalInertia(mass,localInertia);

        glm::mat4 startMatrix;
        startMatrix[3][0] = initialPosition.x;
        startMatrix[3][1] = initialPosition.y;
        startMatrix[3][2] = initialPosition.z;

        btTransform worldTransform;
        worldTransform.setFromOpenGLMatrix(glm::value_ptr(startMatrix));

        m_motionState = new btDefaultMotionState();
        m_motionState->setWorldTransform(worldTransform);
        btRigidBody::btRigidBodyConstructionInfo cinfo(mass, m_motionState, shape, localInertia);
//        cinfo.m_startWorldTransform = worldTransform;

        m_btBody = new btRigidBody(cinfo);
        Application::instance()->dynamicsWorld()->addRigidBody(m_btBody);
}

btScalar PhysicalObject::mass() const
{
    btScalar inverseMass = btBody()->getInvMass();
    if(inverseMass == btScalar(0))
        return btScalar(0);
    else
        return btScalar(1)/inverseMass;
}
