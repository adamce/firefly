#ifndef CAMERA_H
#define CAMERA_H

#include "Object.h"
#include "auxiliary/ViewPlane.h"


class Camera : public Object
{
public:
    Camera(float fov = 70.f, float zNear = 0.01f, float zFar = 1000.f, Object* parent = nullptr);
    ~Camera();
    void setViewport(int width, int height);
    glm::ivec2 viewPort() const;
    void activate();
	void increaseFovy(float value);
    float fovY() const { return m_fovy; }
    void setFovY(float value);
    glm::mat4 viewProjectionMatrix() const;
    glm::mat4 inverseViewProjectionMatrix() const;
    glm::mat4 UVW() const;
    glm::mat4 cumulatedModelMatrix() const;
    glm::mat4 viewMatrix() const;
	glm::vec3 position() const;
    glm::vec3 ViewDirection() const;
	glm::vec4 upVector() const;
	void updateFrustum();
	ViewPlane* viewPlanes();
    virtual void setModelMatrix(const glm::mat4&  matrix) override;


private:
    Object* m_parent = nullptr;

    int m_viewPortWidth;
    int m_viewPortHeight;
	float m_zNear;
	float m_zFar;
    float m_fovy;
    bool m_viewFrustumDirty = true;
	ViewPlane planes[6];

    glm::mat4 m_projectionMatrix;
};

#endif // CAMERA_H
