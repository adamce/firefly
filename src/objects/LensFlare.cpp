#include "LensFlare.h"
#include "Camera.h"
#include "glm/gtc/matrix_transform.hpp"
#include <set>

LensFlare::LensFlare(glm::vec4 color) : Object(FlareMesh), m_color(color), m_lastaxis(glm::vec3())
{

}
LensFlare::~LensFlare()
{

}
void LensFlare::setCamera(Camera* cam)
{
	m_camera=cam;
}
void LensFlare::setSun(Object* sun)
{
	m_sun=sun;
}
void LensFlare::setColor(glm::vec4 color)
{
	m_color = color;
}

Object* LensFlare::getSun() const
{
	return m_sun;
}

glm::vec4 LensFlare::getColor() const
{
	return m_color;
}
float LensFlare::glareFactor() const
{
    return m_glareFactor;
}

//do this everytime you want to draw the Lensflare
void LensFlare::update(float time)
{



}

void LensFlare::calculateLensflare()
{
	//camera position vector
	glm::vec3 cam_pos = m_camera->position();
	//camera view direction vector
	glm::vec3 cam_viewDir = m_camera->ViewDirection();
	//sun position vector
	glm::vec3 sun_pos = m_sun->position();
	//direction from camera to sun
	glm::vec3 camToSun = sun_pos - cam_pos;
	//distance from camera to sun
	float camToSunDistance = glm::length(camToSun);
	//calculate intersection point
	glm::vec3 intersectionPoint_pos = cam_viewDir * camToSunDistance;
	intersectionPoint_pos = cam_pos + intersectionPoint_pos;
	// calculate direction from sun to intersection point
	glm::vec3 sunToIntersectioPoint = intersectionPoint_pos - sun_pos;
	//calculate distance from sun to intersection point
	float sunToIpDistance = glm::length(sunToIntersectioPoint);
	//normalize the sun to  intersection point vector
	//sunToIntersectioPoint = sunToIntersectioPoint/sunToIpDistance;
	sunToIntersectioPoint = glm::normalize(sunToIntersectioPoint);
	glm::vec3 rotAxis = glm::cross<float>(glm::vec3(1.0f,0.0f,0.0f),sunToIntersectioPoint);
	//glm::vec3 rotAxis = glm::cross<float>(sunToIntersectioPoint,glm::vec3(1.0f,0.0f,0.0f));
	float rotAxisLength = glm::length(rotAxis);
	//rotAxis = rotAxis/rotAxisLength;
	float radangle;
	float dot;
	float angle;
	if (rotAxisLength > 0.00001f){
        //angle = glm::atan(rotAxisLength,glm::dot<float>(glm::vec3(1.0f,0.0f,0.0f),sunToIntersectioPoint));
        dot = glm::dot<float>(glm::vec3(1.0f,0.0f,0.0f),sunToIntersectioPoint);
        //dot = glm::dot<float>(sunToIntersectioPoint,glm::vec3(1.0f,0.0f,0.0f));
        radangle = glm::acos(dot);
        angle= glm::degrees(radangle);
	}
	else {
		//this should never happen due to vissibility detection
		rotAxis=m_lastaxis;
		if(glm::dot(glm::vec3(1.0f,0.0f,0.0f),sunToIntersectioPoint) > 0.0f)
			angle = 0.1f;
		else
			angle= 180.0f;

	}

	m_lastaxis = rotAxis;
	//sunToIpDistance = sunToIpDistance/6;
	glm::mat4 scale = glm::scale(glm::mat4(1.0), glm::vec3(sunToIpDistance));
    glm::mat4 rotMatrix = glm::rotate(glm::mat4(1.0), glm::radians(angle), rotAxis);

	glm::mat4 translation = glm::translate(glm::mat4(1.0), intersectionPoint_pos);


	setModelMatrix(translation*scale*rotMatrix);


    m_glareFactor = glm::dot(glm::normalize(camToSun), glm::normalize(cam_viewDir));
    m_glareFactor -= 0.8f;
    m_glareFactor *= 5.0f;
    if(m_glareFactor < 0)
        m_glareFactor = 0;

    //std::cout << "LensFlare::calculateLensflare: m_glareFactor=" << m_glareFactor << " dot(cam_viewDir, camToSun)=" << glm::dot(glm::normalize(camToSun), glm::normalize(cam_viewDir)) << std::endl;
}

