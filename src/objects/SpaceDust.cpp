/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "SpaceDust.h"
#include "Application.h"
#include "shaders/ShaderManager.h"
#include "shaders/Shader.h"

SpaceDust::SpaceDust() : Object(SpaceDustMesh)
{
    m_vbo1Id = mesh()->vboId();

    glGenBuffers(1, &m_vbo2Id);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo2Id);
    glBufferData(GL_ARRAY_BUFFER, mesh()->vertexDataSize(), 0, GL_DYNAMIC_COPY);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void SpaceDust::update(float time)
{
//     return;
    this->mesh()->activate();
    Shader* shader = Application::instance()->shaderManager()->activateShader(ShaderManager::SpaceDust);

    shader->setMode(2);
    shader->setTime(time);

    glBindBuffer(GL_ARRAY_BUFFER, m_vbo1Id);
    Shader::setupVertexAttribPointers();
    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_vbo2Id);

    shader->enableVertexAttribArrays();
    glEnable(GL_RASTERIZER_DISCARD);
    glBeginTransformFeedback(GL_POINTS);

    //drawing
    glDrawArrays(GL_POINTS, 0, 20000);

    glEndTransformFeedback();
    glDisable(GL_RASTERIZER_DISCARD);
    shader->disableVertexAttribArrays();

    int tmp = m_vbo2Id;
    m_vbo2Id = m_vbo1Id;
    m_vbo1Id = tmp;

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);
    glBindVertexArray(0);

}

void SpaceDust::activateMeshForDrawing()
{
    Object::activateMeshForDrawing();
    Application::instance()->shaderManager()->activeShader()->setMode(1);
}

