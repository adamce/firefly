#ifndef SHIP_H
#define SHIP_H

#include <string>

#include "SteerableObject.h"

class PowerUp;

class Ship : public SteerableObject
{
public:
    Ship(const std::string& name);
    void update(float time);
    void reset();
    std::string name() const;

private:
    std::string m_name;
};

#endif // SHIP_H
