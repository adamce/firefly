#include "Camera.h"

#include <iostream>


#define GLM_FORCE_RADIANS
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"




Camera::Camera(float fov, float zNear, float zFar, Object *parent) : Object(EmptyMesh), m_parent(parent)
{
    m_fovy=fov;
    m_zNear=zNear;
    m_zFar=zFar;
    m_projectionMatrix = glm::perspective(glm::radians(m_fovy), 1.0f, m_zNear, m_zFar);
//    glm::mat4 lookAt = glm::lookAt(glm::vec3(0.,-7.,1.), glm::vec3(0.,0.,1.0), glm::vec3(0., 0., 1.));
    //    setModelMatrix(glm::inverse(lookAt));
}

Camera::~Camera()
{
}

glm::mat4 Camera::viewProjectionMatrix() const
{
//    glm::mat4 rot = glm::rotate<float>(glm::mat4(), -90., glm::vec3(1,0,0));
//    return m_projectionMatrix * modelMatrix() * m_ship->modelMatrix();
    return m_projectionMatrix * viewMatrix();
}

void Camera::setViewport(int width, int height)
{
    m_viewPortWidth = width;
    m_viewPortHeight = height;

    m_projectionMatrix = glm::perspective(glm::radians(m_fovy), ((float)m_viewPortWidth)/((float)m_viewPortHeight), m_zNear, m_zFar);
}

glm::ivec2 Camera::viewPort() const
{
    return glm::ivec2(m_viewPortWidth, m_viewPortHeight);
}

void Camera::activate()
{
    glViewport(0, 0, m_viewPortWidth, m_viewPortHeight);
}

void Camera::increaseFovy(float value)
{
	float newFov = m_fovy+value;
    if( newFov > 10.0f && newFov < 90.0f ){
		m_fovy=newFov;
        m_projectionMatrix = glm::perspective(glm::radians(m_fovy), ((float)m_viewPortWidth)/((float)m_viewPortHeight), m_zNear, m_zFar);
    }
}

void Camera::setFovY(float value)
{
    m_fovy = value;
    m_projectionMatrix = glm::perspective(glm::radians(m_fovy), ((float)m_viewPortWidth)/((float)m_viewPortHeight), m_zNear, m_zFar);
}

glm::mat4 Camera::cumulatedModelMatrix() const
{
    if(m_parent != nullptr)
        return m_parent->modelMatrix() * modelMatrix();
    else
        return modelMatrix();
}

glm::mat4 Camera::viewMatrix() const
{
    return glm::inverse(cumulatedModelMatrix());
}

glm::mat4 Camera::inverseViewProjectionMatrix() const
{
    return glm::inverse(viewProjectionMatrix());
}

glm::vec3 Camera::position() const
{
    glm::vec3 pos;
    pos = glm::vec3(cumulatedModelMatrix() * glm::vec4(0.f, 0.f, 0.f, 1.f));
    return pos;
}

glm::mat4 Camera::UVW() const {
    glm::mat4 viewMatrix = glm::transpose(this->viewMatrix());
    glm::mat4 uvwMatrix = glm::mat4(0.f);

    float aspectRation = ((float)m_viewPortWidth)/((float)m_viewPortHeight);
    float fovTan = tan(glm::radians(m_fovy * 0.5f));
    float ulen = fovTan * aspectRation;
    float vlen = fovTan;

    uvwMatrix[0] = glm::vec4(glm::normalize(glm::vec3(viewMatrix[0])) * ulen, 0.f); // U
    uvwMatrix[1] = glm::vec4(glm::normalize(glm::vec3(viewMatrix[1])) * vlen, 0.f); // V
    uvwMatrix[2] = glm::vec4(-glm::normalize(glm::vec3(viewMatrix[2])), 0.f);       // W, negative because the camera looks into negative z direction. this is otherwise handled by glm::perspective
    uvwMatrix[3][3] = 1.f;

    return uvwMatrix;
}

glm::vec3 Camera::ViewDirection() const
{
	//glm::vec3 viewDest = glm::vec3(cumulatedModelMatrix() * glm::vec4(0.0f,0.0f,0.0f,1.0f));
	glm::vec3 viewDest = glm::vec3(cumulatedModelMatrix() * glm::vec4(0.0f,0.0f,-1.0f,1.0f));
	//glm::vec3 viewDest = glm::vec3(cumulatedModelMatrix() * glm::vec4(0,0,1,1));
	return glm::normalize(viewDest - position());
}

glm::vec4 Camera::upVector() const
{
	return /*glm::normalize*/((cumulatedModelMatrix()*glm::vec4(0.f, 1.f, 0.f, 1.f)) - glm::vec4(position(), 0));
    //return glm::normalize(m_ship->modelMatrix()*glm::vec4(0.f, 0.f, 1.f, 1.f));
}

// void output(glm::vec3 vec) {
//     std::cout << "x/y/z: " << vec.x << "/" << vec.y << "/" << vec.z << std::endl;
// }

void Camera::updateFrustum()
{
	glm::vec3 up = glm::vec3(upVector());
	glm::vec3 d = glm::vec3(ViewDirection());
	glm::vec3 p = glm::vec3(position());
//     glm::vec3 up = glm::vec3(0.f,1.f,0.f);
//     glm::vec3 d = glm::vec3(0.f, 0.f, -1.f);
//     glm::vec3 p = glm::vec3(0,0,0);

	float farDist = m_zFar;
	float nearDist = m_zNear;
    float ratio = ((float)m_viewPortWidth)/((float)m_viewPortHeight);
	float Hnear = 2 * tan(glm::radians(m_fovy) / 2.f) * nearDist;
	float Wnear = Hnear * ratio;
    float Hfar = 2 * tan(glm::radians(m_fovy) / 2.f) * farDist;
	float Wfar = Hfar * ratio;

	glm::vec3 right = -glm::cross(up,d);

	glm::vec3 fc = p + d * farDist;

	glm::vec3 ftl = fc + (up * Hfar/2.f) - (right * Wfar/2.f);
	glm::vec3 fbl = fc - (up * Hfar/2.f) - (right * Wfar/2.f);
	glm::vec3 fbr = fc - (up * Hfar/2.f) + (right * Wfar/2.f);
	glm::vec3 ftr = fc + (up * Hfar/2.f) + (right * Wfar/2.f);

	glm::vec3 nc = p + d * nearDist;

	glm::vec3 ntl = nc + (up * Hnear/2.f) - (right * Wnear/2.f);
	glm::vec3 nbl = nc - (up * Hnear/2.f) - (right * Wnear/2.f);
	glm::vec3 nbr = nc - (up * Hnear/2.f) + (right * Wnear/2.f);
	glm::vec3 ntr = nc + (up * Hnear/2.f) + (right * Wnear/2.f);

//     std::cout << "======================" << std::endl;
//     output(p);
//     output(d);
//     output(up);
//     output(right);
//     std::cout << "far plane" << std::endl;
//     output(fc);
//     output(ftl);
//     output(fbl);
//     output(fbr);
//     output(ftr);
//     std::cout << "near plane" << std::endl;
//     output(nc);
//     output(ntl);
//     output(nbl);
//     output(nbr);
//     output(ntr);

	//left plane
	planes[0].set3Points(ntl,nbl,fbl);
	//right plane
	planes[1].set3Points(nbr,ntr,fbr);
	//top plane
	planes[2].set3Points(ntr,ntl,ftl);
	//bottom plane
	planes[3].set3Points(nbl,nbr,fbr);
	//near plane
	planes[4].set3Points(ntl,ntr,nbr);
	//far plane
	planes[5].set3Points(ftr,ftl,fbl);
    m_viewFrustumDirty = false;
}

ViewPlane* Camera::viewPlanes()
{
    if(m_viewFrustumDirty) updateFrustum();
    return static_cast<ViewPlane*> (planes);
}

void Camera::setModelMatrix(const glm::mat4 &matrix)
{
    m_viewFrustumDirty = true;
    Object::setModelMatrix(matrix);
}




