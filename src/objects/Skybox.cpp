#include "Skybox.h"
#include "Camera.h"
#include "Ship.h"

Skybox::Skybox():Object(SkyboxMesh)
{
}

void Skybox::update(float time)
{
}

void Skybox::updatePosition()
{
    setPosition(m_camera->position());
}

void Skybox::setCamera(Camera* camera)
{
    m_camera=camera;
}
