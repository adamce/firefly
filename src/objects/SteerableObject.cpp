/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "SteerableObject.h"
#define GLM_FORCE_RADIANS
#include "glm/gtc/matrix_transform.hpp"
#include <BulletCollision/CollisionShapes/btCollisionShape.h>

SteerableObject::SteerableObject(MeshType type, float mass): PhysicalObject(type, mass)
{
    m_tmpWorkaroundBreakActive=0;
    m_speed = 10;
    m_hasWantedFlightVector=false;
    setAnglularCompensationFactor(1);
    setLinearCompensationFactor(1);
}

SteerableObject::SteerableObject(MeshType type, btCollisionShape* shape, float mass): PhysicalObject(type, shape, mass)
{
    m_tmpWorkaroundBreakActive=0;
    m_speed = 10;
    m_hasWantedFlightVector=false;
    setAnglularCompensationFactor(1);
    setLinearCompensationFactor(1);
}

SteerableObject::SteerableObject(MeshType type, const btRigidBody::btRigidBodyConstructionInfo& cinfo): PhysicalObject(type, cinfo)
{
    m_tmpWorkaroundBreakActive=0;
    m_speed = 10;
    m_hasWantedFlightVector=false;
    setAnglularCompensationFactor(1);
    setLinearCompensationFactor(1);
}

SteerableObject::~SteerableObject()
{}

void SteerableObject::update(float time)
{
    btVector3 linearVelocity = btBody()->getLinearVelocity();
    btTransform transform = btBody()->getCenterOfMassTransform();
    transform.setOrigin(btVector3(0, 0, 0));
    btVector3 wantedVelocity;
    if(m_hasWantedFlightVector) {
        wantedVelocity = m_wantedFlightVector;
    }
    else {
        wantedVelocity = transform * btVector3(0, speed(), 0);
    }

    btVector3 correction = wantedVelocity - linearVelocity;
    btBody()->applyCentralImpulse(correction * time * mass() * m_linearCompensationFactor);

    btVector3 angularVelocity = btBody()->getAngularVelocity();
    btBody()->applyTorqueImpulse(-angularVelocity * time * mass() * m_angularCompensationFactor);

    PhysicalObject::update(time);
    m_tmpWorkaroundBreakActive--;
}

void SteerableObject::pitch(float degree)
{
    btTransform transform = btBody()->getCenterOfMassTransform();
    transform.setOrigin(btVector3(0, 0, 0));
    btBody()->applyTorqueImpulse(transform*btVector3(glm::radians(degree) * mass(), 0, 0));
}

void SteerableObject::roll(float degree)
{
    btTransform transform = btBody()->getCenterOfMassTransform();
    transform.setOrigin(btVector3(0, 0, 0));
    btBody()->applyTorqueImpulse(transform*btVector3(0, glm::radians(degree) * mass(), 0));
}

void SteerableObject::turn(float degree)
{
    btTransform transform = btBody()->getCenterOfMassTransform();
    transform.setOrigin(btVector3(0, 0, 0));
    btBody()->applyTorqueImpulse(transform*btVector3(0, 0, glm::radians(degree) * mass()));
}

void SteerableObject::accelerate(float upToMeterPerSecond)
{
    m_speed = upToMeterPerSecond;
}

void SteerableObject::activateBreak(float time)
{
    //translate(glm::vec3(0., -speed()/*/2.*/, 0) * glm::vec3(time));
	accelerate(5.0f);
    m_tmpWorkaroundBreakActive=2;
}

float SteerableObject::speed() const
{
    if(m_tmpWorkaroundBreakActive>0)
        return 0.0001f;
    else
        return m_speed;
}

glm::vec3 SteerableObject::flightVector() const
{
    glm::vec3 v = glm::vec3(modelMatrix() * (glm::vec4(0, 1, 0, 1))) - position();
    v *= speed();
    return v;
}

void SteerableObject::setAnglularCompensationFactor(float factor)
{
    m_angularCompensationFactor = factor;
}

void SteerableObject::setLinearCompensationFactor(float factor)
{
    m_linearCompensationFactor = factor*5;
}

// void output(glm::vec3 vec) {
//      std::cout << "x/y/z: " << vec.x << "/" << vec.y << "/" << vec.z << std::endl;
// }

void SteerableObject::setWantedFlightVector(const glm::vec3& direction)
{
    m_wantedFlightVector.setValue(direction.x, direction.y, direction.z);
    m_hasWantedFlightVector = true;
//     std::cout << "SteerableObject::setWantedFlightVector: ";
//     output(direction);
}

