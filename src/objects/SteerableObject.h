/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef STEERABLEOBJECT_H
#define STEERABLEOBJECT_H

#include "PhysicalObject.h"


class SteerableObject : public PhysicalObject
{

public:
    SteerableObject(MeshType type, float mass = 0.f);
    SteerableObject(MeshType type, btCollisionShape* shape, float mass = 0.f);
    SteerableObject(MeshType type, const btRigidBody::btRigidBodyConstructionInfo& cinfo);
    virtual ~SteerableObject();

    void update(float time);

    void pitch(float degree); // rotate x
    void roll(float degree);  // rotate y, the longitudinal axis
    void turn(float degree);  // rotate z, turn left and right

    void accelerate(float upToMeterPerSecond);
    void activateBreak(float time);

    void setWantedFlightVector(const glm::vec3& direction);

    /// a compensation factor of 1 means nearly instant compensation.
    void setAnglularCompensationFactor(float factor);
    void setLinearCompensationFactor(float factor);

    float speed() const;
    glm::vec3 flightVector() const;

private:
    float m_speed;
    int m_tmpWorkaroundBreakActive;
    float m_angularCompensationFactor;
    float m_linearCompensationFactor;
    btVector3 m_wantedFlightVector;
    bool m_hasWantedFlightVector;
};

#endif // STEERABLEOBJECT_H
