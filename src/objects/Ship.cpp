
#include "Ship.h"

#include <iostream>
#include <string>
#include <btBulletDynamicsCommon.h>

#include "Application.h"

Ship::Ship(const std::string &name) :
    SteerableObject(ShipMesh, new btBoxShape(btVector3(0.5f,0.5f,0.5f)), 10.f),
    m_name(name)
{
    accelerate(0);
    setAnglularCompensationFactor(0.0f);
    setLinearCompensationFactor(0.3f);
}

void Ship::update(float time)
{
	//log position
	//std::cout << m_name << " pos: ("<< shipPos.x << ", " << shipPos.y << ", " << shipPos.z << ")" << std::endl;

//    roll(10.0f * time);
//    SteerableObject::update(time);
    Object::rotateY(180.f * time);
//    Object::update(time);
}

std::string Ship::name() const
{
    return m_name;
}
