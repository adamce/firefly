#ifndef OBJECT_H
#define OBJECT_H


#define GLM_FORCE_RADIANS
#include "glm/glm.hpp"
#include <GL/glew.h>
#include "../meshes/MeshManager.h"


class ObjectAnimator;

/**
 * This represents an Object in Chawah.
 * Objects must not be scaled or sheared, because the bullet library doesn't allow it (more precisely, PhysicalObjects mustn't be scaled and sheared. so if you add it here, be sure to forbid it in PhysicalObject).
 */
class Object
{
public:
    Object(MeshType type);
    Object(const std::string& path);
    virtual ~Object();

    virtual void activateMeshForDrawing();
    virtual void draw(const glm::mat4& viewProjectionMatrix);
    virtual void update(float time);
    // static here means, that it's model matrix won't be updated after initialisation.
    /// todo: physical objects need to be set as cinematic!
    virtual void setStatic();
    bool isStatic() const { return m_isStatic; }

    void setPosition(const glm::vec3& position);
    void setPosition(float x, float y, float z);
    void translate(const glm::vec3& translation);
    void scale(float x, float y, float z); // warning must not be called for physical meshes, as scaling is not supported in bullet

    /// rotation angle is in degrees
    void rotateX(float angle);
    void rotateY(float angle);
    void rotateZ(float angle);

    virtual void setModelMatrix(const glm::mat4&  matrix);
    virtual glm::mat4 modelMatrix() const;
    virtual glm::vec3 position() const;
	Mesh* mesh() const;

	void switchTextureFiltering(int value) const;


    void readAnimFromFile(std::string path);
    void stopAnimation() { m_animated = false; }

protected:
    void loadMesh(MeshType type);
    void loadMesh(const std::string& path);

private:
    ObjectAnimator* m_objectAnimator = nullptr;
    bool m_animated = false;
    float m_time = 0.0;
    int m_frame = 0;

protected:
    bool m_isStatic = false;    // used for bvh acceleration structure
    Mesh* m_mesh;
    glm::mat4 m_modelMatrix;
};

#endif // OBJECT_H
