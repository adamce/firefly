#ifndef LIGHT_H
#define LIGHT_H


#include "Object.h"

#include <GL/glew.h>

#define GLM_FORCE_RADIANS
#include "glm/glm.hpp"

class Camera;

class Light : public Object
{
public:
    Light(Object* parent = nullptr);
    ~Light();
    
    void setModelMatrix(const glm::mat4& matrix) override;
    void setColor(GLfloat red, GLfloat green, GLfloat blue);
    glm::vec3 color() const;
    Camera* lightCamera() const { return m_lightCamera; }
    glm::mat4 modelMatrix() const;

private:
    glm::mat4 cumulatedModelMatrix() const;
    Object* m_parent = nullptr;
    glm::vec3 m_color;
    Camera* m_lightCamera = nullptr;
};

#endif // LIGHT_H
