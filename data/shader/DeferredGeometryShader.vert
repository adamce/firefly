in vec4 vertexPosition;
in vec3 vertexNormal;
in vec2 vertexTexCoords;
in int vertexMaterialId;

uniform mat4 modelMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;

out vec3 varaNormal;
out vec4 varaPosition;
out vec2 varaTexCoords;
flat out int varaMaterialId;


void main(void) {
	//varaNormal = normalMatrix * vertexNormal;
	varaNormal = normalize(normalMatrix * vertexNormal);

	varaTexCoords=vertexTexCoords;
	//calculate vertex position in world space
	//vec4 varaPosition = modelMatrix * vertexPosition;
	vec4 pos1 = modelMatrix * vertexPosition;
	varaPosition = pos1;

	varaMaterialId = vertexMaterialId;

	vec3 pos = pos1.xyz/pos1.w;
	gl_Position = modelViewProjectionMatrix * vertexPosition;
}