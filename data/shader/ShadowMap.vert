#version 150

in vec4 vertexPosition;

uniform mat4 modelMatrix;
uniform mat4 modelViewProjectionMatrix;
//uniform mat3 normalMatrix;

out vec4 varaPosition;


void main(void) {
	//calculate vertex position in world space
	varaPosition = modelMatrix * vertexPosition;
	//vec3 pos = varaPosition.xyz;//vertexPosition.w;
	
	gl_Position = modelViewProjectionMatrix * vertexPosition;
}