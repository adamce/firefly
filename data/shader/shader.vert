#version 150

in vec4 vertexPosition;
in vec3 vertexNormal;

uniform mat4 modelMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;

//uniform int lightCount
uniform vec3 lightPositions[4];
//uniform vec3 diffuseLightColors
//uniform vec3 ambientLightColors
//uniform vec3 specularLightColors
uniform vec3 lightAttenuationConstants[4];

out vec3 varaNormal;
out vec4 varaPosition;
out vec3 varaLightDirections[4];
out float varaLightIntensity[4];


void main(void) {
    
	//varaNormal = normalMatrix * vertexNormal;
	varaNormal = normalize(normalMatrix * vertexNormal);

	
	//calculate vertex position in world space
	vec4 varaPosition = modelMatrix * vertexPosition;
	vec3 pos = varaPosition.xyz;//vertexPosition.w;
	
	for (int i =0; i<1 ; i++){
		vec3 dir = lightPositions[i]-pos;
		varaLightDirections[i] = normalize(dir);
		float distance = length(dir);
		if(!(lightAttenuationConstants[i].x == 0.0))
		varaLightIntensity[i] = 1.0/(lightAttenuationConstants[i].x + lightAttenuationConstants[i].y * distance + lightAttenuationConstants[i].z * distance * distance);
	}
	
	gl_Position = modelViewProjectionMatrix * vertexPosition;
}