#version 150

in vec4 vertexPosition;
in float flareSize;
in int flareType;
in vec4 flareColor;

uniform mat4 modelViewProjectionMatrix;

flat out int varaArrayIndex; // do not interpolate
flat out vec4 varaColor;

void main(void) {
	varaArrayIndex = flareType;
    varaColor = flareColor;
	gl_PointSize = flareSize*150;
	gl_Position = modelViewProjectionMatrix * vertexPosition;
}