//specular highliting is not implemented yet

#version 150



in vec4 varaPosition;
in vec3 varaNormal;
in vec3 varaLightDirections[4];
in float varaLightIntensity[4];
//in vec2 varaTexCoords;

//uniform vec3 cameraPosition;
//uniform mat3 normalMatrix;

uniform vec3 diffuseLightColors[4];
uniform vec3 ambientLightColors[4];
//uniform sampler2D colorMap0;
//uniform int lightCount
//uniform vec3 lightPositions[4];
//uniform vec3 diffuseLightColors
//uniform vec3 ambientLightColors
//uniform vec3 specularLightColors
//uniform vec3 lightAttenuationConstants[4];

out vec4 vFragColor;

void main(void) {
	
	//float ambientFactor = 0.6;
	
	//vec3 specularColor = vec3(1.0, 1.0, 1.0);
	//materialADSReflectivity ... vector containing the material reflectivity parameters
	vec3 materialADSReflectivity = vec3(0.6, 0.9, 1.0);
	vec3 total= vec3(0.0, 0.0, 0.0);

	for(int i = 0; i < 1; i++){
	
		float diffuseFactor = max(0.0, dot(normalize(varaNormal),normalize(varaLightDirections[i])));
		//vec3 diffusePart = varaLightIntensity[i] * diffuseFactor * lightColors[i];
		vec3 diffusePart = diffuseLightColors[i] * diffuseFactor * materialADSReflectivity.y;
		vec3 ambientPart = ambientLightColors[i] * materialADSReflectivity.x;
		
		//total +=(ambientPart+ diffusePart)*texture(colorMap0, varaTexCoords);
		//specular part calculation
		//vec3 reflectionVector = normalize(reflect(-normalize(varaLightDirections[i]),normalize(varaNormal)));
		//float spec = max(0.0,dot(normalize(varaNormal),reflectionVector));
		//if(diffuseFactor != 0){
		//	float fSpec = pow(spec,64.0);
		//	total += vec3(fspec, fspec, fspec);
		//}

	}
    vFragColor = vec4(total,1.0);
}