out vec4 vFragColor;

uniform samplerCube cubeMap;

in vec3 vVaryingTexCoords;


layout (location = 0) out uvec4 buffOut0;
//out ivec4 color0;
//out uvec4 color1;

//void viennaPack(out ivec4 out0, out uvec4 out1, in vec3 position, in vec3 normal, in vec4 texColour, in vec2 texUV, in int materialId)

void main(void) {
    vec4 colour = vec4(texture(cubeMap, vVaryingTexCoords).rgb,0.0);
    viennaPack(buffOut0, vec3(1), colour.xyz, colour, vec2(0), 1000000);
}
