#version 150

flat in int varaArrayIndex;
flat in vec4 varaColor;

uniform sampler2D colorMap0;
uniform sampler2D colorMap1;
uniform sampler2D colorMap2;
uniform vec4 flareColor;

out vec4 vFragColor;

void main(void) {
	vec4 textureColor;
	switch(varaArrayIndex) {
		case 0:
            textureColor = texture(colorMap0, gl_PointCoord)*flareColor*varaColor;
		break;
		case 1:
            textureColor = texture(colorMap1, gl_PointCoord)*flareColor*varaColor;
		break;
		case 2:
            textureColor = texture(colorMap2, gl_PointCoord)*flareColor*varaColor;
		break;
		default:
            textureColor = vec4(0,0,0,1);
	}
	vFragColor = textureColor;
}