uniform vec3 cameraPosition;

void viennaPack(out uvec4 out0, in vec3 position, in vec3 normal, in vec4 texColour, in vec2 texUV, in uint materialId) {
	float dist = length(cameraPosition - position);
	out0.x = floatBitsToUint(dist);
	
	// packing of normal taken from http://aras-p.info/texts/CompactNormalStorage.html (Lambert Azimuthal Equal-Area projection, see also wikipedia)
//	float f = sqrt(8.0*normal.z+8.0);
//	out0.y = packHalf2x16(normal.xy / f + 0.5);
	// packing of normal taken from http://aras-p.info/texts/CompactNormalStorage.html (Spherical Coordinates)
	//out0.y = packHalf2x16((vec2(atan(normal.y,normal.x)/3.141592653589793238, normal.z)+vec2(1.0))*0.5);
	
	out0.y = packSnorm4x8(vec4(normal, 0));
	
	out0.z = packUnorm4x8(texColour);
	out0.w = materialId;
}