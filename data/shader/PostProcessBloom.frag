#version 150

in vec2 varaTexCoords;
uniform sampler2D colorMap;
uniform sampler2DRect colorMapRect;
uniform int shaderModeSwitch;
uniform int framebufferHeight;
uniform int framebufferWidth;
uniform float time;

out vec4 vFragColor;

void main(void) {

    vec2 texCoords = gl_FragCoord.xy/vec2(framebufferWidth, framebufferHeight);

    if(shaderModeSwitch==3 || shaderModeSwitch==4) {
        // bluring mode with small texture in colorMap

        vec4 outColor = vec4(0., 0., 0., 0.);

//        float factors[5] = float[5](1., 5., 7., 5., 1.);
//        float factors[7] = float[7](1., 4.4, 10.7, 14.4, 10.7, 4.4, 1.);
        float factors[7] = float[7](1, 1, 2, 2, 2, 1, 1);


        for(int i=-3; i<=3; i++) {
            float gausfactor = factors[(i+2)];
            if(shaderModeSwitch==3) {
                vec4 colour = texture(colorMapRect, vec2(gl_FragCoord.x, gl_FragCoord.y+i)).rgba;
                if(dot(colour.xyz, vec3(0.2126, 0.7152, 0.0722)) > 0.90)
                    outColor += colour * gausfactor;
            }
            else {
                vec4 colour = texture(colorMapRect, vec2(gl_FragCoord.x+i, gl_FragCoord.y)).rgba;
                outColor += colour * gausfactor;
            }
        }
//        outColor/=19.;
//        outColor/=46.6;
        outColor/=10.;

        vFragColor = outColor;

    }
    else if (shaderModeSwitch==2) {
        vec3 a = texture(colorMapRect, gl_FragCoord.xy).rgb;
        vec3 b = 1.0*texture(colorMap, texCoords).rgb + 0.0 * texture(colorMap, texCoords).a;

        //screen
        vec3 blended = vec3(1) - (vec3(1)-a) * (vec3(1)-b);

        vFragColor = vec4(blended, 1);

    }
    else if(shaderModeSwitch == 1) {
        vec3 a = texture(colorMapRect, gl_FragCoord.xy).rgb;
        vec3 b = vec3(1)*time*0.8;

        //screen
        vec3 blended =  vec3(1) - (vec3(1)-a) * (vec3(1)-b);

        vFragColor = vec4(blended, 1);
    }
}
