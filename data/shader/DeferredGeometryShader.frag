in vec4 varaPosition;
in vec3 varaNormal;
in vec2 varaTexCoords;;
flat in int varaMaterialId;

struct TextureDescriptions {
    uint textureIndex[100];
    uint width[100];
    uint height[100];
    uint textureArrayNo[100];
};

layout(binding = 1) uniform Materials {
    TextureDescriptions normalMapTextures, diffuseTextures; //32 bytes
    uint diffuseRGBA[100];
    float phongExponents[100];
    uint types[100];
} materials;

layout (binding = 0) uniform sampler2DArray textures256;
layout (binding = 1) uniform sampler2DArray textures512;
layout (binding = 2) uniform sampler2DArray textures1024;
layout (binding = 3) uniform sampler2DArray textures2048;
layout (location = 0) out uvec4 buffOut0;
//layout (location = 1) out uvec4 buffOut1;


//void viennaPack(out ivec4 out0, out uvec4 out1, in vec3 position, in vec3 normal, in vec4 texColour, in vec2 texUV, in int materialId)

void main(void) {
//        viennaPack(buffOut0, varaPosition.xyz, normal, texture(colorMap0, varaTexCoords), varaTexCoords, varaMaterialId);
    vec2 texCoords = varaTexCoords;
	//while (texCoords.x > 1.0) texCoords.x -= 1.0;
	//while (texCoords.y > 1.0) texCoords.y -= 1.0;
	//while (texCoords.x < 0.0) texCoords.x += 1.0;
	//while (texCoords.y < 0.0) texCoords.y += 1.0;
	
	//texCoords.x -= floor(texCoords.x);
	//texCoords.y -= floor(texCoords.y);
	vec3 normal = normalize(varaNormal);
	
	//texCoords *= vec2(materials.diffuseTextures.width[varaMaterialId], materials.diffuseTextures.height[varaMaterialId]);

        if(materials.types[varaMaterialId] == 2 || materials.types[varaMaterialId] == 4 || materials.types[varaMaterialId] == 5) {  // flat, macro and glowing
                viennaPack(buffOut0, varaPosition.xyz, normal, unpackUnorm4x8(materials.diffuseRGBA[varaMaterialId]), varaTexCoords, varaMaterialId);
		return;
	}
	
	//float w = materials.diffuseTextures.width[varaMaterialId];
	//float h = materials.diffuseTextures.height[varaMaterialId];
    switch(materials.diffuseTextures.textureArrayNo[varaMaterialId]) {
    case 0:
        //texCoords /= vec2(256, 256);
        viennaPack(buffOut0, varaPosition.xyz, normal, texture(textures256, vec3(texCoords, materials.diffuseTextures.textureIndex[varaMaterialId])), varaTexCoords, varaMaterialId);
        break;
    case 1:
        //texCoords /= vec2(512, 512);
        viennaPack(buffOut0, varaPosition.xyz, normal, texture(textures512, vec3(texCoords, materials.diffuseTextures.textureIndex[varaMaterialId])), varaTexCoords, varaMaterialId);
        break;
    case 2:
		{
		//ivec2 integerTexCoords = ivec2(texCoords);
		//vec2 fractionalPart = texCoords - vec2(integerTexCoords);
		//integerTexCoords = ivec2(integerTexCoords.x % materials.diffuseTextures.width[varaMaterialId], integerTexCoords.y % materials.diffuseTextures.height[varaMaterialId]);
		//texCoords = vec2(integerTexCoords) + fractionalPart;
		
		
			//texCoords = vec2(ivec2(texCoords));
			//if (texCoords.x > float(1023) || texCoords.x < float(1)) {
			//	texCoords.x -= floor(texCoords.x / w) * w;
				//if(int(texCoords.x) == 0) texCoords.x = 1.f;
			//}
			//if (texCoords.y > float(1023) || texCoords.y < float(1)) {
			//	texCoords.y -= floor(texCoords.y / h) * h;
			//}
			//if(texCoords.x < 2 || texCoords.y < 2 || texCoords.x > 1022 || texCoords.y > 1022) {
			//	viennaPack(buffOut0, varaPosition.xyz, normal, vec4(1, 0, 0, 0), varaTexCoords, varaMaterialId);
			//	break;
			//}
			//texCoords /= vec2(1024, 1024);
			viennaPack(buffOut0, varaPosition.xyz, normal, texture(textures1024, vec3(texCoords, materials.diffuseTextures.textureIndex[varaMaterialId])), varaTexCoords, varaMaterialId);
			break;
		}
    case 3:
        //texCoords /= vec2(2048, 2048);
        viennaPack(buffOut0, varaPosition.xyz, normal, texture(textures2048, vec3(texCoords, materials.diffuseTextures.textureIndex[varaMaterialId])), varaTexCoords, varaMaterialId);
        break;
    default:
        viennaPack(buffOut0, varaPosition.xyz, normal, vec4(1, 0, 1, 0), varaTexCoords, varaMaterialId);
    }
//    if(materials.types[varaMaterialId] == 0)
//        viennaPack(buffOut0, varaPosition.xyz, normal, vec4(0, 0, 0, 0), varaTexCoords, varaMaterialId);
//    else if(materials.types[varaMaterialId] == 0)
//        viennaPack(buffOut0, varaPosition.xyz, normal, vec4(0, 0, 1, 0), varaTexCoords, varaMaterialId);
//    else if(materials.types[varaMaterialId] == 1)
//        viennaPack(buffOut0, varaPosition.xyz, normal, vec4(0, 1, 0, 0), varaTexCoords, varaMaterialId);
//    else if(materials.types[varaMaterialId] == 2)
//        viennaPack(buffOut0, varaPosition.xyz, normal, vec4(0, 1, 1, 0), varaTexCoords, varaMaterialId);
//    else
//        viennaPack(buffOut0, varaPosition.xyz, normal, vec4(1, 0, 0, 0), varaTexCoords, varaMaterialId);
}
