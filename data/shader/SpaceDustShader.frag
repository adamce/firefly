#version 440 core

flat in vec4 varaColor;
//in vec2 gl_PointCoord;
/*
in vec4 varaPosition;
in vec3 varaLightDirections[4];
in vec3 varaNormal;
in float varaLightIntensity[4];
in vec2 varaTexCoords;
in vec3 varaEnvMapTexCoords;

uniform vec3 cameraPosition;
uniform mat3 normalMatrix;

uniform vec3 diffuseLightColors[4];
uniform vec3 ambientLightColors[4];
uniform vec3 specularLightColors[4];
*/
uniform sampler2D colorMap0;
//uniform samplerCube cubeMap;

out vec4 vFragColor;

void main(void) {
    vec4 textureColor = texture(colorMap0, gl_PointCoord) * varaColor;
	vFragColor = textureColor;
}