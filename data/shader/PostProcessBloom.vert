#version 150

in vec4 vertexPosition;
in vec3 vertexNormal;
in vec2 vertexTexCoords;

uniform mat4 modelMatrix;
uniform mat4 modelViewProjectionMatrix;

out vec2 varaTexCoords;

void main(void) {
    // the next 3 lines' sole purpose is just to prevent vertexNormal from being optimised out
    vec2 dud = (vertexNormal.xy)*vec2(0.001, 0.001);
    if(dud.x<0) dud.x*=-1;
    if(dud.x>0)
        varaTexCoords = vertexTexCoords;
	gl_Position = modelViewProjectionMatrix * vertexPosition;
}