#version 150

in vec4 vertexPosition;
in vec4 vertexNormal;
in vec2 vertexTexCoords;
in uint vertexMaterialId;

uniform int framebufferHeight;
uniform int framebufferWidth;
uniform int shaderModeSwitch;
uniform mat4 modelViewProjectionMatrix;
uniform vec4 randomNumber;
uniform float time;

flat out vec4 varaColor;

out vec4 transformFeedbackPosition;
out vec4 transformFeedbackNormal;
out vec2 transformFeedbackTextcoord;
out uint transformFeedbackMaterialIndex;
out uint padding;

void main(void) {
    if(shaderModeSwitch==1) {
        vec4 position = modelViewProjectionMatrix * vertexPosition;
        gl_Position = position;
        float size = ((framebufferWidth+framebufferHeight)*0.1)/position.z;
//         float size = 150/position.z;
        float alpha = 1;
        if(size < 6)
            alpha = ((size-2)/4.);

        if(size <= 2)
            alpha = 0;

        varaColor = vec4(vec3(1), 0.7*alpha);


        gl_PointSize = size;
    }
    else {

        vec3 direction = vertexNormal.xyz;
        float directionChangeCountDown = vertexTexCoords.x-time;
        if(directionChangeCountDown<0) {
            directionChangeCountDown = randomNumber.w*10;
            direction = randomNumber.xyz*2-vec3(1);
        }


        vec3 newPosition = vec3(vertexPosition)+direction*time*0.5;
        if(newPosition.x>100) newPosition.x=-100;
        if(newPosition.y>100) newPosition.y=-100;
        if(newPosition.z>100) newPosition.z=-100;
        if(newPosition.x<-100) newPosition.x=100;
        if(newPosition.y<-100) newPosition.y=100;
        if(newPosition.z<-100) newPosition.z=100;

        transformFeedbackPosition = vec4(newPosition, 1);
        transformFeedbackNormal = vec4(direction, 1);
        transformFeedbackTextcoord = vec2(directionChangeCountDown, vertexTexCoords.y);
        transformFeedbackMaterialIndex = vertexMaterialId;
		padding = vertexMaterialId;
    }
}